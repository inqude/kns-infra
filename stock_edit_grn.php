<?php

/* SESSION INITIATE - START */

session_start();

/* SESSION INITIATE - END */



/* FILE HEADER - START */

// LAST UPDATED ON: 10-Oct-2016

// LAST UPDATED BY: Lakshmi

/* FILE HEADER - END */



/* TBD - START */

/* TBD - END */



/* INCLUDES - START */

$base = $_SERVER['DOCUMENT_ROOT'];



include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_purchase_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock'.DIRECTORY_SEPARATOR.'stock_grn_functions.php');



if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))

{

	// Session Data

	$user 		   = $_SESSION["loggedin_user"];

	$role 		   = $_SESSION["loggedin_role"];

	$loggedin_name = $_SESSION["loggedin_user_name"];



	/* DATA INITIALIZATION - START */

	$alert_type = -1;

	$alert = "";

	/* DATA INITIALIZATION - END */

	if(isset($_POST["grn_id"]))

	{

		$grn_id = $_POST["grn_id"];

	}

	else

	{

		$grn_id = "-1";

	}

	

	if(isset($_POST["order_id"]))

	{

		$order_id = $_POST["order_id"];

	}

	else

	{

		$order_id = "-1";

	}



	// Capture the form data

	if(isset($_POST["edit_grn_submit"]))

	{



        $grn_id               = $_POST["hd_grn_id"];

		$purchase_order_id    = $_POST["hd_order_id"];

		$invoice_number       = $_POST["num_invoice_number"];

		$invoice_date         = $_POST["date_invoice_date"];
		$doc	              = upload("file_remarks_doc",$user);
		$dc_number	          = $_POST["stxt_dc_number"];

		$dc_date              = $_POST["date_dc_date"];

		$vehicle_number 	  = $_POST["num_vehicle_number"];

		$remarks 	          = $_POST["txt_remarks"];

		

		// Check for mandatory fields

		if(($purchase_order_id != "") && ($invoice_number != "") && ($invoice_date != "") && ($dc_number != "") && ($dc_date != "") && ($vehicle_number != ""))

		{
			$grn_update_data = array("purchase_order_id"=>$purchase_order_id,"invoice_number"=>$invoice_number,"invoice_date"=>$invoice_date,"doc"=>$doc,"dc_number"=>$dc_number,"dc_date"=>$dc_date,"vehicle_number"=>$vehicle_number,"remarks"=>$remarks);
			$grn_iresult = i_update_grn($grn_id,$grn_update_data);

			

			if($grn_iresult["status"] == SUCCESS)

			{	

				$alert_type = 1;

				header('location:stock_grn_list.php');

			}

		    else

			{

				$alert_type = 0;

			}

			

			$alert = $grn_iresult["data"];

		}

		else

		{

			$alert = "Please fill all the mandatory fields";

			$alert_type = 0;

		}

	}

	

	// Get purchase modes already added

	$stock_purchase_order_search_data = array();

	$purchase_order_list = i_get_stock_purchase_order_list($stock_purchase_order_search_data);

	if($purchase_order_list['status'] == SUCCESS)

	{

		$purchase_order_list_data = $purchase_order_list['data'];

	}

	else

	{

		$alert = $purchase_order_list["data"];

		$alert_type = 0;

	}

	

	// Get grn modes already added

	$stock_grn_search_data = array("grn_id"=>$grn_id);

	$grn_list = i_get_stock_grn_list($stock_grn_search_data);

	if($grn_list['status'] == SUCCESS)

	{

		$grn_list_data = $grn_list['data'];

	}	

}

else

{

	header("location:login.php");

}	
// Functions
function upload($file_id,$user_id) 
{
	if($_FILES[$file_id]["name"]!="")
	{
		if ($_FILES[$file_id]["error"] > 0)
		{
			// echo "Return Code: " . $_FILES[$file_id]["error"] . "<br />";
		}
		else
		{
			$_FILES[$file_id]["name"]=$user_id."_".$_FILES[$file_id]["name"];
			move_uploaded_file($_FILES[$file_id]["tmp_name"], "documents/".$_FILES[$file_id]["name"]);							  
		}
	}
	
	return $_FILES[$file_id]["name"];
}	
?>



<!DOCTYPE html>

<html lang="en">

  

<head>

    <meta charset="utf-8">

    <title>Edit GRN</title>

    

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <meta name="apple-mobile-web-app-capable" content="yes">    

    

    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">

    

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">

    <link href="css/font-awesome.css" rel="stylesheet">

    

    <link href="css/style.css" rel="stylesheet">

   





    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->

    <!--[if lt IE 9]>

      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>

    <![endif]-->



  </head>



<body>

    

<?php

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');

?>    



<div class="main">

	

	<div class="main-inner">



	    <div class="container">

	

	      <div class="row">

	      	

	      	<div class="span12">      		

	      		

	      		<div class="widget ">

	      			

	      			<div class="widget-header">

	      				<i class="icon-user"></i>

	      				<h3>Edit GRN</h3>

	  				</div> <!-- /widget-header -->

					

					<div class="widget-content">

						

						

						

						<div class="tabbable">

						<ul class="nav nav-tabs">

						  <li>

						    <a href="#formcontrols" data-toggle="tab">Edit GRN</a>

						  </li>	

						</ul>

						<br>

							<div class="control-group">												

								<div class="controls">

								<?php 

								if($alert_type == 0) // Failure

								{

								?>

									<div class="alert">

                                        <button type="button" class="close" data-dismiss="alert">&times;</button>

                                        <strong><?php echo $alert; ?></strong>

                                    </div>  

								<?php

								}

								?>

                                

								<?php 

								if($alert_type == 1) // Success

								{

								?>								

                                    <div class="alert alert-success">

                                        <button type="button" class="close" data-dismiss="alert">&times;</button>

                                        <strong><?php echo $alert; ?><a href = "stock_edit_grn<?php echo $grn_id ;?>">Click here to</a></strong>

                                    </div>

								<?php

								}

								?>

								</div> <!-- /controls -->	                                                

							</div> <!-- /control-group -->

							<div class="tab-content">

								<div class="tab-pane active" id="formcontrols">
								<form id="edit_grn_form" class="form-horizontal" method="post" action="stock_edit_grn.php" enctype="multipart/form-data">
								<input type="hidden" name="hd_grn_id" value="<?php echo $grn_id; ?>" />

								<input type="hidden" name="hd_order_id" value="<?php echo $order_id; ?>" />

									<fieldset>										

										

											<div class="control-group">											

											<label class="control-label" for="num_invoice_number">Invoice Number*</label>

											<div class="controls">

												<input type="text" class="span6" name="num_invoice_number" placeholder="Invoice Number" value="<?php echo $grn_list_data[0]["stock_grn_invoice_number"] ;?>" required="required">

											</div> <!-- /controls -->	

										</div> <!-- /control-group -->

										

										<div class="control-group">											

											<label class="control-label" for="date_invoice_date">Invoice Date</label>

											<div class="controls">

												<input type="date" class="span6" name="date_invoice_date" value="<?php echo $grn_list_data[0]["stock_grn_invoice_date"] ;?>"placeholder="Invoice Date" required="required">

											</div> <!-- /controls -->	

										</div> <!-- /control-group -->

										

										<div class="control-group">											
											<label class="control-label" for="file_remarks_doc">Document</label>
											<div class="controls">
											<input type="file" name="file_remarks_doc" id="file_remarks_doc" value="<?php echo $grn_list_data[0]["stock_grn_doc"] ;?>" />
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="stxt_dc_number">DC Number</label>

											<div class="controls">

												<input type="text" class="span6" name="stxt_dc_number" value="<?php echo $grn_list_data[0]["stock_grn_dc_number"] ;?>"placeholder="Unique code for no">

											</div> <!-- /controls -->					

										</div> <!-- /control-group -->

											

										<div class="control-group">											

											<label class="control-label" for="date_dc_date">DC Date</label>

											<div class="controls">

												<input type="date" class="span6" name="date_dc_date" value="<?php echo $grn_list_data[0]["stock_grn_dc_date"] ;?>"placeholder="Dc Date" required="required">

											</div> <!-- /controls -->					

										</div> <!-- /control-group -->

										

										<div class="control-group">											

											<label class="control-label" for="num_vehicle_number">Vehicle Number</label>

											<div class="controls">

												<input type="text" class="span6" name="num_vehicle_number" value="<?php echo $grn_list_data[0]["stock_grn_vehicle_number"] ;?>" placeholder="Unique code for no">

											</div> <!-- /controls -->					

										</div> <!-- /control-group -->

										

										<div class="control-group">											

											<label class="control-label" for="txt_remarks">Remarks</label>

											<div class="controls">

												<input type="text" class="span6" name="txt_remarks" value="<?php echo $grn_list_data[0]["stock_grn_remarks"] ;?>" placeholder="Remarks">

											</div> <!-- /controls -->					

										</div> <!-- /control-group -->

                                                                                                                                                               										 <br />

										

											

										<div class="form-actions">

											<input type="submit" class="btn btn-primary" name="edit_grn_submit" value="Submit" />

											<button type="reset" class="btn">Cancel</button>

										</div> <!-- /form-actions -->

									</fieldset>

								</form>

								</div>

								

							</div> 

							

					</div> <!-- /widget-content -->

						

				</div> <!-- /widget -->

	      		

		    </div> <!-- /span8 -->

	      	

	      	

	      	

	      	

	      </div> <!-- /row -->

	

	    </div> <!-- /container -->

	    

	</div> <!-- /main-inner -->

    

</div> <!-- /main -->

    

    

    

 

<div class="extra">



	<div class="extra-inner">



		<div class="container">



			<div class="row">

                    

                </div> <!-- /row -->



		</div> <!-- /container -->



	</div> <!-- /extra-inner -->



</div> <!-- /extra -->





    

    

<div class="footer">

	

	<div class="footer-inner">

		

		<div class="container">

			

			<div class="row">

				

    			<div class="span12">

    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.

    			</div> <!-- /span12 -->

    			

    		</div> <!-- /row -->

    		

		</div> <!-- /container -->

		

	</div> <!-- /footer-inner -->

	

</div> <!-- /footer -->

    





<script src="js/jquery-1.7.2.min.js"></script>

	

<script src="js/bootstrap.js"></script>

<script src="js/base.js"></script>





  </body>



</html>

