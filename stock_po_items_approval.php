<?php

/* SESSION INITIATE - START */

session_start();

/* SESSION INITIATE - END */



/* FILE HEADER - START */

// LAST UPDATED ON: 23rd Sep 2016

// LAST UPDATED BY: Lakshmi

/* FILE HEADER - END */



/* TBD - START */

/* TBD - END */
$_SESSION['module'] = 'Stock Transactions';



/* DEFINES - START */

define('PO_FUNC_ID','169');

/* DEFINES - END */



/* INCLUDES - START */

$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_indent_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_quotation_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_purchase_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock'.DIRECTORY_SEPARATOR.'stock_grn_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

/* INCLUDES - END */



if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))

{

	// Session Data

	$user 		   = $_SESSION["loggedin_user"];

	$role 		   = $_SESSION["loggedin_role"];

	$loggedin_name = $_SESSION["loggedin_user_name"];



	// Get permission settings for this user for this page	

	$edit_perms_list = i_get_user_perms($user,'',PO_FUNC_ID,'3','1');	

	

	/* DATA INITIALIZATION - START */

	$alert_type = -1;

	$alert = "";
	


	if(isset($_GET['quotation_id']))

	{

		$quotation_id = $_GET['quotation_id'];

	}

	else

	{

		$quotation_id = "-1";

	}

	

	if(isset($_POST["file_search_submit"]))

	{
		$search_status    = $_POST["search_status"];



	}

	else

	{

		$search_status ='Pending';

	}

	// Get Material Details

	$stock_material_search_data = array();

	$material_list = i_get_stock_material_master_list($stock_material_search_data);
	
	if($material_list["status"] == SUCCESS)

	{

		$material_list_data = $material_list["data"];

	}

	else

	{

		

	}

	//Get Uom List

	$stock_unit_search_data = array();

	$unit_list = i_get_stock_unit_measure_list($stock_unit_search_data);

	if($unit_list["status"] == SUCCESS)

	{

		$unit_list_data = $unit_list["data"];

	}

	else

	{

		$alert = $unit_list["data"];

		$alert_type = 0;

	}

	//Get Tax Type List

	$stock_tax_type_master_search_data = array();

	$tax_type_list = i_get_stock_tax_type_master_list($stock_tax_type_master_search_data);

	if($tax_type_list["status"] == SUCCESS)

	{

		$tax_type_list_data = $tax_type_list["data"];

	}

	else

	{

		$alert = $tax_type_list["data"];

		$alert_type = 0;

	}

	

	//Get Transporation List

	$transportation_search_data = array();

	$transportation_list = i_get_transportation_list($transportation_search_data);

	if($transportation_list["status"] == SUCCESS)

	{

		$transportation_list_data = $transportation_list["data"];

	}

	else

	{

		$alert = $transportation_list["data"];

		$alert_type = 0;

	}

	

	// Get Purchase Item Details

	$stock_purchase_order_items_search_data = array("active"=>'1',"status"=>$search_status);

	$purchase_order_items_list = i_get_stock_sum_of_purchase_order_items_list($stock_purchase_order_items_search_data);
	
	if($purchase_order_items_list["status"] == SUCCESS)

	{

		$purchase_order_items_list_data = $purchase_order_items_list["data"];

	}

	else

	{

		$alert = $alert."Alert: ".$purchase_order_items_list["data"];

	}

	

	//Get Purchase List

	$stock_purchase_order_search_data = array('status'=>$search_status);

	$purchase_order_list = i_get_stock_purchase_order_list($stock_purchase_order_search_data);

	if($purchase_order_list["status"] == SUCCESS)

	{

		$purchase_order_list_data = $purchase_order_list["data"];

	}

	else

	{

		$alert = $purchase_order_list["data"];

		$alert_type = 0;

	}

	

}

else

{

	header("location:login.php");

}	

?>



<!DOCTYPE html>

<html lang="en">

  

<head>

    <meta charset="utf-8">

    <title>Add Purchase Order Items</title>

    

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <meta name="apple-mobile-web-app-capable" content="yes">    

    

    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">

    

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">

    <link href="css/font-awesome.css" rel="stylesheet">

    

    <link href="css/style.css" rel="stylesheet">

   





    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->

    <!--[if lt IE 9]>

      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>

    <![endif]-->



  </head>



<body>



<?php

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');

?>    



<div class="main">

	

	<div class="main-inner">



	    <div class="container">

	

	      <div class="row">

	      	

	      	<div class="span12">      		

	      		

	      		<div class="widget ">

	      			

	      			<div class="widget-header">

	      				<i class="icon-user"></i>

	      				<h3>Purchase Order Details</h3>

						<!--<div class="pull-right"><a style="padding-right:10px" href="stock_purchase_order_items.php?<? echo $item_id ;?>" >Edit </a></div>-->

	  				</div> <!-- /widget-header -->

			              
				
			  	

			 		  

            </div>

           

            <!-- /widget-header -->

            <div class="widget-content">
             <div class="" style="height:50px; padding-top:10px;"> 
             <form method="post" id="file_search_form" action="stock_po_items_approval.php">

			  <span style="padding-left:20px; padding-right:20px;">

			  <select name="search_status">

			 <option value="Pending" <?php if($search_status == "Pending"){?> selected <?php } ?>>Pending</option>

			  <option value="Approved" <?php if($search_status == "Approved"){?> selected <?php } ?>>Approved</option>

			  <option value="Rejected" <?php if($search_status == "Rejected"){?> selected <?php } ?>>Rejected</option>

			  <option value="Completed" <?php if($search_status == "Completed"){?> selected <?php } ?>>Completed</option>

			  </select>

			  </span>
				 <input type="submit" name="file_search_submit" />

			  </form>
			  </div>			

              <table class="table table-bordered">

                <thead>

                  <tr>
                  	<th> Project </th>
					<th>Purchase Order No</th>

					<th>PO Date</th>

					<th>Vendor</th>

					<th>Value</th>

					<th>Status</th>

					<th>Printview</th>

					<th colspan="2" style="text-align:center;">Action</th>	

				</tr>

				</thead>

				<tbody>

				 <?php

				 if($purchase_order_list["status"] == SUCCESS)

				 {

					 $total_value = 0;

					 $mode = "view";

					for($count = 0; $count < count($purchase_order_list_data); $count++)

					{					

						$po_value = 0;

				

						$stock_purchase_order_items_search_data = array("active"=>'1',"order_id"=>$purchase_order_list_data[$count]['stock_purchase_order_id']);

						$purchase_order_items_list = i_get_stock_purchase_order_items_list($stock_purchase_order_items_search_data);

						if($purchase_order_items_list["status"] == SUCCESS)

						{

							$purchase_order_items_list_data = $purchase_order_items_list["data"];

							for($item_count = 0; $item_count < count($purchase_order_items_list_data); $item_count++)

							{


								$new_po_items_value[]   =  $purchase_order_items_list_data[$item_count]["stock_purchase_order_total_amount"];	

							}
							$items_value = array_sum($new_po_items_value);
                    		$total_po_value = $items_value + $purchase_order_list_data[$count]['stock_transportation_total'] + $purchase_order_list_data[$count]['stock_other_total'] +
                          $purchase_order_list_data[$count]['stock_fright_total'];
								

						}

					

					?>				

					<tr>
						<td><?php echo $purchase_order_list_data[$count]["stock_project_name"]; ?></td>
						<td><?php echo $purchase_order_list_data[$count]["stock_purchase_order_number"]; ?></td>

						<td><?php echo date("d-M-Y",strtotime($purchase_order_list_data[$count]["stock_purchase_order_added_on"])); ?></td>

						<td><?php echo $purchase_order_list_data[$count]["stock_vendor_name"]; ?></td>

						<td><?php echo $total_po_value ?></td>

						<td><?php echo $purchase_order_list_data[$count]["stock_purchase_order_status"]; ?></td>

						<td><a href="stock_purchase_order_items.php?order_id=<?php echo $purchase_order_list_data[$count]["stock_purchase_order_id"]; ?>&mode=0">View PO Items</a></td>

						<td><?php if($edit_perms_list['status'] == SUCCESS){ ?><?php if(($purchase_order_list_data[$count]["stock_purchase_order_status"] == "Rejected") || ($purchase_order_list_data[$count]["stock_purchase_order_status"] == "Pending")){?><a href="#" onclick="return approve_indent(<?php echo $purchase_order_list_data[$count]["stock_purchase_order_id"]; ?>);">Approve</a><?php }?><?php } ?></td>

						<td><?php if($edit_perms_list['status'] == SUCCESS){ ?><?php if(($purchase_order_list_data[$count]["stock_purchase_order_status"] == "Approved") || ($purchase_order_list_data[$count]["stock_purchase_order_status"] == "Pending")){?><a href="#" onclick="return reject_indent(<?php echo $purchase_order_list_data[$count]["stock_purchase_order_id"]; ?>);">Reject</a><?php } else { echo "Rejected" ; } ?><?php } ?></td>

					</tr>

					<?php 		

					}

				}

				else

				{

				?>

				<td colspan="8">No Items added</td>

				<?php

				}

				 ?>	



                </tbody>

				</table>

				<br/>					

				<div class="modal-body">

			    <div class="row">

                <!--<center><a href="#" data-toggle="modal" data-target="#myModal"><strong>+Purchase Order Items</strong></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</center> -->

				  </div>

				  </div>			  			            

            <!-- /widget-content --> 

          </div>

								</div> <!-- /controls -->	                                                

							</div> <!-- /control-group -->

               <br><br>

               <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

                  <div class="modal-dialog" role="document"  style="padding:10px; text-align:center">

                     <div class="modal-content">

                       

                           <div class="modal-header">

                              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                              <h4 class="modal-title" id="myModalLabel">Add Purchase</h4>

                           </div>

                            <form method="post" action="stock_purchase_order_items.php">

							<input type="hidden" name="hd_order_id" value="<?php echo $order_id; ?>" />

							

							             <div class="control-group" style="width:50%; float:left">											

											<label class="control-label" for="ddl_item">Item*</label>

											<div class="controls">

												<select name="ddl_item" required>

												<option>- - -Select items- - -</option>

												<?php

												for($count = 0; $count < count($material_list_data); $count++)

												{

												?>

												<option value="<?php echo $material_list_data[$count]["stock_material_id"]; ?>"><?php echo $material_list_data[$count]["stock_material_name"]; ?></option>

												<?php

												}

												?>

												</select>

											</div> <!-- /controls -->					

										</div> <!-- /control-group -->

										

							            

										<div class="control-group"style="width:50%; float:left">											

											<label class="control-label" for="num_qty">Quantity*</label>

											<div class="controls">

												<input type="number" name="num_qty" placeholder="Quantity" required="required">

											</div> <!-- /controls -->					

										</div> <!-- /control-group -->

										

										<div class="control-group" style="width:50%; float:left">											

											<label class="control-label" for="ddl_unit">Unit Of Measure*</label>

											<div class="controls">

												<select name="ddl_unit" required>

												<option>- - -Select Unit- - -</option>

												<?php

												for($count = 0; $count < count($unit_list_data); $count++)

												{

												?>

												<option value="<?php echo $unit_list_data[$count]["stock_unit_id"]; ?>"><?php echo $unit_list_data[$count]["stock_unit_name"]; ?></option>

												<?php

												}

												?>

												</select>

											</div> <!-- /controls -->					

										</div> <!-- /control-group -->

										

										

										<div class="control-group" style="width:50%; float:left">											

											<label class="control-label" for="ddl_tax_type_name">Tax Type*</label>

											<div class="controls">

												<select name="ddl_tax_type_name" required>

												<option>- - -Select Tax Type- - -</option>

												<?php

												for($count = 0; $count < count($tax_type_list_data); $count++)

												{

												?>

												<option value="<?php echo $tax_type_list_data[$count]["stock_tax_type_master_id"]; ?>"><?php echo $tax_type_list_data[$count]["stock_tax_type_master_name"]; ?></option>

												<?php

												}

												?>

												</select>

											</div> <!-- /controls -->					

										</div> <!-- /control-group -->

										

										<div class="control-group" style="width:50%; float:left">											

											<label class="control-label" for="num_cost">cost</label>

											<div class="controls">

												<input type="number" name="num_cost" placeholder="Cost" required="required">

											</div> <!-- /controls -->					

										</div> <!-- /control-group -->

										

										<div class="control-group">											

											<label class="control-label" for="ddl_transport">Transport*</label>

											<div class="controls">

												<select name="ddl_transport" required>

												<option value="">- - Select Transportation - -</option>

												<?php

												for($count = 0; $count < count($transportation_list_data); $count++)

												{

												?>

												<option value="<?php echo $transportation_list_data[$count]["stock_transportation_id"]; ?>"><?php echo $transportation_list_data[$count]["stock_transportation_name"]; ?></option>

												<?php

												}

												?>

												</select>

											</div> <!-- /controls -->					

										</div> <!-- /control-group -->

										

										<div class="control-group" style="width:50%; float:left">											

											<label class="control-label" for="txt_delivery_schedule">Delivery Schedule*</label>

											<div class="controls">

												<input type="name" name="txt_delivery_schedule" placeholder="Delivery Schedule" required="required">

											</div> <!-- /controls -->					

										</div> <!-- /control-group -->

										

										<div class="control-group">											

											<label class="control-label" for="txt_remarks">Remarks</label>

											<div class="controls">

												<input type="text" name="txt_remarks" placeholder="Remarks">

											</div> <!-- /controls -->					

										</div> <!-- /control-group -->

										

                           <div class="modal-footer">                              

                            <button type="submit" name="purchase_order_items_submit" class="btn btn-primary">Save changes</button>					  

                           </div>

                           </form>

                     </div>

                  </div>

               </div>

			   

			    <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="top:39%;">

                  <div class="modal-dialog" role="document"  style="padding:10px; text-align:center ;">

                     <div class="modal-content">

                       

                           <div class="modal-header">

                              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                              <h4 class="modal-title" id="myModalLabel">Add Purchase</h4>

                           </div>

                            <form method="post" action="stock_purchase_order_items.php">

							<input type="hidden" name="hd_order_id" value="<?php echo $order_id; ?>" />

							<input type="hidden" name="quotation_id" value="<?php echo $quotation_id; ?>" />

							

							             <table class="table table-bordered">

                <thead>

                  <tr>

					<th>Item</th>

					<th>Quantity</th>

					<th>Unit Of Measure</th>

					<th>Active</th>

					<th colspan="2" style="text-align:center;">Actions</th>	



				</tr>

				</thead>

				<tbody>

				 <?php

				 $stock_quotation_compare_search_data = array("quotation_id"=>$quotation_id);

				$quotation_item_list = i_get_stock_quotation_compare_list($stock_quotation_compare_search_data);

				if($quotation_item_list["status"] == SUCCESS)

				{

					$indent_item_id = $quotation_item_list["data"][0]["stock_quotation_indent_id"];

					$stock_indent_items_search_data = array("item_id"=>$indent_item_id);

					$indent_item_list = i_get_indent_items_list($stock_indent_items_search_data);

					if($indent_item_list["status"] == SUCCESS)

					{

						$indent_id = $indent_item_list["data"][0]["stock_indent_id"];

						$stock_indent_items_search_data = array("indent_id"=>$indent_id);

						$indent_item_list_details = i_get_indent_items_list($stock_indent_items_search_data);

						

					}

				}

				if($indent_item_list_details["status"] == SUCCESS)

				{

					$indent_item_list_details_data = $indent_item_list_details["data"];

					for($count = 0; $count < count($indent_item_list_details_data); $count++)

					{	

						if($indent_item_list_details_data[$count]["stock_indent_item_id"] != $indent_item_id)

						{

				

							?>				

							<tr>

							

								<td><?php echo $indent_item_list_details_data[$count]["stock_material_name"]; ?></td>

								<td><?php echo $indent_item_list_details_data[$count]["stock_indent_item_quantity"]; ?></td>

								<td><?php echo $indent_item_list_details_data[$count]["stock_indent_item_uom"]; ?></td>

								<td><?php echo $indent_item_list_details_data[$count]["stock_indent_item_active"]; ?></td>

								<td><?php echo $indent_item_list_details_data[$count]["stock_indent_item_id"]; ?></td>

								<td><input type="checkbox" name="cb_item[]" value="<?php echo $indent_item_list_details_data[$count]["stock_indent_item_id"] ;?>"><br></td>

							</tr>

						<?php 		

						}

					}

				}

				else

				{

				?>

				<td colspan="2">No Items added</td>

				<?php

				}

				 ?>	



                </tbody>

					</table>

										

                           <div class="modal-footer">                              

                            <button type="submit" name="items_submit" class="btn btn-primary">Save changes</button>					  

                           </div>

                           </form>

                     </div>

                  </div>

               </div>

						  

					</div> <!-- /widget-content -->

						

				</div> <!-- /widget -->

	      		

		    </div> <!-- /span8 -->

	      	

	      </div> <!-- /row -->

	

	    </div> <!-- /container -->

	    

	</div> <!-- /main-inner -->

    

</div> <!-- /main -->

 

<div class="extra">



	<div class="extra-inner">



		<div class="container">



			<div class="row">

                    

                </div> <!-- /row -->



		</div> <!-- /container -->



	</div> <!-- /extra-inner -->



</div> <!-- /extra -->  

    

<div class="footer">

	

	<div class="footer-inner">

		

		<div class="container">

			

			<div class="row">

				

    			<div class="span12">

    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.

    			</div> <!-- /span12 -->

    			

    		</div> <!-- /row -->

    		

		</div> <!-- /container -->

		

	</div> <!-- /footer-inner -->

	

</div> <!-- /footer -->

    





<script src="js/jquery-1.7.2.min.js"></script>

	

<script src="js/bootstrap.js"></script>

<script src="js/base.js"></script>

<script>

function delete_purchase_order_items(item_id,po_id)

{

	var ok = confirm("Are you sure you want to Delete?")

	{         

		if (ok)

		{



			if (window.XMLHttpRequest)

			{// code for IE7+, Firefox, Chrome, Opera, Safari

				xmlhttp = new XMLHttpRequest();

			}

			else

			{// code for IE6, IE5

				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");

			}



			xmlhttp.onreadystatechange = function()

			{

				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)

				{

					if(xmlhttp.responseText != "SUCCESS")

					{

					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;

					 document.getElementById("span_msg").style.color = "red";

					}

					else					

					{

					 window.location = "stock_purchase_order_items.php";

					}

				}

			}



			xmlhttp.open("POST", "stock_delete_purchase_order_items.php");   // file name where delete code is written

			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

			xmlhttp.send("item_id=" + item_id + "po_id=" + po_id +  "&action=0");

		}

	}	

}

/*function go_to_po_item(po_id,mode)

{		

	var form = document.createElement("form");

    form.setAttribute("method", "post");

    form.setAttribute("action", "stock_purchase_order_items.php");

	

	var hiddenField2 = document.createElement("input");

	hiddenField2.setAttribute("type","hidden");

	hiddenField2.setAttribute("name","po_id");

	hiddenField2.setAttribute("value",po_id);

	

	var hiddenField3 = document.createElement("input");

	hiddenField3.setAttribute("type","hidden");

	hiddenField3.setAttribute("name","mode");

	hiddenField3.setAttribute("value",mode);

	

	form.appendChild(hiddenField2);

	form.appendChild(hiddenField3);

	

	document.body.appendChild(form);

    form.submit();

}*/

function item_for_approval(order_id)

{

	var ok = confirm("Are you sure you want to send for approvals?")

	{         

		if (ok)

		{



			if (window.XMLHttpRequest)

			{// code for IE7+, Firefox, Chrome, Opera, Safari

				xmlhttp = new XMLHttpRequest();

			}

			else

			{// code for IE6, IE5

				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");

			}



			xmlhttp.onreadystatechange = function()

			{

				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)

				{

					if(xmlhttp.responseText != "SUCCESS")

					{

					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;

					 document.getElementById("span_msg").style.color = "red";

					}

					else					

					{

					 window.location = "stock_po_items_approval.php";

					}

				}

			}



			xmlhttp.open("POST", "stock_po_items_for_approval.php");   // file name where delete code is written

			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

			xmlhttp.send("order_id=" + order_id + "&action=0");

		}

	}	

}

function approve_indent(po_id)

{

	var ok = confirm("Are you sure you want to Approve?")

	{         

		if (ok)

		{



			if (window.XMLHttpRequest)

			{// code for IE7+, Firefox, Chrome, Opera, Safari

				xmlhttp = new XMLHttpRequest();

			}

			else

			{// code for IE6, IE5

				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");

			}



			xmlhttp.onreadystatechange = function()

			{

				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)

				{					

					if(xmlhttp.responseText != "SUCCESS")

					{

					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;

					 document.getElementById("span_msg").style.color = "red";

					}

					else					

					{

					 window.location = "stock_po_items_approval.php";

					}

				}

			}



			xmlhttp.open("POST", "ajax/stock_approve_po_item.php");   // file name where delete code is written

			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

			xmlhttp.send("po_id=" + po_id + "&action=Approved");

		}

	}	

}



function reject_indent(indent_item_id)

{

	var ok = confirm("Are you sure you want to Reject?")

	{         

		if (ok)

		{



			if (window.XMLHttpRequest)

			{// code for IE7+, Firefox, Chrome, Opera, Safari

				xmlhttp = new XMLHttpRequest();

			}

			else

			{// code for IE6, IE5

				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");

			}



			xmlhttp.onreadystatechange = function()

			{

				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)

				{

					if(xmlhttp.responseText != "SUCCESS")

					{

					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;

					 document.getElementById("span_msg").style.color = "red";

					}

					else					

					{

					 window.location = "stock_po_items_approval.php";

					}

				}

			}



			xmlhttp.open("POST", "ajax/stock_reject_po_item.php");   // file name where delete code is written

			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

			xmlhttp.send("indent_item_id=" + indent_item_id + "&action=Rejected");

		}

	}	

}



</script>

<script>
/* Open the sidenav */
function openNav() {
    document.getElementById("mySidenav").style.width = "75%";
}

/* Close/hide the sidenav */
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}
</script>



  </body>



</html>

