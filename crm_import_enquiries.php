<?php

/* SESSION INITIATE - START */

session_start();

/* SESSION INITIATE - END */



/* FILE HEADER - START */

// LAST UPDATED ON: 14th Sep 2015

// LAST UPDATED BY: Nitin Kashyap

/* FILE HEADER - END */

define('CRM_IMPORT_ENQUIRIES_FUNC_ID','113');

/* TBD - START */

/* TBD - END */
$_SESSION['module'] = 'CRM Transactions';


/* INCLUDES - START */

$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_transaction_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_masters'.DIRECTORY_SEPARATOR.'crm_masters_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_projects'.DIRECTORY_SEPARATOR.'crm_project_functions.php');

/* INCLUDES - END */



if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))

{

	// Session Data

	$user 		   = $_SESSION["loggedin_user"];

	$role 		   = $_SESSION["loggedin_role"];

	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Get permission settings for this user for this page
	$add_perms_list     = i_get_user_perms($user,'',CRM_IMPORT_ENQUIRIES_FUNC_ID,'1','1');
	$view_perms_list    = i_get_user_perms($user,'',CRM_IMPORT_ENQUIRIES_FUNC_ID,'2','1');
	$edit_perms_list    = i_get_user_perms($user,'',CRM_IMPORT_ENQUIRIES_FUNC_ID,'3','1');
	$delete_perms_list  = i_get_user_perms($user,'',CRM_IMPORT_ENQUIRIES_FUNC_ID,'4','1');


	/* DATA INITIALIZATION - START */

	$alert_type = -1;

	$alert = "";

	/* DATA INITIALIZATION - END */



	// Capture the form data

	if(isset($_POST["import_submit"]))

	{

		$import_file = upload("enquiry_file",$user);

			// var_dump($import_file);
		// Check for mandatory fields

		if($import_file != "")

		{

			ini_set("max_execution_time", 0);



			$row = 1;

			$import_file="documents/".$import_file;
			if (($handle = fopen($import_file, "r")) !== FALSE)

			{

				while (($enquiry_data = fgetcsv($handle, 0, ",")) !== FALSE)

				{

					if($row > 1)

					{

						// Status

						$status = 1;



						// Field 1 - Project

						$project_name     = $enquiry_data[0];

						if(($project_name == '') || ($project_name == '0'))

						{

							$project_name = '-1';

						}

						$project_sdetails = i_get_project_list($project_name,'');

						if($project_sdetails["status"] == SUCCESS)

						{

							$project = $project_sdetails["data"][0]["project_id"];

						}

						else

						{

							$alert = $alert. "Import failed at row no. ".$row." due to invalid project;";

							$status = ($status&0);

						}



						// Field 2 - Name

						$name = $enquiry_data[1];

						if (!preg_match('/^[a-zA-Z .]*$/',$name)){
								$alert = $alert. "Import failed at row no. ".$row." due to invalid name with special characters;";
								$status = ($status&0);
						}
						// Field 3 - Mobile

						$cell = $enquiry_data[2];

						$ccell = substr($cell, -10, 10);
							$enquiry_list = i_get_enquiry_list('','','','','',$ccell,'','','','','','','','','','','','','','');
							if($enquiry_list["status"] != SUCCESS)
							{
								$allow_enquiry = true;
							}
							else
							{
								$allow_enquiry = false;
							}

						if (!preg_match('/^[0-9]*$/',$cell)){
								$alert = $alert. "Import failed at row no. ".$row." due to invalid cell with special characters or alphabets;";
								$status = ($status&0);
						}

						if (!$allow_enquiry){
								$alert = $alert. "Import failed at row no. ".$row." Record with cell ".$cell." Already exists;";
								$status = ($status&0);
						}
						// Field 4 - Email

						$email = $enquiry_data[3];



						// Field 5 - Company

						$company = $enquiry_data[4];



						// Field 6 - Location

						// $location = $enquiry_data[5];



						// Field 7 - Source

						$source_name = $enquiry_data[5];

						if(($source_name == '') || ($source_name == '0'))

						{

							$source_name = '-1';

						}

						$source_sdetails = i_get_enquiry_source_list($source_name,'');

						if($source_sdetails["status"] == SUCCESS)

						{

							$source = $source_sdetails["data"][0]["enquiry_source_master_id"];

						}

						else

						{

							$alert = $alert. "Import failed at row no. ".$row." due to invalid enquiry source;";

							$status = ($status&0);

						}



						// Field 8 - Remarks

						$remarks = $enquiry_data[6];



						// Field 9 - Interest Status

						$interest_status = $enquiry_data[7];

						if(($interest_status == '') || ($interest_status == '0'))

						{

							$interest_status = '-1';

						}

						$int_status_sdetails = i_get_interest_status_list($interest_status,'');

						if($int_status_sdetails["status"] == SUCCESS)

						{

							$int_status = $int_status_sdetails["data"][0]["crm_cust_interest_status_id"];

						}

						else

						{

							$alert = $alert."Import failed at row no. ".$row." due to invalid interest status;";

							$status = ($status&0);

						}



						// Field 10 - Follow Up Date

						$fup_date = $enquiry_data[8];

						if($fup_date == '')

						{

							$follow_up_date = date("Y-m-d",strtotime(date('Y-m-d').'+ 1 day'));

						}

						else

						{

							$date_array = explode('-',$fup_date);

							if(($date_array[2] == date('Y')) || ($date_array[2] == (date('Y') + 1)))

							{

								$follow_up_date = date("Y-m-d",strtotime($date_array[2].'-'.$date_array[1].'-'.$date_array[0]));

							}

							else

							{

								$alert = $alert."Import failed at row no. ".$row." due to a date beyond this year and next year;";

								$status = ($status&0);

							}

						}



						// Field 11 - Assigned To

						$assigned_to = $enquiry_data[9];

						if(($assigned_to == '') || ($assigned_to == '0'))

						{

							$assigned_to = '-1';

						}

						$assignee_sdetails = i_get_user_list('','',$assigned_to,'');

						if($assignee_sdetails["status"] == SUCCESS)

						{

							$assignee = $assignee_sdetails["data"][0]["user_id"];

						}

						else

						{

							$alert = $alert."Import failed at row no. ".$row." due to invalid assignee;";

							$status = ($status&0);

						}



						// Field 12 - Assigned By

						$assigned_by = $enquiry_data[10];

						if(($assigned_by == '') || ($assigned_by == '0'))

						{

							$assigned_by = '-1';

						}

						$assigner_sdetails = i_get_user_list('','',$assigned_by,'');

						if($assigner_sdetails["status"] == SUCCESS)

						{

							$assigner = $assigner_sdetails["data"][0]["user_id"];

						}

						else

						{

							$alert = $alert."Import failed at row no. ".$row." due to invalid assigner;";

							$status = ($status&0);

						}



						if($status != 0)

						{

							$_SESSION["loggedin_user_name"] = $assigner_sdetails["data"][0]["user_name"];

							$enquiry_iresult = i_add_enquiry($project,$name,$cell,$email,$company,'',$source,$remarks,$int_status,'0',$follow_up_date,$assignee,$assigner);

							if($enquiry_iresult["status"] == SUCCESS)

							{

								$alert = "Import Successful!";

								$alert_type = 1;

							}

							$_SESSION["loggedin_user_name"] = $loggedin_name;

						}

						else

						{

							$alert_type = 0;

						}

					}



					$row++;

				}

				fclose($handle);

			}

		}

		else

		{

			$alert = "Please fill all the mandatory fields";

			$alert_type = 0;

		}

	}

}

else

{

	header("location:login.php");

}


// Functions

function upload($file_id,$user_id)

{

	if($_FILES[$file_id]["name"]!="")

	{

		if ($_FILES[$file_id]["error"] > 0)

		{

			// echo "Return Code: " . $_FILES[$file_id]["error"] . "<br />";

		}

		else

		{

			$_FILES[$file_id]["name"]=$user_id."_".$_FILES[$file_id]["name"];

			move_uploaded_file($_FILES[$file_id]["tmp_name"], "documents/".$_FILES[$file_id]["name"]);

		}

	}


	return $_FILES[$file_id]["name"];

}

?>



<!DOCTYPE html>

<html lang="en">



<head>

    <meta charset="utf-8">

    <title>Import Enquiries</title>



    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <meta name="apple-mobile-web-app-capable" content="yes">



    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">



    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">

    <link href="css/font-awesome.css" rel="stylesheet">



    <link href="css/style.css" rel="stylesheet">







    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->

    <!--[if lt IE 9]>

      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>

    <![endif]-->



  </head>



<body>



<?php

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');

?>



<div class="main">



	<div class="main-inner">



	    <div class="container">



	      <div class="row">



	      	<div class="span12">



	      		<div class="widget ">



	      			<div class="widget-header">

	      				<i class="icon-user"></i>

	      				<h3>Your Account</h3>

	  				</div> <!-- /widget-header -->



					<div class="widget-content">







						<div class="tabbable">

						<ul class="nav nav-tabs">

						  <li>

						    <a href="#formcontrols" data-toggle="tab">Import Enquiries</a>

						  </li>

						</ul>



						<br>

							<div class="control-group">

								<div class="controls">

								<?php

								if($alert_type == 0) // Failure

								{

								?>

									<div class="alert">

                  <button type="button" class="close" data-dismiss="alert">&times;</button>

                   <strong><?php $errors=explode(";",$alert);
										for($i=0;$i<count($errors)-1;$i++) {
										?> <li><ul><?php echo $errors[$i];?> </ul></li>
										<?php } ?></strong>
                </div>

								<?php

								}

								?>



								<?php

								if($alert_type == 1) // Success

								{

								?>

                  <div class="alert alert-success">

                    <button type="button" class="close" data-dismiss="alert">&times;</button>

                    <strong><?php echo $alert; ?></strong>

                  </div>

								<?php

								}

								?>

								</div> <!-- /controls -->

							</div> <!-- /control-group -->

							<div class="tab-content">

								<div class="tab-pane active" id="formcontrols">

								<form id="import_enquiries" class="form-horizontal" method="post" action="crm_import_enquiries.php" enctype="multipart/form-data">

									<fieldset>


										<div class="control-group">

											<label class="control-label" for="bank_name">Enquiry File</label>

											<div class="controls">

												<input type="file" class="span6" name="enquiry_file" placeholder="Name of the bank" required="required">

											</div> <!-- /controls -->

										</div> <!-- /control-group -->

      							<div class="form-actions">

											<input type="submit" class="btn btn-primary" name="import_submit" value="Submit" />

											<button type="reset" class="btn">Cancel</button>

										</div> <!-- /form-actions -->

									</fieldset>

								</form>

								</div>

							</div>

						</div>

				</div> <!-- /widget-content -->



				</div> <!-- /widget -->



		    </div> <!-- /span8 -->

	      </div> <!-- /row -->



	    </div> <!-- /container -->



	</div> <!-- /main-inner -->



</div> <!-- /main -->

<div class="extra">



	<div class="extra-inner">



		<div class="container">



			<div class="row">



                </div> <!-- /row -->



		</div> <!-- /container -->



	</div> <!-- /extra-inner -->



</div> <!-- /extra -->

<div class="footer">



	<div class="footer-inner">



		<div class="container">



			<div class="row">



    			<div class="span12">

    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.

    			</div> <!-- /span12 -->



    		</div> <!-- /row -->



		</div> <!-- /container -->



	</div> <!-- /footer-inner -->



</div> <!-- /footer -->







<script src="js/jquery-1.7.2.min.js"></script>



<script src="js/bootstrap.js"></script>

<script src="js/base.js"></script>

<script>
/* Open the sidenav */
function openNav() {
    document.getElementById("mySidenav").style.width = "75%";
}

/* Close/hide the sidenav */
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}
</script>



  </body>



</html>
