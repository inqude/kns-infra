<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: bd_file_list.php
CREATED ON	: 11-Nov-2015
CREATED BY	: Nitin Kashyap
PURPOSE     : List of BD files
*/

/*
TBD: 
1. Date display and calculation
2. Permission management
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_projects'.DIRECTORY_SEPARATOR.'bd_project_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_projects'.DIRECTORY_SEPARATOR.'bd_payment_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_masters'.DIRECTORY_SEPARATOR.'bd_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'files'.DIRECTORY_SEPARATOR.'file_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Query String Data
	if(isset($_GET["msg"]))
	{
		$msg = $_GET["msg"];
	}
	else
	{
		$msg = "";
	}
	// Nothing here

	// Temp data
	$alert = "";

	$survey_no  = "";
	$village    = "";			
	$custody    = "1";
	$bank       = "";
	$start_date = "";
	$end_date   = "";

	// Search parameters
	if(isset($_POST["bd_file_search_submit"]))
	{		
		$survey_no  = $_POST["stxt_survey_no"];
		$village    = $_POST["ddl_village"];
		$custody    = $_POST["ddl_custody"];
		$bank       = $_POST["ddl_bank"];	
		$start_date = $_POST['dt_start_date'];		
		$end_date   = $_POST['dt_end_date'];
	}
	
	// Get list of files	
	$bd_land_bank_search_data = array('survey_no'=>$survey_no,'village'=>$village,'active'=>'1');
	
	$bd_file_list = i_get_bd_land_bank_list($bd_land_bank_search_data);

	if($bd_file_list["status"] == SUCCESS)
	{
		$bd_file_list_data = $bd_file_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$bd_file_list["data"];
	}	
	
	// Get list of villages
	$village_list = i_get_village_list('');
	if($village_list["status"] == SUCCESS)
	{
		$village_list_data = $village_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$village_list["data"];
	}	
	
	// Get Bank List
	$bank_list = i_get_bank_list('','1');
	if($bank_list["status"] == SUCCESS)
	{
		$bank_list_data = $bank_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$bank_list["data"];
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>BD File Borrow List</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>    

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header" style="height:60px;"> <i class="icon-th-list"></i>				
              <h3>BD File List&nbsp;&nbsp;&nbsp;Total Extent: <span id="total_extent_span"><i>Calculating</i></span> guntas&nbsp;&nbsp;&nbsp;<span id="total_extent_span_acres"><i>Calculating</i></span> acres&nbsp;&nbsp;&nbsp;Total Files: <span id="total_file_count"><i>Calculating</i></span>&nbsp;&nbsp;&nbsp;Total Mortgage Value: <span id="total_mort_value"><i>0</i></span>&nbsp;&nbsp;&nbsp;Total Paid Value: <span id="total_paid_value"><i>0</i></span>&nbsp;&nbsp;&nbsp;Total Balance Value: <span id="total_balance_value"><i>0</i></span><br><br>			  			 <!-- <a href="excelexports/bd_borrow_details_export.php?village=<?php echo $village; ?>&bank=<?php echo $bank; ?>&start_date=<?php echo $start_date; ?>&end_date=<?php echo $end_date; ?>" target="_blank">Export to Excel</a>-->			 			 <a href="excelexports/bd_borrow_live_export.php?village=<?php echo $village; ?>&survey=<?php echo $survey_no; ?>&custody=<?php echo $custody; ?>" target="_blank">Export Live to Excel</a></h3>		  
            </div>
            <!-- /widget-header -->
			
			<div class="widget-header" style="height:90px; padding-top:10px;">               
			  <form method="post" id="file_search_form" action="bd_file_borrow_list.php">
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="ddl_village">
			  <option value="">- - Select Village - -</option>
			  <?php for($count = 0; $count < count($village_list_data); $count++)
			  {?>
			  <option value="<?php echo $village_list_data[$count]["village_id"]; ?>" <?php if($village_list_data[$count]["village_id"] == $village) { ?> selected="selected" <?php } ?>><?php echo $village_list_data[$count]["village_name"]; ?></option>
			  <?php
			  }?>
			  </select>			  
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="ddl_custody">			  
			  <option value="">- - Select Custody - -</option>
			  <option value="1" <?php if($custody == "1"){ ?> selected <?php } ?>>Mortgaged</option>
			  <option value="2" <?php if($custody == "2"){ ?> selected <?php } ?>>Not Mortgaged</option>
			  </select>
			  </span>	
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="ddl_bank">
			  <option value="">- - Select Bank - -</option>
			  <?php for($count = 0; $count < count($bank_list_data); $count++)
			  {?>
			  <option value="<?php echo $bank_list_data[$count]["crm_bank_master_id"]; ?>" <?php if($bank_list_data[$count]["crm_bank_master_id"] == $bank) { ?> selected="selected" <?php } ?>><?php echo $bank_list_data[$count]["crm_bank_name"]; ?></option>
			  <?php
			  }?>
			  </select>
			  </span>
			  <span style="padding-left:20px; padding-right:20px;">
			  <input type="text" name="stxt_survey_no" value="<?php echo $survey_no; ?>" placeholder="Search by survey number" />
			  </span>
			  <span style="padding-left:20px; padding-right:20px;">
			  <input type="date" name="dt_start_date" value="<?php echo $start_date; ?>" />
			  </span>
			  <span style="padding-left:20px; padding-right:20px;">
			  <input type="date" name="dt_end_date" value="<?php echo $end_date; ?>" />
			  </span>
			  <input type="submit" name="bd_file_search_submit" />
			  </form>			  
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
			<?php echo $msg; ?>
              <table class="table table-bordered" style="table-layout: fixed;">
                <thead>
                  <tr>				    				    
					<th style="word-wrap:break-word;">Survey No</th>					
					<th style="word-wrap:break-word;">Borrower</th>	
					<th style="word-wrap:break-word;">KNS Account</th>	
					<th style="word-wrap:break-word;">Village</th>						
					<th style="word-wrap:break-word;">Extent in Guntas</th>
					<th style="word-wrap:break-word;">Extent in Acres</th>
					<th style="word-wrap:break-word;">Extent in Sq.Ft</th>
					<th style="word-wrap:break-word;">Status</th>
					<th style="word-wrap:break-word;">Custody</th>								
					<th style="word-wrap:break-word;">Bank</th>					
					<th style="word-wrap:break-word;">Mortgage Date</th>
					<th style="word-wrap:break-word;">Lead Time</th>					
					<th style="word-wrap:break-word;">Loan Value</th>	
					<th style="word-wrap:break-word;">Released Date</th>									
					<th style="word-wrap:break-word;">Paid Loan value</th>
					<th style="word-wrap:break-word;">&nbsp;</th>
					<th style="word-wrap:break-word;">&nbsp;</th>
					<th style="word-wrap:break-word;">&nbsp;</th>
				</tr>
				</thead>
				<tbody>
				<?php
				$total_extent     = 0;
				$file_count       = 0;
				$total_mort_value = 0;
				$total_paid_value = 0;
				$sl_no            = 0;
				if($bd_file_list["status"] == SUCCESS)
				{				
					for($count = 0; $count < count($bd_file_list_data); $count++)
					{	
						$dont_show   = false;
						$dont_delete = false;
						// Corresponding Borrow Details, if any
						$bd_file_borrow_list = i_get_bd_borrow_list('',$bd_file_list_data[$count]["bd_project_file_id"],'','','','','','','','');
							
						if($bd_file_borrow_list["status"] == SUCCESS)
						{
							$bd_file_borrow_list_data   = $bd_file_borrow_list["data"];
							$borrow_id 				 	= $bd_file_borrow_list["data"][0]["bd_file_borrow_id"];														$borrower 				 	= $bd_file_borrow_list["data"][0]["bd_file_borrower"];
							if($bd_file_borrow_list["data"][0]["bd_file_borrow_custody"] == 1)
							{
								$custody_name  = "Mortgage";
								$mortgage_date = date('d-M-Y',strtotime($bd_file_borrow_list["data"][0]["bd_file_borrow_mortgage"]));
								if($bd_file_borrow_list["data"][0]["bd_file_borrow_released_date"] != '0000-00-00')
								{
									$released_date = date('d-M-Y',strtotime($bd_file_borrow_list["data"][0]["bd_file_borrow_released_date"]));
								}
								else
								{
									$released_date = '';
								}
								
								// Calculate lead time from mortgage
								$lead_time_data = get_date_diff($bd_file_borrow_list["data"][0]["bd_file_borrow_mortgage"],date('Y-m-d'));
								$lead_time = $lead_time_data['data'];
								
								$bank_name  = $bd_file_borrow_list["data"][0]["crm_bank_name"];								
								$loan_value	= $bd_file_borrow_list["data"][0]["bd_file_borrow_recieved_loan_value"];
								
								$payment_value = 0;
								$payment_request_data = array("borrow_id"=>$borrow_id );
								$borrow_payment_list = i_get_borrow_bd_payment_list($payment_request_data);
								if($borrow_payment_list["status"] == SUCCESS)
								{
									for($item_count = 0; $item_count < count($borrow_payment_list["data"]); $item_count++)
									{
										$payment_value = $payment_value + $borrow_payment_list["data"][$item_count]["bd_file_borrow_payment_amount"] ;
									}
								}
								else
								{
									$payment_value = '0';							
								}
								
								$dont_delete = true;
							}
							else
							{
								$custody_name  = "Not Mortgage";
								$mortgage_date = '';
								$released_date = '';
								$lead_time     = '';
								$bank_name     = '';
								$loan_value	   = '';
								$payment_value = '0';								
							}
							$file_id 					= $bd_file_borrow_list["data"][0]["bd_file_id"];							
							$active 					= $bd_file_borrow_list["data"][0]["bd_file_borrow_active"];
							$remarks 					= $bd_file_borrow_list["data"][0]["bd_file_borrow_remarks"];
							
							if(($bd_file_borrow_list["data"][0]["bd_file_borrow_custody"] != $custody) && ('' != $custody))
							{
								$dont_show = true;
							}
							
							if(($bd_file_borrow_list["data"][0]["bd_file_borrow_bank"] != $bank) && ('' != $bank))
							{
								$dont_show = true;
							}
						}
						else
						{
							$alert = $alert."Alert: ".$bd_file_borrow_list["data"];
							$borrow_id	   = "";
							$custody_name  = "Not Mortgage";
							$file_id       = "";
							$bank_name	   = "";
							$mortgage_date = "";
							$loan_value    = "";
							$released_date = "";
							$active		   = "";
							$remarks 	   = "";
							$lead_time     = "";
							$payment_value = '0';
							
							if(($custody != '') && ($custody != '2'))
							{
								$dont_show = true;
							}
						}

						if((((strtotime($mortgage_date)) >= (strtotime($start_date))) && ((strtotime($mortgage_date)) <= (strtotime($end_date)))) || (($start_date == '') && ($end_date == '')))
						{
							// Do nothing
						}
						else
						{
							$dont_show = true;
						}
						
						if($dont_show == false)
						{
							?>
							<tr <?php
							if($bd_file_list_data[$count]["bd_file_owner_status"] == '8')
							{
							?>						
							style="color:red;"
							<?php
							}
							?>>					
							<?php
							$total_extent = $total_extent + $bd_file_list_data[$count]["bd_file_extent"];
							$file_count++;
							$total_mort_value = $total_mort_value + $loan_value;
							$total_paid_value = $total_paid_value + $payment_value;							
							$sl_no = $sl_no + 1;																		
							?>						
								<td style="word-wrap:break-word;"><?php echo $bd_file_list_data[$count]["bd_file_survey_no"]; ?></td>						
								<td style="word-wrap:break-word;"><?php echo $borrower; ?></td>
								<td style="word-wrap:break-word;"><?php echo $bd_file_list_data[$count]["bd_own_account_master_account_name"]; ?></td>
								<td style="word-wrap:break-word;"><?php echo $bd_file_list_data[$count]["village_name"]; ?></td>
								<td style="word-wrap:break-word;"><?php echo $bd_file_list_data[$count]["bd_file_extent"]; ?></td>
								<td style="word-wrap:break-word;"><?php echo $extent_in_acres = (($bd_file_list_data[$count]["bd_file_extent"])/GUNTAS_PER_ACRE); ?></td>
								<td style="word-wrap:break-word;"><?php echo get_formatted_amount(($extent_in_acres * SQUARE_FOOT_PER_ACRE),'INDIA') ; ?></td>
								<td style="word-wrap:break-word;"><?php echo $bd_file_list_data[$count]["bd_file_owner_status_name"]; ?></td>
								<td style="word-wrap:break-word;"><?php echo $custody_name; ?></td>
								<td style="word-wrap:break-word;"><?php echo $bank_name; ?></td>
								<td style="word-wrap:break-word;"><?php echo $mortgage_date; ?></td>
								<td style="word-wrap:break-word;"><?php echo $lead_time; ?></td>
								<td style="word-wrap:break-word;"><?php echo $loan_value; ?></td>
								<td style="word-wrap:break-word;"><?php echo $released_date; ?></td>						
								<td style="word-wrap:break-word;"><?php echo $payment_value ;?></td>
								<td><a href="#" onclick="return go_to_add_file_borrow('<?php echo $bd_file_list_data[$count]["bd_project_file_id"]; ?>');">Borrow Details</a></td> 
								<td><?php if($dont_delete == false){ ?><a href="#" onclick="return change_land_bank_status('<?php echo $bd_file_list_data[$count]["bd_land_bank_id"]; ?>','0');">Delete</a><?php } ?></td>
								<td><?php if($dont_delete == false){ ?><a href="#" onclick="return change_land_bank_status('<?php echo $bd_file_list_data[$count]["bd_land_bank_id"]; ?>','2');">Close for Mortgage</a><?php } ?></td>
							</tr>
							<?php
						}					
					}
				}
				else
				{
					?>
					<td colspan="17">No Files added yet!</td>
					<?php					
				}
				 ?>	
				 <script>
				 document.getElementById('total_extent_span').innerHTML = <?php echo $total_extent; ?>;
				 document.getElementById('total_extent_span_acres').innerHTML = <?php echo ($total_extent/GUNTAS_PER_ACRE); ?>;
				 document.getElementById('total_file_count').innerHTML = <?php echo $file_count; ?>;
				 document.getElementById('total_mort_value').innerHTML = <?php echo $total_mort_value; ?>;
				 document.getElementById('total_paid_value').innerHTML = <?php echo $total_paid_value; ?>;
				 document.getElementById('total_balance_value').innerHTML = <?php echo ($total_mort_value - $total_paid_value); ?>;
				 </script>				 
                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>

<script>
function confirm_deletion(bd_file)
{
	var ok = confirm("Are you sure you want to delete?")
	{         
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					 window.location = "bd_file_list.php";
				}
			}

			xmlhttp.open("GET", "bd_file_delete.php?file=" + bd_file);   // file name where delete code is written
			xmlhttp.send();
		}
	}	
}
function go_to_add_file_borrow(file)
{		
	var form = document.createElement("form");
    form.setAttribute("method", "post");
    form.setAttribute("action", "bd_file_add_borrow_details.php");
	
	var hiddenField2 = document.createElement("input");
	hiddenField2.setAttribute("type","hidden");
	hiddenField2.setAttribute("name","file_id");	
	hiddenField2.setAttribute("value",file);
    	
	form.appendChild(hiddenField2);	
	
	document.body.appendChild(form);
    form.submit();
}

function change_land_bank_status(lb_id,action)
{
	if(action == '0')
	{
		action_message = 'delete';
	}
	else
	{
		action_message = 'proceed';
	}
	
	var ok = confirm("Are you sure you want to " + action_message + "?")
	{         
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{				
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{					
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else					
					{
					 window.location = "bd_file_borrow_list.php";
					}
				}
			}

			xmlhttp.open("POST", "bd_land_bank_delete.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("land_bank_id=" + lb_id + "&action=" + action);
		}
	}	
}
</script>

</body>

</html>