<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: crm_site_visit_completed_list.php
CREATED ON	: 03-Oct-2015
CREATED BY	: Nitin Kashyap
PURPOSE     : List of Enquiries with site visit completed
*/
define('CRM_SITE_VISIT_COMPLETED_LIST_FUNC_ID','151');
/*
TBD: 
*/$_SESSION['module'] = 'CRM Transactions';

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_masters'.DIRECTORY_SEPARATOR.'crm_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_transaction_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];		// Get permission settings for this user for this page	$add_perms_list     = i_get_user_perms($user,'',CRM_SITE_VISIT_COMPLETED_LIST_FUNC_ID,'1','1');	$view_perms_list    = i_get_user_perms($user,'',CRM_SITE_VISIT_COMPLETED_LIST_FUNC_ID,'2','1');	$edit_perms_list    = i_get_user_perms($user,'',CRM_SITE_VISIT_COMPLETED_LIST_FUNC_ID,'3','1');	$delete_perms_list  = i_get_user_perms($user,'',CRM_SITE_VISIT_COMPLETED_LIST_FUNC_ID,'4','1');
	// Query String / Get form Data
	// Nothing here
	
	if(isset($_POST["svp_complete_search_submit"]))
	{		
		$assignee   = $_POST["ddl_search_user"];
		$int_status = $_POST["ddl_interest_status"];
		$start_date = $_POST["dt_start_date"];
		$end_date   = $_POST["dt_end_date"];
		
		if(isset($_POST["cb_unqualifiable"]))
		{
			$unqualifiable = 1;
		}
		else
		{
			$unqualifiable = 0;
		}
	}
	else
	{
		if(($role == 1) || ($role == 5))
		{
			$assignee = "";
		}
		else
		{
			$assignee = $user;
		}
		$int_status    = "";
		$start_date    = "";
		$end_date      = 
		$unqualifiable = 0;
	}
	
	$site_visit_completed_list = i_get_site_visit_completed_enquiries('',$start_date,$end_date,$user);
	if($site_visit_completed_list["status"] == SUCCESS)
	{
		$site_visit_completed_list_data = $site_visit_completed_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$site_visit_completed_list["data"];
	}
	
	// User List
	$user_list = i_get_user_list('','','','','1');
	if($user_list["status"] == SUCCESS)
	{
		$user_list_data = $user_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$user_list["data"];
		$alert_type = 0; // Failure
	}
	
	// Interest Status List
	$int_status_list = i_get_interest_status_list('','1');
	if($int_status_list["status"] == SUCCESS)
	{
		$int_status_list_data = $int_status_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$int_status_list["data"];
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Site Visit Completed List</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Site Visit Completed List (Total Count = <span id="total_count_section">
			  <?php
			  if($site_visit_completed_list["status"] == SUCCESS)
			  {
				$preload_count = "<i>Calculating</i>";			
			  }
			  else
			  {
			    $preload_count = 0;
			  }
			  
			  echo $preload_count;
			  ?></span>)</h3>
            </div>
			
			<div class="widget-header" style="height:80px; padding-top:10px;">
			<?php if(($role == 1) || ($role == 5))
			{?>
			  <form method="post" id="svp_complete_search" action="crm_site_visit_completed_list.php">			  			  
			  <span style="padding-left:8px; padding-right:8px;">
			  <select name="ddl_search_user">
			  <option value="">- - Select STM - -</option>
			  <?php
				for($count = 0; $count < count($user_list_data); $count++)
				{
					if(($user_list_data[$count]["user_role"] == "1") || ($user_list_data[$count]["user_role"] == "5") || ($user_list_data[$count]["user_role"] == "6") || ($user_list_data[$count]["user_role"] == "7") || ($user_list_data[$count]["user_role"] == "8") || ($user_list_data[$count]["user_role"] == "9")) 
					{
					?>
					<option value="<?php echo $user_list_data[$count]["user_id"]; ?>" <?php 
					if($assignee == $user_list_data[$count]["user_id"])
					{
					?>												
					selected="selected"
					<?php
					}?>><?php echo $user_list_data[$count]["user_name"]; ?></option>								
					<?php
					}
				}
      		  ?>
			  </select>
			  </span>
			  <span style="padding-left:8px; padding-right:8px;">
			  <select name="ddl_interest_status">
			  <option value="">- - Select Interest Status - -</option>
			  <?php
				for($count = 0; $count < count($int_status_list_data); $count++)
				{
					?>
					<option value="<?php echo $int_status_list_data[$count]["crm_cust_interest_status_id"]; ?>" <?php 
					if($int_status == $int_status_list_data[$count]["crm_cust_interest_status_id"])
					{
					?>						
					selected="selected"
					<?php
					}?>><?php echo $int_status_list_data[$count]["crm_cust_interest_status_name"]; ?></option>								
					<?php					
				}
      		  ?>
			  </select>
			  </span>
			  <span style="padding-left:8px; padding-right:8px;">
			  <input type="date" name="dt_start_date" value="<?php echo $start_date; ?>" />
			  </span>
			  <span style="padding-left:8px; padding-right:8px;">
			  <input type="date" name="dt_end_date" value="<?php echo $end_date; ?>" />
			  </span>
			  <span style="padding-left:8px; padding-right:8px;">
			  Include unqualified&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="cb_unqualifiable" <?php if($unqualifiable == 1){ ?> checked <?php } ?>/>
			  </span>
			  <span style="padding-left:8px; padding-right:8px;">
			  <input type="submit" name="svp_complete_search_submit" />
			  </span>
			  </form>
			<?php
			}?>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
			
              <table class="table table-bordered" style="table-layout: fixed;">
                <thead>
                  <tr>
					<th>SL No</th>					
					<th>Enquiry No</th>					
					<th>Project</th>					
					<th>Name</th>
					<th>Mobile</th>
					<th>Email</th>
					<th>Company</th>
					<th>Source</th>
					<th>Walk In</th>
					<th>Latest Status</th>
					<th>Enquiry Date</th>
					<th>Follow Up Count</th>
					<th>Next FollowUp</th>					
					<th>Site Visit Count</th>
					<th>Last Site Visit</th>
					<th>Assignee</th>					
					<th>&nbsp;</th>
					<th>&nbsp;</th>
					<th>&nbsp;</th>
					<th>&nbsp;</th>
					<?php if(($role == 1) || ($role == 5))
					{?>
					<th>&nbsp;</th>
					<?php
					}?>
				</tr>
				</thead>
				<tbody>							
				<?php
				if($site_visit_completed_list["status"] == SUCCESS)
				{
					$sl_no                 		  = 0;
					$unq_count             		  = 0;
					$user_filter_out_count  	  = 0;
					$int_filter_out_count  		  = 0;
					$unqualifiable_count          = 0;
					$prospective_filter_out_count = 0;
					for($count = 0; $count < count($site_visit_completed_list_data); $count++)
					{						
						$enquiry_list = i_get_enquiry_list($site_visit_completed_list_data[$count]["enquiry_id"],'','','','','','','','','','','','','','','','','','','');
						$enquiry_list_data = $enquiry_list["data"];
						
						$latest_follow_up_data = i_get_enquiry_fup_list($site_visit_completed_list_data[$count]["enquiry_id"],'','','added_date_desc','0','1');						
						$follow_up_data = i_get_enquiry_fup_list($site_visit_completed_list_data[$count]["enquiry_id"],'','','','','');
						if($follow_up_data["status"] == SUCCESS)
						{
							$fup_count = count($follow_up_data["data"]);
						}
						else
						{
							$fup_count = 0;
						}
						
						// Site Visit Data
						$site_visit_data = i_get_site_travel_plan_list('','',$site_visit_completed_list_data[$count]["enquiry_id"],'','','','2','','');
						
						if($site_visit_data["status"] == SUCCESS)
						{
							$sv_count = count($site_visit_data["data"]);
						}
						else
						{
							$sv_count = 0;
						}
						
						// Is the enquiry unqualified
						$unqualified_data = i_get_unqualified_leads('',$site_visit_completed_list_data[$count]["enquiry_id"],'','','','','');
												
						if($unqualified_data["status"] != SUCCESS)
						{
						    // Is the enquiry moved to prospective client
							$prospective_list = i_get_prospective_clients('',$site_visit_completed_list_data[$count]["enquiry_id"],'','','');
							
							if($prospective_list["status"] != SUCCESS)
							{
								if(($assignee == "") || ($enquiry_list_data[0]["assigned_to"] == $assignee))
								{
									if(($int_status == "") || ($int_status == $latest_follow_up_data["data"][0]["crm_cust_interest_status_id"]))
									{
										if((($unqualifiable == 0) && ($latest_follow_up_data["data"][0]["crm_cust_interest_status_id"] != "4")) || ($unqualifiable == 1))
										{
									$sl_no++;
							?>
							<tr>
							<td style="word-wrap:break-word;"><?php echo $sl_no; ?></td>
							<td style="word-wrap:break-word;"><?php echo $enquiry_list_data[0]["enquiry_number"]; ?></td>					
							<td style="word-wrap:break-word;"><?php echo $enquiry_list_data[0]["project_name"]; ?>								
							<td style="word-wrap:break-word;"><?php echo $enquiry_list_data[0]["name"]; ?></td>
							<td style="word-wrap:break-word;"><?php echo $enquiry_list_data[0]["cell"]; ?></td>
							<td style="word-wrap:break-word;"><?php echo $enquiry_list_data[0]["email"]; ?></td>
							<td style="word-wrap:break-word;"><?php echo $enquiry_list_data[0]["company"]; ?></td>
							<td style="word-wrap:break-word;"><?php echo $enquiry_list_data[0]["enquiry_source_master_name"]; ?></td>
							<td style="word-wrap:break-word;"><?php if($enquiry_list_data[0]["walk_in"] == "1")
							{
								echo "Yes";
							}
							else
							{
								echo "No";
							}?></td>
							<td style="word-wrap:break-word;"><?php echo $latest_follow_up_data["data"][0]["crm_cust_interest_status_name"];
							if($latest_follow_up_data["data"][0]["crm_cust_interest_status_id"] == "5")
							{?>
							<br />
							<br />
							<a href="crm_add_prospective_profile.php?enquiry=<?php echo $enquiry_list_data[0]["enquiry_id"]; ?>">Add Prospective Client Profile</a>
							<?php
							}?></td>
							<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($enquiry_list_data[0]["added_on"])); ?></td>
							<td style="word-wrap:break-word;"><?php echo $fup_count; ?></td>
							<td style="word-wrap:break-word;"><?php echo date("d-M-Y H:i",strtotime($latest_follow_up_data["data"][0]["enquiry_follow_up_date"])); ?></td>
							<td style="word-wrap:break-word;"><?php echo $sv_count; ?></td>
							<td style="word-wrap:break-word;"><?php echo date("d-M-Y H:i",strtotime($site_visit_data["data"][0]["crm_site_travel_plan_date"])); ?></td>
							<td style="word-wrap:break-word;"><?php echo $enquiry_list_data[0]["user_name"]; ?></td>					
							<td style="word-wrap:break-word;"><a href="crm_enquiry_fup_list.php?enquiry=<?php echo $site_visit_completed_list_data[$count]["enquiry_id"]; ?>">Follow Up</a></td>
							<td style="word-wrap:break-word;"><a href="crm_add_site_visit_plan.php?enquiry=<?php echo $site_visit_completed_list_data[$count]["enquiry_id"]; ?>">Site Visit Plan</a><br /><br /><a href="crm_site_visit_plan_list.php?enquiry=<?php echo $site_visit_completed_list_data[$count]["enquiry_id"]; ?>">Site Visit Plan List</a></td>
							<td style="word-wrap:break-word;"><a href="crm_assign_enquiry.php?enquiry=<?php echo $site_visit_completed_list_data[$count]["enquiry_id"]; ?>">Assign</a></td>
							<td style="word-wrap:break-word;"><a href="crm_add_site_travel_actual.php?enquiry=<?php echo $site_visit_completed_list_data[$count]["enquiry_id"]; ?>">Site Visit Actual</a><br /><br /><a href="crm_site_travel_actual_list.php?enquiry=<?php echo $site_visit_completed_list_data[$count]["enquiry_id"]; ?>">Actual Site Visit List</a></td>
							<?php if(($role == 1) || ($role == 5))
							{?>
							<td style="word-wrap:break-word;"><a href="crm_add_unqualified_enquiry.php?enquiry=<?php echo $site_visit_completed_list_data[$count]["enquiry_id"]; ?>">Mark as Unqualified</a></td>
							<?php
								
							}?>
							</tr>
								<?php
									}
									else
									{
										$unqualifiable_count++;
									}
								}
								else
								{
									$int_filter_out_count++;
								}
							}
							else
							{
								$user_filter_out_count++;
							}
						}
						else
						{
							$prospective_filter_out_count++;
						}
					}
					else
					{
						$unq_count++;
					}
					}
				}
				else
				{
				?>
				<tr><td colspan="14">No site visits completed yet!</td></tr>
				<?php
				}	
				if($site_visit_completed_list["status"] == SUCCESS)
				{
					$final_count = count($site_visit_completed_list_data) - $unq_count - $user_filter_out_count - $int_filter_out_count - $unqualifiable_count - $prospective_filter_out_count;			
				}
				else
				{
					$final_count = 0;
				}
				 ?>	
				<script>
				document.getElementById("total_count_section").innerHTML = <?php echo $final_count; ?>;
				</script>
                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>/* Open the sidenav */function openNav() {    document.getElementById("mySidenav").style.width = "75%";}/* Close/hide the sidenav */function closeNav() {    document.getElementById("mySidenav").style.width = "0";}</script>

  </body>

</html>