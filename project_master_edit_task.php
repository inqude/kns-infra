<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 08-Nov-2016
// LAST UPDATED BY: Lakshmi
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');

if ((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != "")) {
    // Session Data
    $user 		   = $_SESSION["loggedin_user"];
    $role 		   = $_SESSION["loggedin_role"];
    $loggedin_name = $_SESSION["loggedin_user_name"];

    /* DATA INITIALIZATION - START */
    $alert_type = -1;
    $alert = "";
    /* DATA INITIALIZATION - END */

    if (isset($_REQUEST["task_master_id"])) {
        $task_master_id = $_REQUEST["task_master_id"];
    } else {
        $task_master_id = "";
    }

    if (isset($_REQUEST["search_process"])) {
        $search_process = $_REQUEST["search_process"];
    } else {
        $search_process = "";
    }
    // Capture the form data
    if (isset($_POST["edit_project_task_master_submit"])) {
        $task_master_id     = $_POST["hd_task_master_id"];
        $search_process     = $_POST["hd_search_process"];
        $name               = $_POST["name"];
        $uom                = $_POST["ddl_unit"];
        $process            = $_POST["ddl_process"];
        $remarks 	        = $_POST["txt_remarks"];

        // Check for mandatory fields
        if (($name != "") && ($process != "")) {
            $project_task_master_update_data = array("name"=>$name,"uom"=>$uom,"process"=>$process,"remarks"=>$remarks);
            $project_task_master_iresult = i_update_project_task_master($task_master_id, $name, $process, $project_task_master_update_data);

            if ($project_task_master_iresult["status"] == SUCCESS) {
                $project_object_output_update_data = array("uom"=>$uom);
                $object_output_uresults = db_update_object_uom($process, $task_master_id, $project_object_output_update_data);
                if ($object_output_uresults["status"] == SUCCESS) {
                    echo "Success";
                } else {
                    //
                }
                $alert_type = 1;

                header("location:project_master_task_list.php?search_process=$search_process");
            } else {
                $alert = "Project Already Exists";
                $alert_type = 0;
            }

            $alert = $project_task_master_iresult["data"];
        } else {
            $alert = "Please fill all the mandatory fields";
        }
    }


    // Get Project Process Master modes already added
    $project_process_master_search_data = array();
    $project_process_master_list = i_get_project_process_master($project_process_master_search_data);
    if ($project_process_master_list['status'] == SUCCESS) {
        $project_process_master_list_data = $project_process_master_list["data"];
    } else {
        $alert = $project_process_master_list["data"];
        $alert_type = 0;
    }


    // Get Stock master uom modes already added
    $project_uom_master_search_data = array("active"=>'1');
    $stock_unit_measure_list = i_get_project_uom_master($project_uom_master_search_data);
    if ($stock_unit_measure_list['status'] == SUCCESS) {
        $stock_unit_measure_list_data = $stock_unit_measure_list['data'];
    } else {
        $alert = $stock_unit_measure_list["data"];
        $alert_type = 0;
    }

    // Get Project Task Master modes already added
    $project_task_master_search_data = array("task_master_id"=>$task_master_id);
    $project_task_master_list = i_get_project_task_master($project_task_master_search_data);
    if ($project_task_master_list['status'] == SUCCESS) {
        $project_task_master_list_data = $project_task_master_list['data'];
    }
} else {
    header("location:login.php");
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Project Master Edit Task</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">

    <link href="css/style.css" rel="stylesheet">



    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">

	<div class="main-inner">

	    <div class="container">

	      <div class="row">

	      	<div class="span12">

	      		<div class="widget ">

	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Project Master Edit Task</h3>
	  				</div> <!-- /widget-header -->

					<div class="widget-content">



						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Project Master Edit Task</a>
						  </li>
						</ul>
						<br>
							<div class="control-group">
								<div class="controls">
								<?php
                                if ($alert_type == 0) { // Failure ?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
                                }
                                ?>

								<?php
                                if ($alert_type == 1) { // Success
                                ?>
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
                                }
                                ?>
								</div> <!-- /controls -->
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="project_master_edit_task_form" class="form-horizontal" method="post" action="project_master_edit_task.php">
								<input type="hidden" name="hd_task_master_id" value="<?php echo $task_master_id; ?>" />
								<input type="hidden" name="hd_search_process" id="hd_search_process" value="<?php echo $search_process; ?>" />
									<fieldset>

											<div class="control-group">
											<label class="control-label" for="name">Task*</label>
											<div class="controls">
												<input type="text" class="span6" name="name" placeholder="Name" value="<?php echo $project_task_master_list_data[0]["project_task_master_name"] ;?>" required="required">
											</div> <!-- /controls -->
										</div> <!-- /control-group -->

										<div class="control-group">
											<label class="control-label" for="ddl_unit">UOM*</label>
											<div class="controls">
												<select name="ddl_unit" required>
												<option value="">- - Select UOM - -</option>
												<?php
                                                for ($count = 0; $count < count($stock_unit_measure_list_data); $count++) {
                                                    ?>
												<option value="<?php echo $stock_unit_measure_list_data[$count]["project_uom_id"]; ?>" <?php if ($stock_unit_measure_list_data[$count]["project_uom_id"] == $project_task_master_list_data[0]["project_task_master_uom"]) {
                                                        ?> selected="selected" <?php
                                                    } ?>><?php echo $stock_unit_measure_list_data[$count]["project_uom_name"]; ?></option>
												<?php
                                                }
                                                ?>
												</select>
											</div> <!-- /controls -->
											</div> <!-- /control-group -->


											<div class="control-group">
											<label class="control-label" for="ddl_process">Process*</label>
											<div class="controls">
												<select name="ddl_process" required>
												<option>- -Select Process- -</option>
												<?php
                                                for ($count = 0; $count < count($project_process_master_list_data); $count++) {
                                                    ?>
												<option value="<?php echo $project_process_master_list_data[$count]["project_process_master_id"]; ?>" <?php if ($project_process_master_list_data[$count]["project_process_master_id"] == $project_task_master_list_data[0]["project_task_master_process"]) {
                                                        ?> selected="selected" <?php
                                                    } ?>><?php echo $project_process_master_list_data[$count]["project_process_master_name"]; ?></option>
												<?php
                                                }
                                                ?>
												</select>
											</div> <!-- /controls -->
										</div> <!-- /control-group -->


										<div class="control-group">
											<label class="control-label" for="txt_remarks">Remarks</label>
											<div class="controls">
												<input type="text" class="span6" name="txt_remarks" placeholder="Remarks" value="<?php echo $project_task_master_list_data[0]["project_task_master_remarks"] ;?>">
											</div> <!-- /controls -->
										</div> <!-- /control-group -->
                                                                                                                                                               										 <br />


										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="edit_project_task_master_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>

							</div>

					</div> <!-- /widget-content -->

				</div> <!-- /widget -->

		    </div> <!-- /span8 -->




	      </div> <!-- /row -->

	    </div> <!-- /container -->

	</div> <!-- /main-inner -->

</div> <!-- /main -->




<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">

                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->




<div class="footer">

	<div class="footer-inner">

		<div class="container">

			<div class="row">

    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->

    		</div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /footer-inner -->

</div> <!-- /footer -->



<script src="js/jquery-1.7.2.min.js"></script>

<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
