<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 5th Aug 2015
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

/* TBD - START */
// 
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'files'.DIRECTORY_SEPARATOR.'file_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_projects'.DIRECTORY_SEPARATOR.'bd_payment_functions.php');


/* INCLUDES - END */
if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert = "";
	$alert_type = -1; // No alert
	/* DATA INITIALIZATION - END */
	
	/* QUERY STRING - START */
	if(isset($_GET["file"]))
	{
		$file_id = $_GET["file"];
	}	
	else
	{
		$file_id = "";
	}
	
	if(isset($_GET["request"]))
	{
		$request_id = $_GET["request"];
	}	
	else
	{
		$request_id = "";
	}
	/* QUERY STRING - END */

	// Capture the form data
	if(isset($_POST["add_file_payment_submit"]))
	{
		$request_id                = $_POST["hd_request_id"];
		$file_id                   = $_POST["file_id"];
		$file_payment_type         = $_POST["ddl_pay_type"];
		$file_payment_payment_mode = $_POST["ddl_pay_mode"];
		$payment_date			   = $_POST["date_pay_date"];
		$file_payment_cheque_dd_no = $_POST["stxt_cheque_dd_no"];
		$file_payment_bank_name    = $_POST["ddl_bank_name"];
		$amount					   = $_POST["num_amount"];
		$done_to                   = $_POST["stxt_done_to"];
		$remarks                   = $_POST["txt_remarks"];
		$added_by                  = $user;
		
		// Check for mandatory fields
		if(($file_id !="") && ($payment_date !="") && ($amount !=""))
		{
			$add_file_pay_result = i_add_file_payment($request_id,$file_id,$file_payment_type,$file_payment_payment_mode,$file_payment_cheque_dd_no,$file_payment_bank_name,$payment_date,$amount,$done_to,$remarks,$added_by);
			if($add_file_pay_result["status"] == SUCCESS)
			{
				$alert = $add_file_pay_result["data"];
				$alert_type = 1;
				
				$bd_payment_request_data = array("status"=>'4');
				$pay_request_update_result = i_update_bd_payment($request_id,$bd_payment_request_data);
				
				if($pay_request_update_result['status'] == SUCCESS)
				{
					// Do nothing
				}
				else
				{
					$alert      = 'Payment was successfully added but request details not updated';
					$alert_type = 1;
				}
			}
			else
			{
				$alert = $add_file_pay_result["data"];
				$alert_type = 0;
			}	
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}
}
else
{
	header("location:login.php");
}	

//Get Bank Name
$bank_name_list = i_get_bank_list('','');
if($bank_name_list["status"] == SUCCESS)
{
	$bank_name_list_data = $bank_name_list["data"];
}
else
{
	$alert = $alert."Alert: ".$bank_name_list["data"];
	$alert_type = 0; // Failure
}

//Get Payment Mode
$payment_mode_list = i_get_payment_mode_list('','');
if($payment_mode_list["status"] == SUCCESS)
{
	$payment_mode_list_data = $payment_mode_list["data"];
}
else
{
	$alert = $alert."Alert: ".$payment_mode_list["data"];
	$alert_type = 0; // Failure
}
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Add File Payment</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>  
    

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Add File</h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Add File Payment</a>
						  </li>						  
						</ul>
						
						<br>
						    <div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="add_file_form" class="form-horizontal" method="post" action="add_file_payment.php">
									<fieldset>
										<input type="hidden" name="file_id" value="<?php echo $file_id; ?>" />
										<input type="hidden" name="hd_request_id" value="<?php echo $request_id; ?>" />
										
										<div class="control-group">											
											<label class="control-label" for="ddl_pay_type">Payment Type *</label>
											<div class="controls">
												<select name="ddl_pay_type" class="span6" required>
												<option value="1">Payment</option>
												<option value="2">Change of Land use Fee</option>
												<option value="3">Conversion Fee</option>
												<option value="4">Brokerage Amount</option>
												<option value="5">Stamp Duty</option>
												<option value="6">Registration Fee</option>
												<option value="7">Other Expenses</option>																				
												</select>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
															
										<div class="control-group">											
											<label class="control-label" for="ddl_pay_mode">Payment Mode*</label>
											<div class="controls">
												<select name="ddl_pay_mode" class="span6" required>
												<?php 
												for($count = 0; $count < count($payment_mode_list_data); $count++)
												{
												?>
												<option value="<?php echo $payment_mode_list_data[$count]["payment_mode_id"]; ?>" <?php 
												if($payment_mode_list_data[$count]["payment_mode_id"] == 3)
												{
												?>
												
												selected="selected"
												<?php
												}?>><?php echo $payment_mode_list_data[$count]["payment_mode_name"]; ?></option>												
												<?php
												}
												?>
												</select>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
									   <div class="control-group">																					
											<label class="control-label" for="date_pay_date">Date *</label>
											<div class="controls">
												<input type="date" class="span6" name="date_pay_date" required="required">
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
												
										<div class="control-group">											
											<label class="control-label" for="stxt_cheque_dd_no">Cheque/DD No*</label>
											<div class="controls">
												<input type="text" class="span6" name="stxt_cheque_dd_no" required="required">
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										
											<div class="control-group">											
											<label class="control-label" for="ddl_bank_name">Bank Name*</label>
											<div class="controls">
												<select name="ddl_bank_name" class="span6" required>
												<?php 
												for($count = 0; $count < count($bank_name_list_data); $count++)
												{
												?>
												<option value="<?php echo $bank_name_list_data[$count]["crm_bank_master_id"]; ?>" <?php 
												if($bank_name_list_data[$count]["crm_bank_master_id"] == 3)
												{
												?>
												
												selected="selected"
												<?php
												}?>><?php echo $bank_name_list_data[$count]["crm_bank_name"]; ?></option>												
												<?php
												}
												?>


													
												</select>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
																																												
										<div class="control-group">											
											<label class="control-label" for="num_amount">Amount *</label>
											<div class="controls">
												<input type="number" step="0.01" min="0" class="span6" name="num_amount" required="required">
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="stxt_done_to">Done To *</label>
											<div class="controls">
												<input type="text" class="span6" name="stxt_done_to" required="required">
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->

										<div class="control-group">											
											<label class="control-label" for="txt_remarks">Remarks</label>
											<div class="controls">
												<textarea name="txt_remarks"></textarea>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->										
                                                                                                                                                               										 <br />
										
											
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="add_file_payment_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>																
								
							</div>
						  
						  
						</div>
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>