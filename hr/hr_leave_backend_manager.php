<?php
//ini_set('memory_limit',$ '-1');
/**
 * @author Perumal
 * @copyright 2015
 */

$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'status_codes.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'hr'.DIRECTORY_SEPARATOR.'hr_attendance_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'hr'.DIRECTORY_SEPARATOR.'hr_employee_functions.php');

// Loop on active employees
$employee_filter_data = array("employee_status"=>'1');
$employee_sresult = i_get_employee_list($employee_filter_data);

for($count = 0; $count < count($employee_sresult['data']); $count++)
{
	$employee = $employee_sresult['data'][$count]['hr_employee_id'];
	
	// Get EL value
	$el_filter_data = array("employee_id"=>$employee,"year"=>"2017");
	$el_sresult = db_get_el_list($el_filter_data);
	if($el_sresult["status"] == DB_RECORD_ALREADY_EXISTS)
	{
		$total_leaves = $el_sresult["data"][0]["hr_earned_leave_count"];
	}
	else
	{
		$total_leaves = 0;
	}		
	
	$leave_filter_data = array("employee_id"=>$employee,"start_date"=>"2017-01-01","end_date"=>"2017-12-31","status"=>'1',"type"=>LEAVE_TYPE_EARNED,"duration"=>ATTENDANCE_TYPE_PRESENT);
	$leave_sresult = db_get_absence_list($leave_filter_data);
	if($leave_sresult["status"] == DB_RECORD_ALREADY_EXISTS)
	{
		$taken_leaves_full = count($leave_sresult["data"]);
	}
	else
	{
		$taken_leaves_full = 0;
	}
	
	$leave_filter_data = array("employee_id"=>$employee,"start_date"=>"2017-01-01","end_date"=>"2017-12-31","status"=>'1',"type"=>LEAVE_TYPE_EARNED,"duration"=>ATTENDANCE_TYPE_HALFDAY);
	$leave_sresult = db_get_absence_list($leave_filter_data);
	if($leave_sresult["status"] == DB_RECORD_ALREADY_EXISTS)
	{
		$taken_leaves_half = count($leave_sresult["data"]);
	}
	else
	{
		$taken_leaves_half = 0;
	}
	$taken_leaves = $taken_leaves_full + ($taken_leaves_half*0.5);
		
	$leaves_to_be_added = $total_leaves - $taken_leaves;

	// Update details	
	$result = t_update_cl($employee,$leaves_to_be_added,"2018");
	var_dump($result);
}