<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];
	if(isset($_GET['type']))
	{
		$vendor_type = $_GET['type'];
	}
	else
	{
		$vendor_type = "";
	}
	if($vendor_type=="Machine"){
		$project_machine_vendor_master_search_data = array("active"=>'1');
		$project_machine_vendor_list = i_get_project_machine_vendor_master_list($project_machine_vendor_master_search_data);
		if ($project_machine_vendor_list["status"] == SUCCESS) {
				$project_vendor_list_data = $project_machine_vendor_list["data"];
		}
	}
	 else {
		 $project_manpower_agency_search_data = array("active"=>'1');
		 $project_manpower_agency_list = i_get_project_manpower_agency($project_manpower_agency_search_data);
		 if ($project_manpower_agency_list['status'] == SUCCESS) {
		 		$project_vendor_list_data = $project_manpower_agency_list['data'];
		 }
	 }
	echo json_encode($project_vendor_list_data);
}
else
  echo "FAILURE"

 ?>
