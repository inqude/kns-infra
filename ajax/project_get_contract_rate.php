<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD:
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Update attendance details
	$contract_rate_id      = $_POST["contract_rate_id"];
	$project_contract_rate_master_search_data = array("contract_rate_id"=>$contract_rate_id,"active"=>'1');
	$project_contract_rate_master_list = i_get_project_contract_rate_max_master($project_contract_rate_master_search_data);
	if($project_contract_rate_master_list['status'] == SUCCESS)
	{
		$project_contract_rate_master_list_data = $project_contract_rate_master_list['data'];
		$contract_rate = $project_contract_rate_master_list_data[0]["max_contract_rate"];
	}
	else
	{
		$contract_rate = "0";
	}
	
	echo $contract_rate;
}
else
{
	header("location:login.php");
}
?>