<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD:
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Update attendance details
	$payment_id  = $_GET["payment_id"];
	$man_power_men_rate = 0;
	$man_power_women_rate = 0;
	$man_power_mason_rate = 0;

	$man_power_search_data = array("man_power_id"=>$payment_id);
	$actual_manpower_list = i_get_man_power_list($man_power_search_data);
	if ($actual_manpower_list["status"] == SUCCESS) {
			$man_power_men_rate = $actual_manpower_list["data"][0]["project_task_actual_manpower_men_rate"];
			$man_power_women_rate = $actual_manpower_list["data"][0]["project_task_actual_manpower_women_rate"];
			$man_power_mason_rate = $actual_manpower_list["data"][0]["project_task_actual_manpower_mason_rate"];
	}

	// Get machine payment mapping for project name
	$project_payment_manpower_mapping_search_data = array('payment_id'=>$payment_id);
	$project_name_sresult = i_get_project_payment_manpower_mapping($project_payment_manpower_mapping_search_data);
	if ($project_name_sresult['status'] == SUCCESS) {
			$project_name = $project_name_sresult['data'][0]['project_master_name'];
	} else {
			$project_name = 'NOT VALID';
	}
	$output = array("men_rate"=>$man_power_men_rate,"women_rate"=>$man_power_women_rate,"mason_rate"=>$man_power_mason_rate,"project"=>$project_name);
  echo json_encode($output);
}
else
{
	header("location:login.php");
}
?>
