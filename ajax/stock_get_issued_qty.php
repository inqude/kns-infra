<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD:
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Update attendance details
	$indent_id      = $_GET["indent_id"];
	$material_id    = $_GET["material_id"];

	$issued_qty    = 0;
	$stock_issue_item_search_data = array("indent_id" => $indent_id,"material_id" => $material_id);
	$stock_issue_item_list        = i_get_stock_issue_item($stock_issue_item_search_data);
	if ($stock_issue_item_list["status"] == SUCCESS) {
			$issued_qty = $stock_issue_item_list["data"][0]["total_issued_item_quantity"];
	} else {
			$issued_qty = 0;
			}
	echo $issued_qty ;
}
else
{
	header("location:login.php");
}
?>
