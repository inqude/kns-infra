<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD:
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Update attendance details
	$project_id  = $_POST["project_id"];
	$active      = $_POST["action"];
	
	$project_update_data = array("active"=>$active);
	$delete_project_result = i_delete_stock_project($project_id,$project_update_data);
	
	if($delete_project_result["status"] == FAILURE)
	{
		echo $delete_project_result["data"];
	}
	else
	{
		echo "SUCCESS";
	}
}
else
{
	header("location:login.php");
}
?>