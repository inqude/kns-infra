<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD:
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'da_project_management.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Update attendance details
	$amount  = $_GET["total_amount"];
	$payment_id  = $_GET["payment_id"];
	// Get machine payment mapping for project name

	$project_payment_contract_mapping_search_data = array("deposit_status"=>"Bill Generated",
	"deposit_accepted_by"=>$user,"deposit_accepted_on"=>date("Y-m-d"),"deposit_bill_no"=>$_GET["bill_no"]);
	$project_name_sresult = db_update_project_actual_contract_payment($payment_id,$project_payment_contract_mapping_search_data);
	if($project_name_sresult['status'] == SUCCESS)
	{
    $status = 'SUCCESS';
	}
	else
	{
		$status = 'FAILURE';
	}
	$output = array("status"=>$status);
  echo json_encode($output);
}
else
{
	header("location:login.php");
}
?>
