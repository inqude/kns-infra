<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD:
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Update attendance details
	if(isset($_POST['machine_id']))
	{
		$machine_id      = $_POST["machine_id"];
	}
	else
	{
		$machine_id      = '-1';
	}
	$kns_fuel_charge = "";
	$vendor_fuel_charge = "";
	$bata = "";
	
	//get stock quantity
	$project_machine_rate_master_search_data = array("machine_id"=>$machine_id,"active"=>'1');
	$project_machine_rate_master_data =  i_get_project_machine_rate_master($project_machine_rate_master_search_data);
	
	if($project_machine_rate_master_data["status"] == SUCCESS)
	{
		$project_machine_master_search_data = array("machine_id"=>$machine_id);
		$project_machine_master_data =  i_get_project_machine_master($project_machine_master_search_data);
		if($project_machine_master_data["status"] == SUCCESS)
		{
			$kns_fuel_charge = $project_machine_rate_master_data["data"][0]["project_machine_kns_fuel"];
			$vendor_fuel_charge = $project_machine_rate_master_data["data"][0]["project_machine_vendor_fuel"];
			$bata = $project_machine_rate_master_data["data"][0]["project_machine_kns_bata"];			
		}
	}
	$result = array("kns_fuel_charge"=>$kns_fuel_charge,"bata"=>$bata,"vendor_fuel_charge"=>$vendor_fuel_charge);
	
	echo json_encode($result);
}
else
{
	header("location:login.php");
}
?>