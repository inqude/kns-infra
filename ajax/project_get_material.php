<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD:
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Update attendance details
	$project_id      = $_GET["project_id"];

	$project_stock_module_mapping_search_data = array("mgmnt_project_id"=>$project_id);
	$project_stock_mapping_list = i_get_project_stock_module_mapping($project_stock_module_mapping_search_data);
	if ($project_stock_mapping_list["status"] == SUCCESS) {
	$stock_project = $project_stock_mapping_list["data"][0]["project_stock_module_mapping_stock_project_id"];
	} else {
	$stock_project = "";
	}
	$total_actual_material_cost = 0 ;
	$project_budget_material_search_data = array("project_id"=>$stock_project);
	$budget_material_list = db_get_project_budget_material($project_budget_material_search_data);
	if($budget_material_list["status"] == DB_RECORD_ALREADY_EXISTS)
	{
			for($material_count = 0 ; $material_count < count($budget_material_list["data"]) ; $material_count++)
			{
				$total_actual_material_cost = $total_actual_material_cost + $budget_material_list["data"][$material_count]["total_cost"];
			}
	}
	else {
		$total_actual_material_cost = "";
	}
	echo $total_actual_material_cost ;
}
else
{
	header("location:login.php");
}
?>
