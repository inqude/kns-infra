<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD:
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base . DIRECTORY_SEPARATOR . 'kns' . DIRECTORY_SEPARATOR . 'Legal' . DIRECTORY_SEPARATOR . 'general_config.php');
include_once($base . DIRECTORY_SEPARATOR . 'kns' . DIRECTORY_SEPARATOR . 'Legal' . DIRECTORY_SEPARATOR . 'projectmgmnt' . DIRECTORY_SEPARATOR . 'project_management_master_functions.php');
include_once($base . DIRECTORY_SEPARATOR . 'kns' . DIRECTORY_SEPARATOR . 'Legal' . DIRECTORY_SEPARATOR . 'projectmgmnt' . DIRECTORY_SEPARATOR . 'project_management_functions.php');
include_once($base . DIRECTORY_SEPARATOR . 'kns' . DIRECTORY_SEPARATOR . 'Legal' . DIRECTORY_SEPARATOR . 'users' . DIRECTORY_SEPARATOR . 'user_functions.php');

if ((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != "")) {
    $search_vendor = $_GET["vendor_id"];

    // Get Project  Payment ManPower modes already added
    $project_actual_machine_payment_search_data = array("active"=>'1',"vendor_id"=>$search_vendor);
    $project_actual_machine_payment_list = i_get_project_payment_machine($project_actual_machine_payment_search_data);
    if($project_actual_machine_payment_list['status'] == SUCCESS)
    {
        $project_actual_machine_payment_list_data = $project_actual_machine_payment_list['data'];
    }
      $total_issued_amount = 0;
      $total_deduction = 0;
      $total_balance = 0;
      $total_amount = 0;
      $issued_amount = 0;
      if($project_actual_machine_payment_list["status"] == SUCCESS)
    {
      for($count = 0; $count < count($project_actual_machine_payment_list_data); $count++)
      {
        if($project_actual_machine_payment_list_data[$count]["project_payment_machine_status"]=='Accepted' ||
        $project_actual_machine_payment_list_data[$count]["project_payment_machine_status"]=='Payment Issued'){
          //Get Delay
          $start_date = date("Y-m-d");
          $end_date = $project_actual_machine_payment_list_data[$count]["project_payment_machine_accepted_on"];
          $delay = get_date_diff($end_date,$start_date);

          //Get total amount
          $amount_before_tds = $project_actual_machine_payment_list_data[$count]["project_payment_machine_amount"];
          $manpower_tds = $project_actual_machine_payment_list_data[$count]["project_payment_machine_tds"];
          $tds_amount = ($manpower_tds/100) * $amount_before_tds;
          $amount = $amount_before_tds - $tds_amount;
          //Get Project Machine Vendor master List
          if ($project_actual_machine_payment_list_data[$count]["project_bata_payment_machine_status"] == 'Bill Generated' ||
           $project_actual_machine_payment_list_data[$count]["project_bata_payment_machine_status"] == 'Payment Issued') {
             $amount = $amount - $project_actual_machine_payment_list_data["$count"]["project_payment_machine_bata"];
          }

          $deduction = 0;
          $project_machine_issue_payment_search_data = array("active"=>'1',"machine_id"=>$project_actual_machine_payment_list_data[$count]["project_payment_machine_id"]);
          $project_machine_issue_payment_list = i_get_project_machine_issue_payment($project_machine_issue_payment_search_data);
          if($project_machine_issue_payment_list["status"] == SUCCESS)
          {
              $project_machine_issue_payment_list_data = $project_machine_issue_payment_list["data"];
              for($issue_count = 0 ; $issue_count < count($project_machine_issue_payment_list_data) ; $issue_count++)
              {
                  $issued_amount = $issued_amount + $project_machine_issue_payment_list_data[$issue_count]["project_machine_issue_payment_amount"];
                  $total_issued_amount = $total_issued_amount + $project_machine_issue_payment_list_data[$issue_count]["project_machine_issue_payment_amount"];
              }
          }
          else
          {
              $issued_amount = 0;
          }
          $balance_amount = ($amount - $issued_amount);

            $total_issued_amount = $total_issued_amount;
           $vendor = $project_actual_machine_payment_list_data[$count]["project_machine_vendor_master_name"];
           $total_amount = $total_amount + $amount;
      }
    }
  }
  $total_balance  = $total_amount-$total_issued_amount;
    $output= array('total_amount' => $total_amount, 'issued_amount'=>$total_issued_amount,'balance_amount'=>$total_balance);
    echo json_encode($output);
} else {
    echo "FAILURE";
}

?>
