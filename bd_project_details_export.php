<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: Attendance Report
CREATED ON	: 26-Mar-2016
CREATED BY	: Nitin Kashyap
PURPOSE     : Attendance Report
*/

/*
TBD: 
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_projects'.DIRECTORY_SEPARATOR.'bd_project_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_masters'.DIRECTORY_SEPARATOR.'bd_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'files'.DIRECTORY_SEPARATOR.'file_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'PHPExcel-1.8'.DIRECTORY_SEPARATOR.'Classes'.DIRECTORY_SEPARATOR.'PHPExcel.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];


	// Temp data
	$alert      = "";
	$alert_type = "";		
	
	// Query String
	if(isset($_GET["project"]))
	{
		$bd_project = $_GET["project"];
	}
	else
	{
		$bd_project = "";
	}
	
	// Get list of projects
	$bd_project_list = i_get_bd_project_list($bd_project,'','','');
	if($bd_project_list["status"] == SUCCESS)
	{
		$bd_project_list_data = $bd_project_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$bd_project_list["data"];
	}

	// Get list of files for this project
	$bd_files_list = i_get_bd_files_list('',$bd_project,'','','','','','1');
	if($bd_project_list["status"] == SUCCESS)
	{
		$bd_files_list_data = $bd_files_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$bd_files_list["data"];
	}
	
	// Get profitability calculation settings	
	$bd_prof_calc_list = i_get_bd_prof_calc_settings('',$bd_project);
	if($bd_prof_calc_list["status"] == SUCCESS)
	{
		$bd_prof_calc_list_data = $bd_prof_calc_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$bd_prof_calc_list["data"];
	}
	/* Create excel sheet and write the column headers - START */
	// Instantiate a new PHPExcel object
	$objPHPExcel = new PHPExcel(); 
	// Set the active Excel worksheet to sheet 0
	$objPHPExcel->setActiveSheetIndex(0); 
	// Initialise the Excel row number
	$row_count = 19; 
	// Excel column identifier array
	$column_array = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ');
	
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[0].$row_count, "SL NO");
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[1].$row_count, "OWNER NAME"); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[2].$row_count, "OWNER No"); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[3].$row_count, "SY NO"); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[4].$row_count, "EXTENT"); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[5].$row_count, "STATUS"); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[6].$row_count, "DELAY"); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[7].$row_count, "LAND COST"); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[8].$row_count, "PAID"); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[9].$row_count, "PAYABLE"); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[10].$row_count, "REMARKS"); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[11].$row_count, "ACCOUNT"); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[12].$row_count, "JD%"); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[13].$row_count, "JD SHARE"); 
	$style_array = array('font' => array('bold' => true));
	$objPHPExcel->getActiveSheet()->getStyle($column_array[0].$row_count.':'.$column_array[13].$row_count)->applyFromArray($style_array);
	$row_count++;
	if($bd_files_list["status"] == SUCCESS)
	{
		$sl_no           = 0;
		$total_extent    = 0;
		$total_jd_extent = 0;
		$total_land_cost = 0;
		$total_paid      = 0;
		$total_payable   = 0;
		for($file_count = 0 ; $file_count < count($bd_files_list_data); $file_count++)
		{
			$sl_no++;
			
			// Get the corresponding legal file details
			$legal_file_list = i_get_file_list('',$bd_files_list_data[$file_count]["bd_project_file_id"],'','','','','','','','');
			
			$already_paid = 0;
			if($legal_file_list["status"] == SUCCESS)
			{
				// Get the payment details for the legal file
				$legal_payment_list = i_get_file_payment_list($legal_file_list["data"][0]["file_id"],'');							
				if($legal_payment_list["status"] == SUCCESS)
				{
					for($lp_count = 0; $lp_count < count($legal_payment_list["data"]); $lp_count++)
					{
						$already_paid = $already_paid + $legal_payment_list["data"][$lp_count]["file_payment_amount"];
					}
				}
				else
				{
					$already_paid = 0;
				}
			}
			
			// Delay details for the file
			$delay_sresult = i_get_delay_details($bd_files_list_data[$file_count]["bd_project_file_id"]);
			if($delay_sresult['status'] == SUCCESS)
			{
				$delay_reason = $delay_sresult['data'][0]['bd_delay_reason_name'];
			}
			else
			{
				$delay_reason = 'NO DELAY';
			}
			$land_cost = $bd_files_list_data[$file_count]["bd_file_land_cost"];
			$extent    = $bd_files_list_data[$file_count]["bd_file_extent"];
			$jd_share_area = round(($bd_files_list_data[$file_count]["bd_file_extent"]/GUNTAS_PER_ACRE)*SALEABLE_AREA*($bd_files_list_data[$file_count]["bd_project_file_jda_share_percent"]/100),2);
			$payable = round(($land_cost - $already_paid),2);
			$objPHPExcel->getActiveSheet()->SetCellValue($column_array[0].$row_count, $sl_no); 
			$objPHPExcel->getActiveSheet()->SetCellValue($column_array[1].$row_count, $bd_files_list_data[$file_count]["bd_file_owner"]); 
			$objPHPExcel->getActiveSheet()->SetCellValue($column_array[2].$row_count, $bd_files_list_data[$file_count]["bd_file_owner_phone_no"]); 
			$objPHPExcel->getActiveSheet()->SetCellValue($column_array[3].$row_count, $bd_files_list_data[$file_count]["bd_file_survey_no"]); 
			$objPHPExcel->getActiveSheet()->SetCellValue($column_array[4].$row_count, $bd_files_list_data[$file_count]["bd_file_extent"]); 
			$objPHPExcel->getActiveSheet()->SetCellValue($column_array[5].$row_count, $bd_files_list_data[$file_count]["bd_file_owner_status_name"]); 
			$objPHPExcel->getActiveSheet()->SetCellValue($column_array[6].$row_count, $delay_reason); 
			$objPHPExcel->getActiveSheet()->SetCellValue($column_array[7].$row_count, round($bd_files_list_data[$file_count]["bd_file_land_cost"],2)); 
			$objPHPExcel->getActiveSheet()->SetCellValue($column_array[8].$row_count, round($already_paid,2)); 
			$objPHPExcel->getActiveSheet()->SetCellValue($column_array[9].$row_count, round(($land_cost - $already_paid),2)); 
			if($legal_file_list["status"] == SUCCESS)
			{
				$status = $legal_file_list["data"][0]["process_name"];
			}
			else
			{
				$status = $bd_files_list_data[$file_count]["process_name"];
			}
			$objPHPExcel->getActiveSheet()->SetCellValue($column_array[10].$row_count, $status); 
			$objPHPExcel->getActiveSheet()->SetCellValue($column_array[11].$row_count, $bd_files_list_data[$file_count]["bd_own_account_master_account_name"]); 
			$objPHPExcel->getActiveSheet()->SetCellValue($column_array[12].$row_count, $bd_files_list_data[$file_count]["bd_project_file_jda_share_percent"].'%'); 
			$objPHPExcel->getActiveSheet()->SetCellValue($column_array[13].$row_count, round(($bd_files_list_data[$file_count]["bd_file_extent"]/GUNTAS_PER_ACRE)*SALEABLE_AREA*($bd_files_list_data[$file_count]["bd_project_file_jda_share_percent"]/100),2)); 
			$row_count++;
			
			$total_extent    = $total_extent + $extent;
			$total_jd_extent = $total_jd_extent + $jd_share_area;
			$total_land_cost = $total_land_cost + $land_cost;
			$total_paid      = $total_paid + $already_paid;
			$total_payable   = $total_payable + $payable;
			$total_ext	 	 = ($total_extent/GUNTAS_PER_ACRE);						
		}
		
		// Totals
		$total_extent    = round($total_extent,2);
		$total_jd_extent = round($total_jd_extent,2);
		$total_land_cost = round($total_land_cost,2);
		$total_paid      = round($total_paid,2);
		$total_payable   = round($total_payable,2);
		$total_ext	 	 = ($total_extent/GUNTAS_PER_ACRE);	
		
		$objPHPExcel->getActiveSheet()->SetCellValue($column_array[3].$row_count,"TOTAL"); 
		$objPHPExcel->getActiveSheet()->SetCellValue($column_array[4].$row_count,$total_extent.' guntas ('.$total_ext.' acres)'); 
		$objPHPExcel->getActiveSheet()->SetCellValue($column_array[7].$row_count,$total_land_cost); 
		$objPHPExcel->getActiveSheet()->SetCellValue($column_array[8].$row_count,$total_paid); 
		$objPHPExcel->getActiveSheet()->SetCellValue($column_array[9].$row_count,$total_payable); 
		$objPHPExcel->getActiveSheet()->SetCellValue($column_array[13].$row_count,$total_jd_extent);
	}
	$objPHPExcel->getActiveSheet()->getStyle($column_array[0].'18'.':'.$column_array[13].$row_count)->getAlignment()->setWrapText(true); 	
	
	// Increment the Excel row counter
	$row_count = 1; 	 	
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[0].$row_count, $bd_project_list_data[0]['bd_project_name']); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[7].$row_count, $total_extent.' guntas'); 
	$objPHPExcel->getActiveSheet()->getRowDimension($row_count)->setRowHeight(30);
	$objPHPExcel->getActiveSheet()->mergeCells($column_array[0].$row_count.':'.$column_array[6].$row_count);
	$objPHPExcel->getActiveSheet()->mergeCells($column_array[7].$row_count.':'.$column_array[13].$row_count);
	$style_array = array('font' => array('bold' => true));
	$objPHPExcel->getActiveSheet()->getStyle($column_array[0].$row_count.':'.$column_array[13].$row_count)->applyFromArray($style_array);
	$objPHPExcel->getActiveSheet()->getStyle($column_array[0].$row_count.':'.$column_array[13].$row_count)->getFont()->setSize(16);
	$row_count++;	
	
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[0].$row_count, "BASIC DETAILS"); 
	$objPHPExcel->getActiveSheet()->getRowDimension($row_count)->setRowHeight(30);
	$objPHPExcel->getActiveSheet()->mergeCells($column_array[0].$row_count.':'.$column_array[13].$row_count);
	$style_array = array('font' => array('bold' => true));
	$objPHPExcel->getActiveSheet()->getStyle('A'.$row_count)->applyFromArray($style_array);
	$objPHPExcel->getActiveSheet()->getStyle('A'.$row_count)->getFont()->setSize(16);
	$row_count++;
	
	$objPHPExcel->getActiveSheet()->mergeCells($column_array[0].$row_count.':'.$column_array[2].$row_count);
	$objPHPExcel->getActiveSheet()->mergeCells($column_array[3].$row_count.':'.$column_array[5].$row_count);
	$objPHPExcel->getActiveSheet()->mergeCells($column_array[6].$row_count.':'.$column_array[8].$row_count);
	$objPHPExcel->getActiveSheet()->mergeCells($column_array[9].$row_count.':'.$column_array[11].$row_count);
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[0].$row_count, "EXTENT"); 	
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[3].$row_count, "SALABLE AREA"); 	
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[6].$row_count, "JD SHARE"); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[9].$row_count, "AVAILABLE AREA"); 
	
	$objPHPExcel->getActiveSheet()->getRowDimension($row_count)->setRowHeight(30);	
	$style_array = array('font' => array('bold' => true));
	$objPHPExcel->getActiveSheet()->getStyle($column_array[0].$row_count.':'.$column_array[13].$row_count)->applyFromArray($style_array);
	$objPHPExcel->getActiveSheet()->getStyle($column_array[0].$row_count.':'.$column_array[13].$row_count)->getFont()->setSize(12);
	$row_count++;
	
	$objPHPExcel->getActiveSheet()->mergeCells($column_array[0].$row_count.':'.$column_array[2].$row_count);
	$objPHPExcel->getActiveSheet()->mergeCells($column_array[3].$row_count.':'.$column_array[5].$row_count);
	$objPHPExcel->getActiveSheet()->mergeCells($column_array[6].$row_count.':'.$column_array[8].$row_count);
	$objPHPExcel->getActiveSheet()->mergeCells($column_array[9].$row_count.':'.$column_array[11].$row_count);
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[0].$row_count, round($total_ext,2)); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[3].$row_count, round(($total_ext*SALEABLE_AREA),2)); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[6].$row_count, round($total_jd_extent,2)); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[9].$row_count, round((($total_ext*SALEABLE_AREA) - $total_jd_extent),2)); 
	$objPHPExcel->getActiveSheet()->getRowDimension($row_count)->setRowHeight(30);	
	$objPHPExcel->getActiveSheet()->getStyle($column_array[0].$row_count.':'.$column_array[13].$row_count)->getFont()->setSize(12);
	$row_count++;
	
	$objPHPExcel->getActiveSheet()->mergeCells($column_array[0].$row_count.':'.$column_array[13].$row_count);
	$row_count++; // An empty row between 
	
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[0].$row_count, "PROFITABILITY CALCULATIONS"); 
	$objPHPExcel->getActiveSheet()->getRowDimension($row_count)->setRowHeight(30);
	$objPHPExcel->getActiveSheet()->mergeCells($column_array[0].$row_count.':'.$column_array[13].$row_count);
	$style_array = array('font' => array('bold' => true));
	$objPHPExcel->getActiveSheet()->getStyle('A'.$row_count)->applyFromArray($style_array);
	$objPHPExcel->getActiveSheet()->getStyle('A'.$row_count)->getFont()->setSize(16);
	$row_count++;
	
	$objPHPExcel->getActiveSheet()->mergeCells($column_array[0].$row_count.':'.$column_array[2].$row_count);
	$objPHPExcel->getActiveSheet()->mergeCells($column_array[3].$row_count.':'.$column_array[5].$row_count);
	$objPHPExcel->getActiveSheet()->mergeCells($column_array[6].$row_count.':'.$column_array[8].$row_count);
	$objPHPExcel->getActiveSheet()->mergeCells($column_array[9].$row_count.':'.$column_array[11].$row_count);
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[0].$row_count, "PARTICULARS"); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[3].$row_count, "AMOUNT"); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[6].$row_count, "RATE"); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[9].$row_count, "FUND REQUIREMENT"); 
	$objPHPExcel->getActiveSheet()->getRowDimension($row_count)->setRowHeight(30);	
	$style_array = array('font' => array('bold' => true));
	$objPHPExcel->getActiveSheet()->getStyle($column_array[0].$row_count.':'.$column_array[13].$row_count)->applyFromArray($style_array);
	$objPHPExcel->getActiveSheet()->getStyle($column_array[0].$row_count.':'.$column_array[13].$row_count)->getFont()->setSize(12);
	$row_count++;
	if($bd_prof_calc_list["status"] == SUCCESS)
	{
		$total_revenue1 = round($bd_prof_calc_list_data[0]["bd_profitability_calc_setting_expected_rate"],2);
		$stamp_duty    = round($bd_prof_calc_list_data[0]["bd_profitability_calc_setting_stamp_duty_charges"],2);
		$dev_cost      = round($bd_prof_calc_list_data[0]["bd_profitability_calc_setting_dev_cost_rate"],2);
		
		$total_revenue = ((($total_ext*SALEABLE_AREA) - $total_jd_extent)*$bd_prof_calc_list_data[0]["bd_profitability_calc_setting_expected_rate"]);
		$total_dev_cost = ($total_ext*SALEABLE_AREA)*$bd_prof_calc_list_data[0]["bd_profitability_calc_setting_dev_cost_rate"]; 
		$admin_mktg_cost = ((($total_ext*SALEABLE_AREA) - $total_jd_extent)*$bd_prof_calc_list_data[0]["bd_profitability_calc_setting_expected_rate"])*$bd_prof_calc_list_data[0]["bd_profitability_calc_setting_admin_charges"]/100; 
		$finance_cost  = ($total_land_cost + $total_dev_cost + $admin_mktg_cost + $bd_prof_calc_list_data[0]["bd_profitability_calc_setting_stamp_duty_charges"])*2*($bd_prof_calc_list_data[0]["bd_profitability_calc_setting_fianance_rate"]/100);
		$total_cost = round(($total_land_cost + $total_dev_cost + $admin_mktg_cost + $bd_prof_calc_list_data[0]["bd_profitability_calc_setting_stamp_duty_charges"] + $finance_cost),2); 
		$net_income = $total_revenue -($total_land_cost + $total_dev_cost + $admin_mktg_cost + $bd_prof_calc_list_data[0]["bd_profitability_calc_setting_stamp_duty_charges"] + $finance_cost); 
		$net_fund_req = round(($total_payable + $bd_prof_calc_list_data[0]["bd_profitability_calc_setting_stamp_duty_charges"]),2);
		$roi = round(($net_income/($total_land_cost + $total_dev_cost + $admin_mktg_cost + $bd_prof_calc_list_data[0]["bd_profitability_calc_setting_stamp_duty_charges"] + $finance_cost)*100),2); 
		$admin_mktg_cost = ((($total_ext*SALEABLE_AREA) - $total_jd_extent)*$bd_prof_calc_list_data[0]["bd_profitability_calc_setting_expected_rate"])*$bd_prof_calc_list_data[0]["bd_profitability_calc_setting_admin_charges"]/100; 
		
		$rot = round(($net_income/$total_revenue*100),2);
				
	}
	else
	{
		$total_revenue = "0";
	}
	
	$objPHPExcel->getActiveSheet()->mergeCells($column_array[0].$row_count.':'.$column_array[2].$row_count);
	$objPHPExcel->getActiveSheet()->mergeCells($column_array[3].$row_count.':'.$column_array[5].$row_count);
	$objPHPExcel->getActiveSheet()->mergeCells($column_array[6].$row_count.':'.$column_array[8].$row_count);
	$objPHPExcel->getActiveSheet()->mergeCells($column_array[9].$row_count.':'.$column_array[11].$row_count);
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[0].$row_count, "Total Revenue Expected"); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[3].$row_count, $total_revenue); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[6].$row_count, $total_revenue1); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[9].$row_count, ""); 
	$objPHPExcel->getActiveSheet()->getRowDimension($row_count)->setRowHeight(30);	
	$objPHPExcel->getActiveSheet()->getStyle($column_array[0].$row_count.':'.$column_array[13].$row_count)->getFont()->setSize(12);
	$row_count++;
	
	$objPHPExcel->getActiveSheet()->mergeCells($column_array[0].$row_count.':'.$column_array[2].$row_count);
	$objPHPExcel->getActiveSheet()->mergeCells($column_array[3].$row_count.':'.$column_array[5].$row_count);
	$objPHPExcel->getActiveSheet()->mergeCells($column_array[6].$row_count.':'.$column_array[8].$row_count);
	$objPHPExcel->getActiveSheet()->mergeCells($column_array[9].$row_count.':'.$column_array[11].$row_count);
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[0].$row_count, "Land Cost");
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[3].$row_count, $total_land_cost);
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[6].$row_count, "");
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[9].$row_count, $total_payable);
	$objPHPExcel->getActiveSheet()->getRowDimension($row_count)->setRowHeight(30);	
	$objPHPExcel->getActiveSheet()->getStyle($column_array[0].$row_count.':'.$column_array[13].$row_count)->getFont()->setSize(12);
	$row_count++;
	
	$objPHPExcel->getActiveSheet()->mergeCells($column_array[0].$row_count.':'.$column_array[2].$row_count);
	$objPHPExcel->getActiveSheet()->mergeCells($column_array[3].$row_count.':'.$column_array[5].$row_count);
	$objPHPExcel->getActiveSheet()->mergeCells($column_array[6].$row_count.':'.$column_array[8].$row_count);
	$objPHPExcel->getActiveSheet()->mergeCells($column_array[9].$row_count.':'.$column_array[11].$row_count);
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[0].$row_count, "Registration, Stamp Duty, Plan Approval, Conversion etc."); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[3].$row_count, $stamp_duty); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[6].$row_count, ""); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[9].$row_count, $stamp_duty); 
	$objPHPExcel->getActiveSheet()->getRowDimension($row_count)->setRowHeight(30);	
	$objPHPExcel->getActiveSheet()->getStyle($column_array[0].$row_count.':'.$column_array[13].$row_count)->getFont()->setSize(12);
	$row_count++;
	
	$objPHPExcel->getActiveSheet()->mergeCells($column_array[0].$row_count.':'.$column_array[2].$row_count);
	$objPHPExcel->getActiveSheet()->mergeCells($column_array[3].$row_count.':'.$column_array[5].$row_count);
	$objPHPExcel->getActiveSheet()->mergeCells($column_array[6].$row_count.':'.$column_array[8].$row_count);
	$objPHPExcel->getActiveSheet()->mergeCells($column_array[9].$row_count.':'.$column_array[11].$row_count);
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[0].$row_count, "Cost of Development"); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[3].$row_count, $total_dev_cost); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[6].$row_count, $dev_cost); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[9].$row_count, ""); 
	$objPHPExcel->getActiveSheet()->getRowDimension($row_count)->setRowHeight(30);	
	$objPHPExcel->getActiveSheet()->getStyle($column_array[0].$row_count.':'.$column_array[13].$row_count)->getFont()->setSize(12);
	$row_count++;
	
	$objPHPExcel->getActiveSheet()->mergeCells($column_array[0].$row_count.':'.$column_array[2].$row_count);
	$objPHPExcel->getActiveSheet()->mergeCells($column_array[3].$row_count.':'.$column_array[5].$row_count);
	$objPHPExcel->getActiveSheet()->mergeCells($column_array[6].$row_count.':'.$column_array[8].$row_count);
	$objPHPExcel->getActiveSheet()->mergeCells($column_array[9].$row_count.':'.$column_array[11].$row_count);
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[0].$row_count, "Admin, Marketing & Others"); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[3].$row_count, $admin_mktg_cost); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[6].$row_count, ""); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[9].$row_count, ""); 
	$objPHPExcel->getActiveSheet()->getRowDimension($row_count)->setRowHeight(30);	
	$objPHPExcel->getActiveSheet()->getStyle($column_array[0].$row_count.':'.$column_array[13].$row_count)->getFont()->setSize(12);
	$row_count++;
	
	$objPHPExcel->getActiveSheet()->mergeCells($column_array[0].$row_count.':'.$column_array[2].$row_count);
	$objPHPExcel->getActiveSheet()->mergeCells($column_array[3].$row_count.':'.$column_array[5].$row_count);
	$objPHPExcel->getActiveSheet()->mergeCells($column_array[6].$row_count.':'.$column_array[8].$row_count);
	$objPHPExcel->getActiveSheet()->mergeCells($column_array[9].$row_count.':'.$column_array[11].$row_count);
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[0].$row_count, "Finance Cost (@18%) for 2 years"); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[3].$row_count, $finance_cost); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[6].$row_count, ""); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[9].$row_count, ""); 
	$objPHPExcel->getActiveSheet()->getRowDimension($row_count)->setRowHeight(30);	
	$objPHPExcel->getActiveSheet()->getStyle($column_array[0].$row_count.':'.$column_array[13].$row_count)->getFont()->setSize(12);
	$row_count++;
	
	$objPHPExcel->getActiveSheet()->mergeCells($column_array[0].$row_count.':'.$column_array[2].$row_count);
	$objPHPExcel->getActiveSheet()->mergeCells($column_array[3].$row_count.':'.$column_array[5].$row_count);
	$objPHPExcel->getActiveSheet()->mergeCells($column_array[6].$row_count.':'.$column_array[8].$row_count);
	$objPHPExcel->getActiveSheet()->mergeCells($column_array[9].$row_count.':'.$column_array[11].$row_count);
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[0].$row_count, "Total Cost"); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[3].$row_count, $total_cost); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[6].$row_count, ""); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[9].$row_count, "");
	$objPHPExcel->getActiveSheet()->getRowDimension($row_count)->setRowHeight(30);	
	$objPHPExcel->getActiveSheet()->getStyle($column_array[0].$row_count.':'.$column_array[13].$row_count)->getFont()->setSize(12);
	$row_count++;
	
	$objPHPExcel->getActiveSheet()->mergeCells($column_array[0].$row_count.':'.$column_array[2].$row_count);
	$objPHPExcel->getActiveSheet()->mergeCells($column_array[3].$row_count.':'.$column_array[5].$row_count);
	$objPHPExcel->getActiveSheet()->mergeCells($column_array[6].$row_count.':'.$column_array[8].$row_count);
	$objPHPExcel->getActiveSheet()->mergeCells($column_array[9].$row_count.':'.$column_array[11].$row_count);
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[0].$row_count, "Net Income"); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[3].$row_count, $net_income); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[6].$row_count, ""); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[9].$row_count, $net_fund_req); 
	$objPHPExcel->getActiveSheet()->getRowDimension($row_count)->setRowHeight(30);	
	$objPHPExcel->getActiveSheet()->getStyle($column_array[0].$row_count.':'.$column_array[13].$row_count)->getFont()->setSize(12);
	$row_count++;
	
	$objPHPExcel->getActiveSheet()->mergeCells($column_array[0].$row_count.':'.$column_array[2].$row_count);
	$objPHPExcel->getActiveSheet()->mergeCells($column_array[3].$row_count.':'.$column_array[5].$row_count);
	$objPHPExcel->getActiveSheet()->mergeCells($column_array[6].$row_count.':'.$column_array[8].$row_count);
	$objPHPExcel->getActiveSheet()->mergeCells($column_array[9].$row_count.':'.$column_array[11].$row_count);
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[0].$row_count, "Return On Investment"); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[3].$row_count, $roi); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[6].$row_count, ""); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[9].$row_count, ""); 
	$objPHPExcel->getActiveSheet()->getRowDimension($row_count)->setRowHeight(30);	
	$objPHPExcel->getActiveSheet()->getStyle($column_array[0].$row_count.':'.$column_array[13].$row_count)->getFont()->setSize(12);
	$row_count++;
	
	$objPHPExcel->getActiveSheet()->mergeCells($column_array[0].$row_count.':'.$column_array[2].$row_count);
	$objPHPExcel->getActiveSheet()->mergeCells($column_array[3].$row_count.':'.$column_array[5].$row_count);
	$objPHPExcel->getActiveSheet()->mergeCells($column_array[6].$row_count.':'.$column_array[8].$row_count);
	$objPHPExcel->getActiveSheet()->mergeCells($column_array[9].$row_count.':'.$column_array[11].$row_count);
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[0].$row_count, "Return On Turnover"); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[3].$row_count, $rot); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[6].$row_count, ""); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[9].$row_count, ""); 
	$objPHPExcel->getActiveSheet()->getRowDimension($row_count)->setRowHeight(30);	
	$objPHPExcel->getActiveSheet()->getStyle($column_array[0].$row_count.':'.$column_array[13].$row_count)->getFont()->setSize(12);
	$row_count++;
	
	$objPHPExcel->getActiveSheet()->mergeCells($column_array[0].$row_count.':'.$column_array[13].$row_count);
	$row_count++;
	
	$objPHPExcel->getActiveSheet()->getStyle($column_array[0].'1'.':'.$column_array[13].($row_count - 1))->getAlignment()->setWrapText(true); 	
	/* Create excel sheet and write the column headers - END */
	
	
	header('Content-Type: application/vnd.ms-excel'); 
	header('Content-Disposition: attachment;filename="project_details.xls"'); 
	header('Cache-Control: max-age=0'); 
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5'); 
	$objWriter->save('php://output');
}		
else
{
	header("location:login.php");
}	
?>