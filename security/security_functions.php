<?php
/* FILE HEADER - START */
// LAST UPDATED ON: 7th June 2015
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'security'.DIRECTORY_SEPARATOR.'security_config.php');
/* INCLUDES - END */

/*
PURPOSE : To Encrypt password
INPUT 	: User entered password
OUTPUT 	: Encrypted password
BY 		: Nitin Kashyap
*/
function encrypt_password($password)
{
    // Salt and encrypt the password
    $salt = salt; // Salt to be defined in config
    $enc_password = sha1(strrev($password.$salt));

    return $enc_password;
}
?>