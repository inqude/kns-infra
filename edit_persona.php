<?php
/* SESSION INITIATE - START */
session_start();

/* DEFINES - START */
define('ADD_USER_FUNC_ID', '1');
/* DEFINES - END */

$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'tasks'.DIRECTORY_SEPARATOR.'general_task_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'da_user.php');
/* INCLUDES - END */

if ((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != "")) {

    // Session Data
    $user 		   = $_SESSION["loggedin_user"];
    $role 		   = $_SESSION["loggedin_role"];
    $loggedin_name = $_SESSION["loggedin_user_name"];

    // // Get permission settings for this user for this page

    $view_perms_list = i_get_user_perms($user, '', ADD_USER_FUNC_ID, '2', '1');
    $add_perms_list  = i_get_user_perms($user, '', ADD_USER_FUNC_ID, '1', '1');

    /* DATA INITIALIZATION - START */
    $alert = "";
    $alert_type = -1;
    /* DATA INITIALIZATION - END */

    if(isset($_GET["persona_user_id"]))
    {
      $persona_user_id = $_GET["persona_user_id"];
    }
    else
    {
      $persona_user_id = "";
    }

    // Capture the form data
    if (isset($_POST["edit_persona_submit"])) {

        $persona_department_id   = $_POST["persona_department_id"];
        $persona_user_id         = $_POST["persona_user"];
        $persona_designation     = $_POST["sel_designation"];
        $persona_dob             = $_POST["dob"];
        $persona_user_phone      = $_POST["user_phone"];
        $persona_gender          = $_POST["sel_gender"];
        $persona_maritial_status = $_POST["maritial_status"];
        $persona_blood_group     = $_POST["blood_group"];
        $persona_pan_number      = $_POST["pan"];
        $persona_aadhar_number        = $_POST["aadhar"];
        $persona_present_address = $_POST["present_address"];
        $persona_present_city       = $_POST["present_city"];
        $persona_present_state        = $_POST["present_state"];
        $persona_present_pincode = $_POST["present_pincode"];
        $persona_permanent_address  = $_POST["permanent_address"];
        $persona_permanent_city     = $_POST["permanent_city"];
        $persona_permanent_state    = $_POST["permanent_state"];
        $persona_permanent_pincode  = $_POST["permanent_pincode"];
        $persona_company_phone      = $_POST["mobile"];
        $persona_sim_number         = $_POST["simnumber"];
        $persona_voice_limit        = $_POST["voice_limit"];
        $persona_data_limit         = $_POST["data_limit"];
        $persona_pf_number        = $_POST["pf_number"];
        $persona_ip_number        = $_POST["ip_number"];
        $persona_ip_name          = $_POST["ip_name"];
        $persona_ip_limit         = $_POST["ip_limit"];

        $persona_uan                = $_POST["uan"];
        $persona_esi_number         = $_POST["esi_number"];

        if(($persona_department_id != "0") && ($persona_user_id != "0")  &&( $persona_designation != "0") && ($persona_dob != "") && ($persona_user_phone != "") &&
        ($persona_gender != "0")  && ($persona_maritial_status != "0") && ($persona_blood_group != "0") && ($persona_pan_number != "") && ($persona_aadhar_number != "") &&
        ($persona_present_address != "") && ($persona_present_city != "") && ($persona_present_state != "") && ($persona_present_pincode != "") && ($persona_permanent_address != "") &&
        ($persona_permanent_city != "") && ($persona_permanent_state != "") && ($persona_permanent_pincode != "") && ($persona_company_phone != "") && ($persona_sim_number != "") &&
        ($persona_voice_limit != "") && ($persona_data_limit != "") && ($persona_pf_number != "") &&
        ($persona_ip_number != "") &&  ($persona_ip_name != "") && ($persona_ip_limit != "")) {

          $nukePersona = db_persona_drop($persona_user_id);

          $response = db_add_persona($persona_department_id, $persona_user_id, $persona_designation, $persona_dob, $persona_user_phone,
          $persona_gender,$persona_maritial_status, $persona_blood_group, $persona_pan_number,$persona_aadhar_number,
          $persona_present_address, $persona_present_city, $persona_present_state, $persona_present_pincode, $persona_permanent_address,
          $persona_permanent_city, $persona_permanent_state, $persona_permanent_pincode, $persona_company_phone, $persona_sim_number,
          $persona_voice_limit, $persona_data_limit, $persona_pf_number, $persona_ip_name, $persona_ip_number, $persona_ip_limit,
          $persona_uan, $persona_esi_number);

          if($response["status"] == 'SUCCESS') {
            $_POST = array();
            ?>
            <script>
              alert("Persona Updated successfully ");
            </script>
            <?php
          }
        }
        else {
          $persona_details_data = $_POST;
          $alert = "Please fill all the mandatory fields";
          $alert_type = 0;
        ?>
          <script>
          alert("Please fill all the mandatory fields");
          </script>
          <?php
        }
    }

    // Get list of Department*
    $department_list = i_get_department_list('', '');

    if ($department_list["status"] == SUCCESS) {
        $department_list_data = $department_list["data"];
    } else {
        $alert = $alert."Alert: ".$department_list["data"];
      }

    // get current persona details
    $persona_details = db_get_persona($persona_user_id);

    if($persona_details["status"] = "DB_RECORD_ALREADY_EXISTS") {
      $persona_details_data = $persona_details["data"][0];

      $user_list = i_get_user_list('', '', '', '', '1', $persona_details_data['persona_department_id']);
      if ($user_list['status'] == SUCCESS) {
          $user_list_data = $user_list['data'];
      }

      $designation_list = db_get_designation_list();
      if($designation_list['status'] == 'DB_RECORD_ALREADY_EXISTS') {
        $designation_list_data = $designation_list['data'];
      }
    }

} else {
    header("location:login.php");
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">
    <title>Edit Persona</title>
    <!-- Bootstrap core CSS -->
    <link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <!-- <link href="../../assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet"> -->
    <!-- Custom styles -->
    <link href="assets/multistepform/css/style.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
  <?php

  include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');

  ?>
<!-- MultiStep Form -->
<div class="row">
    <div class="span8" style="margin-top:-50px; margin-bottom:50px">
        <form id="msform" method="post">
            <!-- progressbar -->
            <ul id="progressbar">
                <li class="active">Personal Details</li>
                <li>Address Details</li>
                <li>Amenities</li>
            </ul>
            <!-- fieldsets -->
            <fieldset>
                <h2 class="fs-title">Personal Details</h2>
                <h3 class="fs-subtitle">Capture Basic details</h3>
                <div class="col-xs-6 custom-label" style="text-align:left">
                  <div class="form-group">
                      <label>Department *</label>
                    <select class="form-control" id="ddl_department_id" name="ddl_department_id" disabled>
                      <option value='0'>- - Select Department - -</option>
                      <?php
                        for ($count = 0; $count < count($department_list_data); $count++) {
                      ?>
                      <option value="<?php echo $department_list_data[$count]["general_task_department_id"]; ?>"
                        <?php if($persona_details_data['persona_department_id'] == $department_list_data[$count]["general_task_department_id"]) {
                        ?> selected="selected"
                      <?php } ?>>
                        <?php echo $department_list_data[$count]["general_task_department_name"]; ?></option>
                      <?php
                      }
                      ?>
                    </select>
                  </div>
                  <div class="form-group">
                    <label>Name *</label>
                    <select class="form-control" disabled>
                    <?php
                    for($count = 0; $count < count($user_list_data); $count++)
                    {
                    ?>
                    <option value="<?php echo $user_list_data[$count]["user_id"]; ?>" <?php if($persona_details_data["persona_user_id"] == $user_list_data[$count]["user_id"]){ ?> selected <?php } ?>><?php echo $user_list_data[$count]["user_name"]; ?></option>
                    <?php
                    }
                    ?>

                    </select>
                  </div>
                  <div class="form-group">
                    <label>Current Designation *</label>
                    <select class="form-control" id="sel_designation" name="sel_designation">
                      <option value="0">-- Select Designation --</option>
                      <?php
                      for($count = 0; $count < count($designation_list_data); $count++)
                      {
                      ?>
                      <option value="<?php echo $designation_list_data[$count]["designation_id"]; ?>" <?php if($persona_details_data["persona_designation"] == $designation_list_data[$count]["designation_id"]){ ?> selected <?php } ?>><?php echo $designation_list_data[$count]["designation_name"]; ?></option>
                      <?php
                      }
                      ?>
                    </select>
                  </div>
                  <!-- <input type="text" name="designation" placeholder="Current Designation"/> -->
                <!-- <input type="text" name="fname" placeholder="First Name" required/>
                <input type="text" name="lname" placeholder="Last Name"/> -->
                <input type="hidden" id="persona_department_id" name="persona_department_id" value="<?= $persona_details_data['persona_department_id'];?>" />
                <input type="hidden" id="persona_user" name="persona_user" value="<?= $persona_details_data['persona_user_id'];?>" />
                <label>DOJ *</label>
                <input type="date" name="dob" id="dob" value="<?= $persona_details_data['persona_dob'];?>"  placeholder="DOB"/>
                <label>User Phone *</label>
                <input type="text" maxlength="10" id="user_phone" name="user_phone" value="<?= $persona_details_data['persona_user_phone'];?>" placeholder="Phone"/>
              </div>
              <div class="col-xs-6" style="text-align:left">
                <div class="form-group">
                  <label for="exampleSelect1">Gender</label>
                  <select class="form-control" id="sel_gender" name="sel_gender">
                    <option value="0">-- Choose Gender --</option>
                    <option value="Male" <?php if($persona_details_data['persona_gender'] == 'Male') {
                      ?> selected="selected" <?php
                    }?>>Male</option>
                    <option value="Female" <?php if($persona_details_data['persona_gender'] == 'Female') {
                      ?> selected="selected" <?php
                    }?>>Female</option>
                  </select>
                </div>
                <div class="form-group">
                  <label>Maritial Status</label>
                  <select class="form-control" id="maritial_status" name="maritial_status">
                    <option value="0">-- Maritial Status --</option>
                    <option value="Married" <?php if($persona_details_data['persona_maritial_status'] == 'Married') {
                      ?> selected="selected" <?php
                    }?>>Married</option>
                    <option value="Single"  <?php if($persona_details_data['persona_maritial_status'] == 'Single') {
                      ?> selected="selected" <?php
                    }?>>Single</option>
                  </select>
                </div>
                <div class="form-group">
                  <label>Blood Group</label>
                  <select class="form-control" id="blood_group" name="blood_group">
                    <option value="0">-- Choose Blood Group --</option>
                    <option value="A+"  <?php if($persona_details_data['persona_blood_group'] == 'A+') {
                      ?> selected="selected" <?php
                    }?>>A+</option>
                    <option value="A-" <?php if($persona_details_data['persona_blood_group'] == 'A-') {
                      ?> selected="selected" <?php
                    }?>>A-</option>
                    <option value="B+" <?php if($persona_details_data['persona_blood_group'] == 'B+') {
                      ?> selected="selected" <?php
                    }?>>B+</option>
                    <option value="B-" <?php if($persona_details_data['persona_blood_group'] == 'B-') {
                      ?> selected="selected" <?php
                    }?>>B-</option>
                    <option value="O+" <?php if($persona_details_data['persona_blood_group'] == 'O+') {
                      ?> selected="selected" <?php
                    }?>>O+</option>
                    <option value="O-" <?php if($persona_details_data['persona_blood_group'] == 'O-') {
                      ?> selected="selected" <?php
                    }?>>O-</option>
                    <option value="AB+" <?php if($persona_details_data['persona_blood_group'] == 'AB+') {
                      ?> selected="selected" <?php
                    }?>>AB+</option>
                    <option value="AB-" <?php if($persona_details_data['persona_blood_group'] == 'AB-') {
                      ?> selected="selected" <?php
                    }?>>AB-</option>
                  </select>
                </div>
                <label>PAN Card Number *</label>
                <input type="text" maxlength="10" name="pan" value="<?= $persona_details_data['persona_pan_number'];?>" placeholder="Pan Number"/>
                <label>AADHAR Card Number*</label>
                <input type="text" maxlength="12" name="aadhar" value="<?= $persona_details_data['persona_aadhar_number'];?>" placeholder="Aadhar Number"/>
              </div>
                <input type="button" name="next" class="next action-button" value="Next"/>
                <br/>
            </fieldset>
            <fieldset>
                <h2 class="fs-title">Address Details</h2>
                <div class="col-xs-6" style="text-align:left">
                  <h3 class="fs-subtitle">Present Address details</h3>
                <label>Address *</label>
                <input type="text" name="present_address" value="<?= $persona_details_data['persona_present_address'];?>" placeholder="Address"/>
                <label>City *</label>
                <input type="text" name="present_city" value="<?= $persona_details_data['persona_present_city'];?>" placeholder="City"/>
                <label>State *</label>
                <input type="text" name="present_state" value="<?= $persona_details_data['persona_present_state'];?>" placeholder="State"/>
                <label>Pincode *</label>
                <input type="text" name="present_pincode" value="<?= $persona_details_data['persona_present_pincode'];?>" placeholder="Pin Code"/>
              </div>
                <div class="col-xs-6" style="text-align:left">
                <h3 class="fs-subtitle">Permanent Address details</h3>
                <label>Address *</label>
                <input type="text" name="permanent_address" value="<?= $persona_details_data['persona_permanent_address'];?>" placeholder="Address"/>
                <label>City *</label>
                <input type="text" name="permanent_city" value="<?= $persona_details_data['persona_permanent_city'];?>" placeholder="City"/>
                <label>State *</label>
                <input type="text" name="permanent_state" value="<?= $persona_details_data['persona_permanent_state'];?>" placeholder="State"/>
                <label>Pincode *</label>
                <input type="text" name="permanent_pincode" value="<?= $persona_details_data['persona_permanent_pincode'];?>" placeholder="Pin Code"/>
              </div>
                <input type="button" name="previous" class="previous action-button-previous" value="Previous"/>
                <input type="button" name="next" class="next action-button" value="Next"/>
            </fieldset>
            <fieldset>
                <h2 class="fs-title">Amenities</h2>
                <h3 class="fs-subtitle">Capture Mobile expenditure details</h3>
                <div class="col-xs-6" style="text-align:left">
                <label>Company Mobile *</label>
                <input type="text" maxlength="10" name="mobile" value="<?= $persona_details_data['persona_company_phone'];?>" placeholder="Mobile"/>
                <label>Voice Limit Amount*</label>
                <input type="number" name="voice_limit" value="<?= $persona_details_data['persona_voice_limit'];?>" placeholder="Voice Limit"/>

                <label>Data SIM Number *</label>
                <input type="text" name="simnumber" value="<?= $persona_details_data['persona_sim_number'];?>" placeholder="SIM Number"/>

                <label>Data Limit Amount*</label>
                <input type="number" name="data_limit" value="<?= $persona_details_data['persona_data_limit'];?>" placeholder="Data Limit"/>

                <label>UAN *</label>
                <input type="number" name="uan" value="<?= $persona_details_data['persona_uan'];?>" placeholder="UAN"/>
                </div>
                <div class="col-xs-6" style="text-align:left">
                  <label>ESI Number*</label>
                  <input type="text" name="esi_number" value="<?= $persona_details_data['persona_esi_number'];?>" placeholder="ESI Number"/>
                  <label>PF Number *</label>
                  <input type="text" name="pf_number" value="<?= $persona_details_data['persona_pf_number'];?>" placeholder="PF Number"/>
                  <label>Insurance Policy Name *</label>
                  <input type="text" name="ip_name" value="<?= $persona_details_data['persona_insurance_name'];?>" placeholder="Insurance Policy Name"/>
                  <label>Insurance Policy Number *</label>
                  <input type="text" name="ip_number" value="<?= $persona_details_data['persona_insurance_number'];?>" placeholder="Insurance Policy Number"/>
                  <label>Insurance Policy Amount   *</label>
                  <input type="text" name="ip_limit" value="<?= $persona_details_data['persona_insurance_amount'];?>" placeholder="Insurance Policy Amount"/>
                </div>
                <input type="button" name="previous" class="previous action-button-previous" value="Previous"/>
                <input type="submit" id="edit_persona_submit" name="edit_persona_submit" class="submit action-button" value="Submit"/>
            </fieldset>
        </form>
    </div>
</div>
<!-- /.MultiStep Form -->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js'></script>
<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/multistepform/js/msform.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<!-- <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script> -->

<script>

$(document).ready(function() {
  $("#ddl_department_id").change(function() {
		var department_id = $(this).val();
		console.log('department_id ', department_id);
		if(department_id == 0) {
			$("#sel_user").empty();
      $("#sel_user").append("  <option value='0'>- - Select User - -</option>");
			return false;
		}

		$.ajax({
			url: 'ajax/getUsers.php',
			data: {department_id: department_id},
			dataType: 'json',
			success: function(response) {
				console.log('hope i got all users ', response);

				$("#sel_user").empty();
        $("#sel_user").append("  <option value='0'>- - Select User - -</option>");
				for(var i=0; i< response.length; i++) {
					var id = response[i]['user_id'];
					var name = response[i]['user_name'];
          // var class = 'list-group-item';

					$("#sel_user").append("<option value='"+id+"'>"+name+"</option>");
					// $("#sel_user").append("<div id='"+id+"'>"+name+"</div>");
				}
			}
		})
	})
})

</script>
</body>
</html>
