<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
/* TBD - START */

// 1. Role dropdown should be from database

/* TBD - END */

/* DEFINES - START */
define('ADD_USER_FUNC_ID', '1');
/* DEFINES - END */

/* INCLUDES - START */

$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'tasks'.DIRECTORY_SEPARATOR.'general_task_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'da_user.php');

/* INCLUDES - END */



if ((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != "")) {

    // Session Data
    $user 		   = $_SESSION["loggedin_user"];
    $role 		   = $_SESSION["loggedin_role"];
    $loggedin_name = $_SESSION["loggedin_user_name"];

    // // Get permission settings for this user for this page

    $view_perms_list = i_get_user_perms($user, '', ADD_USER_FUNC_ID, '2', '1');

    $add_perms_list  = i_get_user_perms($user, '', ADD_USER_FUNC_ID, '1', '1');

    /* DATA INITIALIZATION - START */
    $alert = "";
    $alert_type = -1;
    /* DATA INITIALIZATION - END */


        if(isset($_GET["persona_salary_user_id"]))
        {
          $persona_user_id = $_GET["persona_salary_user_id"];
        }
        else
        {
          $persona_user_id = "";
        }

    // Capture the form data
    if (isset($_POST["edit_persona_salary_submit"])) {
        $persona_department_id   = $_POST["ddl_department_id"];
        $persona_user_id         = $_POST["sel_user"];
        $basic                   = $_POST["basic"];
        $hra                     = $_POST["hra"];
        $conveyance_allowance    = $_POST["conveyance_allowance"];
        $special_allowance       = $_POST["special_allowance"];
        $educational_allowance   = $_POST["educational_allowance"];
        $medical_reimbursement   = $_POST["medical_reimbursement"];
        $pf_amount               = $_POST["pf_amount"];
        $statutory_bonus         = $_POST["statutory_bonus"];
        $gratuity                = $_POST["gratuity"];
        $lta                     = $_POST["lta"];
        $pli                     = $_POST["pli"];
        $annual_benefits         = $_POST["annual_benefits"];
        $annual_ctc              = $_POST["annual_ctc"];
        $ddl_company             = $_POST["ddl_company"];
        $bank_name               = $_POST["bank_name"];
        $bank_account_number     = $_POST["bank_account_number"];
        $ifsc_code               = $_POST["ifsc_code"];
        $bank_branch             = $_POST["bank_branch"];
        $bank_city               = $_POST["bank_city"];
        $esi_employee_amount     = $_POST["esi_employee_amount"];
        $esi_company_amount      = $_POST["esi_company_amount"];

          if (($persona_department_id != "0") && ($persona_user_id != "0")  &&($basic != "0") && ($hra != "") && ($conveyance_allowance != "0") &&
          ($special_allowance != "0")  && ($educational_allowance != "0") && ($medical_reimbursement != "0") && ($pf_amount != "") &&
          ($statutory_bonus != "") && ($gratuity != "") && ($lta != "") && ($pli != "") && ($ddl_company != "") &&
          ($ifsc_code != "") && ($bank_branch != "") && ($bank_city != "")) {

              $isExists = db_persona_salary_drop($persona_user_id);

              $response = db_add_persona_salary($persona_department_id,
              $persona_user_id,
              $basic,
              $hra,
              $conveyance_allowance,
              $special_allowance,
              $educational_allowance,
              $medical_reimbursement,
              $pf_amount,
              $statutory_bonus,
              $gratuity,
              $lta,
              $pli,
              $ddl_company,
              $bank_name,
              $bank_account_number,
              $ifsc_code,
              $bank_branch,
              $bank_city,
              $esi_employee_amount,
              $esi_company_amount
                  );

              if ($response["status"] == 'SUCCESS') {
                  $_POST = array(); ?>
                    <script>
                    alert("Persona Updated successfully ");
                     console.log('saved values ', <?= json_encode($_POST); ?>);
                    </script>
                    <?php
              }
          } else {
              $alert = "Please fill all the mandatory fields";
              $alert_type = 0; ?>
          <script>
          alert("Please fill all the mandatory fields");
            console.log('values ', <?= json_encode($_POST); ?>);
          </script>
          <?php
          }
    }

    // Get list of Department*
    $department_list = i_get_department_list('', '');

    if ($department_list["status"] == SUCCESS) {
        $department_list_data = $department_list["data"];
    } else {
        $alert = $alert."Alert: ".$department_list["data"];
    }

    $designation_list = db_get_designation_list();
    if ($designation_list['status'] == 'DB_RECORD_ALREADY_EXISTS') {
        $designation_list_data = $designation_list['data'];
    }

    // Get list of Companies
    $stock_company_master_search_data = array();

    $company_list = i_get_company_list($stock_company_master_search_data);

    if ($company_list["status"] == SUCCESS) {
        $company_list_data = $company_list["data"];
    }

    // get current persona details
    $persona_details = db_get_persona_salary($persona_user_id);

    if($persona_details["status"] = "DB_RECORD_ALREADY_EXISTS") {
      $persona_details_data = $persona_details["data"][0];

      $user_list = i_get_user_list('', '', '', '', '1', $persona_details_data['persona_salary_department_id']);
      if ($user_list['status'] == SUCCESS) {
          $user_list_data = $user_list['data'];
      }

      $designation_list = db_get_designation_list();
      if($designation_list['status'] == 'DB_RECORD_ALREADY_EXISTS') {
        $designation_list_data = $designation_list['data'];
      }
    }



} else {
    header("location:login.php");
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">
    <title>Edit Persona Salary</title>
    <!-- Bootstrap core CSS -->
    <link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <!-- <link href="../../assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet"> -->
    <!-- Custom styles -->
    <link href="assets/multistepform/css/style.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
  <?php

  // include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');

  ?>
<!-- MultiStep Form -->
<div class="row">
    <div class="span8" style="margin-top:-50px; margin-bottom:50px">
        <form id="msform" method="post">
            <!-- progressbar -->
            <ul id="progressbar">
                <li class="active">Basic Details</li>
                <li>Salary Details</li>
                <li>Bank Details</li>
            </ul>
            <!-- fieldsets -->
            <fieldset>
                <h2 class="fs-title">Basic Details</h2>
                <h3 class="fs-subtitle">User Information</h3>
                <div class="col-xs-6" style="text-align:left">
                  <input type="hidden" id="ddl_department_id" name="ddl_department_id" value="<?= $persona_details_data['persona_salary_department_id'];?>" />

                  <div class="form-group">
                    <label style="font-weight:400">Department</label>
                    <select class="form-control" disabled>
                      <option value='0'>- - Select Department - -</option>
                      <?php
                        for ($count = 0; $count < count($department_list_data); $count++) {
                            ?>
                            <option value="<?php echo $department_list_data[$count]["general_task_department_id"]; ?>"
                              <?php if($persona_details_data['persona_salary_department_id'] == $department_list_data[$count]["general_task_department_id"]) {
                              ?> selected="selected"
                            <?php } ?>>
                              <?php echo $department_list_data[$count]["general_task_department_name"]; ?></option>
                      <?php
                        }
                      ?>
                    </select>
                  </div>
                  <input type="hidden" id="sel_user" name="sel_user" value="<?= $persona_details_data['persona_salary_user_id'];?>" />
                  <div class="form-group">
                    <label style="font-weight:400">User</label>
                    <select class="form-control" disabled>
                    <?php
                    for($count = 0; $count < count($user_list_data); $count++)
                    {
                    ?>
                    <option value="<?php echo $user_list_data[$count]["user_id"]; ?>" <?php if($persona_details_data["persona_salary_user_id"] == $user_list_data[$count]["user_id"]){ ?> selected <?php } ?>><?php echo $user_list_data[$count]["user_name"]; ?>
                    </option>
                    <?php
                    }
                    ?>

                    </select>
                  </div>
                  <label style="font-weight:400">Manager</label>
                  <input type="text" id="manager" name="manager" placeholder="DOJ" disabled/>
                  <label style="font-weight:400">DOJ</label>
                  <input type="text" id="doj" name="doj" placeholder="DOJ" disabled/>
              </div>
              <div class="col-xs-6" style="text-align:left">
                <label style="font-weight:400">Role</label>
                <input type="text" id="user_role" name="user_role" placeholder="Role" disabled/>
                <label style="font-weight:400">E-mail</label>
                <input type="text" id="user_email_id" name="user_email_id" placeholder="E-mail" disabled/>
                <label style="font-weight:400">Location</label>
                <input type="text" id="user_location" name="user_location" placeholder="Location" disabled/>
              </div>
                <input type="button" name="next" class="next action-button" value="Next"/>
                <br/>
            </fieldset>
            <fieldset>
                <h2 class="fs-title">Salary Details</h2>
                <h3 class="fs-subtitle">Salary Break-down Details</h3>
                <div class="col-xs-6" style="text-align:left">
                  <label style="font-weight:400">Basic</label>
                  <input type="number" value="<?= $persona_details_data['persona_salary_basic'];?>" min="0" id="basic" name="basic" onchange="return calculate()" placeholder="Basic"/>
                  <label style="font-weight:400">HRA</label>
                  <input type="number" value="<?= $persona_details_data['persona_salary_hra'];?>" min="0" id="hra" name="hra" onchange="return calculate()" placeholder="HRA"/>
                  <label style="font-weight:400">Conveyance Allowance</label>
                  <input type="number" value="<?= $persona_details_data['persona_salary_conveyance_allowance'];?>" min="0" id="conveyance_allowance" name="conveyance_allowance" onchange="return calculate()" placeholder="Conveyance Allowance"/>
                  <label style="font-weight:400">Special Allowance</label>
                  <input type="number" value="<?= $persona_details_data['persona_salary_special_allowance'];?>" min="0" id="special_allowance" name="special_allowance" onchange="return calculate()" placeholder="Special Allowance"/>
                  <label style="font-weight:400">Eduacational Allowance</label>
                  <input type="number" value="<?= $persona_details_data['persona_salary_educational_allowance'];?>" id="educational_allowance" name="educational_allowance" placeholder="Educational Allowance"/>
                  <label style="font-weight:400">Medical Reimbursement</label>
                  <input type="number" value="<?= $persona_details_data['persona_salary_medical_reimbursement'];?>" min="0" id="medical_reimbursement" name="medical_reimbursement" onchange="return calculate()" placeholder="Medical Reimbursement"/>
                  <!-- auto-calculate fields -->
                  <label style="font-weight:400">Gross Monthly</label>
                  <input type="number" min="0" id="gross_monthly" name="gross_monthly" placeholder="Gross Monthly" disabled/>
                  <label style="font-weight:400">Gross Annual</label>
                  <input type="number" min="0" id="gross_annual" name="gross_annual" placeholder="Gross Annual" disabled/>
                  <label style="font-weight:400">Employee Contribution ESI Amount</label>
                  <input type="number" id="esi_employee_amount" name="esi_employee_amount" value="<?= $persona_details_data['persona_esi_employee_amount'];?>" placeholder="Employee Contribution ESI Amount"/>
              </div>
                <div class="col-xs-6" style="text-align:left">

                  <label style="font-weight:400">Company Contribution ESI Amount</label>
                  <input type="number" id="esi_company_amount" name="esi_company_amount" value="<?= $persona_details_data['persona_esi_company_amount'];?>" placeholder="Company Contribution ESI Amount"/>
                  <label style="font-weight:400">Provident Fund Employer</label>
                  <input type="number" id="pf_amount" name="pf_amount" placeholder="PF Employer"/>
                  <label style="font-weight:400">Statutory Bonus</label>
                  <input type="number" id="statutory_bonus" name="statutory_bonus" placeholder="Statutory Bonus"/>
                  <label style="font-weight:400">Gratuity</label>
                  <input type="number" id="gratuity" name="gratuity" placeholder="Gratuity"/>
                  <label style="font-weight:400">LTA</label>
                  <input type="number" id="lta" name="lta" placeholder="LTA"/>
                  <label style="font-weight:400">PLI</label>
                  <input type="number" id="pli" name="pli" placeholder="PLI"/>
                  <label style="font-weight:400">Annual Benefits</label>
                  <input type="number" id="annual_benefits" name="annual_benefits" placeholder="Annual Benefits"/>
                  <label style="font-weight:400">Total CTC</label>
                  <input type="number" id="annual_ctc" name="annual_ctc" placeholder="Total CTC"/>
              </div>
                <input type="button" name="previous" class="previous action-button-previous" value="Previous"/>
                <input type="button" name="next" class="next action-button" value="Next"/>
            </fieldset>
            <fieldset>
                <h2 class="fs-title">Bank Details</h2>
                <h3 class="fs-subtitle">Salary crediting Bank details</h3>
                <div class="col-xs-6" style="text-align:left">
                  <div class="form-group">
                    <label style="font-weight:400">Company</label>
                    <select class="form-control" id="ddl_company" name="ddl_company" required>
                    <option value="">- - - Select Company - - -</option>
                    <?php
                    for ($count = 0; $count < count($company_list_data); $count++) {
                        ?>
                    <option value="<?php echo $company_list_data[$count]["stock_company_master_id"]; ?>" <?php if($persona_details_data["persona_salary_company"] == $company_list_data[$count]["stock_company_master_id"]){ ?> selected <?php } ?>><?php echo $company_list_data[$count]["stock_company_master_name"]; ?>
                    </option>
                    <?php
                    }
                    ?>
                    </select>
                  </div>
                  <label style="font-weight:400">Bank Name</label>
                  <input type="text" value="<?= $persona_details_data['persona_salary_bank_name'];?>" name="bank_name" placeholder="Bank Name"/>
                  <label style="font-weight:400">Account Number</label>
                  <input type="text" value="<?= $persona_details_data['persona_salary_bank_account_number'];?>" name="bank_account_number" placeholder="Bank Account Number"/>
                </div>
                <div class="col-xs-6" style="text-align:left">
                  <label style="font-weight:400">IFSC Code</label>
                  <input type="text" value="<?= $persona_details_data['persona_salary_ifsc_code'];?>" name="ifsc_code" placeholder="IFSC Code"/>
                  <label style="font-weight:400">Branch Name</label>
                  <input type="text" value="<?= $persona_details_data['persona_salary_bank_branch'];?>" name="bank_branch" placeholder="Branch"/>
                  <label style="font-weight:400">City</label>
                  <input type="text" value="<?= $persona_details_data['persona_salary_bank_city'];?>" name="bank_city" placeholder="City"/>
                </div>
                <input type="button" name="previous" class="previous action-button-previous" value="Previous"/>
                <input type="submit" id="edit_persona_salary_submit" name="edit_persona_salary_submit" class="submit action-button" value="Submit"/>
            </fieldset>
        </form>
    </div>
</div>
<!-- /.MultiStep Form -->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js'></script>
<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/multistepform/js/msform.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<!-- <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script> -->

<script>
function calculate(){
  console.log('am in calculate ');

  var basic = document.getElementById('basic').value ? document.getElementById('basic').value : 0,
      hra = document.getElementById('hra').value ? document.getElementById('hra').value : 0,
     conveyance_allowance = document.getElementById('conveyance_allowance').value ? document.getElementById('conveyance_allowance').value : 0,
     special_allowance = document.getElementById('special_allowance').value  ? document.getElementById('special_allowance').value : 0,
     educational_allowance = document.getElementById('special_allowance').value  ? document.getElementById('special_allowance').value : 0,
     medical_reimbursement = document.getElementById('medical_reimbursement').value ? document.getElementById('medical_reimbursement').value : 0;

     console.log('default ',basic, hra, conveyance_allowance, special_allowance, medical_reimbursement);

    var gross_monthly = parseInt(basic) + parseInt(hra) + parseInt(educational_allowance) + parseInt(conveyance_allowance) + parseInt(special_allowance) + parseInt(medical_reimbursement);
    document.getElementById('gross_monthly').value = gross_monthly;
    document.getElementById('gross_annual').value = gross_monthly * 12;

    var pf_amount = parseInt(((12 * parseInt(basic)) / 100) * 12);
    document.getElementById('pf_amount').value = pf_amount;

    var statutory_bonus = parseInt(((8.33 * parseInt(basic)) / 100) * 12);
    document.getElementById('statutory_bonus').value = statutory_bonus;

    var gratuity = parseInt(((4.81 * parseInt(basic)) / 100) * 12);
    document.getElementById('gratuity').value = gratuity;

    document.getElementById('lta').value = parseInt(basic);

    var annual_benefits = pf_amount + statutory_bonus + gratuity + parseInt(basic);
    document.getElementById('annual_benefits').value = annual_benefits;


    var annual_ctc = (gross_monthly * 12) + annual_benefits;
    document.getElementById('annual_ctc').value = annual_ctc;

    // var pli = ((20 * parseInt(annual_ctc)) / 100) * 12;
    document.getElementById('pli').value = 0;

}

$(document).ready(function() {
  var users;
  get_details();
  calculate();

  function get_details() {
    var user_id = <?= $persona_user_id; ?>;
    $.ajax({
      url: 'ajax/get_persona_basic_details.php',
      data: {user_id: user_id},
      dataType: 'json',
      success: function(response) {
        // console.log('hope users basic', response);
        $("#user_email_id").val(response[0]['user_email_id']);
        $("#user_role").val(get_user_role_name(response[0]['user_role']));
        $("#manager").val(response[0]['manager']);
        $("#user_location").val(response[0]['user_location']);
        $("#doj").val(response[0]['persona_dob']);
      }
    })
  }

function get_user_role_name(role_id) {

  var role = {
  1: 'Admin',
  2: 'Legal HOD',
  3: 'Legal User',
  4: 'Other User',
  5: 'Sales HOD',
  6: 'CRM Telecaller',
  7: 'CRM Sales',
  8: 'CRM Channel Partner',
  9: 'Cab Manager',
  10: 'CRM HOD',
  11: 'CRM User',
  12: 'Finance Manager',
  13: 'HR Manager'
  }
return role[role_id];
}



})

</script>
</body>
</html>
