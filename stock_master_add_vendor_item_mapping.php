<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 15th June 2015
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

/* TBD - START */
// 1. Module ID should be from config
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_vendor_functions.php');
/* INCLUDES - END */

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	
	if((isset($_GET['material'])) && ($_GET['material'] != ''))
	{
		$material_id = $_GET['material'];
	}
	else
	{
		$material_id = '-1';
	}

	// Capture the form data
	if(isset($_POST["add_process_user_submit"]))
	{
		$vendor   = $_POST["hd_vendor_id"];
		$material = $_POST["hd_material_id"];
		$remarks  = $_POST["txt_remarks"];
		
		// Check for mandatory fields
		if(($vendor !="") && ($material !=""))
		{
			$vendor_item_iresult = i_add_stock_vendor_item_mapping($vendor,$material,$remarks,$user);
			
			if($vendor_item_iresult["status"] == SUCCESS)
			{
				$alert_type = 1;
				header("location:stock_vendor_item_mapping_list.php");
			}
			else
			{
				$alert_type = 0;
			}
			
			$alert = $vendor_item_iresult["data"];
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}
	// Get Material Details
	$stock_material_search_data = array('material_id'=>$material_id);	
	
	$material_list = i_get_stock_material_master_list($stock_material_search_data);	
	if($material_list["status"] == SUCCESS)
	{
		$material_list_data = $material_list["data"];
		$material_name = $material_list_data[0]['stock_material_name'];
	}
	else
	{		
		$material_name = '';
	}
	
	// Get list of Vendors
	$stock_vendor_master_search_data = array();
	$vendor_list = i_get_stock_vendor_master_list($stock_vendor_master_search_data);
	if($vendor_list["status"] == SUCCESS)
	{
		$vendor_list_data = $vendor_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$vendor_list["data"];
		$alert_type = 0; // Failure
	}
}	
else
{
	header("location:login.php");
}
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Add Process User Mapping</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
    <link href="css/style1.css" rel="stylesheet">


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Map vendor to item</h3><span style="float:right; padding-right:20px;"><a href="stock_master_vendor_item_mapping_list.php">Vendor Item Mapping List</a></span>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Add Vendor Item Mapping</a>
						  </li>						  
						</ul>
						
						<br>
						    <div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="add_process_user_form" class="form-horizontal" method="post" action="stock_master_add_vendor_item_mapping.php">
								 <input type="hidden" name="hd_vendor_id" id="hd_vendor_id" value="" />
								 <input type="hidden" name="hd_material_id" id="hd_material_id" value="<?php echo $material_id; ?>" />
									<fieldset>										
																				
										<div class="control-group">											
											<label class="control-label" for="ddl_vendor">Vendor</label>
											<div class="controls">
											     <input type="text" name="stxt_vendor" class="span6" autocomplete="off" id="stxt_vendor" onkeyup="return get_vendor_list();" />
												 <div id="search_results" class="dropdown-content"></div>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->																												   
										
										<div class="control-group">											
											<label class="control-label" for="stxt_material">Item</label>
											<div class="controls">
												<input type="text" name="stxt_material" class="span6" autocomplete="off" id="stxt_material" onkeyup="return get_material_list();" value="<?php echo $material_name; ?>" />
												<div id="search_mresults" class="dropdown-content"></div>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="txt_remarks">Remarks</label>
											<div class="controls">
												<input type="text" class="span6" name="txt_remarks" placeholder="Remarks">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
											
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="add_process_user_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>																
								
							</div>
						  
						  
						</div>
						
						
						
						
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>

function get_vendor_list()
{ 
	var searchstring = document.getElementById('stxt_vendor').value;
	
	if(searchstring.length >= 3)
	{
		if (window.XMLHttpRequest)
		{// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp = new XMLHttpRequest();
		}
		else
		{// code for IE6, IE5
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}

		xmlhttp.onreadystatechange = function()
		{				
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
			{	
				if(xmlhttp.responseText != 'FAILURE')
				{
					document.getElementById('search_results').style.display = 'block';
					document.getElementById('search_results').innerHTML     = xmlhttp.responseText;
				}
				else
				{
					document.getElementById('search_results').style.display = 'none';
				}
			}
		}

		xmlhttp.open("POST", "ajax/stock_get_vendor.php");   // file name where delete code is written
		xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xmlhttp.send("search=" + searchstring);				
	}
	else	
	{
		document.getElementById('search_results').style.display = 'none';
	}
}

function select_vendor(vendor_id,vendor)
{
	document.getElementById('hd_vendor_id').value = vendor_id;
	
	document.getElementById('stxt_vendor').value = vendor;
	
	document.getElementById('search_results').style.display = 'none';
}
function get_material_list()
{ 
	var searchstring = document.getElementById('stxt_material').value;
	
	if(searchstring.length >= 3)
	{
		if (window.XMLHttpRequest)
		{// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp = new XMLHttpRequest();
		}
		else
		{// code for IE6, IE5
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}

		xmlhttp.onreadystatechange = function()
		{				
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
			{		
				if(xmlhttp.responseText != 'FAILURE')
				{
					document.getElementById('search_mresults').style.display = 'block';
					document.getElementById('search_mresults').innerHTML     = xmlhttp.responseText;
				}
				else
				{
					document.getElementById('search_mresults').style.display = 'none';
				}
			}
		}

		xmlhttp.open("POST", "ajax/get_material.php");   
		xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xmlhttp.send("search=" + searchstring);				
	}
	else	
	{
		document.getElementById('search_mresults').style.display = 'none';
	}
}

function select_material(material_id,material)
{
	document.getElementById('hd_material_id').value 	= material_id;

	document.getElementById('stxt_material').value = material;
	
	document.getElementById('search_mresults').style.display = 'none';
}

</script>

  </body>

</html>
