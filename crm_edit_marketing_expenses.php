<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 11th Sep 2016
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_marketing'.DIRECTORY_SEPARATOR.'crm_marketing_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_masters'.DIRECTORY_SEPARATOR.'crm_masters_functions.php');
/* INCLUDES - END */

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	
	// Navigation Data
	if(isset($_POST['hd_navigate_expense_id']))
	{
		$expense_id = $_POST['hd_navigate_expense_id'];
	}
	else
	{
		$expense_id = '-1';
	}

	// Capture the form data
	if(isset($_POST["edit_marketing_expensess_submit"]))
	{
		$expense_id            = $_POST["hd_navigate_expense_id"];		
		$applicable_start_date = $_POST["date_start"];
		$applicable_end_date   = $_POST["date_end"];
		$amount                = $_POST["num_amount"];
		$leads 				   = $_POST["num_leads"];	
		$contact_person		   = $_POST["stxt_contact_person"];	
		$contact_no			   = $_POST["stxt_contact_no"];	
		$contact_email		   = $_POST["stxt_email"];			
		$added_by			   = $user;
		
		// Check for mandatory fields
		if(($applicable_start_date != "") && ($applicable_end_date != "") && ($amount != "") && ($leads != ""))
		{
			if((strtotime($applicable_start_date)) <= (strtotime($applicable_end_date)))
			{
				$marketing_expenses_data = array('start_date'=>$applicable_start_date,'end_date'=>$applicable_end_date,'amount'=>$amount,'leads'=>$leads,'contact_person'=>$contact_person,'contact_no'=>$contact_no,'email'=>$contact_email,'updated_by'=>$user);
				$marketing_iresult = i_update_marketing_expenses($expense_id,$marketing_expenses_data);
				
				if($marketing_iresult["status"] == SUCCESS)
				{
					$alert_type = 1;
				}
				else
				{
					$alert_type = 0;
				}
				
				$alert = $marketing_iresult["data"];
			}
			else
			{
				$alert_type = 0;
				$alert      = 'Applicable start date cannot be later than applicable end date';
			}
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}	
	 
	// Get Marketing Expense Details
	$expense_details = i_get_marketing_expenses_list($expense_id,'','','','','','','','','');
	if($expense_details["status"] == SUCCESS)
	{
		$expense_details_data = $expense_details["data"];
	}
	else
	{
		$alert = $expense_details["data"];
		$alert_type = 0;
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Edit Marketing Expenses</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>
    
<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>    

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Marketing Expenses</h3><span style="float:right; padding-right:10px;"><strong>Tip:If it is for one day activity like newspaper ad, choose end date same as start date</strong></span>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Edit Marketing Expenses</a>
						  </li>						  
						</ul>
						
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="edit_marketing_expenses_form" class="form-horizontal" method="post" action="crm_edit_marketing_expenses.php">
								<input type="hidden" name="hd_navigate_expense_id" value="<?php echo $expense_id; ?>" />
									<fieldset>					
																																				
										<div class="control-group">											
											<label class="control-label" for="date_start">Marketing From Date*</label>
											<div class="controls">
												<input type="date" class="span6" name="date_start" required="required" value="<?php echo $expense_details_data[0]['crm_marketing_expenses_applicable_start_date']; ?>">
												<p class="help-block">Date from which marketing will start from this source</p>
											</div> <!-- /controls -->																
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="date_end">Marketing To Date*</label>
											<div class="controls">
												<input type="date" class="span6" name="date_end" required="required" value="<?php echo $expense_details_data[0]['crm_marketing_expenses_applicable_end_date']; ?>">
												<p class="help-block">Date until which marketing will happen from this source</p>
											</div> <!-- /controls -->												
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="num_amount">Amount*</label>
											<div class="controls">
												<input type="text" class="span6" name="num_amount" placeholder="Amount Payable" required="required" value="<?php echo $expense_details_data[0]['crm_marketing_expenses_amount']; ?>">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->

										<div class="control-group">											
											<label class="control-label" for="num_leads">Leads*</label>
											<div class="controls">
												<input type="text" class="span6" name="num_leads" placeholder="Number of leads promised. Enter 0 if no promise" required="required" value="<?php echo $expense_details_data[0]['crm_marketing_expenses_predicted_leads']; ?>">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->

										<div class="control-group">											
											<label class="control-label" for="stxt_contact_person">Contact Person</label>
											<div class="controls">
												<input type="text" class="span6" name="stxt_contact_person" placeholder="Contact Person from the source" value="<?php echo $expense_details_data[0]['crm_marketing_expenses_contact_person']; ?>">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="stxt_contact_no">Contact No</label>
											<div class="controls">
												<input type="text" class="span6" name="stxt_contact_no" placeholder="Contact Number of the above contact person" value="<?php echo $expense_details_data[0]['crm_marketing_expenses_contact_no']; ?>">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="stxt_email">Email</label>
											<div class="controls">
												<input type="text" class="span6" name="stxt_email" placeholder="Contact email of the above contact person" value="<?php echo $expense_details_data[0]['crm_marketing_expenses_email']; ?>">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->										
                                                                                                                                                               										 <br />										
											
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="edit_marketing_expensess_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>																
								
							</div>
						  
						  
						</div>
						
						
						
						
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
