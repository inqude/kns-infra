<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 31st Mar 2016
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

/* TBD - START */
// 1. Role dropdown should be from database
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'tasks'.DIRECTORY_SEPARATOR.'general_task_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');
/* INCLUDES - END */

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert = "";
	$alert_type = -1;
	/* DATA INITIALIZATION - END */
	
	/* QUERY STRING - START */
	if(isset($_GET["user"]))
	{
		$user_id = $_GET["user"];
	}
	else
	{
		$user_id = "";
	}
	/* QUERY STRING - END */

	// Capture the form data
	if(isset($_POST["edit_user_submit"]))
	{
		$user_id  		  = $_POST["hd_user"];
		$name     		  = $_POST["name"];
		$email   		  = $_POST["email"];		
		$user_role 		  = $_POST["role"];
		$department       = $_POST["ddl_department"];
		$location         = $_POST["ddl_location"];
		$manager  		  = $_POST["manager"];		
		// Check for mandatory fields
		if(($name !="") && ($email !="") && ($user_role !="") && ($department !="") && ($location != ""))
		{
			$user_data = array("name"=>$name,"email"=>$email,"role"=>$user_role,"department"=>$department,"manager"=>$manager,"location"=>$location);
			$edit_user_result = i_edit_user($user_id,$user_data);
			if($edit_user_result["status"] == SUCCESS)
			{				
				$alert_type = 1;
				header("location:user_list.php");
			}
			else
			{				
				$alert_type = 0;
			}
			$alert = $edit_user_result["data"];
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}

	// Get list of users
	$user_list = i_get_user_list('','','','');

	if($user_list["status"] == SUCCESS)
	{
		$user_list_data = $user_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$user_list["data"];
	}
	
	// Get user details
	$user_details = i_get_user_list($user_id,'','','');
	if($user_details["status"] == SUCCESS)
	{
		$user_details_data = $user_details["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$user_details["data"];
	}
	
	// Get list of Department*
	$department_list = i_get_department_list('','');

	if($department_list["status"] == SUCCESS)
	{
		$department_list_data = $department_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$department_list["data"];
	}
	
	// Get Location List
	$stock_location_master_search_data = array('location_id'=>$location_id);
	$location_list =  i_get_stock_location_master_list($stock_location_master_search_data);
	if($location_list['status'] == SUCCESS)
	{
		$location_list_data = $location_list['data'];		
	}	
	else
	{
		$alert = $location_list["data"];
		$alert_type = 0;		
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Add User</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Edit User Details</h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Edit User</a>
						  </li>						  
						</ul>
						
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="edit_user_form" class="form-horizontal" method="post" action="edit_user.php">
									<fieldset>
										<input type="hidden" name="hd_user" value="<?php echo $user_details_data[0]["user_id"]; ?>" />
																				
										<div class="control-group">											
											<label class="control-label" for="name">Name</label>
											<div class="controls">
												<input type="text" class="span6" name="name" placeholder="User's full name" required="required" value="<?php echo $user_details_data[0]["user_name"]; ?>">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->																														
										<div class="control-group">											
											<label class="control-label" for="email">Email Address</label>
											<div class="controls">
												<input type="text" class="span6" name="email" required="required" value="<?php echo $user_details_data[0]["user_email_id"]; ?>">
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="ddl_department">Department*</label>
											<div class="controls">
												<select name="ddl_department">
												<?php
												for($count = 0; $count < count($department_list_data); $count++)
												{
												?>
												<option value="<?php echo $department_list_data[$count]["general_task_department_id"]; ?>" <?php if($department_list_data[$count]["general_task_department_id"] == $user_details_data[0]["user_department"]){ ?> selected <?php } ?>><?php echo $department_list_data[$count]["general_task_department_name"]; ?></option>
												<?php
												}
												?>
												</select>
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="ddl_location">Location*</label>
											<div class="controls">
												<select name="ddl_location" required>
												<option value="">- - Select Location - -</option>
												<?php
												for($count = 0; $count < count($location_list_data); $count++)
												{
												?>
												<option value="<?php echo $location_list_data[$count]["stock_location_id"]; ?>" <?php if($location_list_data[$count]["stock_location_id"] == $user_details_data[0]["user_location"]){ ?> selected <?php } ?>><?php echo $location_list_data[$count]["stock_location_name"]; ?></option>
												<?php
												}
												?>
												</select>
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="manager">Manager*</label>
											<div class="controls">
												<select name="manager">
												<?php
												for($count = 0; $count < count($user_list_data); $count++)
												{
												?>
												<option value="<?php echo $user_list_data[$count]["user_id"]; ?>" <?php if($user_details_data[0]["user_manager"] == $user_list_data[$count]["user_id"]){ ?> selected <?php } ?>><?php echo $user_list_data[$count]["user_name"]; ?></option>
												<?php
												}
												?>
												</select>
												<p class="help-block">Reporting Manager of the user</p>
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="role">Role*</label>
											<div class="controls">
												<select name="role">
												<option value="1" <?php if($user_details_data[0]["user_role"] == "1"){ ?> selected <?php } ?>>Admin</option>
												<option value="2" <?php if($user_details_data[0]["user_role"] == "2"){ ?> selected <?php } ?>>Legal HOD</option>
												<option value="3" <?php if($user_details_data[0]["user_role"] == "3"){ ?> selected <?php } ?>>Legal User</option>
												<option value="4" <?php if($user_details_data[0]["user_role"] == "4"){ ?> selected <?php } ?>>Other User</option>
												<option value="5" <?php if($user_details_data[0]["user_role"] == "5"){ ?> selected <?php } ?>>Sales HOD</option>
												<option value="6" <?php if($user_details_data[0]["user_role"] == "6"){ ?> selected <?php } ?>>CRM Telecaller</option>
												<option value="7" <?php if($user_details_data[0]["user_role"] == "7"){ ?> selected <?php } ?>>CRM Sales</option>
												<option value="8" <?php if($user_details_data[0]["user_role"] == "8"){ ?> selected <?php } ?>>CRM Channel Partner</option>
												<option value="9" <?php if($user_details_data[0]["user_role"] == "9"){ ?> selected <?php } ?>>Cab Manager</option>
												<option value="10" <?php if($user_details_data[0]["user_role"] == "10"){ ?> selected <?php } ?>>CRM HOD</option>
												<option value="11" <?php if($user_details_data[0]["user_role"] == "11"){ ?> selected <?php } ?>>CRM User</option>
												<option value="12" <?php if($user_details_data[0]["user_role"] == "12"){ ?> selected <?php } ?>>Finance Manager</option>
												<option value="13" <?php if($user_details_data[0]["user_role"] == "13"){ ?> selected <?php } ?>>HR Manager</option>
												</select>
												<p class="help-block">User role</p>
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
                                                                                                                                                               										 <br />
										
											
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="edit_user_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>																
								
							</div>
						  
						  
						</div>
						
						
						
						
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
