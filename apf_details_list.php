<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: survey_details_list.php
CREATED ON	: 05-Jan-2017
CREATED BY	: Lakshmi
PURPOSE     : List of project for customer withdrawals
*/
/* DEFINES - START */
define('APF_DETAILS_LIST_FUNC_ID','319');
/* DEFINES - END */

/*
TBD: 
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'apf_masters'.DIRECTORY_SEPARATOR.'apf_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_masters'.DIRECTORY_SEPARATOR.'bd_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_projects'.DIRECTORY_SEPARATOR.'bd_project_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

	$alert_type = -1;
	$alert = "";
if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];
	
	// Get permission settings for this user for this page
	$add_perms_list    = i_get_user_perms($user,'',APF_DETAILS_LIST_FUNC_ID,'1','1');
	$view_perms_list   = i_get_user_perms($user,'',APF_DETAILS_LIST_FUNC_ID,'2','1');
	$edit_perms_list   = i_get_user_perms($user,'',APF_DETAILS_LIST_FUNC_ID,'3','1');
	$delete_perms_list = i_get_user_perms($user,'',APF_DETAILS_LIST_FUNC_ID,'4','1');

	// Query String Data
	// Nothing
	
	if(isset($_GET['apf_id']))
	{
		$apf_id = $_GET['apf_id'];
	}
	else
	{
		$apf_id = "";
	}
	
	if(isset($_GET["apf_process_id"]))
	{
		$process_id = $_GET["apf_process_id"];
	}
	else
	{
		$process_id = "";
	}
	
	if(isset($_GET['bank_id']))
	{
		$bank = $_GET['bank_id'];
	}
	else
	{
		$bank = "";
	}
	
	if(isset($_GET['source']))
	{
		$source = $_GET['source'];
	}
	else
	{
		$source = "";
	}
	
	$search_project    = "";
	$search_bank       = "";
	$search_village    = "";
	
	if(isset($_POST['file_search_submit']))
	{
		$search_project    = $_POST["search_project"];
		$search_bank       = $_POST["search_bank"];
		$search_village    = $_POST["search_village"];
		$apf_id            = $_POST["hd_apf_id"];
	}
	
	// Get APF Process Master modes already added
	$apf_process_master_search_data = array("active"=>'1',"process_id"=>$process_id);
	$apf_process_master_list = i_get_apf_process_master($apf_process_master_search_data);
	if($apf_process_master_list['status'] == SUCCESS)
	{
		$apf_process_master_list_data = $apf_process_master_list['data'];
	}	

	else
	{
		$alert = $apf_process_master_list["data"];
		$alert_type = 0;
	}
	
	// Get Bank Master modes already added
	$apf_bank_master_search_data = array("active"=>'1');
	$bank_master_list = i_get_apf_bank_master($apf_bank_master_search_data);
	if($bank_master_list['status'] == SUCCESS)
	{
		$bank_master_list_data = $bank_master_list["data"];
	}
	else
	{
		$alert = $bank_master_list["data"];
		$alert_type = 0;
	}
	
	// Get Village Master modes already added
	$village_master_list = i_get_village_list('');
	if($village_master_list['status'] == SUCCESS)
	{
		$village_master_list_data = $village_master_list["data"];
	}
	else
	{
		$alert = $village_master_list["data"];
		$alert_type = 0;
	}
	
	
	// Get Project Master already added
	$apf_project_master_search_data = array("active"=>'1');
	$apf_project_master_list = i_get_apf_project_master($apf_project_master_search_data);
	if($apf_project_master_list['status'] == SUCCESS)
	{
		$apf_project_master_list_data = $apf_project_master_list['data'];
	}
	else
	{
		$alert = $apf_project_master_list["data"];
		$alert_type = 0;
	}
	

	// Get APF Details already added
	$apf_details_search_data = array("active"=>'1',"apf_id"=>$apf_id,"bank"=>$bank,"project_name"=>$search_project,"bank_name"=>$search_bank,"village"=>$search_village);
	$apf_details_list = i_get_apf_details($apf_details_search_data);
	if($apf_details_list['status'] == SUCCESS)
	{
		$apf_details_list_data = $apf_details_list['data'];
	}
	
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>APF Details List</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>
    

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>APF Details List</h3><?php if($add_perms_list['status'] == SUCCESS){?><span style="float:right; padding-right:20px;"><a href="apf_add_details.php">APF Add Details</a></span><?php } ?>
            </div>
            <!-- /widget-header -->
			
			<div class="widget-header" style="height:50px; padding-top:10px;">
			<form method="post" id="file_search_form" action="apf_details_list.php">
			<input type="hidden" name="hd_apf_id" value="<?php echo $apf_id; ?>" />
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_project">
			  <option value="">- - Select Project - -</option>
			  <?php
			  for($apf_count = 0; $apf_count < count($apf_project_master_list_data); $apf_count++)
			  {
			  ?>
			  <option value="<?php echo $apf_project_master_list_data[$apf_count]["apf_project_master_id"]; ?>" <?php if($search_project == $apf_project_master_list_data[$apf_count]["apf_project_master_id"]) { ?> selected="selected" <?php } ?>><?php echo $apf_project_master_list_data[$apf_count]["apf_project_master_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>
			  
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_bank">
			  <option value="">- - Select Bank - -</option>
			  <?php
			  for($bank_count = 0; $bank_count < count($bank_master_list_data); $bank_count++)
			  {
			  ?>
			  <option value="<?php echo $bank_master_list_data[$bank_count]["apf_bank_master_id"]; ?>" <?php if($search_bank == $bank_master_list_data[$bank_count]["apf_bank_master_id"]) { ?> selected="selected" <?php } ?>><?php echo $bank_master_list_data[$bank_count]["apf_bank_master_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>
			  
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_village">
			  <option value="">- - Select Village - -</option>
			  <?php
			  for($village_count = 0; $village_count < count($village_master_list_data); $village_count++)
			  {
			  ?>
			  <option value="<?php echo $village_master_list_data[$village_count]["village_id"]; ?>" <?php if($search_village == $village_master_list_data[$village_count]["village_id"]) { ?> selected="selected" <?php } ?>><?php echo $village_master_list_data[$village_count]["village_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>
			  <input type="submit" name="file_search_submit" />
			  </form>
			  </div>
            <div class="widget-content">
			<?php
			if($view_perms_list['status'] == SUCCESS)
			{
			?>
              <table class="table table-bordered">
                <thead>
                  <tr>
				    <th>SL No</th>
					<th>Project</th>
					<th>Village</th>
					<th>Bank</th>
					<th>Extent</th>
					<th>Planned Start Date</th>
					<th>Planned End Date</th>
					<th>Remarks</th>
					<th>Added By</th>					
					<th>Added On</th>									
					<th colspan="5" style="text-align:center;">Actions</th>
    					
				</tr>
				</thead>
				<tbody>							
				<?php
				if($apf_details_list["status"] == SUCCESS)
				{
					$sl_no = 0;
					for($count = 0; $count < count($apf_details_list_data); $count++)
					{
						$sl_no++;
							
						// Get Project Master already added
						$apf_project_master_search_data = array("active"=>'1',$apf_details_list_data[$count]["apf_details_project"]);
						$apf_project_master_list = i_get_apf_project_master($apf_project_master_search_data);
						if($apf_project_master_list['status'] == SUCCESS)
						{
							$apf_project_master_list_data = $apf_project_master_list["data"];
							$project_name = $apf_project_master_list_data[0]["apf_project_master_name"];
						}
						
						// Get Survey File already added
						$apf_file_search_data = array("active"=>'1',$apf_details_list_data[$count]["apf_details_id"]);
						$apf_file_list = i_get_apf_file($apf_file_search_data);
						if($apf_file_list['status'] == SUCCESS)
						{
							$apf_file_list_data = $apf_file_list["data"];
							$extent = $apf_file_list_data[0]["apf_survey_master_extent"];
						}
						else
						{
							$extent = '';
						}
									
						?>
						<tr>
						<td><?php echo $sl_no; ?></td>
						<td><?php echo $apf_details_list_data[$count]["apf_project_master_name"]; ?></td>
						<td><?php echo $apf_details_list_data[$count]["village_name"]; ?></td>
						<td><?php echo $apf_details_list_data[$count]["apf_bank_master_name"]; ?></td>
						<td><?php echo $extent; ?></td>
						<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($apf_details_list_data[$count][
						"apf_details_planned_start_date"])); ?></td>
						<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($apf_details_list_data[$count][
						"apf_details_planned_end_date"])); ?></td>
						<td><?php echo $apf_details_list_data[$count]["apf_details_remarks"]; ?></td>
						<td><?php echo $apf_details_list_data[$count]["user_name"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($apf_details_list_data[$count][
						"apf_details_added_on"])); ?></td>
						<td style="word-wrap:break-word;"><?php if($edit_perms_list['status'] == SUCCESS){?><a style="padding-right:10px" href="#" onclick="return go_to_apf_edit_details('<?php echo $apf_details_list_data[$count]["apf_details_id"]; ?>');">Edit</a><?php } ?></td>
						<td><?php if(($apf_details_list_data[$count]["apf_details_active"] == "1") && ($delete_perms_list['status'] == SUCCESS)){?><a href="#" onclick="return apf_delete_details(<?php echo $apf_details_list_data[$count]["apf_details_id"]; ?>);">Delete</a><?php } ?></td>
						<td style="word-wrap:break-word;"><?php if($view_perms_list['status'] == SUCCESS){?><a style="padding-right:10px" href="#" onclick="return go_to_apf_process('<?php echo $apf_details_list_data[$count]["apf_details_id"]; ?>','<?php echo $apf_details_list_data[$count]["apf_details_project"]; ?>','view');">Add APF Process</a><?php } ?></td>
						<td style="word-wrap:break-word;"><?php if($view_perms_list['status'] == SUCCESS){?><a style="padding-right:10px" href="#" onclick="return go_to_apf_file('<?php echo $apf_details_list_data[$count]["apf_details_id"]; ?>','view','<?php echo $apf_details_list_data[$count]["apf_details_project"]; ?>','<?php echo $apf_details_list_data[$count]["apf_details_village"]; ?>');">Survey File List</a><?php } ?></td>
						</tr>
						<?php
					}
					
				}
				else
				{
				?>
				<td colspan="6">No APF Details added yet!</td>
				
				<?php
				}
				 ?>	

                </tbody>
              </table>
			<?php
			}
			else
			{
				echo 'You are not authorized to view this page';
			}
			?>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function apf_delete_details(apf_id)
{
	var ok = confirm("Are you sure you want to Delete?")
	{         
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{					
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else					
					{
					 window.location = "apf_details_list.php";
					}
				}
			}

			xmlhttp.open("POST", "ajax/apf_delete_details.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("apf_id=" + apf_id + "&action=0");
		}
	}	
}
function go_to_apf_edit_details(apf_id)
{		
	var form = document.createElement("form");
    form.setAttribute("method", "GET");
    form.setAttribute("action", "apf_edit_details.php");
	
	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","apf_id");
	hiddenField1.setAttribute("value",apf_id);
	
	form.appendChild(hiddenField1);
	
	document.body.appendChild(form);
    form.submit();
}

function go_to_apf_process(apf_id,project,source)
{		
	var form = document.createElement("form");
    form.setAttribute("method", "GET");
    form.setAttribute("action", "apf_process_list.php");
	
	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","apf_id");
	hiddenField1.setAttribute("value",apf_id);
	
	var hiddenField2 = document.createElement("input");
	hiddenField2.setAttribute("type","hidden");
	hiddenField2.setAttribute("name","project");
	hiddenField2.setAttribute("value",project);
	
	var hiddenField3 = document.createElement("input");
	hiddenField3.setAttribute("type","hidden");
	hiddenField3.setAttribute("name","source");
	hiddenField3.setAttribute("value",source);
	
	form.appendChild(hiddenField1);
	form.appendChild(hiddenField2);
	form.appendChild(hiddenField3);
	
	document.body.appendChild(form);
    form.submit();
}

function go_to_apf_file(apf_id,source,project,village)
{		
	var form = document.createElement("form");
    form.setAttribute("method", "GET");
    form.setAttribute("action", "apf_file_list.php");
	
	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","apf_id");
	hiddenField1.setAttribute("value",apf_id);
	
	var hiddenField2 = document.createElement("input");
	hiddenField2.setAttribute("type","hidden");
	hiddenField2.setAttribute("name","source");
	hiddenField2.setAttribute("value",source);
	
	var hiddenField3 = document.createElement("input");
	hiddenField3.setAttribute("type","hidden");
	hiddenField3.setAttribute("name","project");
	hiddenField3.setAttribute("value",project);
	
	var hiddenField4 = document.createElement("input");
	hiddenField4.setAttribute("type","hidden");
	hiddenField4.setAttribute("name","village");
	hiddenField4.setAttribute("value",village);
	
	form.appendChild(hiddenField1);
	form.appendChild(hiddenField2);
	form.appendChild(hiddenField3);
	form.appendChild(hiddenField4);
	
	document.body.appendChild(form);
    form.submit();
}

function go_to_apf_documents(details_id)
{		
	var form = document.createElement("form");
    form.setAttribute("method", "GET");
    form.setAttribute("action", "apf_document_list.php");
	
	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","details_id");
	hiddenField1.setAttribute("value",details_id);
	
	form.appendChild(hiddenField1);
	
	document.body.appendChild(form);
    form.submit();
}

</script>

  </body>

</html>