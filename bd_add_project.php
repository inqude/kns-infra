<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 10th Nov 2015
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */
$_SESSION['module'] = 'BD';

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_projects'.DIRECTORY_SEPARATOR.'bd_project_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_masters'.DIRECTORY_SEPARATOR.'bd_masters_functions.php');
/* INCLUDES - END */

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */

	// Capture the form data
	if(isset($_POST["add_bd_project_submit"]))
	{
		$project_name = $_POST["stxt_bd_project_name"];		
		$location     = $_POST["stxt_bd_project_location"];
		$start_date   = $_POST["dt_start_date"];
		$gfs_month    = $_POST["dt_gfs_month"];
		$launch_month = $_POST["dt_launch_date"];
		$dev_time     = $_POST["stxt_dev_time"];
		$sale_time    = $_POST["stxt_sales_duration"];
		$file_path    = upload("file_plan",$user);
		$fund_source  = $_POST["ddl_funding_source"];		
		
		// Check for mandatory fields
		if(($project_name !="") && ($location !="") && ($fund_source !=""))
		{
			if(strtotime($gfs_month) <= strtotime($launch_month))
			{
				$project_iresult = i_add_bd_project($project_name,$location,$start_date,$gfs_month,$launch_month,$dev_time,$sale_time,$file_path,$fund_source,$user);
				
				if($project_iresult["status"] == SUCCESS)
				{
					$alert_type = 1;
				}
				else
				{
					$alert_type = 0;
				}
				
				$alert = $project_iresult["data"];
			}
			else
			{
				$alert_type = 0;
				$alert = "Launch Date cannot be earlier than GFS date";
			}
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}
	
	// Get funding source list
	$fund_source_list = i_get_fund_source('','1');
	if($fund_source_list["status"] == SUCCESS)
	{
		$fund_source_list_data = $fund_source_list["data"];
	}
	else
	{
		$alert_type = 1;
		$alert = $fund_source_list["data"];
	}
}
else
{
	header("location:login.php");
}	

// Functions
function upload($file_id,$user_id) 
{
	if($_FILES[$file_id]["name"]!="")
	{
		if ($_FILES[$file_id]["error"] > 0)
		{
			// echo "Return Code: " . $_FILES[$file_id]["error"] . "<br />";
		}
		else
		{
			$_FILES[$file_id]["name"]=$user_id."_".$_FILES[$file_id]["name"];
			move_uploaded_file($_FILES[$file_id]["tmp_name"], "bd_files".DIRECTORY_SEPARATOR.$_FILES[$file_id]["name"]);							  
		}
	}
	
	return $_FILES[$file_id]["name"];
}
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Add BD Project</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Add BD Project</h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Add BD project Details</a>
						  </li>						  
						</ul>
						
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="add_bd_project" class="form-horizontal" method="post" action="bd_add_project.php" enctype="multipart/form-data">
									<fieldset>										
																				
										<div class="control-group">											
											<label class="control-label" for="stxt_bd_project_name">Project Name*</label>
											<div class="controls">
												<input type="text" class="span6" name="stxt_bd_project_name" placeholder="Name of the project. Ex: Donnenahalli - Unnamed" required="required">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->																				
                                                                                                                                                               										   <br />
																																																	<div class="control-group">											
											<label class="control-label" for="stxt_bd_project_location">Project Location*</label>
											<div class="controls">
												<input type="text" class="span6" name="stxt_bd_project_location" placeholder="Village in which the project is planned" required="required">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->  
										
										<div class="control-group">											
											<label class="control-label" for="dt_start_date">Project Start Date</label>
											<div class="controls">
												<input type="date" class="span6" name="dt_start_date">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="dt_gfs_month">GFS Month (Ready for launch)</label>
											<div class="controls">
												<input type="date" class="span6" name="dt_gfs_month">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="dt_launch_date">Launch Date</label>
											<div class="controls">
												<input type="date" class="span6" name="dt_launch_date">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="stxt_dev_time">Development Time</label>
											<div class="controls">
												<input type="text" class="span6" name="stxt_dev_time" placeholder="In months">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="stxt_sales_duration">Sales Time</label>
											<div class="controls">
												<input type="text" class="span6" name="stxt_sales_duration" placeholder="In months">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->

										<div class="control-group">											
											<label class="control-label" for="file_plan">Plan</label>
											<div class="controls">
												<input type="file" class="span6" name="file_plan">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->

										<div class="control-group">											
											<label class="control-label" for="ddl_funding_source">Funding Source*</label>
											<div class="controls">
												<select name="ddl_funding_source" required>
												<?php
												for($count = 0; $count < count($fund_source_list_data); $count++)
												{
												?>
												<option value="<?php echo $fund_source_list_data[$count]["bd_fund_source_id"]; ?>"><?php echo $fund_source_list_data[$count]["bd_fund_source_name"]; ?></option>
												<?php
												}
												?>
												</select>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->	
											
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="add_bd_project_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>																
								
							</div>
						  
						  
						</div>
						
						
						
						
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
/* Open the sidenav */
function openNav() {
    document.getElementById("mySidenav").style.width = "75%";
}

/* Close/hide the sidenav */
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}
</script>

  </body>

</html>
