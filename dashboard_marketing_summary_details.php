<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: process_list.php
CREATED ON	: 05-June-2015
CREATED BY	: Nitin Kashyap
PURPOSE     : List of Process Plans
*/

/*
TBD: 
1. Date display and calculation
2. Permission management
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_masters'.DIRECTORY_SEPARATOR.'crm_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_transaction_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_post_sales_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Query String Data
	if(isset($_GET['source_type']))
	{
		$source_type = $_GET['source_type'];
	}
	else
	{
		$source_type = '';
	}
	
	if(isset($_GET['source_type_name']))
	{
		$source_type_name = $_GET['source_type_name'];
	}
	else
	{
		$source_type_name = '';
	}
	
	// Get list of enquiry sources
	$enquiry_source_list = i_get_enquiry_source_list('','1','',$source_type);
	if($enquiry_source_list["status"] == SUCCESS)
	{
		$enquiry_source_list_data = $enquiry_source_list["data"];
	}
	else
	{
		$alert = $enquiry_source_list["data"];
		$alert_type = 0;
	}	
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Marketing Report</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header" style="height:50px; padding-top:10px;"> <i class="icon-th-list"></i>
              <h3>Marketing Report - <?php echo $source_type_name; ?>&nbsp;&nbsp;&nbsp;Total Leads: <span id="total_num_leads"><i>Calculating</i></span>&nbsp;&nbsp;&nbsp;Total Sales: <span id="total_num_sales"><i>Calculating</i></span></h3>			  
            </div>
            <!-- /widget-header -->
			
            <div class="widget-content">
              <table class="table table-bordered" style="width:70%;">
                <thead>
                  <tr>
				    <th width="8%" style="word-wrap:beak-word;">SL No</th>					
					<th width="30%" style="word-wrap:beak-word;">Source</th>						
					<th width="23%" style="word-wrap:beak-word;">Source Type</th>						
					<th width="13%" style="word-wrap:beak-word;">No. of leads</th>
					<th width="13%" style="word-wrap:beak-word;">No. of sales</th>
					<th width="13%" style="word-wrap:beak-word;">Performance</th>
				</tr>
				</thead>
				<tbody>
				<?php					
				$sl_no = 0;				
				$total_num_leads   = 0;				
				if($enquiry_source_list["status"] == SUCCESS)
				{									
					$sorted_array_count = 0;
					for($count = 0; $count < count($enquiry_source_list_data); $count++)
					{
						// No of leads generated from this source
						$enquiry_sresult = i_get_enquiry_list('','','','','','','','',$enquiry_source_list_data[$count]['enquiry_source_master_id'],'','','','','','','','','','','');
						if($enquiry_sresult['status'] == SUCCESS)
						{
							$number_of_leads = count($enquiry_sresult['data']);							
						}
						else
						{
							$number_of_leads = 0;
						}	
						$enquiry_source_list_data[$count]['lcount'] = $number_of_leads;

						// No of sales done through leads generated from this source
						$booking_sresult = i_get_site_booking('','','','','','1','','','','','','','','','','','','','',$enquiry_source_list_data[$count]['enquiry_source_master_id'],'','');						
						if($booking_sresult['status'] == SUCCESS)
						{
							$number_of_sales = count($booking_sresult['data']);
						}
						else
						{
							$number_of_sales = 0;
						}
						$enquiry_source_list_data[$count]['scount'] = $number_of_sales;
						
						$enquiry_source_list_data[$count]['pcount'] = $number_of_sales/$number_of_leads;
					}	

					array_sort_conditional($enquiry_source_list_data,'sort_on_number');	
					
					for($count = 0; $count < count($enquiry_source_list_data); $count++)
					{
						$sl_no = $sl_no + 1;
						
						// Initialize						
						$number_of_leads = 0;
						$number_of_sales = 0;						
																
						// No of leads generated from this source
						$number_of_leads = $enquiry_source_list_data[$count]['lcount'];
						$total_num_leads = $total_num_leads + $number_of_leads;	
							
						// No of sales generated from this source
						$number_of_sales = $enquiry_source_list_data[$count]['scount'];
						$total_num_sales = $total_num_sales + $number_of_sales;													
					?>
					<tr id="row_<?php echo $count; ?>" style="color:black;">
						<td style="word-wrap:break-word;"><?php echo $sl_no; ?></td>
						<td style="word-wrap:break-word;"><?php echo $enquiry_source_list_data[$count]['enquiry_source_master_name']; ?></td>
						<td style="word-wrap:break-word;"><?php echo $enquiry_source_list_data[$count]['enquiry_source_type_name']; ?></td>
						<td style="word-wrap:break-word;"><?php echo $number_of_leads; ?></td>
						<td style="word-wrap:break-word;"><?php echo $number_of_sales; ?></td>
						<td style="word-wrap:break-word;"><?php echo round(($enquiry_source_list_data[$count]['pcount']*100),2); ?>%</td>
					</tr>								
					<?php						
					}
				}
				else
				{
				?>
				<td colspan="4">No marketing source added!</td>
				<?php
				}
				?>	

                </tbody>
              </table>			  
			  <script>
			  document.getElementById('total_num_leads').innerHTML = <?php echo $total_num_leads; ?>;
			  document.getElementById('total_num_sales').innerHTML = <?php echo $total_num_sales; ?>;
			  </script>
            </div>
			
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    
<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>

  </body>

</html>
