<?php
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'status_codes.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'connection.php');

/*
LIST OF TBDs
1. Wherever there is name search, there should be a wildcard
*/

/*
PURPOSE : To add new site travel plan
INPUT 	: Site Visit Plan ID, Cab ID, Project, Date Time, Added By
OUTPUT 	: Site Travel Plan ID, success or failure message
BY 		: Nitin Kashyap
*/
function db_add_site_travel_plan($site_visit_plan_id,$cab_id,$project,$date_time,$added_by)
{
	// Query
    $stp_iquery = "insert into crm_site_travel_plan (crm_site_travel_plan_svp,crm_site_travel_plan_cab,crm_site_travel_plan_project,crm_site_travel_plan_date,crm_site_travel_plan_status,crm_site_travel_actual_cab,crm_site_travel_plan_added_by,crm_site_travel_plan_added_on) values (:svp_id,:cab_id,:project,:date_time,:status,:actual_cab,:added_by,:now)";
    try
    {
        $dbConnection = get_conn_handle();

        $stp_istatement = $dbConnection->prepare($stp_iquery);

        // Data
        $stp_idata = array(':svp_id'=>$site_visit_plan_id,':cab_id'=>$cab_id,':project'=>$project,':date_time'=>$date_time,':status'=>'1',':actual_cab'=>'',':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));
		$dbConnection->beginTransaction();
        $stp_istatement->execute($stp_idata);
		$stp_id = $dbConnection->lastInsertId();
		$dbConnection->commit();

        $return["status"] = SUCCESS;
		$return["data"]   = $stp_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }

    return $return;
}

/*
PURPOSE : To add new site travel actual
INPUT 	: Date, Enquiry ID, Project, Vehicle Details, Opening Km, Closing Km, Driver, No. of passengers, Remarks
OUTPUT 	: Site Travel Actual ID, success or failure message
BY 		: Nitin Kashyap
*/
function db_add_site_travel_actual($site_travel_date,$enquiry_id,$project,$vehicle,$opening_km,$closing_km,$driver,$num_passengers,$remarks,$added_by)
{
	// Query
    $sta_iquery = "insert into crm_site_travel_actual (crm_site_travel_actual_date,crm_site_travel_actual_enquiry,crm_site_travel_actual_project,crm_site_travel_actual_vehicle_number,crm_site_travel_actual_opening_km,crm_site_travel_actual_closing_km,crm_site_travel_actual_driver,crm_site_travel_actual_num_passengers,crm_site_travel_actual_remarks,crm_site_travel_actual_added_by,crm_site_travel_actual_added_on) values (:date,:enquiry,:project,:vehicle,:opening,:closing,:driver,:num_passengers,:remarks,:added_by,:now)";

    try
    {
        $dbConnection = get_conn_handle();

        $sta_istatement = $dbConnection->prepare($sta_iquery);

        // Data
        $sta_idata = array(':date'=>$site_travel_date,':enquiry'=>$enquiry_id,':project'=>$project,':vehicle'=>$vehicle,':opening'=>$opening_km,':closing'=>$closing_km,':driver'=>$driver,':num_passengers'=>$num_passengers,':remarks'=>$remarks,':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));
		$dbConnection->beginTransaction();
        $sta_istatement->execute($sta_idata);
		$sta_id = $dbConnection->lastInsertId();
		$dbConnection->commit();

        $return["status"] = SUCCESS;
		$return["data"]   = $sta_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }

    return $return;
}

/*
PURPOSE : To get site travel plan list
INPUT 	: Travel Plan ID, Site Visit ID, Enquiry ID, Cab, Project, Date, Status, Assigned To, Added By, Start Date, End Date, Relationship, Project Mapped User
OUTPUT 	: List of travel plan
BY 		: Nitin Kashyap
REMARKS : Email and Company are accepted as inputs. Processing to be done later.
*/
function db_get_site_travel_plan($travel_plan_id,$site_visit_id,$enquiry_id,$cab,$project,$date,$status,$assigned_to,$added_by,$start_date,$end_date,$relationship="",$travel_start_date,$travel_end_date,$project_user='')
{
	$get_stp_squery_base = "select * from crm_site_travel_plan STP inner join crm_site_visit_plan SVP on SVP.crm_site_visit_plan_id = STP.crm_site_travel_plan_svp inner join crm_enquiry E on E.enquiry_id = SVP.crm_site_visit_plan_enquiry inner join crm_project_master PM on PM.project_id = SVP.crm_site_visit_plan_project left outer join crm_cab_master CM on CM.crm_cab_id = STP.crm_site_travel_plan_cab inner join users U on U.user_id = STP.crm_site_travel_plan_added_by";

	$get_stp_squery_where = "";

	$filter_count = 0;

	// Data
	$get_stp_sdata = array();

	if($travel_plan_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_stp_squery_where = $get_stp_squery_where." where crm_site_travel_plan_id=:stp_id";
		}
		else
		{
			// Query
			$get_stp_squery_where = $get_stp_squery_where." and crm_site_travel_plan_id=:stp_id";
		}

		// Data
		$get_stp_sdata[':stp_id']  = $travel_plan_id;

		$filter_count++;
	}

	if($site_visit_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_stp_squery_where = $get_stp_squery_where." where crm_site_travel_plan_svp=:svp_id";
		}
		else
		{
			// Query
			$get_stp_squery_where = $get_stp_squery_where." and crm_site_travel_plan_svp=:svp_id";
		}

		// Data
		$get_stp_sdata[':svp_id']  = $site_visit_id;

		$filter_count++;
	}

	if($enquiry_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_stp_squery_where = $get_stp_squery_where." where E.enquiry_id=:enquiry_id";
		}
		else
		{
			// Query
			$get_stp_squery_where = $get_stp_squery_where." and E.enquiry_id=:enquiry_id";
		}

		// Data
		$get_stp_sdata[':enquiry_id']  = $enquiry_id;

		$filter_count++;
	}

	if($project_user != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_stp_squery_where = $get_stp_squery_where." where E.project_id IN (select crm_user_project_mapping_project_id from crm_user_project_mapping where crm_user_project_mapping_user_id = :project_user_id and crm_user_project_mapping_active = 1)";
		}
		else
		{
			// Query
			$get_stp_squery_where = $get_stp_squery_where." and E.project_id IN (select crm_user_project_mapping_project_id from crm_user_project_mapping where crm_user_project_mapping_user_id = :project_user_id and crm_user_project_mapping_active = 1)";
		}

		// Data
		$get_stp_sdata[':project_user_id']  = $project_user;

		$filter_count++;
	}

	if($cab != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_stp_squery_where = $get_stp_squery_where." where crm_site_travel_plan_cab=:cab";
		}
		else
		{
			// Query
			$get_stp_squery_where = $get_stp_squery_where." and crm_site_travel_plan_cab=:cab";
		}

		// Data
		$get_stp_sdata[':cab']  = $cab;

		$filter_count++;
	}

	if($project != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_stp_squery_where = $get_stp_squery_where." where crm_site_travel_plan_project=:project";
		}
		else
		{
			// Query
			$get_stp_squery_where = $get_stp_squery_where." and crm_site_travel_plan_project=:project";
		}

		// Data
		$get_stp_sdata[':project']  = $project;

		$filter_count++;
	}

	if($date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_stp_squery_where = $get_stp_squery_where." where crm_site_travel_plan_date >= :start_date_time and crm_site_travel_plan_date <= :end_date_time";
		}
		else
		{
			// Query
			$get_stp_squery_where = $get_stp_squery_where." and crm_site_travel_plan_date >= :start_date_time and crm_site_travel_plan_date <= :end_date_time";
		}

		// Data
		$get_stp_sdata[':start_date_time']  = $date." 00:00:00";
		$get_stp_sdata[':end_date_time']  = $date." 23:59:59";

		$filter_count++;
	}

	if($status != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_stp_squery_where = $get_stp_squery_where." where crm_site_travel_plan_status=:status";
		}
		else
		{
			// Query
			$get_stp_squery_where = $get_stp_squery_where." and crm_site_travel_plan_status=:status";
		}

		// Data
		$get_stp_sdata[':status']  = $status;

		$filter_count++;
	}

	if($assigned_to != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_stp_squery_where = $get_stp_squery_where." where E.assigned_to=:assigned_to";
		}
		else
		{
			// Query
			$get_stp_squery_where = $get_stp_squery_where." and E.assigned_to=:assigned_to";
		}

		// Data
		$get_stp_sdata[':assigned_to']  = $assigned_to;

		$filter_count++;
	}

	if($added_by != "")
	{
		if($filter_count == 0)
		{
			switch($relationship)
			{
			case "planned_by";
			$get_stp_squery_where = $get_stp_squery_where." where crm_site_visit_plan_added_by=:added_by";
			break;

			case "stm";
			$get_stp_squery_where = $get_stp_squery_where." where E.assigned_to=:added_by";
			break;

			case "added_by";
			$get_stp_squery_where = $get_stp_squery_where." where E.added_by=:added_by";
			break;

			case "all";
			$get_stp_squery_where = $get_stp_squery_where." where crm_site_visit_plan_added_by=:added_by or E.assigned_to=:added_by or E.added_by=:added_by";
			break;

			default;
			$get_stp_squery_where = $get_stp_squery_where." where crm_site_visit_plan_added_by=:added_by or E.assigned_to=:added_by or E.added_by=:added_by";
			break;
			}
		}
		else
		{
			switch($relationship)
			{
			case "planned_by";
			$get_stp_squery_where = $get_stp_squery_where." and crm_site_visit_plan_added_by=:added_by";
			break;

			case "stm";
			$get_stp_squery_where = $get_stp_squery_where." and E.assigned_to=:added_by";
			break;

			case "added_by";
			$get_stp_squery_where = $get_stp_squery_where." and E.added_by=:added_by";
			break;

			case "all";
			$get_stp_squery_where = $get_stp_squery_where." and crm_site_visit_plan_added_by=:added_by or E.assigned_to=:added_by or E.added_by=:added_by";
			break;

			default;
			$get_stp_squery_where = $get_stp_squery_where." and crm_site_visit_plan_added_by=:added_by or E.assigned_to=:added_by or E.added_by=:added_by";
			break;
			}
		}

		// Data
		$get_stp_sdata[':added_by']  = $added_by;

		$filter_count++;
	}

	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_stp_squery_where = $get_stp_squery_where." where crm_site_travel_plan_added_on >= :start_date";
		}
		else
		{
			// Query
			$get_stp_squery_where = $get_stp_squery_where." and crm_site_travel_plan_added_on >= :start_date";
		}

		//Data
		$get_stp_sdata[':start_date']  = $start_date;

		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_stp_squery_where = $get_stp_squery_where." where crm_site_travel_plan_added_on <= :end_date";
		}
		else
		{
			// Query
			$get_stp_squery_where = $get_stp_squery_where." and crm_site_travel_plan_added_on <= :end_date";
		}

		//Data
		$get_stp_sdata[':end_date']  = $end_date;

		$filter_count++;
	}

	if($travel_start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_stp_squery_where = $get_stp_squery_where." where crm_site_travel_plan_date >= :travel_start_date";
		}
		else
		{
			// Query
			$get_stp_squery_where = $get_stp_squery_where." and crm_site_travel_plan_date >= :travel_start_date";
		}

		//Data
		$get_stp_sdata[':travel_start_date']  = $travel_start_date;

		$filter_count++;
	}

	if($travel_end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_stp_squery_where = $get_stp_squery_where." where crm_site_travel_plan_date <= :travel_end_date";
		}
		else
		{
			// Query
			$get_stp_squery_where = $get_stp_squery_where." and crm_site_travel_plan_date <= :travel_end_date";
		}

		//Data
		$get_stp_sdata[':travel_end_date']  = $travel_end_date;

		$filter_count++;
	}

	$get_stp_squery_order = " order by crm_site_travel_plan_date desc";

	$get_stp_squery = $get_stp_squery_base.$get_stp_squery_where.$get_stp_squery_order;
	//SPALURU echo $get_stp_squery;
	try
	{
		$dbConnection = get_conn_handle();

		$get_stp_sstatement = $dbConnection->prepare($get_stp_squery);

		$get_stp_sstatement -> execute($get_stp_sdata);

		$get_stp_sdetails = $get_stp_sstatement -> fetchAll();

		if(FALSE === $get_stp_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_stp_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_stp_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}

	return $return;
}

function db_update_site_travel_plan($travel_plan_id,$status,$cab,$cab_id)
{
	// Query
    $site_travel_uquery = "update crm_site_travel_plan set crm_site_travel_plan_status=:status, crm_site_travel_actual_cab=:cab,crm_site_travel_plan_cab=:cab_id where crm_site_travel_plan_id=:travel_plan_id";

    try
    {
        $dbConnection = get_conn_handle();

        $site_travel_ustatement = $dbConnection->prepare($site_travel_uquery);

        // Data
        $site_travel_udata = array(':status'=>$status,':cab'=>$cab,':cab_id'=>$cab_id,':travel_plan_id'=>$travel_plan_id);

        $site_travel_ustatement -> execute($site_travel_udata);

        $return["status"] = SUCCESS;
		$return["data"]   = $travel_plan_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }

	return $return;
}

/*
PURPOSE : To get site travel actual list
INPUT 	: Travel Plan ID, Travel Date, Enquiry ID, Project, Added By, Start Date, End Date
OUTPUT 	: List of actual site travel
BY 		: Nitin Kashyap
REMARKS : Email and Company are accepted as inputs. Processing to be done later.
*/
function db_get_site_travel_actual($travel_actual_id,$travel_date,$enquiry_id,$project,$added_by,$start_date,$end_date)
{
	$get_sta_squery_base = "select * from crm_site_travel_actual STA inner join crm_enquiry E on E.enquiry_id = STA.crm_site_travel_actual_enquiry inner join crm_project_master PM on PM.project_id = STA.crm_site_travel_actual_project inner join users U on U.user_id = STA.crm_site_travel_actual_added_by";

	$get_sta_squery_where = "";

	$filter_count = 0;

	// Data
	$get_sta_sdata = array();

	if($travel_actual_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_sta_squery_where = $get_sta_squery_where." where crm_site_travel_actual_id=:sta_id";
		}
		else
		{
			// Query
			$get_sta_squery_where = $get_sta_squery_where." and crm_site_travel_actual_id=:sta_id";
		}

		// Data
		$get_sta_sdata[':sta_id']  = $travel_plan_id;

		$filter_count++;
	}

	if($travel_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_sta_squery_where = $get_sta_squery_where." where crm_site_travel_actual_date=:date";
		}
		else
		{
			// Query
			$get_sta_squery_where = $get_sta_squery_where." and crm_site_travel_actual_date=:date";
		}

		// Data
		$get_sta_sdata[':date']  = $travel_date;

		$filter_count++;
	}

	if($enquiry_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_sta_squery_where = $get_sta_squery_where." where crm_site_travel_actual_enquiry=:enquiry_id";
		}
		else
		{
			// Query
			$get_sta_squery_where = $get_sta_squery_where." and crm_site_travel_actual_enquiry=:enquiry_id";
		}

		// Data
		$get_sta_sdata[':enquiry_id']  = $enquiry_id;

		$filter_count++;
	}

	if($project != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_sta_squery_where = $get_sta_squery_where." where crm_site_travel_actual_project=:project";
		}
		else
		{
			// Query
			$get_sta_squery_where = $get_sta_squery_where." and crm_site_travel_actual_project=:project";
		}

		// Data
		$get_sta_sdata[':project']  = $project;

		$filter_count++;
	}

	if($added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_sta_squery_where = $get_sta_squery_where." where crm_site_travel_actual_added_by=:added_by";
		}
		else
		{
			// Query
			$get_sta_squery_where = $get_sta_squery_where." and crm_site_travel_actual_added_by=:added_by";
		}

		// Data
		$get_sta_sdata[':added_by']  = $added_by;

		$filter_count++;
	}

	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_sta_squery_where = $get_sta_squery_where." where crm_site_travel_actual_added_on >= :start_date";
		}
		else
		{
			// Query
			$get_sta_squery_where = $get_sta_squery_where." and crm_site_travel_actual_added_on >= :start_date";
		}

		//Data
		$get_sta_sdata[':start_date']  = $start_date;

		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_sta_squery_where = $get_sta_squery_where." where crm_site_travel_actual_added_on <= :end_date";
		}
		else
		{
			// Query
			$get_sta_squery_where = $get_sta_squery_where." and crm_site_travel_actual_added_on <= :end_date";
		}

		//Data
		$get_sta_sdata[':end_date']  = $end_date;

		$filter_count++;
	}

	$get_sta_squery = $get_sta_squery_base.$get_sta_squery_where;

	try
	{
		$dbConnection = get_conn_handle();

		$get_sta_sstatement = $dbConnection->prepare($get_sta_squery);

		$get_sta_sstatement -> execute($get_sta_sdata);

		$get_sta_sdetails = $get_sta_sstatement -> fetchAll();

		if(FALSE === $get_sta_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_sta_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_sta_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}

	return $return;
}

/*
PURPOSE : To get completed site travel plan list
INPUT 	: Added By, Start Date, End Date, Project Mapped User
OUTPUT 	: List of completed site travel plans
BY 		: Nitin Kashyap
*/
function db_get_site_travel_plan_completed($added_by,$start_date,$end_date,$project_user='')
{
	$get_completed_sv_squery_base = "select distinct(CV.enquiry_id) as enquiry_id from (select * from crm_site_travel_plan STP inner join crm_site_visit_plan SVP on SVP.crm_site_visit_plan_id = STP.crm_site_travel_plan_svp inner join crm_enquiry CE on CE.enquiry_id = SVP.crm_site_visit_plan_enquiry where crm_site_travel_plan_status='2') as CV";

	$get_completed_sv_squery_where = "";

	$get_completed_sv_sdata = array();

	$filter_count = 0;

	if($added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_completed_sv_squery_where = $get_completed_sv_squery_where." where CV.crm_site_visit_plan_added_by=:added_by";
		}
		else
		{
			// Query
			$get_completed_sv_squery_where = $get_completed_sv_squery_where." and CV.crm_site_visit_plan_added_by=:added_by";
		}

		// Data
		$get_completed_sv_sdata[':added_by']  = $added_by;

		$filter_count++;
	}

	if($project_user != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_completed_sv_squery_where = $get_completed_sv_squery_where." where CV.project_id IN (select crm_user_project_mapping_project_id from crm_user_project_mapping where crm_user_project_mapping_user_id = :project_user_id)";
		}
		else
		{
			// Query
			$get_completed_sv_squery_where = $get_completed_sv_squery_where." and CV.project_id IN (select crm_user_project_mapping_project_id from crm_user_project_mapping where crm_user_project_mapping_user_id = :project_user_id)";
		}

		// Data
		$get_completed_sv_sdata[':project_user_id']  = $project_user;

		$filter_count++;
	}

	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_completed_sv_squery_where = $get_completed_sv_squery_where." where CV.crm_site_travel_plan_date >= :start_date";
		}
		else
		{
			// Query
			$get_completed_sv_squery_where = $get_completed_sv_squery_where." and CV.crm_site_travel_plan_date >= :start_date";
		}

		//Data
		$get_completed_sv_sdata[':start_date']  = $start_date;

		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_completed_sv_squery_where = $get_completed_sv_squery_where." where CV.crm_site_travel_plan_date <= :end_date";
		}
		else
		{
			// Query
			$get_completed_sv_squery_where = $get_completed_sv_squery_where." and CV.crm_site_travel_plan_date <= :end_date";
		}

		//Data
		$get_completed_sv_sdata[':end_date']  = $end_date;

		$filter_count++;
	}
	$get_completed_sv_squery = $get_completed_sv_squery_base.$get_completed_sv_squery_where;

	try
	{
		$dbConnection = get_conn_handle();

		$get_completed_sv_sstatement = $dbConnection->prepare($get_completed_sv_squery);

		$get_completed_sv_sstatement -> execute($get_completed_sv_sdata);

		$get_completed_sv_sdetails = $get_completed_sv_sstatement -> fetchAll();

		if(FALSE === $get_completed_sv_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_completed_sv_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_completed_sv_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}

	return $return;
}

/*
PURPOSE : To update site visit confirmation status
INPUT 	: Site Visit Plan ID, Site Visit Data
OUTPUT 	: SVP ID; Message of success or failure
BY 		: Nitin Kashyap
*/
function db_update_site_visit($svp_id,$sv_data)
{
	//Extract Data
	if(array_key_exists("status",$sv_data))
	{
		$status = $sv_data["status"];
	}
	else
	{
		$status = "";
	}
	if(array_key_exists("site_visit_plan_drive",$sv_data))
	{
		$crm_site_visit_plan_drive = $sv_data["site_visit_plan_drive"];
	}
	else
	{
		$crm_site_visit_plan_drive = "";
	}
	if(array_key_exists("status1",$sv_data))
	{
		$status1 = $sv_data["status1"];
	}
	else
	{
		$status1 = "";
	}

	if(array_key_exists("date",$sv_data))
	{
		$date = $sv_data["date"];
	}
	else
	{
		$date = "";
	}

	if(array_key_exists("time",$sv_data))
	{
		$time = $sv_data["time"];
	}
	else
	{
		$time = "";
	}

	if(array_key_exists("location",$sv_data))
	{
		$location = $sv_data["location"];
	}
	else
	{
		$location = "";
	}

	// Query
    $svp_conf_uquery_base = "update crm_site_visit_plan set";

	$svp_conf_uquery_set = "";

	$svp_conf_uquery_where = " where crm_site_visit_plan_id=:svp_id";

	$svp_conf_udata = array(":svp_id"=>$svp_id);

	$filter_count = 0;

	if($status != "")
	{
		$svp_conf_uquery_set = $svp_conf_uquery_set." crm_site_visit_plan_confirmation=:status,";
		$svp_conf_udata[":status"] = $status;
		$filter_count++;
	}

	if($crm_site_visit_plan_drive != "")
	{
		$svp_conf_uquery_set = $svp_conf_uquery_set." crm_site_visit_plan_drive=:site_visit_plan_drive,";
		$svp_conf_udata[":site_visit_plan_drive"] = $crm_site_visit_plan_drive;
		$filter_count++;
	}

	if($status1 != "")
	{
		$svp_conf_uquery_set = $svp_conf_uquery_set." crm_site_visit_status=:status1,";
		$svp_conf_udata[":status1"] = $status1;
		$filter_count++;
	}

	if($date != "")
	{
		$svp_conf_uquery_set = $svp_conf_uquery_set." crm_site_visit_plan_date=:date,";
		$svp_conf_udata[":date"] = $date;
		$filter_count++;
	}

	if($time != "")
	{
		$svp_conf_uquery_set = $svp_conf_uquery_set." crm_site_visit_plan_time=:time,";
		$svp_conf_udata[":time"] = $time;
		$filter_count++;
	}

	if($location != "")
	{
		$svp_conf_uquery_set = $svp_conf_uquery_set." crm_site_visit_pickup_location=:location,";
		$svp_conf_udata[":location"] = $location;
		$filter_count++;
	}

	if($filter_count > 0)
	{
		$svp_conf_uquery_set = trim($svp_conf_uquery_set,',');
	}

	$svp_conf_uquery = $svp_conf_uquery_base.$svp_conf_uquery_set.$svp_conf_uquery_where;


    try
    {
        $dbConnection = get_conn_handle();

        $svp_conf_ustatement = $dbConnection->prepare($svp_conf_uquery);

        $svp_conf_ustatement -> execute($svp_conf_udata);

        $return["status"] = SUCCESS;
		$return["data"]   = $svp_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }

	return $return;
}
?>
