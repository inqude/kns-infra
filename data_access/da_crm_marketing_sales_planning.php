<?php
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'status_codes.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'connection.php');

/*
LIST OF TBDs
1. Wherever there is name search, there should be a wildcard
*/

/*
PURPOSE : To add new sales target
INPUT 	: Month, Year, Target Count, Target Area, Added By
OUTPUT 	: Sales Target ID, success or failure message
BY 		: Nitin Kashyap
*/
function db_add_sales_target($month,$year,$target_count,$target_area,$added_by)
{
	// Query
    $sales_target_iquery = "insert into crm_sales_targets (crm_sales_target_month,crm_sales_target_year,crm_sales_target,crm_sales_target_area,crm_sales_target_added_by,crm_sales_target_added_on) values (:month,:year,:target_count,:target_area,:added_by,:now)";  
	
    try
    {
        $dbConnection = get_conn_handle();
        
        $sales_target_istatement = $dbConnection->prepare($sales_target_iquery);
        
        // Data
        $sales_target_idata = array(':month'=>$month,':year'=>$year,':target_count'=>$target_count,':target_area'=>$target_area,':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));	
		$dbConnection->beginTransaction();
        $sales_target_istatement->execute($sales_target_idata);
		$sales_target_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
        
        $return["status"] = SUCCESS;
		$return["data"]   = $sales_target_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get sales targets
INPUT 	: Sales Target Data Array
OUTPUT 	: Sales Targets
BY 		: Nitin Kashyap
*/
function db_get_sales_targets($sales_target_data)
{
	// Extract all data
	$sales_target_id = "";
	$month           = "";
	$year            = "";
	$order           = "";
	
	if(array_key_exists("target_id",$sales_target_data))
	{	
		$sales_target_id = $sales_target_data["target_id"];
	}
	
	if(array_key_exists("month",$sales_target_data))
	{	
		$month = $sales_target_data["month"];
	}
	
	if(array_key_exists("year",$sales_target_data))
	{	
		$year = $sales_target_data["year"];
	}
	
	if(array_key_exists("order",$sales_target_data))
	{	
		$order = $sales_target_data["order"];
	}

	$get_sales_target_squery_base = "select * from crm_sales_targets";		
	
	$get_sales_target_squery_where = "";
	
	$get_sales_target_squery_order = "";		
	
	$filter_count = 0;
	
	// Data
	$get_sales_target_sdata = array();
	
	if($sales_target_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_sales_target_squery_where = $get_sales_target_squery_where." where crm_sales_target_id=:target_id";								
		}
		else
		{
			// Query
			$get_sales_target_squery_where = $get_sales_target_squery_where." and crm_sales_target_id=:target_id";				
		}
		
		// Data
		$get_sales_target_sdata[':target_id']  = $sales_target_id;
		
		$filter_count++;
	}
	
	if($month != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_sales_target_squery_where = $get_sales_target_squery_where." where crm_sales_target_month=:month";								
		}
		else
		{
			// Query
			$get_sales_target_squery_where = $get_sales_target_squery_where." and crm_sales_target_month=:month";				
		}
		
		// Data
		$get_sales_target_sdata[':month']  = $month;
		
		$filter_count++;
	}
	
	if($year != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_sales_target_squery_where = $get_sales_target_squery_where." where crm_sales_target_year=:year";								
		}
		else
		{
			// Query
			$get_sales_target_squery_where = $get_sales_target_squery_where." and crm_sales_target_year=:year";				
		}
		
		// Data
		$get_sales_target_sdata[':year']  = $year;
		
		$filter_count++;
	}
	
	switch($order)
	{
		case "added_on_desc":
		$get_sales_target_squery_order = " order by crm_sales_target_added_on desc";
		break;
		
		default:
		break;
	}
	
	$get_sales_target_squery = $get_sales_target_squery_base.$get_sales_target_squery_where.$get_sales_target_squery_order;		
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_sales_target_sstatement = $dbConnection->prepare($get_sales_target_squery);
		
		$get_sales_target_sstatement -> execute($get_sales_target_sdata);
		
		$get_sales_target_sdetails = $get_sales_target_sstatement -> fetchAll();
		
		if(FALSE === $get_sales_target_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_sales_target_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_sales_target_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}
?>