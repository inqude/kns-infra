<?php
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'status_codes.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'connection.php');

/*
LIST OF TBDs
1. Wherever there is name search, there should be a wildcard
*/
/*
PURPOSE : To add new Stock Vendor Master
INPUT 	: Vendor Code, Address, Contact Person, Contact Number, Email Id, Type of Service, Country, Pan No, Service Contact No, Approved List No, Remarks,
          Added By
OUTPUT 	: Vendor Id, success or failure message
BY 		: Lakshmi
*/
function db_add_stock_vendor_master($vendor_code,$address,$vendor_name,$contact_person,$contact_number,$email_id,$type_of_service,$country,$pan_no,$service_contact_no,$approved_list_no,$remarks,$added_by)
{
	// Query
    $stock_vendor_master_iquery = "insert into stock_vendor_master
(stock_vendor_code,stock_vendor_address,stock_vendor_name,stock_vendor_contact_person,stock_vendor_contact_number,stock_vendor_email_id, stock_vendor_type_of_service,stock_vendor_country,stock_vendor_pan_no,stock_vendor_service_contact_no,stock_vendor_approved_list_no,
stock_vendor_remarks,stock_vendor_active,stock_vendor_added_by,stock_vendor_added_on)
values(:vendor_code,:address,:vendor_name,:contact_person,:contact_number,:email_id,:type_of_service,:country,:pan_no,:service_contact_no,
:approved_list_no,:remarks,:active,:added_by,:added_on)";  
 
    try
    {
        $dbConnection = get_conn_handle();
        $stock_vendor_master_istatement = $dbConnection->prepare($stock_vendor_master_iquery);
       
        // Data
         $stock_vendor_master_idata = array(':vendor_code'=>$vendor_code,':address'=>$address,':vendor_name'=>$vendor_name,':contact_person'=>$contact_person,
		':contact_number'=>$contact_number,':email_id'=>$email_id,':type_of_service'=>$type_of_service,':country'=>$country,
		':pan_no'=>$pan_no,':service_contact_no'=>$service_contact_no,':approved_list_no'=>$approved_list_no,':remarks'=>$remarks,
		':active'=>'1',':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));	
	     
		$dbConnection->beginTransaction();
        $stock_vendor_master_istatement->execute($stock_vendor_master_idata);
		$stock_vendor_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
       
        $return["status"] = SUCCESS;
		$return["data"]   = $stock_vendor_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get Stock Vendor Master List
INPUT 	: Vendor Id, Vendor Code, Address, Contact Person, Contact Number, Email Id, Type of Service, Country, Pan No, Service Contact No, Approved List No,
          Active, Added by, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of stock Vendor Master
BY 		: Lakshmi
*/
function db_get_stock_vendor_master_list($stock_vendor_master_search_data)
{  
 	
	if(array_key_exists("vendor_id",$stock_vendor_master_search_data))
	{
		$vendor_id = $stock_vendor_master_search_data["vendor_id"];
	}
	else
	{
		$vendor_id= "";
	}
	
	if(array_key_exists("vendor_code",$stock_vendor_master_search_data))
	{
		$vendor_code = $stock_vendor_master_search_data["vendor_code"];
	}
	else
	{
		$vendor_code= "";
	}
	
	if(array_key_exists("address",$stock_vendor_master_search_data))
	{
		$address = $stock_vendor_master_search_data["address"];
	}
	else
	{
		$address= "";
	}
	
	if(array_key_exists("vendor_name",$stock_vendor_master_search_data))
	{
		$vendor_name = $stock_vendor_master_search_data["vendor_name"];
	}
	else
	{
		$vendor_name= "";
	}
	
	if(array_key_exists("contact_person",$stock_vendor_master_search_data))
	{
		$contact_person = $stock_vendor_master_search_data["contact_person"];
	}
	else
	{
		$contact_person= "";
	}
	
	if(array_key_exists("vendor_name_check",$stock_vendor_master_search_data))
	{
		$vendor_name_check = $stock_vendor_master_search_data["vendor_name_check"];
	}
	else
	{
		$vendor_name_check= "";
	}
	
	
	if(array_key_exists("contact_number",$stock_vendor_master_search_data))
	{
		$contact_number = $stock_vendor_master_search_data["contact_number"];
	}
	else
	{
		$contact_number= "";
	}
	
	if(array_key_exists("email_id",$stock_vendor_master_search_data))
	{
		$email_id = $stock_vendor_master_search_data["email_id"];
	}
	else
	{
		$email_id= "";
	}
	
	if(array_key_exists("type_of_service",$stock_vendor_master_search_data))
	{
		$type_of_service = $stock_vendor_master_search_data["type_of_service"];
	}
	else
	{
		$type_of_service= "";
	}
	
	if(array_key_exists("country",$stock_vendor_master_search_data))
	{
		$country = $stock_vendor_master_search_data["country"];
	}
	else
	{
		$country= "";
	}
	
	if(array_key_exists("pan_no",$stock_vendor_master_search_data))
	{
		$pan_no = $stock_vendor_master_search_data["pan_no"];
	}
	else
	{
		$pan_no= "";
	}
	
	if(array_key_exists("service_contact_no",$stock_vendor_master_search_data))
	{
		$service_contact_no = $stock_vendor_master_search_data["service_contact_no"];
	}
	else
	{
		$service_contact_no= "";
	}
	
	if(array_key_exists("approved_list_no",$stock_vendor_master_search_data))
	{
		$approved_list_no = $stock_vendor_master_search_data["approved_list_no"];
	}
	else
	{
		$approved_list_no= "";
	}
			

	if(array_key_exists("active",$stock_vendor_master_search_data))
	{
		$active= $stock_vendor_master_search_data["active"];
	}
	else
	{
		$active= "";
	}

	
	if(array_key_exists("added_by",$stock_vendor_master_search_data))
	{
		$added_by= $stock_vendor_master_search_data["added_by"];
	}
	else
	{
		$added_by= "";
	}
	if(array_key_exists("start_date",$stock_vendor_master_search_data))
	{
		$start_date= $stock_vendor_master_search_data["start_date"];
	}
	else
	{
		$start_date= "";
	}
	
	if(array_key_exists("end_date",$stock_vendor_master_search_data))
	{
		$end_date= $stock_vendor_master_search_data["end_date"];
	}
	else
	{
		$end_date= "";
	}

	$get_stock_vendor_master_list_squery_base = "select * from stock_vendor_master SVM inner join users U on U.user_id = SVM.stock_vendor_added_by inner join stock_vendor_type_of_service_master STSM on STSM.stock_type_of_service_id = SVM.stock_vendor_type_of_service";
	
	
	$get_stock_vendor_master_list_squery_where = "";
	$get_vendor_list_squery_order = "";
	
	$filter_count = 0;
	
	// Data
	$get_stock_vendor_master_list_sdata = array();
	
	if($vendor_id != "")
	{
		if($filter_count == 0)
		{
			// Query
		$get_stock_vendor_master_list_squery_where = $get_stock_vendor_master_list_squery_where." where stock_vendor_id=:vendor_id";
		
		}										
		else
		{
			// Query
			$get_stock_vendor_master_list_squery_where = $get_stock_vendor_master_list_squery_where." and stock_vendor_id=:vendor_id";				
		}
		// Data
		$get_stock_vendor_master_list_sdata[':vendor_id'] = $vendor_id;
		
		$filter_count++;
	}
	
	if($vendor_code != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_stock_vendor_master_list_squery_where = $get_stock_vendor_master_list_squery_where." where stock_vendor_code=:vendor_code";								
		}
		else
		{
			// Query
			$get_stock_vendor_master_list_squery_where = $get_stock_vendor_master_list_squery_where." and stock_vendor_code=:vendor_code";				
		}
		
		// Data
		$get_stock_vendor_master_list_sdata[':vendor_code']  = $vendor_code;
		
		$filter_count++;
	}
	
	if($address != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_stock_vendor_master_list_squery_where = $get_stock_vendor_master_list_squery_where." where stock_vendor_address=:address";								
		}
		else
		{
			// Query
			$get_stock_vendor_master_list_squery_where = $get_stock_vendor_master_list_squery_where." and stock_vendor_address:address";				
		}
		
		// Data
		$get_stock_vendor_master_list_sdata[':address']  = $address;
		
		$filter_count++;
	}
	
	if($vendor_name != "")
	{
		if($filter_count == 0)
		{			
			if($vendor_name_check == '1')
			{
				$get_stock_vendor_master_list_squery_where = $get_stock_vendor_master_list_squery_where." where stock_vendor_name = :vendor_name";			
					
				// Data
				$get_stock_vendor_master_list_sdata[':vendor_name']  = $vendor_name;
			}
			else if($vendor_name_check == '2')
			{
				$get_stock_vendor_master_list_squery_where = $get_stock_vendor_master_list_squery_where." where stock_vendor_name like :vendor_name";						

				// Data
				$get_stock_vendor_master_list_sdata[':vendor_name']  = $vendor_name.'%';
			}
			else
			{
				$get_stock_vendor_master_list_squery_where = $get_stock_vendor_master_list_squery_where." where stock_vendor_name like :vendor_name";						

				// Data
				$get_stock_vendor_master_list_sdata[':vendor_name']  = '%'.$vendor_name.'%';
			}							
		}
		else
		{
			if($vendor_name_check == '1')
			{
				$get_stock_vendor_master_list_squery_where = $get_stock_vendor_master_list_squery_where." and stock_vendor_name = :vendor_name";	
					
				// Data
				$get_stock_vendor_master_list_sdata[':vendor_name']  = $vendor_name;
			}
			else if($vendor_name_check == '2')
			{
				$get_stock_vendor_master_list_squery_where = $get_stock_vendor_master_list_squery_where." and stock_vendor_name like :vendor_name";						

				// Data
				$get_stock_vendor_master_list_sdata[':vendor_name']  = $vendor_name.'%';
			}
			else
			{
				$get_stock_vendor_master_list_squery_where = $get_stock_vendor_master_list_squery_where." and stock_vendor_name like :vendor_name";
				
				// Data
				$get_stock_vendor_master_list_sdata[':vendor_name']  = '%'.$vendor_name.'%';
			}	
		}
		
		$filter_count++;
	}
	if($contact_person != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_stock_vendor_master_list_squery_where = $get_stock_vendor_master_list_squery_where." where stock_vendor_contact_person=:contact_person";
		}
		else
		{
			// Query
			$get_stock_vendor_master_list_squery_where = $get_stock_vendor_master_list_squery_where." and stock_vendor_contact_person=:contact_person";				
		}
		
		// Data
		$get_stock_vendor_master_list_sdata[':contact_person']  = $contact_person;
		
		$filter_count++;
	}
	
	if($contact_number != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_stock_vendor_master_list_squery_where = $get_stock_vendor_master_list_squery_where." where stock_vendor_contact_number=:contact_number";
		}
		else
		{
			// Query
			$get_stock_vendor_master_list_squery_where = $get_stock_vendor_master_list_squery_where." and stock_vendor_contact_number=:contact_number";
		}
		
		// Data
		$get_stock_vendor_master_list_sdata[':contact_number']  = $contact_number;
		
		$filter_count++;
	}
	
	if($email_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_stock_vendor_master_list_squery_where = $get_stock_vendor_master_list_squery_where." where stock_vendor_email_id=:email_id";
		}
		else
		{
			// Query
			$get_stock_vendor_master_list_squery_where = $get_stock_vendor_master_list_squery_where." and stock_vendor_email_id=:email_id";				
		}
		
		// Data
		$get_stock_vendor_master_list_sdata[':email_id']  = $email_id;
		
		$filter_count++;
	}
	
	if($type_of_service != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_stock_vendor_master_list_squery_where = $get_stock_vendor_master_list_squery_where." where stock_vendor_type_of_service=:type_of_service";
		}
		else
		{
			// Query
			$get_stock_vendor_master_list_squery_where = $get_stock_vendor_master_list_squery_where." and stock_vendor_type_of_service=:type_of_service";
		}
		
		// Data
		$get_stock_vendor_master_list_sdata[':type_of_service']  = $type_of_service;
		
		$filter_count++;
	}
	
	if($country != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_stock_vendor_master_list_squery_where = $get_stock_vendor_master_list_squery_where." where stock_vendor_country=:country";
		}
		else
		{
			// Query
			$get_stock_vendor_master_list_squery_where = $get_stock_vendor_master_list_squery_where." and stock_vendor_country=:country";				
		}
		
		// Data
		$get_stock_vendor_master_list_sdata[':country']  = $country;
		
		$filter_count++;
	}
	
	if($pan_no != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_stock_vendor_master_list_squery_where = $get_stock_vendor_master_list_squery_where." where stock_vendor_pan_no=:pan_no";
		}
		else
		{
			// Query
			$get_stock_vendor_master_list_squery_where = $get_stock_vendor_master_list_squery_where." and stock_vendor_pan_no=:pan_no";				
		}
		
		// Data
		$get_stock_vendor_master_list_sdata[':pan_no']  = $pan_no;
		
		$filter_count++;
	}
	
	if($service_contact_no != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_stock_vendor_master_list_squery_where = $get_stock_vendor_master_list_squery_where." where stock_vendor_service_contact_no=:service_contact_no";								
		}
		else
		{
			// Query
			$get_stock_vendor_master_list_squery_where = $get_stock_vendor_master_list_squery_where." and stock_vendor_service_contact_no=:service_contact_no";				
		}
		
		// Data
		$get_stock_vendor_master_list_sdata[':service_contact_no']  = $service_contact_no;
		
		$filter_count++;
	}
	
	if($approved_list_no != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_stock_vendor_master_list_squery_where = $get_stock_vendor_master_list_squery_where." where stock_vendor_approved_list_no=:approved_list_no";								
		}
		else
		{
			// Query
			$get_stock_vendor_master_list_squery_where = $get_stock_vendor_master_list_squery_where." and stock_vendor_approved_list_no=:approved_list_no";
		}
		
		// Data
		$get_stock_vendor_master_list_sdata[':approved_list_no']  = $approved_list_no;
		
		$filter_count++;
	}

	if($active != "")
	{	
		if($filter_count == 0)
		{
			// Query
			$get_stock_vendor_master_list_squery_where = $get_stock_vendor_master_list_squery_where." where stock_vendor_active=:active";
		}
		else
		{
			// Query
			$get_stock_vendor_master_list_squery_where = $get_stock_vendor_master_list_squery_where." and stock_vendor_active=:active";				
		}
		
		// Data
		$get_stock_vendor_master_list_sdata[':active']  = $active;
		
		$filter_count++;
	}
	if($added_by!= "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_stock_vendor_master_list_squery_where = $get_stock_vendor_master_list_squery_where." where stock_vendor_added_by = :added_by";								
		}
		else
		{
			// Query
			$get_stock_vendor_master_list_squery_where = $get_stock_vendor_master_list_squery_where." and stock_vendor_added_by = :added_by";				
		}
		
		//Data
		$get_stock_vendor_master_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_stock_vendor_master_list_squery_where = $get_stock_vendor_master_list_squery_where." where stock_vendor_added_on >= :start_date";
		}
		else
		{
			// Query
			$get_stock_vendor_master_list_squery_where = $get_stock_vendor_master_list_squery_where." and stock_vendor_added_on >= :start_date";
		}
		
		//Data
		$get_stock_vendor_master_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_stock_vendor_master_list_squery_where = $get_stock_vendor_master_list_squery_where." where stock_vendor_added_on <= :end_date";
		}
		else
		{
			// Query
			$get_stock_vendor_master_list_squery_where = $get_stock_vendor_master_list_squery_where." and stock_vendor_added_on <= :end_date";				
		}
		
		//Data
		$get_stock_vendor_master_list_sdata['end_date']  = $end_date;
		
		$filter_count++;
	}
	
	if(array_key_exists("sort",$stock_vendor_master_search_data))
	{
		if($stock_vendor_master_search_data['sort'] == '1')
		{
			$get_indent_list_squery_order = " order by cast(substring(stock_vendor_code,locate('.',stock_vendor_code,6)+1) as signed) desc limit 0,1";
		}
		else
		{
			$get_indent_list_squery_order = " order by stock_vendor_added_on DESC";
		}
	}
	$get_stock_vendor_master_list_order = " order by stock_vendor_added_on DESC";
	$get_stock_vendor_master_list_squery = $get_stock_vendor_master_list_squery_base.$get_stock_vendor_master_list_squery_where.$get_vendor_list_squery_order.$get_stock_vendor_master_list_order ;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_stock_vendor_master_list_sstatement = $dbConnection->prepare($get_stock_vendor_master_list_squery);

			

		$get_stock_vendor_master_list_sstatement -> execute($get_stock_vendor_master_list_sdata);
		
		$get_stock_vendor_master_list_sdetails = $get_stock_vendor_master_list_sstatement -> fetchAll();
		if(FALSE === $get_stock_vendor_master_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_stock_vendor_master_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_stock_vendor_master_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
 }
 
/*
PURPOSE : To update Vendor Master
INPUT 	: Vendor ID, Vendor Master Update Array
OUTPUT 	: Vendor ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_vendor_master($vendor_id,$vendor_master_update_data)
{
	if(array_key_exists("vendor_code",$vendor_master_update_data))
	{	
		$vendor_code = $vendor_master_update_data["vendor_code"];
	}
	else
	{
		$vendor_code = "";
	}
	
	if(array_key_exists("address",$vendor_master_update_data))
	{	
		$address = $vendor_master_update_data["address"];
	}
	else
	{
		$address = "";
	}
	if(array_key_exists("vendor_name",$vendor_master_update_data))
	{	
		$vendor_name = $vendor_master_update_data["vendor_name"];
	}
	else
	{
		$vendor_name = "";
	}
	if(array_key_exists("contact_person",$vendor_master_update_data))
	{	
		$contact_person = $vendor_master_update_data["contact_person"];
	}
	else
	{
		$contact_person = "";
	}
	if(array_key_exists("contact_number",$vendor_master_update_data))
	{	
		$contact_number = $vendor_master_update_data["contact_number"];
	}
	else
	{
		$contact_number = "";
	}
	if(array_key_exists("email_id",$vendor_master_update_data))
	{	
		$email_id = $vendor_master_update_data["email_id"];
	}
	else
	{
		$email_id = "";
	}
	if(array_key_exists("type_of_service",$vendor_master_update_data))
	{	
		$type_of_service = $vendor_master_update_data["type_of_service"];
	}
	else
	{
		$type_of_service = "";
	}
	if(array_key_exists("country",$vendor_master_update_data))
	{	
		$country = $vendor_master_update_data["country"];
	}
	else
	{
		$country = "";
	}
	if(array_key_exists("pan_no",$vendor_master_update_data))
	{	
		$pan_no = $vendor_master_update_data["pan_no"];
	}
	else
	{
		$pan_no = "";
	}
	if(array_key_exists("service_contact_no",$vendor_master_update_data))
	{	
		$service_contact_no = $vendor_master_update_data["service_contact_no"];
	}
	else
	{
		$service_contact_no = "";
	}
	if(array_key_exists("approved_list_no",$vendor_master_update_data))
	{	
		$approved_list_no = $vendor_master_update_data["approved_list_no"];
	}
	else
	{
		$approved_list_no = "";
	}
	if(array_key_exists("active",$vendor_master_update_data))
	{	
		$active = $vendor_master_update_data["active"];
	}
	else
	{
		$active = "";
	}
	if(array_key_exists("added_by",$vendor_master_update_data))
	{	
		$added_by = $vendor_master_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	if(array_key_exists("added_on",$vendor_master_update_data))
	{	
		$added_on = $vendor_master_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}
	
	
	// Query
    $vendor_master_update_uquery_base = "update stock_vendor_master set";  
	
	$vendor_master_update_uquery_set = "";
	
	$vendor_master_update_uquery_where = " where stock_vendor_id=:vendor_id";
	
	$vendor_master_update_udata = array(":vendor_id"=>$vendor_id);
	
	$filter_count = 0;
	
	if($vendor_code != "")
	{
		$vendor_master_update_uquery_set = $vendor_master_update_uquery_set." stock_vendor_code=:vendor_code,";
		$vendor_master_update_udata[":vendor_code"] = $vendor_code;
		$filter_count++;
	}
	
	if($address != "")
	{
		$vendor_master_update_uquery_set = $vendor_master_update_uquery_set." stock_vendor_address=:address,";
		$vendor_master_update_udata[":address"] = $address;
		$filter_count++;
	}
	
	if($vendor_name != "")
	{
		$vendor_master_update_uquery_set = $vendor_master_update_uquery_set." stock_vendor_name=:vendor_name,";
		$vendor_master_update_udata[":vendor_name"] = $vendor_name;
		$filter_count++;
	}
	
	if($contact_person != "")
	{
		$vendor_master_update_uquery_set = $vendor_master_update_uquery_set." stock_vendor_contact_person=:contact_person,";
		$vendor_master_update_udata[":contact_person"] = $contact_person;
		$filter_count++;
	}
	
	if($contact_number != "")
	{
		$vendor_master_update_uquery_set = $vendor_master_update_uquery_set." stock_vendor_contact_number=:contact_number,";
		$vendor_master_update_udata[":contact_number"] = $contact_number;
		$filter_count++;
	}
	if($email_id != "")
	{
		$vendor_master_update_uquery_set = $vendor_master_update_uquery_set." stock_vendor_email_id=:email_id,";
		$vendor_master_update_udata[":email_id"] = $email_id;
		$filter_count++;
	}
	if($type_of_service != "")
	{
		$vendor_master_update_uquery_set = $vendor_master_update_uquery_set." stock_vendor_type_of_service=:type_of_service,";
		$vendor_master_update_udata[":type_of_service"] = $type_of_service;
		$filter_count++;
	}
	if($country != "")
	{
		$vendor_master_update_uquery_set = $vendor_master_update_uquery_set." stock_vendor_country=:country,";
		$vendor_master_update_udata[":country"] = $country;
		$filter_count++;
	}
	if($pan_no != "")
	{
		$vendor_master_update_uquery_set = $vendor_master_update_uquery_set." stock_vendor_pan_no=:pan_no,";
		$vendor_master_update_udata[":pan_no"] = $pan_no;
		$filter_count++;
	}
	if($service_contact_no != "")
	{
		$vendor_master_update_uquery_set = $vendor_master_update_uquery_set." stock_vendor_service_contact_no=:service_contact_no,";
		$vendor_master_update_udata[":service_contact_no"] = $service_contact_no;
		$filter_count++;
	}
	if($approved_list_no != "")
	{
		$vendor_master_update_uquery_set = $vendor_master_update_uquery_set." stock_vendor_approved_list_no=:approved_list_no,";
		$vendor_master_update_udata[":approved_list_no"] = $approved_list_no;
		$filter_count++;
	}
	
	if($active != "")
	{
		$vendor_master_update_uquery_set = $vendor_master_update_uquery_set." stock_vendor_active=:active,";
		$vendor_master_update_udata[":active"] = $active;
		$filter_count++;
	}
	if($added_by != "")
	{
		$vendor_master_update_uquery_set = $vendor_master_update_uquery_set." stock_vendor_added_by=:added_by,";
		$vendor_master_update_udata[":added_by"] = $added_by;		
		$filter_count++;
	}
	if($added_on != "")
	{
		$vendor_master_update_uquery_set = $vendor_master_update_uquery_set." stock_vendor_added_on=:added_on,";
		$vendor_master_update_udata[":added_on"] = $added_on;		
		$filter_count++;
	}
	
	if($filter_count > 0)
	{
		$vendor_master_update_uquery_set = trim($vendor_master_update_uquery_set,',');
	}
	
	$vendor_master_update_uquery = $vendor_master_update_uquery_base.$vendor_master_update_uquery_set.$vendor_master_update_uquery_where;

    try
    {
        $dbConnection = get_conn_handle();
        
        $vendor_master_update_ustatement = $dbConnection->prepare($vendor_master_update_uquery);		
        
        $vendor_master_update_ustatement -> execute($vendor_master_update_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $vendor_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}

/*
LIST OF TBDs
1. Wherever there is name search, there should be a wildcard
*/
/*
PURPOSE : To add new Stock Vendor Type Of Service Master
INPUT 	: Name,Active,Remarks,Added By,Added On
OUTPUT 	: Stock Type Of Service Id, success or failure message
BY 		: Lakshmi
*/
function db_add_stock_type_of_service_master($service_name,$remarks,$added_by)
{
	// Query
    $stock_type_of_service_iquery = "insert into stock_vendor_type_of_service_master
    (stock_type_of_service_name,stock_type_of_service_active,stock_type_of_service_remarks,stock_type_of_service_added_by,stock_type_of_service_added_on)
     values(:service_name,:active,:remarks,:added_by,:added_on)";  
	
    try
    {
        $dbConnection = get_conn_handle();
        $stock_type_of_service_istatement = $dbConnection->prepare($stock_type_of_service_iquery);
        
        // Data
    $stock_type_of_service_idata = array(':service_name'=>$service_name,
	':active'=>'1',
	':remarks'=>$remarks,
	':added_by'=>$added_by,
	':added_on'=>date("Y-m-d H:i:s"));	
	 
	 
		$dbConnection->beginTransaction();
        $stock_type_of_service_istatement->execute($stock_type_of_service_idata);
		$stock_type_of_service_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
       
        $return["status"] = SUCCESS;
		$return["data"]   = $stock_type_of_service_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get Stock Vendor Type Of Service Master list
INPUT 	: Service Id,Service Name,active,Added By,Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Stock Type Of Service MasterS
BY 		: Lakshmi
*/
function db_get_stock_type_of_service_list($stock_type_of_service_search_data)
{  
	if(array_key_exists("service_id",$stock_type_of_service_search_data))
	{
		$service_id = $stock_type_of_service_search_data["service_id"];
	}
	else
	{
		$service_id = "";
	}
	
	if(array_key_exists("service_name",$stock_type_of_service_search_data))
	{
		$service_name= $stock_type_of_service_search_data["service_name"];
	}
	else
	{
		$service_name = "";
	}
	
	if(array_key_exists("service_name_check",$stock_type_of_service_search_data))
	{
		$service_name_check = $stock_type_of_service_search_data["service_name_check"];
	}
	else
	{
		$service_name_check = "";
	}
	
	if(array_key_exists("active",$stock_type_of_service_search_data))
	{
		$active= $stock_type_of_service_search_data["active"];
	}
	else
	{
		$active = "";
	}
	if(array_key_exists("added_by",$stock_type_of_service_search_data))
	{
		$added_by= $stock_type_of_service_search_data["added_by"];
	}
	else
	{
		$added_by= "";
	}
	if(array_key_exists("start_date",$stock_type_of_service_search_data))
	{
		$start_date= $stock_type_of_service_search_data["start_date"];
	}
	else
	{
		$start_date= "";
	}
	if(array_key_exists("end_date",$stock_type_of_service_search_data))
	{
		$end_date= $stock_type_of_service_search_data["end_date"];
	}
	else
	{
		$end_date= "";
	}
				

	$get_stock_type_of_service_list_squery_base = "select * from stock_vendor_type_of_service_master SV inner join users U on U.user_id = SV.stock_type_of_service_added_by";
	
	$get_stock_type_of_service_list_squery_where = "";
	
	$filter_count = 0;
	
	// Data
	$get_stock_type_of_service_list_sdata = array();
	
	if($service_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_stock_type_of_service_list_squery_where = $get_stock_type_of_service_list_squery_where." where stock_type_of_service_id =:service_id";								
		}
		else
		{
			// Query
			$get_stock_type_of_service_list_squery_where = $get_stock_type_of_service_list_squery_where." and stock_type_of_service_id =:service_id";				
		}
		
		// Data
		$get_stock_type_of_service_list_sdata[':service_id'] = $service_id;
		
		$filter_count++;
	}
	
	if($service_name != "")
	{
		if($filter_count == 0)
		{
			if($service_name_check == '1')
			{
				// Query
				$get_stock_type_of_service_list_squery_where = $get_stock_type_of_service_list_squery_where." where stock_type_of_service_name = :service_name";
				
				// Data
				$get_stock_type_of_service_list_sdata[':service_name'] = $service_name;
			}
			else
			{
				// Query
				$get_stock_type_of_service_list_squery_where = $get_stock_type_of_service_list_squery_where." where stock_type_of_service_name like :service_name";
				
				// Data
				$get_stock_type_of_service_list_sdata[':service_name'] = '%'.$service_name.'%';
			}
		}
		else
		{
			if($service_name_check == '1')
			{
				// Query
				$get_stock_type_of_service_list_squery_where = $get_stock_type_of_service_list_squery_where." and stock_type_of_service_name = :service_name";
				
				// Data
				$get_stock_type_of_service_list_sdata[':service_name'] = $service_name;
			}
			else
			{
				// Query
				$get_stock_type_of_service_list_squery_where = $get_stock_type_of_service_list_squery_where." and stock_type_of_service_name like :service_name";
				
				// Data
				$get_stock_type_of_service_list_sdata[':service_name'] = '%'.$service_name.'%';
			}
		}				
		
		$filter_count++;
	}
	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_stock_type_of_service_list_squery_where = $get_stock_type_of_service_list_squery_where." where stock_type_of_service_active =:active";								
		}
		else
		{
			// Query
			$get_stock_type_of_service_list_squery_where = $get_stock_type_of_service_list_squery_where." and stock_type_of_service_active =:active";				
		}
		
		// Data
		$get_stock_type_of_service_list_sdata[':active']  = $active;
		
		$filter_count++;
	}

	
	if($added_by!= "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_stock_type_of_service_list_squery_where = $get_stock_type_of_service_list_squery_where." where stock_type_of_service_added_by >= :added_by";								
		}
		else
		{
			// Query
			$get_stock_type_of_service_list_squery_where = $get_stock_type_of_service_list_squery_where." and stock_type_of_service_added_by >= :added_by";				
		}
		
		//Data
		$get_stock_type_of_service_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}

	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_stock_type_of_service_list_squery_where = $get_stock_type_of_service_list_squery_where." where stock_type_of_service_added_on >= :start_date";								
		}
		else
		{
			// Query
			$get_stock_type_of_service_list_squery_where = $get_stock_type_of_service_list_squery_where." and stock_type_of_service_added_on >= :start_date";				
		}
		
		//Data
		$get_stock_type_of_service_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_stock_type_of_service_list_squery_where = $get_stock_type_of_service_list_squery_where." where stock_type_of_service_added_on <= :end_date";								
		}
		else
		{
			// Query
			$get_stock_type_of_service_list_squery_where = $get_stock_type_of_service_list_squery_where." and stock_type_of_service_added_on <= :end_date";				
		}
		
		//Data
		$get_stock_type_of_service_list_sdata['end_date']  = $end_date;
		
		$filter_count++;
	}
	
	$get_stock_type_of_service_list_squery = $get_stock_type_of_service_list_squery_base.$get_stock_type_of_service_list_squery_where;
	
	try
	{
		$dbConnection = get_conn_handle();
		

		$get_stock_type_of_service_list_sstatement = $dbConnection->prepare($get_stock_type_of_service_list_squery);
		
		$get_stock_type_of_service_list_sstatement -> execute($get_stock_type_of_service_list_sdata);
		
		$get_stock_type_of_service_list_sdetails = $get_stock_type_of_service_list_sstatement -> fetchAll();
		
		if(FALSE === $get_stock_type_of_service_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_stock_type_of_service_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_stock_type_of_service_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
 }
 
/*
PURPOSE : To update Stock Type OF Service Master
INPUT 	: Service ID,Stock Type OF Service Master Update Array
OUTPUT 	: Service ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_stock_type_of_service($service_id,$stock_type_of_service_update_data)
{
	if(array_key_exists("service_name",$stock_type_of_service_update_data))
	{	
		$service_name = $stock_type_of_service_update_data["service_name"];
	}
	else
	{
		$service_name = "";
	}
	
	if(array_key_exists("remarks",$stock_type_of_service_update_data))
	{	
		$remarks = $stock_type_of_service_update_data["remarks"];
	}
	else
	{
		$remarks = "";
	}
	
	if(array_key_exists("active",$stock_type_of_service_update_data))
	{	
		$active = $stock_type_of_service_update_data["active"];
	}
	else
	{
		$active = "";
	}
	
	if(array_key_exists("added_by",$stock_type_of_service_update_data))
	{	
		$added_by = $stock_type_of_service_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	if(array_key_exists("added_on",$stock_type_of_service_update_data))
	{	
		$added_on = $stock_type_of_service_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}
	
	// Query
    $stock_type_of_service_update_uquery_base = "update stock_vendor_type_of_service_master set";  
	
	$stock_type_of_service_update_uquery_set = "";
	
	$stock_type_of_service_update_uquery_where = " where stock_type_of_service_id=:service_id";
	
	$stock_type_of_service_update_udata = array(":service_id" =>$service_id);
	
	$filter_count = 0;
	
	if($service_name != "")
	{
		$stock_type_of_service_update_uquery_set = $stock_type_of_service_update_uquery_set." stock_type_of_service_name=:service_name,";
		$stock_type_of_service_update_udata[":service_name"] = $service_name;
		$filter_count++;
	}
	
	if($remarks != "")
	{
		$stock_type_of_service_update_uquery_set = $stock_type_of_service_update_uquery_set." stock_type_of_service_remarks=:remarks,";
		$stock_type_of_service_update_udata[":remarks"] = $remarks;
		$filter_count++;
	}
	
	if($active != "")
	{
		$stock_type_of_service_update_uquery_set = $stock_type_of_service_update_uquery_set." stock_type_of_service_active=:active,";
		$stock_type_of_service_update_udata[":active"] = $active;
		$filter_count++;
	}
	
	if($added_by != "")
	{
		$stock_type_of_service_update_uquery_set = $stock_type_of_service_update_uquery_set." stock_type_of_service_added_by=:added_by,";
		$stock_type_of_service_update_udata[":added_by"] = $added_by;		
		$filter_count++;
	}
	if($added_on != "")
	{
		$stock_type_of_service_update_uquery_set = $stock_type_of_service_update_uquery_set." stock_type_of_service_added_on=:added_on,";
		$stock_type_of_service_update_udata[":added_on"] = $added_on;		
		$filter_count++;
	}
	
	if($filter_count > 0)
	{
		$stock_type_of_service_update_uquery_set = trim($stock_type_of_service_update_uquery_set,',');
	}
	
	$stock_type_of_service_update_uquery = $stock_type_of_service_update_uquery_base.$stock_type_of_service_update_uquery_set.$stock_type_of_service_update_uquery_where;

    try
    {
        $dbConnection = get_conn_handle();
        
        $stock_type_of_service_update_ustatement = $dbConnection->prepare($stock_type_of_service_update_uquery);		
        
        $stock_type_of_service_update_ustatement -> execute($stock_type_of_service_update_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $service_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}
/*
PURPOSE : To add new Stock Vendor Item Mapping
INPUT 	: Vendor ID, Item ID, Remarks, Added By
OUTPUT 	: Mapping ID, success or failure message
BY 		: Lakshmi
*/
function db_add_stock_vendor_item_mapping($vendor_id,$item_id,$remarks,$added_by)
{
	// Query
   $stock_vendor_item_mapping_iquery = "insert into stock_vendor_item_mapping
   (stock_mapping_vendor_id,stock_mapping_item_id,stock_mapping_active,stock_mapping_remarks,stock_mapping_added_by,stock_mapping_added_on)
   values(:vendor_id,:item_id,:active,:remarks,:added_by,:added_on)";  
	
    try
    {
        $dbConnection = get_conn_handle();
        $stock_vendor_item_mapping_istatement = $dbConnection->prepare($stock_vendor_item_mapping_iquery);
        
        // Data
        $stock_vendor_item_mapping_idata = array(':vendor_id'=>$vendor_id,':item_id'=>$item_id,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,
		':added_on'=>date("Y-m-d H:i:s"));	
	
		$dbConnection->beginTransaction();
        $stock_vendor_item_mapping_istatement->execute($stock_vendor_item_mapping_idata);
		$stock_vendor_item_mapping_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
       
        $return["status"] = SUCCESS;
		$return["data"]   = $stock_vendor_item_mapping_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get Stock Vendor Item Mapping list
INPUT 	: Mapping ID, Vendor ID, Item ID, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Stock Vendor Item Mapping
BY 		: Lakshmi
*/
function db_get_stock_vendor_item_mapping($stock_vendor_item_mapping_search_data)
{  
 
	if(array_key_exists("mapping_id",$stock_vendor_item_mapping_search_data))
	{
		$mapping_id = $stock_vendor_item_mapping_search_data["mapping_id"];
	}
	else
	{
		$mapping_id= "";
	}
	
	if(array_key_exists("vendor_id",$stock_vendor_item_mapping_search_data))
	{
		$vendor_id = $stock_vendor_item_mapping_search_data["vendor_id"];
	}
	else
	{
		$vendor_id = "";
	}
	
	if(array_key_exists("item_id",$stock_vendor_item_mapping_search_data))
	{
		$item_id = $stock_vendor_item_mapping_search_data["item_id"];
	}
	else
	{
		$item_id = "";
	}
	
	if(array_key_exists("active",$stock_vendor_item_mapping_search_data))
	{
		$active = $stock_vendor_item_mapping_search_data["active"];
	}
	else
	{
		$active = "";
	}
	
	if(array_key_exists("added_by",$stock_vendor_item_mapping_search_data))
	{
		$added_by = $stock_vendor_item_mapping_search_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("start_date",$stock_vendor_item_mapping_search_data))
	{
		$start_date= $stock_vendor_item_mapping_search_data["start_date"];
	}
	else
	{
		$start_date= "";
	}
	
	if(array_key_exists("end_date",$stock_vendor_item_mapping_search_data))
	{
		$end_date= $stock_vendor_item_mapping_search_data["end_date"];
	}
	else
	{
		$end_date= "";
	}
	
	$get_stock_vendor_item_mapping_list_squery_base = " select * from stock_vendor_item_mapping SVIM inner join stock_vendor_master SVM on SVM.stock_vendor_id = SVIM.stock_mapping_vendor_id inner join stock_material_master SMM on SMM.stock_material_id = SVIM.stock_mapping_item_id inner join users U on U.user_id = SVIM.stock_mapping_added_by";
	
	$get_stock_vendor_item_mapping_list_squery_where = "";
	
	$filter_count = 0;
	
	// Data
	$get_stock_vendor_item_mapping_list_sdata = array();
	
	if($mapping_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_stock_vendor_item_mapping_list_squery_where = $get_stock_vendor_item_mapping_list_squery_where." where stock_vendor_item_mapping_id = :mapping_id";								
		}
		else
		{
			// Query
			$get_stock_vendor_item_mapping_list_squery_where = $get_stock_vendor_item_mapping_list_squery_where." and stock_vendor_item_mapping_id = :mapping_id";				
		}
		
		// Data
		$get_stock_vendor_item_mapping_list_sdata[':mapping_id'] = $mapping_id;
		
		$filter_count++;
	}
	
	if($vendor_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_stock_vendor_item_mapping_list_squery_where = $get_stock_vendor_item_mapping_list_squery_where." where stock_mapping_vendor_id = :vendor_id";								
		}
		else
		{
			// Query
			$get_stock_vendor_item_mapping_list_squery_where = $get_stock_vendor_item_mapping_list_squery_where." and stock_mapping_vendor_id = :vendor_id";				
		}
		
		// Data
		$get_stock_vendor_item_mapping_list_sdata[':vendor_id'] = $vendor_id;
		
		$filter_count++;
	}
	
	if($item_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_stock_vendor_item_mapping_list_squery_where = $get_stock_vendor_item_mapping_list_squery_where." where stock_mapping_item_id = :item_id";								
		}
		else
		{
			// Query
			$get_stock_vendor_item_mapping_list_squery_where = $get_stock_vendor_item_mapping_list_squery_where." and stock_mapping_item_id = :item_id";				
		}
		
		// Data
		$get_stock_vendor_item_mapping_list_sdata[':item_id'] = $item_id;
		
		$filter_count++;
	}
	
	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_stock_vendor_item_mapping_list_squery_where = $get_stock_vendor_item_mapping_list_squery_where." where stock_mapping_active = :active";								
		}
		else
		{
			// Query
			$get_stock_vendor_item_mapping_list_squery_where = $get_stock_vendor_item_mapping_list_squery_where." and stock_mapping_active = :active";				
		}
		
		// Data
		$get_stock_vendor_item_mapping_list_sdata[':active']  = $active;
		
		$filter_count++;
	}
	
	if($added_by!= "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_stock_vendor_item_mapping_list_squery_where = $get_stock_vendor_item_mapping_list_squery_where." where stock_mapping_added_by = :added_by";								
		}
		else
		{
			// Query
			$get_stock_vendor_item_mapping_list_squery_where = $get_stock_vendor_item_mapping_list_squery_where." and stock_mapping_added_by = :added_by";				
		}
		
		//Data
		$get_stock_vendor_item_mapping_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_stock_vendor_item_mapping_list_squery_where = $get_stock_vendor_item_mapping_list_squery_where." where stock_mapping_added_on >= :start_date";								
		}
		else
		{
			// Query
			$get_stock_vendor_item_mapping_list_squery_where = $get_stock_vendor_item_mapping_list_squery_where." and stock_mapping_added_on >= :start_date";				
		}
		
		//Data
		$get_stock_vendor_item_mapping_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_stock_vendor_item_mapping_list_squery_where = $get_stock_vendor_item_mapping_list_squery_where." where stock_mapping_added_on <= :end_date";								
		}
		else
		{
			// Query
			$get_stock_vendor_item_mapping_list_squery_where = $get_stock_vendor_item_mapping_list_squery_where." and stock_mapping_added_on <= :end_date";				
		}
		
		//Data
		$get_stock_vendor_item_mapping_list_sdata['end_date']  = $end_date;
		
		$filter_count++;
	}
	
	$get_stock_vendor_item_mapping_list_order =" order by stock_mapping_added_on DESC";
	$get_stock_vendor_item_mapping_list_squery = $get_stock_vendor_item_mapping_list_squery_base.$get_stock_vendor_item_mapping_list_squery_where.$get_stock_vendor_item_mapping_list_order;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_stock_vendor_item_mapping_list_sstatement = $dbConnection->prepare($get_stock_vendor_item_mapping_list_squery);
		
		$get_stock_vendor_item_mapping_list_sstatement -> execute($get_stock_vendor_item_mapping_list_sdata);
		
		$get_stock_vendor_item_mapping_list_sdetails = $get_stock_vendor_item_mapping_list_sstatement -> fetchAll();
		
		if(FALSE === $get_stock_vendor_item_mapping_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_stock_vendor_item_mapping_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_stock_vendor_item_mapping_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
 }
 
 /*
PURPOSE : To update Stock Vendor Item Mapping
INPUT 	: Mapping ID, Stock Vendor Item Mapping Update Array
OUTPUT 	: Mapping ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_stock_vendor_item_mapping($mapping_id,$stock_vendor_item_mapping_update_data)
{
	
	if(array_key_exists("vendor_id",$stock_vendor_item_mapping_update_data))
	{	
		$vendor_id = $stock_vendor_item_mapping_update_data["vendor_id"];
	}
	else
	{
		$vendor_id = "";
	}
	
	if(array_key_exists("item_id",$stock_vendor_item_mapping_update_data))
	{	
		$item_id = $stock_vendor_item_mapping_update_data["item_id"];
	}
	else
	{
		$item_id = "";
	}
	
	if(array_key_exists("active",$stock_vendor_item_mapping_update_data))
	{	
		$active = $stock_vendor_item_mapping_update_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("remarks",$stock_vendor_item_mapping_update_data))
	{	
		$remarks = $stock_vendor_item_mapping_update_data["remarks"];
	}
	else
	{
		$remarks = "";
	}
	
	if(array_key_exists("added_by",$stock_vendor_item_mapping_update_data))
	{	
		$added_by = $stock_vendor_item_mapping_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("added_on",$stock_vendor_item_mapping_update_data))
	{	
		$added_on = $stock_vendor_item_mapping_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}
	
	// Query
    $stock_vendor_item_mapping_update_uquery_base = "update stock_vendor_item_mapping set";  
	
	$stock_vendor_item_mapping_update_uquery_set = "";
	
	$stock_vendor_item_mapping_update_uquery_where = " where stock_vendor_item_mapping_id = :mapping_id";
	
	$stock_vendor_item_mapping_update_udata = array(":mapping_id"=>$mapping_id);
	
	$filter_count = 0;
	
	if($vendor_id != "")
	{
		$stock_vendor_item_mapping_update_uquery_set = $stock_vendor_item_mapping_update_uquery_set." stock_mapping_vendor_id = :vendor_id,";
		$stock_vendor_item_mapping_update_udata[":vendor_id"] = $vendor_id;
		$filter_count++;
	}
	
	if($item_id != "")
	{
		$stock_vendor_item_mapping_update_uquery_set = $stock_vendor_item_mapping_update_uquery_set." stock_mapping_item_id = :item_id,";
		$stock_vendor_item_mapping_update_udata[":item_id"] = $item_id;
		$filter_count++;
	}
	
	if($active != "")
	{
		$stock_vendor_item_mapping_update_uquery_set = $stock_vendor_item_mapping_update_uquery_set." stock_mapping_active = :active,";
		$stock_vendor_item_mapping_update_udata[":active"] = $active;
		$filter_count++;
	}
	
	if($remarks != "")
	{
		$stock_vendor_item_mapping_update_uquery_set = $stock_vendor_item_mapping_update_uquery_set." stock_mapping_remarks = :remarks,";
		$stock_vendor_item_mapping_update_udata[":remarks"] = $remarks;
		$filter_count++;
	}
	
	if($added_by != "")
	{
		$stock_vendor_item_mapping_update_uquery_set = $stock_vendor_item_mapping_update_uquery_set." stock_mapping_added_by = :added_by,";
		$stock_vendor_item_mapping_update_udata[":added_by"] = $added_by;
		$filter_count++;
	}
	
	if($added_on != "")
	{
		$stock_vendor_item_mapping_update_uquery_set = $stock_vendor_item_mapping_update_uquery_set." stock_mapping_added_on = :added_on,";
		$stock_vendor_item_mapping_update_udata[":added_on"] = $added_on;		
		$filter_count++;
	}
	
	if($filter_count > 0)
	{
		$stock_vendor_item_mapping_update_uquery_set = trim($stock_vendor_item_mapping_update_uquery_set,',');
	}
	
	$stock_vendor_item_mapping_update_uquery = $stock_vendor_item_mapping_update_uquery_base.$stock_vendor_item_mapping_update_uquery_set.$stock_vendor_item_mapping_update_uquery_where;

    try
    {
        $dbConnection = get_conn_handle();
        
        $stock_vendor_item_mapping_update_ustatement = $dbConnection->prepare($stock_vendor_item_mapping_update_uquery);		
        
        $stock_vendor_item_mapping_update_ustatement -> execute($stock_vendor_item_mapping_update_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $mapping_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}
 ?>