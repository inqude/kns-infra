<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: process_list.php
CREATED ON	: 05-June-2015
CREATED BY	: Nitin Kashyap
PURPOSE     : List of Process Plans
*/

/*
TBD: 
1. Date display and calculation
2. Permission management
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'files'.DIRECTORY_SEPARATOR.'file_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'process'.DIRECTORY_SEPARATOR.'process_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'tasks'.DIRECTORY_SEPARATOR.'task_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Query String Data
	if(isset($_GET['bprocess']))
	{
		$bprocess_id = $_GET['bprocess'];
	}
	else
	{
		header("location:pending_bulk_project_list.php");
	}

	// Temp data
	$alert      = "";
	$alert_type = -1;
	
	// Get list of files associated with this bulk process
	$legal_bulk_files_search_data = array('legal_bulk_process_files_process_id'=>$bprocess_id);
	$files_list = i_get_bulk_legal_process_plan_files($legal_bulk_files_search_data);

	if($files_list["status"] == SUCCESS)
	{
		$files_list_data = $files_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$files_list["data"];
	}		
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Bulk Project File List</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>     

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>File List&nbsp;&nbsp;&nbsp;Total Files: <span id="total_files_count_area"><i>Calculating</i></span></h3>			  
            </div>
            <!-- /widget-header -->
						
            <div class="widget-content">
              <table class="table table-bordered" style="table-layout: fixed;">
                <thead>
                  <tr>
				    <th>Bulk Process No</th>	
					<th>File Number</th>										
				    <th>Survey Number</th>
					<th>Land Owner</th>	
					<th>PAN No.</th>						
					<th>Extent</th>
					<th>Village</th>															
				</tr>
				</thead>
				<tbody>
				 <?php
				$total_files  = 0;
				$total_extent = 0;
				if($files_list["status"] == SUCCESS)
				{				
					for($count = 0; $count < count($files_list_data); $count++)
					{						
							$total_files++;
							$total_extent = $total_extent + $files_list_data[$count]["file_extent"];
					?>
					<tr>
						<td style="word-wrap:break-word;"><?php echo $bprocess_id; ?></td>
						<td style="word-wrap:break-word;"><?php echo $files_list_data[$count]["file_number"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $files_list_data[$count]["file_survey_number"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $files_list_data[$count]["file_land_owner"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $files_list_data[$count]["file_pan_number"]; ?></td>
						<td style="word-wrap:break-word;"><?php $extent_array = get_acre_from_guntas($files_list_data[$count]["file_extent"]);
								  echo $extent_array["acres"]." acres ".$extent_array["guntas"]." guntas"; ?></td>	
						<td style="word-wrap:break-word;"><?php if($files_list_data[$count]["village_name"] != "")
						{
							echo $files_list_data[$count]["village_name"]; 
						}
						else
						{
							echo $files_list_data[$count]["file_village"]; 
						}?></td>															
					</tr>
					<?php 												
					}					
					$total_extent_array = get_acre_from_guntas($total_extent);		
					$acres = $total_extent_array["acres"].' acres';
					$guntas = $total_extent_array["guntas"].' guntas';				 				 
				}
				else
				{
				?>
				<tr>
				<td colspan="16">No Files added yet!</td>
				</tr>
				<?php				
				}				
				?>								
                </tbody>
				</table>			
				<script>				
				 document.getElementById("total_files_count_area").innerHTML = <?php echo $total_files; ?>;					 
				 </script>				 				
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function confirm_deletion(file_id)
{
	var ok = confirm("Are you sure you want to delete?")
	{         
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					 window.location = "pending_file_list.php";
				}
			}

			xmlhttp.open("GET", "legal_file_delete.php?file=" + file_id);   // file name where delete code is written
			xmlhttp.send();
		}
	}	
}

</script>

  </body>

</html>
