<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 28-July-2017
// LAST UPDATED BY: Lakshmi
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	
	if(isset($_GET["source"]))
	{
		$source = $_GET["source"];
	}
	else
	{
		$source = "";
	}
	
	if(isset($_GET["cancelation_id"]))
	{
		$cancelation_id   = $_GET["cancelation_id"];
	}
	else
	{
		$cancelation_id = "";
	}
	
	if(isset($_REQUEST['task_id']))
	{
		$task_id = $_REQUEST['task_id'];
	}
	else
	{
		$task_id = '';
	}
	
	$search_plan_type   	 = "";
	
	if(isset($_POST["search_plan_type"]))
	{
		$search_plan_type   = $_POST["search_plan_type"];
	}
	
	// Capture the form data
	if(isset($_POST["edit_plan_cancelation_submit"]))
	{
		$cancelation_id       = $_POST["hd_cancelation_id"];
		$task_id              = $_POST["hd_task_id"];
		$plan_type            = $_POST["txt_plan_type"];
		$remarks 	          = $_POST["txt_remarks"];
		
		// Check for mandatory fields
		if(($plan_type != ""))
		{
			$project_plan_cancelation_update_data = array("plan_type"=>$plan_type,"remarks"=>$remarks);
			$project_plan_cancelation_iresult = i_update_project_plan_cancelation($cancelation_id,$project_plan_cancelation_update_data);
			
			if($project_plan_cancelation_iresult["status"] == SUCCESS)				
			{	
				$alert_type = 1;
		    
				header("location:project_plan_cancelation_list.php");
			}
			else
			{
				$alert 		= $project_plan_cancelation_iresult["data"];
				$alert_type = 0;	
			}						
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}
	
	// Temp Project Plan Cancelation List
	$project_plan_cancelation_search_data = array("active"=>'1',"cancelation_id"=>$cancelation_id);
	$project_plan_cancelation_list = i_get_project_plan_cancelation($project_plan_cancelation_search_data);
	if($project_plan_cancelation_list["status"] == SUCCESS)
	{
		$project_plan_cancelation_list_data = $project_plan_cancelation_list["data"];
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Project Plan - Edit Cancelation</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>
    
<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>    

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Project Plan - Edit Cancelation</h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Project Plan - Edit Cancelation</a>
						  </li>	
						</ul>
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <?php echo $alert; ?>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="project_plan_edit_cancelation_form" class="form-horizontal" method="post" action="project_plan_edit_cancelation.php">
								<input type="hidden" name="hd_task_id" value="<?php echo $task_id; ?>" />
								<input type="hidden" name="hd_cancelation_id" value="<?php echo $cancelation_id; ?>" />
								<input type="hidden" name="hd_source_plan" value="<?php echo $source; ?>" />
									<fieldset>										
																
										<div class="control-group">											
											<label class="control-label" for="txt_plan_type">Plan Type</label>
											<div class="controls">
											<input type="text" name="txt_plan_type" placeholder="Plan Type" value="<?php echo $source ;?>">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="txt_remarks">Remarks</label>
											<div class="controls">
												<input type="text" name="txt_remarks" placeholder="Remarks" value="<?php echo $project_plan_cancelation_list_data[0]["project_plan_cancelation_remarks"] ;?>">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
                                                                                                                                                               										 <br />
										
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="edit_plan_cancelation_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>
								
							</div> 
							
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
