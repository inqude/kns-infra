<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: crm_approved_booking_list.php
CREATED ON	: 04-Nov-2015
CREATED BY	: Nitin Kashyap
PURPOSE     : Approved Booking List
*/

/*
TBD: 
*/$_SESSION['module'] = 'CRM Reports';

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_post_sales_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_transaction_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_sales_process.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_marketing_sales_planning.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_projects'.DIRECTORY_SEPARATOR.'crm_project_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Query String Data
	// Nothing

	// Initialization of data
	$alert = "";
	$sel_month = "";
	$sel_year  = "";
	
	if(isset($_POST["summary_search_submit"]))
	{
		$sel_year  = $_POST["ddl_year"];
		$sel_month = $_POST["ddl_month"];
		
		$selected_month = $sel_year."-".$sel_month;
	}
	else
	{
		$sel_year  = date("Y");
		$sel_month = date("m");
	
		$selected_month = date("Y-m");
	}
	
	// Project List
	$project_list = i_get_project_list('','1');
	if($project_list["status"] == SUCCESS)
	{
		$project_list_data = $project_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$project_list["data"];
		$alert_type = 0; // Failure
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Marketing and Sales Summary Report</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>
    

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">            
			<div class="widget-header" style="height:80px; padding-top:10px;">               
			  <form method="post" id="search_summary_report" action="crm_marketing_sales_summary_report.php">			  		  			  
			  <span style="padding-left:8px; padding-right:8px;">
			    <select name="ddl_month" required>
			    <option value="">- - Select Start Month - -</option>
			    <option value="01" <?php if($sel_month == "01"){ ?> selected <?php } ?>>January</option>
				<option value="02" <?php if($sel_month == "02"){ ?> selected <?php } ?>>February</option>
				<option value="03" <?php if($sel_month == "03"){ ?> selected <?php } ?>>March</option>
				<option value="04" <?php if($sel_month == "04"){ ?> selected <?php } ?>>April</option>
				<option value="05" <?php if($sel_month == "05"){ ?> selected <?php } ?>>May</option>
				<option value="06" <?php if($sel_month == "06"){ ?> selected <?php } ?>>June</option>
				<option value="07" <?php if($sel_month == "07"){ ?> selected <?php } ?>>July</option>
				<option value="08" <?php if($sel_month == "08"){ ?> selected <?php } ?>>August</option>
				<option value="09" <?php if($sel_month == "09"){ ?> selected <?php } ?>>September</option>
				<option value="10" <?php if($sel_month == "10"){ ?> selected <?php } ?>>October</option>
				<option value="11" <?php if($sel_month == "11"){ ?> selected <?php } ?>>November</option>
				<option value="12" <?php if($sel_month == "12"){ ?> selected <?php } ?>>December</option>
			    </select>
			  </span>						
			  <span style="padding-left:8px; padding-right:8px;">
			    <select name="ddl_year" required>
			    <option value="">- - Select Start Year - -</option>
			    <?php
				$start_year = date("Y",strtotime(date("Y")."- 10 years"));
				$end_year   = date("Y");
				for($count = $end_year; $count >= $start_year; $count--)
				{
				?>
				<option value="<?php echo $count; ?>" <?php if($count == $sel_year) {?> selected <?php } ?>><?php echo $count; ?></option>
				<?php
				}
				?>												
			    </select>
			  </span>
			  <span style="padding-left:8px; padding-right:8px;">
			  <input type="submit" name="summary_search_submit" />
			  </span>
			  </form>			  
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
			
              <table class="table table-bordered" style="table-layout: fixed;">
                <thead>
                  <tr>
					<th>Month</th>
					<th>Enquiry Count</th>
					<th>Site Visits</th>
					<th>Sales Target</th>
					<th>Sales</th>					
					<th>Cumulative Enquiry Count</th>
					<th>Cumulative Site Visits</th>
					<th>Cumulative Sales Target</th>			
					<th>Cumulative Sales</th>									
				</tr>
				</thead>
				<tbody>							
				<?php					
					$cum_enquiry_count = 0;
					$cum_sv_count      = 0;
					$cum_sales_target  = 0;
					$cum_sales         = 0;
					for($count = 0; $count < 12; $count++)
					{
						$start_date = $selected_month."-01 00:00:00";
						$end_date   = $selected_month."-31 23:59:59";												
					
						// Enquiry Count
						$enquiry_count_data = i_get_enquiry_list('','','','','','','','','','','','','',$start_date,$end_date,'','','','','');
						if($enquiry_count_data["status"] == SUCCESS)
						{
							$enq_count = count($enquiry_count_data["data"]);
						}
						else
						{
							$enq_count = 0;
						}
						$cum_enquiry_count = $cum_enquiry_count + $enq_count;
						
						// Site Visit Count
						$sv_count_data = i_get_site_travel_plan_list('','','','','','','2','','','',$start_date,$end_date);
						if($sv_count_data["status"] == SUCCESS)
						{
							$sv_count = count($sv_count_data["data"]);
						}
						else
						{
							$sv_count = 0;
						}
						$cum_sv_count = $cum_sv_count + $sv_count;
						
						// Sales Count i.e. Booking approved count
						$approved_booking_list = i_get_site_booking('','','','','','1','','','','','','','','',$start_date,$end_date,'');
						if($approved_booking_list["status"] == SUCCESS)
						{
							$sales_count = count($approved_booking_list["data"]);
						}
						else
						{
							$sales_count = 0;
						}
						$cum_sales = $cum_sales + $sales_count;
						
						// Sales Target
						$cur_month_year_array = explode('-',$selected_month);
						$sales_target_data = array("month"=>$cur_month_year_array[1],"year"=>$cur_month_year_array[0],"order"=>"added_on_desc");
						$sales_target = i_get_sales_target($sales_target_data);
						if($sales_target["status"] == SUCCESS)
						{
							$sales_target_value = $sales_target["data"][0]["crm_sales_target"];
						}
						else
						{
							$sales_target_value = 0;
						}
						$cum_sales_target = $cum_sales_target + $sales_target_value - $sales_count;
						?>
						<tr>
						<td style="word-wrap:break-word;"><?php echo date("M-Y",strtotime($selected_month)); ?></td>
						<td style="word-wrap:break-word;"><?php echo $enq_count; ?></td>
						<td style="word-wrap:break-word;"><?php echo $sv_count; ?></td>
						<td style="word-wrap:break-word; background-color:#E6E6FA;"><?php echo $sales_target_value; ?></td>
						<td style="word-wrap:break-word; background-color:#98FB98;"><?php echo $sales_count; ?></td>
						<td style="word-wrap:break-word;"><?php echo $cum_enquiry_count; ?></td>
						<td style="word-wrap:break-word;"><?php echo $cum_sv_count; ?></td>					
						<td style="word-wrap:break-word;"><?php echo $cum_sales_target; ?></td>
						<td style="word-wrap:break-word;"><?php echo $cum_sales; ?></td>											
						</tr>
						<?php
						$selected_month = date("Y-m",strtotime($selected_month.' + 1 month'));
					}
					?>
                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>/* Open the sidenav */function openNav() {    document.getElementById("mySidenav").style.width = "75%";}/* Close/hide the sidenav */function closeNav() {    document.getElementById("mySidenav").style.width = "0";}</script>

  </body>

</html>