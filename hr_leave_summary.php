<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: hr_leave_summary.php
CREATED ON	: 02-June-2016
CREATED BY	: Nitin Kashyap
PURPOSE     : Leave Summary
*/

/*
TBD: 
1. Date display and calculation
2. Permission management
*/
$_SESSION['module'] = 'HR';

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'hr'.DIRECTORY_SEPARATOR.'hr_attendance_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'hr'.DIRECTORY_SEPARATOR.'hr_employee_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Query String Data
	// Nothing here

	// Temp data
	$alert      = "";
	$alert_type = -1;	
	
	// Logged In Employee
	$employee_filter_data = array("employee_user"=>$user,"user_status"=>'1');
	$search_user_sresult = i_get_employee_list($employee_filter_data);		

	// Search parameters
	if(isset($_POST["leave_search_submit"]))
	{				
		$selected_employee = $_POST["ddl_employee"];
	}
	else
	{				
		$selected_employee = $search_user_sresult["data"][0]["hr_employee_id"];
	}

	
	// Get list of employees for drop down
	if(($role == "13") || ($user == "143620071466608200"))
	{
		$employee_filter_data = array("user_status"=>'1');
	}
	else
	{
		$employee_filter_data = array("employee_manager"=>$user,"user_status"=>'1');
	}
	$employee_list = i_get_employee_list($employee_filter_data);

	if($employee_list["status"] == SUCCESS)
	{
		$employee_list_data = $employee_list["data"];
	}
	else
	{
		$alert      = $alert."Alert: ".$employee_list["data"];
		$alert_type = "1";
	}
	
	// Get filtered list of employees for display
	$disp_employee_filter_data = array();	
	
	if($selected_employee != "")
	{
		$disp_employee_filter_data["employee_id"] = $selected_employee;
	}
	else
	{
		if(($role == "13") || ($user == "143620071466608200"))
		{
			// Nothing here
		}
		else
		{
			// Check if this user is a manager to any other user		
			$mgr_employee_filter_data = array("employee_manager"=>$user,"user_status"=>'1');
			$mgr_employee_list = i_get_employee_list($mgr_employee_filter_data);
			if($mgr_employee_list["status"] == SUCCESS)
			{
				$disp_employee_filter_data = array("employee_manager"=>$user);
			}
			else
			{
				$disp_employee_filter_data = array("employee_manager"=>"-1");
			}
		}
	}
	
	$disp_employee_list = i_get_employee_list($disp_employee_filter_data);

	if($disp_employee_list["status"] == SUCCESS)
	{
		$disp_employee_list_data = $disp_employee_list["data"];
	}
	else
	{
		$alert      = $alert."Alert: ".$disp_employee_list["data"];
		$alert_type = "1";
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Leave Summary</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>     

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Leave Summary</h3>			  
            </div>
            <!-- /widget-header -->
			
			<div class="widget-header" style="height:50px; padding-top:10px;">               
			  <form method="post" id="leave_search_form" action="hr_leave_summary.php">			  			  			  			  		  
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="ddl_employee">			  
			  <option value="">- - All Employees - -</option>			  			 
			  <option value="<?php echo $search_user_sresult["data"][0]["hr_employee_id"]; ?>" <?php if($selected_employee == $search_user_sresult["data"][0]["hr_employee_id"]) { ?> selected="selected" <?php } ?>><?php echo $search_user_sresult["data"][0]["hr_employee_name"]; ?></option>			  
			  <?php
			  for($count = 0; $count < count($employee_list_data); $count++)
			  {
				if($search_user_sresult["data"][0]["hr_employee_id"] != $employee_list_data[$count]["hr_employee_id"])
				{
			  ?>
			  <option value="<?php echo $employee_list_data[$count]["hr_employee_id"]; ?>" <?php if($selected_employee == $employee_list_data[$count]["hr_employee_id"]) { ?> selected="selected" <?php } ?>><?php echo $employee_list_data[$count]["hr_employee_name"]; ?></option>
			  <?php
			    }
			  }
			  ?>
			  </select>
			  </span>			  
			  <input type="submit" name="leave_search_submit" />
			  </form>			  
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-bordered" style="table-layout: fixed;">
                <thead>
                  <tr>
				    <th>Leaves As on</th>
					<th>Employee Name</th>
					<th>Employee Code</th>
					<th>Casual Leave</th>
					<th>Sick Leave</th>
					<th>Comp Off</th>
					<th>Total</th>
				  </tr>
				</thead>
				<tbody>	
				<?php
				if($disp_employee_list['status'] == SUCCESS)
				{
					for($count = 0; $count < count($disp_employee_list_data); $count++)
					{
						$employee = $disp_employee_list_data[$count]["hr_employee_id"];
						if(($selected_employee != "") || ($search_user_sresult['data'][0]["hr_employee_id"] != $employee))
						{
							// Get number of CLs
							$cl_count = p_get_pending_leaves($employee,LEAVE_TYPE_EARNED,date("Y-m-d"));
							// Get number of SLs
							$sl_count = p_get_pending_leaves($employee,LEAVE_TYPE_SICK,date("Y-m-d"));
							// Get number of CLs
							$co_count = p_get_pending_leaves($employee,LEAVE_TYPE_COMP_OFF,date("Y-m-d"));
							?>
							  <tr>				  
								<td><?php echo date("d-M-Y"); ?></td>
								<td><?php echo $disp_employee_list_data[$count]["hr_employee_name"]; ?></td>
								<td><?php echo $disp_employee_list_data[$count]["hr_employee_code"]; ?></td>
								<td><?php echo $cl_count; ?></td>
								<td><?php echo $sl_count; ?></td>
								<td><a href="hr_comp_off_list.php?employee_id=<?php echo $disp_employee_list_data[$count]["hr_employee_id"]; ?>"><?php echo $co_count; ?></a></td>
								<td><?php echo $cl_count + $sl_count + $co_count; ?></td>
							  </tr>
							<?php
						}
					}
				}				
				if($selected_employee == "")
				{
					$employee = $search_user_sresult['data'][0]["hr_employee_id"];
					// Get number of CLs
					$cl_count = p_get_pending_leaves($employee,LEAVE_TYPE_EARNED,date("Y-m-d"));
					// Get number of SLs
					$sl_count = p_get_pending_leaves($employee,LEAVE_TYPE_SICK,date("Y-m-d"));
					// Get number of CLs
					$co_count = p_get_pending_leaves($employee,LEAVE_TYPE_COMP_OFF,date("Y-m-d"));
					?>
					<tr>				  
						<td><?php echo date("d-M-Y"); ?></td>
						<td><?php echo $search_user_sresult['data'][0]["hr_employee_name"]; ?></td>
						<td><?php echo $search_user_sresult['data'][0]["hr_employee_code"]; ?></td>
						<td><?php echo $cl_count; ?></td>
						<td><?php echo $sl_count; ?></td>
						<td><?php echo $co_count; ?></td>
						<td><?php echo $cl_count + $sl_count + $co_count; ?></td>
					</tr>
					<?php
				}				
				?>
                </tbody>
              </table>			  
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function confirm_deletion(file_id)
{
	var ok = confirm("Are you sure you want to delete?")
	{         
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					 window.location = "pending_file_list.php";
				}
			}

			xmlhttp.open("GET", "legal_file_delete.php?file=" + file_id);   // file name where delete code is written
			xmlhttp.send();
		}
	}	
}

</script>
<script>
/* Open the sidenav */
function openNav() {
    document.getElementById("mySidenav").style.width = "75%";
}

/* Close/hide the sidenav */
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}
</script>

  </body>

</html>
