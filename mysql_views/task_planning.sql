-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 28, 2018 at 09:45 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vafa_5`
--

-- --------------------------------------------------------

--
-- Structure for view `task_planning`
--

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `task_planning`  AS  select `ptp`.`project_task_planning_id` AS `project_task_planning_id`,`ptp`.`project_task_planning_project_id` AS `project_task_planning_project_id`,`ptp`.`project_task_planning_task_id` AS `project_task_planning_task_id`,`ptp`.`project_task_planning_measurment` AS `project_task_planning_measurment`,`ptp`.`project_task_planning_no_of_roads` AS `project_task_planning_no_of_roads`,`ptp`.`project_task_planning_no_of_object` AS `project_task_planning_no_of_object`,`ptp`.`project_task_planning_total_days` AS `project_task_planning_total_days`,`ptp`.`project_task_planning_object_type` AS `project_task_planning_object_type`,`ptp`.`project_task_planning_machine_type` AS `project_task_planning_machine_type`,`ptp`.`project_task_planning_uom` AS `project_task_planning_uom`,`ptp`.`project_task_planning_rate` AS `project_task_planning_rate`,`ptp`.`project_task_planning_per_day_out` AS `project_task_planning_per_day_out`,`ptp`.`project_task_planning_start_date` AS `project_task_planning_start_date`,`ptp`.`project_task_planning_end_date` AS `project_task_planning_end_date`,`ptp`.`project_task_planning_active` AS `project_task_planning_active`,`ptp`.`project_task_planning_remarks` AS `project_task_planning_remarks`,`ptp`.`project_task_planning_added_by` AS `project_task_planning_added_by`,`ptp`.`project_task_planning_added_on` AS `project_task_planning_added_on`,`u`.`user_id` AS `user_id`,`u`.`user_name` AS `user_name`,`ppt`.`project_process_id` AS `project_process_id`,`ppp`.`project_plan_process_name` AS `project_plan_process_name`,`pmpm`.`project_master_name` AS `project_master_name`,`pmpm`.`project_management_master_id` AS `project_management_master_id`,`ppm`.`project_process_master_id` AS `project_process_master_id`,`ppm`.`project_process_master_order` AS `project_process_master_order`,`pslmm`.`project_site_location_mapping_master_name` AS `project_site_location_mapping_master_name`,`pslmm`.`project_site_location_mapping_master_order` AS `project_site_location_mapping_master_order`,`ppm`.`project_process_master_name` AS `project_process_master_name`,`ptm`.`project_task_master_name` AS `project_task_master_name`,`ptm`.`project_task_master_id` AS `project_task_master_id`,`ptm`.`project_task_master_order` AS `project_task_master_order`,`pum`.`project_uom_name` AS `uom_name`,`poom`.`project_object_output_object_type` AS `project_object_output_object_type`,`poom`.`project_object_output_object_type_reference_id` AS `project_object_output_object_type_reference_id`,`poom`.`project_object_output_obejct_per_hr` AS `project_object_output_obejct_per_hr`,`ppt`.`project_process_task_status` AS `pause_status`,`pcwm`.`project_cw_master_name` AS `project_cw_master_name`,`pmtm`.`project_machine_type_master_name` AS `project_machine_master_name` from ((((((((((((((`project_task_planning` `ptp` join `users` `u` on((`u`.`user_id` = `ptp`.`project_task_planning_added_by`))) join `project_plan_process_task` `ppt` on((`ppt`.`project_process_task_id` = `ptp`.`project_task_planning_task_id`))) join `project_plan_process` `ppp` on((`ppp`.`project_plan_process_id` = `ppt`.`project_process_id`))) join `project_plan` `pp` on((`pp`.`project_plan_id` = `ppp`.`project_plan_process_plan_id`))) join `project_management_project_master` `pmpm` on((`pmpm`.`project_management_master_id` = `pp`.`project_plan_project_id`))) left join `project_site_location_mapping_master` `pslmm` on((`pslmm`.`project_site_location_mapping_master_id` = `ptp`.`project_task_planning_no_of_roads`))) join `project_process_master` `ppm` on((`ppm`.`project_process_master_id` = `ppp`.`project_plan_process_name`))) join `project_task_master` `ptm` on((`ptm`.`project_task_master_id` = `ppt`.`project_process_task_type`))) left join `project_object_output_master` `poom` on((`poom`.`project_object_output_master_id` = `ptp`.`project_task_planning_object_type`))) left join `project_cw_master` `pcwm` on((`ptp`.`project_task_planning_machine_type` = `pcwm`.`project_cw_master_id`))) left join `project_machine_master` `pmm` on((`poom`.`project_object_output_object_type_reference_id` = `pmm`.`project_machine_master_id`))) left join `project_machine_type_master` `pmtm` on((`pmtm`.`project_machine_type_master_id` = `ptp`.`project_task_planning_machine_type`))) left join `project_cw_master` `pcm` on((`pcm`.`project_cw_master_id` = `ptp`.`project_task_planning_machine_type`))) left join `project_uom_master` `pum` on((`pum`.`project_uom_id` = `ptp`.`project_task_planning_uom`))) ;

--
-- VIEW  `task_planning`
-- Data: None
--


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
