-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 27, 2018 at 12:13 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vafa_5`
--

-- --------------------------------------------------------

--
-- Structure for view `project_budget_history`
--

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `project_budget_history`  AS  select `pbch`.`budget_history_id` AS `budget_history_id`,`pbch`.`budget_history_project` AS `budget_history_project`,`pbch`.`budget_history_process_id` AS `budget_history_process_id`,`pbch`.`budget_history_task_id` AS `budget_history_task_id`,`pbch`.`budget_history_road_id` AS `budget_history_road_id`,`pbch`.`budget_history_manpower` AS `budget_history_manpower`,`pbch`.`budget_history_machine` AS `budget_history_machine`,`pbch`.`budget_history_contract` AS `budget_history_contract`,`pbch`.`budget_history_material` AS `budget_history_material`,`pbch`.`budget_history_file` AS `budget_history_file`,`pbch`.`budget_history_remarks` AS `budget_history_remarks`,`pbch`.`budget_history_changed_by` AS `budget_history_changed_by`,`pbch`.`budget_history_changed_on` AS `budget_history_changed_on`,`pmpm`.`project_master_name` AS `project_master_name`,`ppm`.`project_process_master_name` AS `project_process_master_name`,`ppm`.`project_process_master_id` AS `project_process_master_id`,`ptm`.`project_task_master_name` AS `project_task_master_name`,`ptm`.`project_task_master_id` AS `project_task_master_id`,`pslmm`.`project_site_location_mapping_master_name` AS `project_site_location_mapping_master_name`,`ppp`.`project_plan_process_id` AS `Plan_process_id`,`u`.`user_name` AS `user_name` from (((((((`project_budget_change_history` `pbch` join `users` `u` on((`u`.`user_id` = `pbch`.`budget_history_changed_by`))) join `project_plan_process_task` `ppt` on((`ppt`.`project_process_task_id` = `pbch`.`budget_history_task_id`))) join `project_plan_process` `ppp` on((`ppp`.`project_plan_process_id` = `ppt`.`project_process_id`))) join `project_management_project_master` `pmpm` on((`pmpm`.`project_management_master_id` = `pbch`.`budget_history_project`))) left join `project_site_location_mapping_master` `pslmm` on((`pslmm`.`project_site_location_mapping_master_id` = `pbch`.`budget_history_road_id`))) join `project_process_master` `ppm` on((`ppm`.`project_process_master_id` = `ppp`.`project_plan_process_name`))) join `project_task_master` `ptm` on((`ptm`.`project_task_master_id` = `ppt`.`project_process_task_type`))) ;

--
-- VIEW  `project_budget_history`
-- Data: None
--


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
