-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 09, 2018 at 07:18 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vafa_5`
--

-- --------------------------------------------------------

--
-- Structure for view `prospective_profile`
--

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `prospective_profile`  AS  select `PM`.`project_name` AS `project_name`,`E`.`enquiry_id` AS `enquiry_id`,`E`.`added_on` AS `added_on`,`E`.`enquiry_number` AS `enquiry_number`,`E`.`project_id` AS `project_id`,`E`.`site_id` AS `site_id`,`E`.`name` AS `name`,`E`.`cell` AS `cell`,`E`.`email` AS `email`,`E`.`company` AS `company`,`E`.`location` AS `location`,`E`.`source` AS `source`,`E`.`interest_status` AS `interest_status`,`E`.`remarks` AS `remarks`,`E`.`walk_in` AS `walk_in`,`cis`.`crm_cust_interest_status_name` AS `crm_cust_interest_status_name`,`EM`.`enquiry_source_master_name` AS `enquiry_source_master_name`,`EM`.`enquiry_source_master_id` AS `enquiry_source_master_id`,`PP`.`crm_prospective_profile_id` AS `crm_prospective_profile_id`,`PP`.`crm_prospective_profile_enquiry` AS `crm_prospective_profile_enquiry`,`PP`.`crm_prospective_profile_occupation` AS `crm_prospective_profile_occupation`,`PP`.`crm_prospective_profile_other_income` AS `crm_prospective_profile_other_income`,`PP`.`crm_prospective_profile_num_dependents` AS `crm_prospective_profile_num_dependents`,`PP`.`crm_prospective_profile_annual_income` AS `crm_prospective_profile_annual_income`,`PP`.`crm_prospective_profile_loan_type` AS `crm_prospective_profile_loan_type`,`PP`.`crm_prospective_profile_total_emi` AS `crm_prospective_profile_total_emi`,`PP`.`crm_prospective_profile_loan_reject` AS `crm_prospective_profile_loan_reject`,`PP`.`crm_prospective_profile_current_living` AS `crm_prospective_profile_current_living`,`PP`.`crm_prospective_profile_rent` AS `crm_prospective_profile_rent`,`PP`.`crm_prospective_profile_buying_interest` AS `crm_prospective_profile_buying_interest`,`PP`.`crm_prospective_profile_funding_source` AS `crm_prospective_profile_funding_source`,`PP`.`crm_prospective_profile_loan_eligibility` AS `crm_prospective_profile_loan_eligibility`,`PP`.`crm_prospective_profile_loan_tenure` AS `crm_prospective_profile_loan_tenure`,`PP`.`crm_prospective_profile_buy_duration` AS `crm_prospective_profile_buy_duration`,`PP`.`crm_prospective_profile_tentative_closure_date` AS `crm_prospective_profile_tentative_closure_date`,`PP`.`crm_prospective_profile_remarks` AS `crm_prospective_profile_remarks`,`PP`.`crm_prospective_profile_added_by` AS `crm_prospective_profile_added_by`,`PP`.`crm_prospective_profile_added_on` AS `crm_prospective_profile_added_on`,`PP`.`sold_status` AS `sold_status`,`U`.`user_name` AS `stm`,`AU`.`user_name` AS `added_by` from ((((((`crm_prospective_profile` `PP` join `crm_enquiry` `E` on((`E`.`enquiry_id` = `PP`.`crm_prospective_profile_enquiry`))) join `crm_project_master` `PM` on((`PM`.`project_id` = `E`.`project_id`))) join `crm_customer_interest_status` `cis` on((`cis`.`crm_cust_interest_status_id` = `E`.`interest_status`))) join `crm_enquiry_source_master` `EM` on((`EM`.`enquiry_source_master_id` = `E`.`source`))) join `users` `U` on((`U`.`user_id` = `E`.`assigned_to`))) join `users` `AU` on((`AU`.`user_id` = `PP`.`crm_prospective_profile_added_by`))) ;

--
-- VIEW  `prospective_profile`
-- Data: None
--


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
