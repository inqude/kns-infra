<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: project_machine_vendor_mapping_list.php
CREATED ON	: 28-Nov-2016
CREATED BY	: Lakshmi
PURPOSE     : List of project for customer withdrawals
*/

/*
TBD: 
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_vendor_functions.php');

	$alert_type = -1;
	$alert = "";
if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Query String Data
	// Nothing
	$search_machine   	 = "";
	$search_vendor       = "";
	
	if(isset($_POST["file_search_submit"]))
	{
		
		$search_machine   = $_POST["hd_machine_id"];
		$search_vendor    = $_POST["hd_vendor_id"];
	}
	
	// Temp data
	$project_machine_master_search_data = array("active"=>'1');
	$project_machine_master_list = i_get_project_machine_master($project_machine_master_search_data);
	if($project_machine_master_list["status"] == SUCCESS)
	{
		$project_machine_master_list_data = $project_machine_master_list["data"];
	}	

	else
	{
		$alert = $project_machine_master_list["data"];
		$alert_type = 0;
	}
	
	//Get Stock Vendor Master List
	$stock_vendor_master_search_data = array("active"=>'1');
	$stock_vendor_master_list = i_get_stock_vendor_master_list($stock_vendor_master_search_data);
	if($stock_vendor_master_list["status"] == SUCCESS)
	{
		$stock_vendor_master_list_data = $stock_vendor_master_list["data"];
	}
	else
	{ 
		$alert = $stock_vendor_master_list["data"];
		$alert_type = 0;
	}

	// Temp data
	$project_machine_vendor_mapping_search_data = array("active"=>'1',"machine_id"=>$search_machine,"vendor_id"=>$search_vendor);
	$project_machine_vendor_mapping_list = i_get_project_machine_vendor_mapping($project_machine_vendor_mapping_search_data);
	if($project_machine_vendor_mapping_list["status"] == SUCCESS)
	{
		$project_machine_vendor_mapping_list_data = $project_machine_vendor_mapping_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$project_machine_vendor_mapping_list["data"];
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Project Machine Vendor Mapping List</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
	<link href="css/style1.css" rel="stylesheet">
   

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>
    

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Project Machine Vendor Mapping List</h3><span style="float:right; padding-right:20px;"><a href="project_add_machine_vendor_mapping.php">Project Add Machine Vendor Mapping</a></span>
            </div>
            <!-- /widget-header -->
			 <div class="widget-header" style=" padding-top:10px;">               
			  <form method="post" id="file_search_form" action="project_machine_vendor_mapping_list.php">
			  <input type="hidden" name="hd_machine_id" id="hd_machine_id"  value="<?php echo $search_machine; ?>" />
              <input type="hidden" name="hd_vendor_id" id="hd_vendor_id" value="<?php echo $search_vendor; ?>" />			  
			    <div style="padding-left:20px; width:220px; float:left;">	
					<input type="text" name="stxt_machine" autocomplete="off" id="stxt_machine" onkeyup="return get_machine_list();"placeholder="Search by Machine" />
					<div id="search_results" class="dropdown-content"></div>
					</div>
					
			     <div style="padding-left:20px; width:220px; float:left;">						
					<input type="text" name="stxt_vendor" autocomplete="off" id="stxt_vendor" onkeyup="return get_vendor_list();" placeholder="Search by vendor name" />
					<div id="search_results_vendor" class="dropdown-content"></div>
			  </div>
			  <input type="submit" name="file_search_submit" />
			  </form>			  
            </div>
            <div class="widget-content">
			
              <table class="table table-bordered">
                <thead>
                  <tr>
				    <th>SL No</th>
					<th>Machine name</th>
					<th>Vendor Name</th>
					<th>Remarks</th>					
					<th>Added On</th>									
					<th colspan="2" style="text-align:center;">Actions</th>
    					
				</tr>
				</thead>
				<tbody>							
				<?php
				if($project_machine_vendor_mapping_list["status"] == SUCCESS)
				{
					$sl_no = 0;
					for($count = 0; $count < count($project_machine_vendor_mapping_list_data); $count++)
					{
						$sl_no++;
					?>
					<tr>
					<td><?php echo $sl_no; ?></td>
					<td><?php echo $project_machine_vendor_mapping_list_data[$count]["project_machine_master_name"]; ?></td>
					<td><?php echo $project_machine_vendor_mapping_list_data[$count]["stock_vendor_name"]; ?></td>
					<td><?php echo $project_machine_vendor_mapping_list_data[$count]["project_machine_vendor_mapping_remarks"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($project_machine_vendor_mapping_list_data[$count][
					"project_machine_vendor_mapping_added_on"])); ?></td>
					<td style="word-wrap:break-word;"><a style="padding-right:10px" href="#" onclick="return go_to_project_edit_machine_vendor_mapping('<?php echo $project_machine_vendor_mapping_list_data[$count]["project_machine_vendor_mapping_id"]; ?>');">Edit </a></div></td>
					<td><?php if(($project_machine_vendor_mapping_list_data[$count]["project_machine_vendor_mapping_active"] == "1")){?><a href="#" onclick="return project_delete_machine_vendor_mapping(<?php echo $project_machine_vendor_mapping_list_data[$count]["project_machine_vendor_mapping_id"]; ?>);">Delete</a><?php } ?></td>
					</tr>
					<?php
					}
					
				}
				else
				{
				?>
				<td colspan="6">No Project Master condition added yet!</td>
				
				<?php
				}
				 ?>	

                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function project_delete_machine_vendor_mapping(mapping_id)
{
	var ok = confirm("Are you sure you want to Delete?")
	{         
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else					
					{
					 window.location = "project_machine_vendor_mapping_list.php";
					}
				}
			}

			xmlhttp.open("POST", "project_delete_machine_vendor_mapping.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("mapping_id=" + mapping_id + "&action=0");
		}
	}	
}
function go_to_project_edit_machine_vendor_mapping(mapping_id)
{		
	var form = document.createElement("form");
    form.setAttribute("method", "post");
    form.setAttribute("action", "project_edit_machine_vendor_mapping.php");
	
	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","mapping_id");
	hiddenField1.setAttribute("value",mapping_id);
	
	form.appendChild(hiddenField1);
	
	document.body.appendChild(form);
    form.submit();
}

function get_machine_list()
{ 
	var searchstring = document.getElementById('stxt_machine').value;
	
	if(searchstring.length >= 3)
	{
		if (window.XMLHttpRequest)
		{// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp = new XMLHttpRequest();
		}
		else
		{// code for IE6, IE5
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}

		xmlhttp.onreadystatechange = function()
		{				
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
			{		
				if(xmlhttp.responseText != 'FAILURE')
				{
					document.getElementById('search_results').style.display = 'block';
					document.getElementById('search_results').innerHTML     = xmlhttp.responseText;
				}
			}
		}

		xmlhttp.open("POST", "ajax/project_get_machine.php");   // file name where delete code is written
		xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xmlhttp.send("search=" + searchstring);				
	}
	else	
	{
		document.getElementById('search_results').style.display = 'none';
	}
}

function select_machine(machine_id,name)
{
	document.getElementById('hd_machine_id').value 	= machine_id;
	document.getElementById('stxt_machine').value = name;
	
	document.getElementById('search_results').style.display = 'none';
}

function get_vendor_list()
{ 
	var searchstring = document.getElementById('stxt_vendor').value;
	
	if(searchstring.length >= 3)
	{
		if (window.XMLHttpRequest)
		{// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp = new XMLHttpRequest();
		}
		else
		{// code for IE6, IE5
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}

		xmlhttp.onreadystatechange = function()
		{				
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
			{	
				if(xmlhttp.responseText != 'FAILURE')
				{
					document.getElementById('search_results_vendor').style.display = 'block';
					document.getElementById('search_results_vendor').innerHTML     = xmlhttp.responseText;
				}
			}
		}

		xmlhttp.open("POST", "ajax/project_get_vendor.php");   // file name where delete code is written
		xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xmlhttp.send("search=" + searchstring);				
	}
	else	
	{
		document.getElementById('search_results_vendor').style.display = 'none';
	}
}

function select_vendor(vendor_id,search_vendor)
{
	document.getElementById('hd_vendor_id').value = vendor_id;
	document.getElementById('stxt_vendor').value = search_vendor;
	
	document.getElementById('search_results_vendor').style.display = 'none';
}

</script>

  </body>

</html>