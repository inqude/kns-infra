<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 4th oct 2016
// LAST UPDATED BY: Lakshmi
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* DEFINES - START */
define('METHOD_PLAN_FUNC_ID','282');
/* DEFINES - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_vendor_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_indent_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_quotation_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Get permission settings for this user for this page
	$view_perms_list   = i_get_user_perms($user,'',METHOD_PLAN_FUNC_ID,'2','1');
	$add_perms_list    = i_get_user_perms($user,'',METHOD_PLAN_FUNC_ID,'1','1');
	$edit_perms_list   = i_get_user_perms($user,'',METHOD_PLAN_FUNC_ID,'3','1');
	$delete_perms_list = i_get_user_perms($user,'',METHOD_PLAN_FUNC_ID,'4','1');

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	// Query String Data
	if(isset($_REQUEST['project_id']))
	{
		$project_id = $_REQUEST['project_id'];
	}
	else
	{
		$project_id = '-1';
	}
	if(isset($_REQUEST['plan_id']))
	{
		$plan_id = $_REQUEST['plan_id'];
	}
	else
	{
		$plan_id = '';
	}
	// Nothing
	if(isset($_REQUEST['process_id']))
	{
		$process_id = $_REQUEST['process_id'];
	}
	else
	{
		$process_id = "";
	}

// Array ( [hd_process_id] => 3 [stxt_remarks] => test [rb_plan_type] => 1 )
	// Capture the form data
	if(isset($_POST["hd_process_id"]))
	{
		$process_id    = $_POST["hd_process_id"];
		$project_id       = $_POST["hd_project_id"];
		$doc 		   = upload("file_remarks_doc",$user);
		$plan_type     = $_POST["rb_plan_type"];
		$remarks       = $_POST["stxt_remarks"];
		$task_iresult  = i_add_project_task_method_planning($process_id,$project_id,$doc,$plan_type,$remarks,$user);
		if($task_iresult['status'] == 0){
			echo '<script>window.parent.closeModalWindow()</script>';
			exit;
		} else {
			echo 'faield to upload, try again';
		}
		exit;
	}

	// Get Project Management Master modes already added
	$project_management_master_search_data = array("active"=>'1', "user_id"=>$user);
	$project_management_master_list = i_get_project_management_master_list($project_management_master_search_data);
	if($project_management_master_list['status'] == SUCCESS)
	{
		$project_management_master_list_data = $project_management_master_list['data'];
	}

	else
	{
		$alert = $project_management_master_list["data"];
		$alert_type = 0;
	}
}
else
{
	header("location:login.php");
}

// Array ( [file_remarks_doc] => Array ( [name] => test [type] => application/octet-stream [tmp_name] => /opt/lampp/temp/php1l2rWq [error] => 0 [size] => 191 ) )
// Functions
function upload($file_id, $user_id)
{
	if($_FILES[$file_id]["name"]!="")
	{
		if ($_FILES[$file_id]["error"] > 0)
		{
			// echo "Return Code: " . $_FILES[$file_id]["error"] . "<br />";
		}
		else
		{
			$_FILES[$file_id]["name"]=$user_id."_".$_FILES[$file_id]["name"];
			move_uploaded_file($_FILES[$file_id]["tmp_name"], "documents/".$_FILES[$file_id]["name"]);
		}
	}

	return $_FILES[$file_id]["name"];
}
?>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	<h4 class="modal-title">Add File</h4>
</div>
<div class="modal-body container-fluid">
	<iframe src="about:blank" id="iframe_file_upload" name="iframe_file_upload" style="width:0px; height:0px;"></iframe>
	<form action="project_upload_documents.php" name="add_method_plan_submit" method="POST" target="iframe_file_upload" enctype="multipart/form-data">
		<input type="hidden" id="hd_process_id" name="hd_process_id" class="form-control" value="<?php echo $process_id; ?>" />
		<input type="hidden" id="hd_project_id" name="hd_project_id" class="form-control" value="<?php echo $project_id; ?>" />

		<div class="form-group">
		<label class="control-label" for="date">Date:</label>
    <input type="input" id="date" name="date" class="form-control" disabled value="<?php echo date("d/m/Y h:i:s a");?>" />
	</div>

	  <div class="form-group">
		<label class="control-label" for="file_remarks_doc">Document:</label>
	 	<input  type="file"  name="file_remarks_doc" id="file_remarks_doc">
  	</div>

		<div class="form-group">
    <label class="control-label" for="stxt_remarks">Remarks:</label>
    <input type="text" class="form-control" id="stxt_remarks" name="stxt_remarks">
    </div>

	  <div class="form-group">
			<input  type="radio" name="rb_plan_type" id="rb_plan_type" value="1" checked/> Study Document
		 	<input  type="radio" name="rb_plan_type" id="rb_plan_type" value="2"/> Check List
		 	<input  type="radio" name="rb_plan_type" id="rb_plan_type" value="3"/> Cad Drawings
	  </div>
	</form>
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	<button type="button" class="btn btn-primary" onclick="uploadFile();">Submit</button>
</div>
