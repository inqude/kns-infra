<?php

/* SESSION INITIATE - START */

session_start();

/* SESSION INITIATE - END */



/* FILE HEADER - START */

// LAST UPDATED ON: 22nd Sep 2016

// LAST UPDATED BY: Lakshmi

/* FILE HEADER - END */



/* TBD - START */

/* TBD - END */



/* DEFINES - START */

define('PO_FUNC_ID','169');

/* DEFINES - END */



/* INCLUDES - START */

$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_purchase_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_vendor_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_quotation_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

/* INCLUDES - END */



if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))

{

	// Session Data

	$user 		   = $_SESSION["loggedin_user"];

	$role 		   = $_SESSION["loggedin_role"];

	$loggedin_name = $_SESSION["loggedin_user_name"];

	

	// Get permission settings for this user for this page	

	$add_perms_list    = i_get_user_perms($user,'',PO_FUNC_ID,'1','1');



	/* DATA INITIALIZATION - START */

	$alert_type = -1;

	$alert = "";

	/* DATA INITIALIZATION - END */

	

	

	if(isset($_REQUEST['order_id']))

	{

		$order_id = $_REQUEST['order_id'];

	}

	else

	{

		$order_id  = "";

	}


	if(isset($_REQUEST['quotation_id']))

	{

		$quotation_id = $_REQUEST['quotation_id'];

	}

	else

	{

		$quotation_id  = "";

	}


	// Get quotation details

	$stock_quotation_compare_search_data = array("po_id"=>$order_id);

	$purchase_order_list = i_get_stock_quotation_compare_list($stock_quotation_compare_search_data);
	if($purchase_order_list['status'] == SUCCESS)

	{

		$purchase_order_list_data = $purchase_order_list['data'];

		$item			= $purchase_order_list_data[0]["stock_material_name"];

		$amount			= $purchase_order_list_data[0]["stock_quotation_amount"];

		$quotation_no	= $purchase_order_list_data[0]["stock_quotation_no"];

		$quantity		= $purchase_order_list_data[0]["stock_quotation_quantity"];

		$vendor			= $purchase_order_list_data[0]["stock_vendor_name"];

		$recieved_date  = $purchase_order_list_data[0]["stock_quotation_received_date"];

		$project_id  	= $purchase_order_list_data[0]["stock_project_id"];


	}

	else

	{

		$item			= "";

		$amount			= "";

		$quotation_no	= "";

		$vendor			= "";

		$recieved_date  = "";

	}

	// Capture the form data

	if(isset($_POST["add_purchase_Order_submit"]))

	{
		
		$order_id				   = $_POST["hd_order_id"];


		$quotation_id              = $_POST["hd_quotation_id"];

		$order_date                = $_POST["txt_date"];		

		//$tax_type                  = $_POST["ddl_tax_type_name"];

		$location                  = $_POST["ddl_loction"];

		$project                   = $_POST["project_id"];

		$transportation_charges    = $_POST["transportation_charges"];
		$excise_duty               = $_POST["excise_duty"];
		$company                   = $_POST["ddl_company"];
		$terms                     = $_POST["terms"];

		$credit_duration           = $_POST["credit_duration"];

		$due_date		           = $_POST["due_date"];





		$transport_charges         = $_POST["transportation_charges"];

		$transportation_discount   = $_POST["transportation_discount"];

		$transportation_taxable    = $_POST["transportation_taxable"];

		$t_cgst                    = $_POST["ddl_tax_type_t_cgst"];

		$t_sgst                    = $_POST["ddl_tax_type_t_sgst"];

		$transportation_total      = $_POST["transportation_total"];




		$fright_charges            = $_POST["fright_charges"];

		$fright_discount           = $_POST["fright_discount"];

		$fright_taxable            = $_POST["fright_taxable"];

		$f_cgst                    = $_POST["ddl_tax_type_f_cgst"];

		$f_sgst                    = $_POST["ddl_tax_type_f_sgst"];

		$fright_total              = $_POST["fright_total"];



		$other_charges             = $_POST["other_charges"];

		$other_discount            = $_POST["other_discount"];

		$other_taxable             = $_POST["other_taxable"];

		$o_cgst                    = $_POST["ddl_tax_type_o_cgst"];

		$o_sgst                    = $_POST["ddl_tax_type_o_sgst"];

		$other_total               = $_POST["other_total"];



		
		// Check for mandatory fields
		if( ($order_date != "") && ($terms != "") && ($location != "") && ($company != "") )
		{
			$purchase_order_update_data = array("quotation_id"=>$quotation_id,"order_date"=>$order_date,"tax_type"=>$tax_type,"location"=>$location,"transportation_charges"=>$transportation_charges,"excise_duty"=>$excise_duty,"company"=>$company,"terms"=>$terms,"credit_duration"=>$credit_duration,
				"project" => $project,
			"due_date"=>$due_date,"remarks"=>$remarks,"updated_by"=>$user,
			"transport_charges" =>$transport_charges , "transportation_discount" =>$transportation_discount , 
			"transportation_taxable" =>$transportation_taxable ,"transportation_total" =>$transportation_total,
			"t_cgst" => $t_cgst, "t_sgst" => $t_sgst,

			"fright_charges" =>$fright_charges , "fright_discount" =>$fright_discount , 
			"fright_taxable" =>$fright_taxable ,"fright_total" =>$fright_total,
			"f_cgst" => $f_cgst, "f_sgst" => $f_sgst,

			"other_charges" =>$other_charges , "other_discount" =>$other_discount , 
			"other_taxable" =>$other_taxable ,"other_total" =>$other_total,
			"o_cgst" => $o_cgst, "o_sgst" => $o_sgst);

			$purchase_order_iresult = i_update_purchase_order($order_id,$purchase_order_update_data);


			if($purchase_order_iresult["status"] == SUCCESS)

			{
				$purchase_order_items_update_data = array("tax_type"=>$tax_type,"excise_duty"=>$excise_duty);
				
				$purchase_order_items = i_update_purchase_order_items($order_id,$purchase_order_items_update_data);

				$purchase_order_id = $purchase_order_iresult["data"];

				header("location:stock_purchase_order_items.php?&po_id=$order_id");

				$alert_type = 1;



			}

			else

			{

				$alert_type = 0;

			}

			

			$alert = $purchase_order_iresult["data"];

		}

		else

		{

			$alert = "Please fill all the mandatory fields";

			$alert_type = 0;

		}

	}

	

	

	//Get Location List

	$stock_location_master_search_data = array();

	$location_list = i_get_stock_location_master_list($stock_location_master_search_data);

	if($location_list["status"] == SUCCESS)

	{

		$location_list_data = $location_list["data"];

	}

	else

	{ 

		$alert = $location_list["data"];

		$alert_type = 0;

	}

	
	//Get Project List
	$stock_project_search_data = array();
	$project_list = i_get_project_list($stock_project_search_data);
	if($project_list["status"] == SUCCESS)
	{
		$project_list_data = $project_list["data"];
	}
	else
	{ 
		$alert = $project_list["data"];
		$alert_type = 0;
	}
	
	//Get Company List

	$stock_company_master_search_data = array();

	$company_list = i_get_company_list($stock_company_master_search_data);

	if($company_list["status"] == SUCCESS)

	{

		$company_list_data = $company_list["data"];

	}

	else

	{ 

		$alert = $company_list["data"];

		$alert_type = 0;

	}

	

	//Get Transporation List

	$transportation_search_data = array();

	$transportation_list = i_get_transportation_list($transportation_search_data);

	if($transportation_list["status"] == SUCCESS)

	{

		$transportation_list_data = $transportation_list["data"];

	}

	else

	{

		$alert = $transportation_list["data"];

		$alert_type = 0;

	}

	

	// Get purchase modes already added

	$stock_purchase_order_search_data = array();

	$purchase_order_list = i_get_stock_purchase_order_list($stock_purchase_order_search_data);

	if($purchase_order_list['status'] == SUCCESS)

	{

		$purchase_order_list_data = $purchase_order_list['data'];

	}

	// Get purchase modes already added

	$stock_quotation_compare_search_data = array("quotation_id"=>$quotation_id);

	$purchase_order_list = i_get_stock_quotation_compare_list($stock_quotation_compare_search_data);

	if($purchase_order_list['status'] == SUCCESS)

	{

		$purchase_order_list_data = $purchase_order_list['data'];

		$item			= $purchase_order_list_data[0]["stock_material_name"];

		$amount			= $purchase_order_list_data[0]["stock_quotation_amount"];

		$quotation_no	= $purchase_order_list_data[0]["stock_quotation_no"];

		$quantity		= $purchase_order_list_data[0]["stock_quotation_quantity"];

		$vendor			= $purchase_order_list_data[0]["stock_vendor_name"];

		$recieved_date  = $purchase_order_list_data[0]["stock_quotation_received_date"];

	}

	else

	{

		$item			= "";

		$amount			= "";

		$quotation_no	= "";

		$vendor			= "";

		$recieved_date  = "";

	}

	//Get Tax Type List

	$stock_tax_type_master_search_data = array('active'=>'1');

	$tax_type_list = i_get_stock_tax_type_master_list($stock_tax_type_master_search_data);

	if($tax_type_list["status"] == SUCCESS)

	{

		$tax_type_list_data = $tax_type_list["data"];

	}

	else

	{

		$alert = $tax_type_list["data"];

		$alert_type = 0;

	}

}

else

{

	header("location:login.php");

}	

?>



<!DOCTYPE html>

<html lang="en">

  

<head>

    <meta charset="utf-8">

    <title>Purchase Order - Add PO</title>

    

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <meta name="apple-mobile-web-app-capable" content="yes">    

    

    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">

    

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">

    <link href="css/font-awesome.css" rel="stylesheet">

    

    <link href="css/style.css" rel="stylesheet">

   
    <script src="js/jquery-1.7.2.min.js"></script>

	

	<script src="js/bootstrap.js"></script>

	<script src="js/base.js"></script>

	<script src="datatable/transport_tax_calculation.js"></script>

	<script src="datatable/fright_tax_calculation.js"></script>

	<script src="datatable/other_tax_calculation.js"></script>
	


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->

    <!--[if lt IE 9]>

      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>

    <![endif]-->



  </head>



<body>



<?php

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');

?>    



<div class="main">

	

	<div class="main-inner">



	    <div class="container">

	

	      <div class="row">

	      	

	      	<div class="span12">      		

	      		

	      		<div class="widget ">

	      			

	      			<div class="widget-header">

	      				<i class="icon-user"></i>

	      				<h3>Add Purchase Order Details</h3><span style="float:right; padding-right:20px;">
	      				<!--<a href="stock_purchase_order_list.php">Purchase Order List</a>--></span>						

	  				</div> <!-- /widget-header -->

					

					<div class="widget-content">

						

						<div class="tabbable">

						<ul class="nav nav-tabs">

						  <li>

						    <p>Add Purchase Order Details Here - Project - <strong> <?php
							for($count = 0; $count < count($project_list_data); $count++)
							{
							?>
							 <?php if($project_list_data[$count]["stock_project_id"] == $project_id){ ?> 
							 <?php echo $project_list_data[$count]["stock_project_name"]; ?>
							  <?php }?>  
							<?php
							}
							?>
							</strong>
							</p>

						  </li>	

						 		  

						</ul>

						

						<br>

							<div class="control-group">												

								<div class="controls">

								<?php 

								if($alert_type == 0) // Failure

								{

								?>

									<div class="alert">

                                        <button type="button" class="close" data-dismiss="alert">&times;</button>

                                        <strong><?php echo $alert; ?></strong>

                                    </div>  

								<?php

								}

								?>

                                

								<?php 

								if($alert_type == 1) // Success

								{

								?>								

                                    <div class="alert alert-success">

                                        <button type="button" class="close" data-dismiss="alert">&times;</button>

                                        <strong><?php echo $alert; ?></strong>

                                    </div>

								<?php

								}

								?>

								</div> <!-- /controls -->	                                                

							</div> <!-- /control-group -->

							<div class="tab-content">

								<div class="tab-pane active" id="formcontrols">

								<form id="add_purchase_form" class="form-horizontal " method="post" action="stock_add_po_details.php">

								<input type="hidden" name="hd_quotation_id" value="<?php echo $quotation_id; ?>" />

								<input type="hidden" name="hd_order_id" value="<?php echo $order_id; ?>" />

									<fieldset>										

																				



										<div class="control-group">											

											<label class="control-label" for="txt_date">Order Date*</label>

											<div class="controls">

												<input type="date" class="span6" name="txt_date" placeholder="Order Date" value="<?php echo date('Y-m-d'); ?>" required="required">

											</div> <!-- /controls -->					

										</div> <!-- /control-group -->

									

										

									<!-- 	<div class="control-group">											

											<label class="control-label" for="ddl_tax_type_name">Tax Type*</label>

											<div class="controls">

												<select name="ddl_tax_type_name" required>

												<option>- - -Select Tax Type- - -</option>

												<?php

												for($count = 0; $count < count($tax_type_list_data); $count++)

												{

												?>

												<option value="<?php echo $tax_type_list_data[$count]["stock_tax_type_master_id"]; ?>"><?php echo $tax_type_list_data[$count]["stock_tax_type_master_name"]; ?> (<?php echo $tax_type_list_data[$count]["stock_tax_type_master_value"]; ?>%)</option>

												<?php

												}

												?>

												</select>

											</div> 					

										</div>  -->



										<div class="control-group">											

											<label class="control-label" for="ddl_loction">Delivery Location*</label>

											<div class="controls">

												<select name="ddl_loction" required>

												<option value="">- - -Select Location- - -</option>

												<?php

												for($count = 0; $count < count($location_list_data); $count++)

												{

												?>

												<option value="<?php echo $location_list_data[$count]["stock_location_id"]; ?>"><?php echo $location_list_data[$count]["stock_location_name"]; ?> (<?php echo $location_list_data[$count]["stock_location_code"]; ?>)</option>

												<?php

												}

												?>

												</select>

											</div> <!-- /controls -->					

										</div> <!-- /control-group -->	
										
										
										
										
										
										

										<div class="control-group">											

											<label class="control-label" for="ddl_company">Company*</label>

											<div class="controls">

												<select name="ddl_company" required>

												<option value="">- - -Select Company- - -</option>

												<?php

												for($count = 0; $count < count($company_list_data); $count++)

												{

												?>

												<option value="<?php echo $company_list_data[$count]["stock_company_master_id"]; ?>"><?php echo $company_list_data[$count]["stock_company_master_name"]; ?></option>

												<?php

												}

												?>

												</select>

											</div> <!-- /controls -->					

										</div> <!-- /control-group -->											

										

										<div class="control-group">											

											<label class="control-label" for="terms">Terms*</label>

											<div class="controls">												

												<textarea class="span6" rows="4" name="terms" required></textarea>

											</div> <!-- /controls -->					

										</div> <!-- /control-group -->

										

										<div class="control-group">											

											<label class="control-label" for="credit_duration">Credit Duration*</label>

											<div class="controls">

												<input type="number" min="0" step="1" class="span6" name="credit_duration" required="required">

											</div> <!-- /controls -->					

										</div> <!-- /control-group -->

										

										<div class="control-group">											

											<label class="control-label" for="due_date">Due Date*</label>

											<div class="controls">

												<input type="date" class="span6" name="due_date" required="required">

											</div> <!-- /controls -->					

										</div> <!-- /control-group -->

										

										<div class="control-group">											

											<label class="control-label" for="txt_remarks">Remarks</label>

											<div class="controls">												

												<textarea class="span6" rows="4" name="txt_remarks"></textarea>

											</div> <!-- /controls -->					

										</div> <!-- /control-group -->

                                     
										<!-- Transport charges -->
										   <div class="control-group form-row" style="border: 0.5px solid black;padding: 4px;">
											<div class="col-sm-4">											
											<label class="control-label" for="transportation_charges">Transporation Charges*</label>
											<div class="controls">
												<input type="number" class="span2" id="transportation_charges" name="transportation_charges" min=0 step="0.01" value="0" required>
											</div> <!-- /controls -->	
											</div>	

											<div class="col-sm-4">	
											<label class="control-label" for="transportation_discount">Discount for Transporation Charges*</label>
											<div class="controls">
												<input type="number" class="span2" id="transportation_discount" name="transportation_discount" min=0 step="0.01" value="0" required>
											</div> 
											</div>

											<div class="col-sm-4">	
											<label class="control-label" for="transportation_taxable">Taxable Amount for Transporation Charges*</label>
											<div class="controls">
												<input type="number" class="span2" readonly="" id="transportation_taxable" name="transportation_taxable" step="0.01" value="0" required>
											</div> 
											</div>

											<br /><br /><br /><br />
											<div class="col-sm-4">	
											<label class="control-label" for="ddl_tax_type_t_cgst">CGST*</label>
											<select class="cgst span2" name="ddl_tax_type_t_cgst" id="t_cgst">

												<option>- - -Select Tax Type- - -</option>

												<?php

												for($count = 0; $count < count($tax_type_list_data); $count++)

												{

												?>

												<option 
												 data-taxvalue="<?php echo $tax_type_list_data[$count]["stock_tax_type_master_value"] ?>" value="<?php echo $tax_type_list_data[$count]["stock_tax_type_master_value"]; ?>" <?php if($tax_type_list_data[$count]["stock_tax_type_master_value"] == $purchase_order_items_list_data[0]["	stock_t_cgst"]){ ?> selected="selected" <?php } ?>><?php echo $tax_type_list_data[$count]["stock_tax_type_master_name"]; ?>
												 </option>	

											

												<?php

												}

												?>

												</select> 
											</div>

											<div class="col-sm-4">	
											<label class="control-label" for="ddl_tax_type_t_sgst">SGST*</label>
												<select name="ddl_tax_type_t_sgst" required id="t_sgst" class="span2" >

												<option>- - -Select Tax Type- - -</option>

												<?php

												for($count = 0; $count < count($tax_type_list_data); $count++)

												{

												?>

												<option data-taxvalue="<?php echo $tax_type_list_data[$count]["stock_tax_type_master_value"] ?>" value="<?php echo $tax_type_list_data[$count]["stock_tax_type_master_value"]; ?>" <?php if($tax_type_list_data[$count]["stock_tax_type_master_value"] == $purchase_order_items_list_data[0]["stock_t_sgst"]){ ?> selected="selected" <?php } ?>><?php echo $tax_type_list_data[$count]["stock_tax_type_master_name"]; ?></option>	

											

												<?php

												}

												?>

												</select>

											</div>

											<div class="col-sm-4">	
											<label class="control-label" for="transportation_total">Total Transporation Charges Amount</label>
											<div class="controls">
												<input type="number" class="span2" id="transportation_total" readonly name="transportation_total" step="0.01" min=0 value="0" required>
											</div> 
											</div>

										</div> <!-- /control-group --> 



										   <div class="control-group form-row" style="border: 0.5px solid black;padding: 4px;">
											<div class="col-sm-4">											
											<label class="control-label" for="fright_charges">Fright/Packing Charges*</label>
											<div class="controls">
												<input type="number" class="span2" id="fright_charges" name="fright_charges" step="0.01" value="0" min=0 required>
											</div> <!-- /controls -->	
											</div>	

											<div class="col-sm-4">	
											<label class="control-label" for="fright_discount">Discount for Fright Charges*</label>
											<div class="controls">
												<input type="number" class="span2" id="fright_discount" name="fright_discount" step="0.01" value="0" min=0 required>
											</div> 
											</div>

											<div class="col-sm-4">	
											<label class="control-label" for="fright_taxable">Taxable Amount for Fright Charges*</label>
											<div class="controls">
												<input type="number" class="span2" readonly="" id="fright_taxable" name="fright_taxable" step="0.01" value="0" required>
											</div> 
											</div>

											<br /><br /><br /><br />
											<div class="col-sm-4">	
											<label class="control-label" for="ddl_tax_type_f_cgst">CGST*</label>
											<select class="cgst span2" name="ddl_tax_type_f_cgst" id="f_cgst">

												<option>- - -Select Tax Type- - -</option>

												<?php

												for($count = 0; $count < count($tax_type_list_data); $count++)

												{

												?>

												<option 
												 data-taxvalue="<?php echo $tax_type_list_data[$count]["stock_tax_type_master_value"] ?>" value="<?php echo $tax_type_list_data[$count]["stock_tax_type_master_value"]; ?>" <?php if($tax_type_list_data[$count]["stock_tax_type_master_value"] == $purchase_order_items_list_data[0]["stock_f_cgst"]){ ?> selected="selected" <?php } ?>><?php echo $tax_type_list_data[$count]["stock_tax_type_master_name"]; ?>
												 </option>	

											

												<?php

												}

												?>

												</select> 
											</div>

											<div class="col-sm-4">	
											<label class="control-label" for="ddl_tax_type_f_sgst">SGST*</label>
												<select name="ddl_tax_type_f_sgst" id="f_sgst" class="span2" >

												<option>- - -Select Tax Type- - -</option>

												<?php

												for($count = 0; $count < count($tax_type_list_data); $count++)

												{

												?>

												<option data-taxvalue="<?php echo $tax_type_list_data[$count]["stock_tax_type_master_value"] ?>" value="<?php echo $tax_type_list_data[$count]["stock_tax_type_master_value"]; ?>" <?php if($tax_type_list_data[$count]["stock_tax_type_master_value"] == $purchase_order_items_list_data[0]["stock_f_sgst"]){ ?> selected="selected" <?php } ?>><?php echo $tax_type_list_data[$count]["stock_tax_type_master_name"]; ?></option>	

											

												<?php

												}

												?>

												</select>

											</div>

											<div class="col-sm-4">	
											<label class="control-label" for="fright_total">Total Fright Charges Amount</label>
											<div class="controls">
												<input type="number" class="span2" id="fright_total" readonly name="fright_total" step="0.01" value="0" required>
											</div> 
											</div>

										</div> <!-- /control-group --> 



										   <div class="control-group form-row" style="border: 0.5px solid black;padding: 4px;">
											<div class="col-sm-4">											
											<label class="control-label" for="other_charges">Other Charges*</label>
											<div class="controls">
												<input type="number" class="span2" required id="other_charges" name="other_charges" step="0.01" value="0" min=0>
											</div> <!-- /controls -->	
											</div>	

											<div class="col-sm-4">	
											<label class="control-label" for="other_discount">Discount for Other Charges*</label>
											<div class="controls">
												<input type="number" class="span2" id="other_discount" name="other_discount" step="0.01" value="0" min=0>
											</div> 
											</div>

											<div class="col-sm-4">	
											<label class="control-label" for="other_taxable">Taxable Amount for Other Charges*</label>
											<div class="controls">
												<input type="number" id="other_taxable" readonly="" required class="span2" name="other_taxable" step="0.01" value="0">
											</div> 
											</div>

											<br /><br /><br /><br />
											<div class="col-sm-4">	
											<label class="control-label" for="ddl_tax_type_o_cgst">CGST*</label>
											<select class="cgst span2" name="ddl_tax_type_o_cgst" id="o_cgst">

												<option>- - -Select Tax Type- - -</option>

												<?php

												for($count = 0; $count < count($tax_type_list_data); $count++)

												{

												?>

												<option 
												 data-taxvalue="<?php echo $tax_type_list_data[$count]["stock_tax_type_master_value"] ?>" value="<?php echo $tax_type_list_data[$count]["stock_tax_type_master_value"]; ?>" <?php if($tax_type_list_data[$count]["stock_tax_type_master_value"] == $purchase_order_items_list_data[0]["stock_o_cgst"]){ ?> selected="selected" <?php } ?>><?php echo $tax_type_list_data[$count]["stock_tax_type_master_name"]; ?>
												 </option>	

											

												<?php

												}

												?>

												</select> 
											</div>

											<div class="col-sm-4">	
											<label class="control-label" for="ddl_tax_type_o_sgst">SGST*</label>
												<select name="ddl_tax_type_o_sgst" required id="o_sgst" class="span2" >

												<option>- - -Select Tax Type- - -</option>

												<?php

												for($count = 0; $count < count($tax_type_list_data); $count++)

												{

												?>

												<option data-taxvalue="<?php echo $tax_type_list_data[$count]["stock_tax_type_master_value"] ?>" value="<?php echo $tax_type_list_data[$count]["stock_tax_type_master_value"]; ?>" <?php if($tax_type_list_data[$count]["stock_tax_type_master_value"] == $purchase_order_items_list_data[0]["stock_o_sgst"]){ ?> selected="selected" <?php } ?>><?php echo $tax_type_list_data[$count]["stock_tax_type_master_name"]; ?></option>	

											

												<?php

												}

												?>

												</select>

											</div>

											<div class="col-sm-4">	
											<label class="control-label" for="other_total">Total Transporation Charges Amount</label>
											<div class="controls">
												<input type="number" class="span2" id="other_total" required readonly name="other_total" step="0.01" value="0">
											</div> 
											</div>

										</div> <!-- /control-group --> 

												                 	                                                                                                      										 <br 	/>

										<input type="hidden"  name="project_id" value="<?php echo $project_id ?>">

										<?php

										if($add_perms_list['status'] == SUCCESS)

										{

										?>										

											<div class="form-actions">

												<input type="submit" class="btn btn-primary" name="add_purchase_Order_submit" value="Submit" />

												<button type="reset" class="btn">Cancel</button>

											</div> <!-- /form-actions -->

										<?php

										}

										else

										{

											?>

											<div class="form-actions">

												You are not authorized to add a PO

											</div> <!-- /form-actions -->

											<?php

										}

										?>

									</fieldset>

								</form>

								</div>																

							</div>

						  

						</div>					

						

					</div> <!-- /widget-content -->

						

				</div> <!-- /widget -->

	      		

		    </div> <!-- /span8 -->

	      	

	      </div> <!-- /row -->

	

	    </div> <!-- /container -->

	    

	</div> <!-- /main-inner -->

    

</div> <!-- /main -->

 

<div class="extra">



	<div class="extra-inner">



		<div class="container">



			<div class="row">

                    

                </div> <!-- /row -->



		</div> <!-- /container -->



	</div> <!-- /extra-inner -->



</div> <!-- /extra -->  

    

<div class="footer">

	

	<div class="footer-inner">

		

		<div class="container">

			

			<div class="row">

				

    			<div class="span12">

    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.

    			</div> <!-- /span12 -->

    			

    		</div> <!-- /row -->

    		

		</div> <!-- /container -->

		

	</div> <!-- /footer-inner -->

	

</div> <!-- /footer -->

    











  </body>



</html>

