var table;
var columnsMapping = {
  '5': 'reason',
  '8': 'site_count',
  '9': 'follow_up_count',
  '12': 'requested_date',
  '13': 'requested_by'
};

function createToolTip(str, length) {
  var substr = (str.length <= 20) ? str : str.substr(0, 15) + '...';
  return '<abbr data-toggle="tooltip" data-placement="right" title="' + str + '">' + substr + '</abbr>'
}

function drawTable() {
  table.draw();
}

function openModal(enquiry_id) {
  $('#exampleModal').modal({
    remote: 'crm_assign_enquiry.php?enquiry=' + enquiry_id
  });

  $('#exampleModal').on('hidden.bs.modal', function(e) {
    $(this).find('.modal-content').empty();
    $(this).data('bs.modal', null);
  });
}

function enquiry_submit() {
  $.ajax({
    url: 'crm_assign_enquiry.php',
    type: 'POST',
    data: {
      hd_enquiry: $('#hd_enquiry').val(),
      ddl_assigned_to: $('#ddl_assigned_to').val(),
      add_assign_enquiry_submit: 'yes'
    },
    success: function(response) {
      $('#exampleModal').modal('hide');
      drawTable();
      alert('success');
    }
  })
}

function mark_unqualified(enquiry_id) {
  $('#modal').modal({
    remote: 'crm_add_unqualified_enquiry.php?enquiry=' + enquiry_id
  });

  $('#modal').on('hidden.bs.modal', function(e) {
    $('form#add_unq_enquiry')[0].reset();
  });
}

function update_status() {
  $.ajax({
    url: 'crm_add_unqualified_enquiry.php',
    type: 'POST',
    data: {
      txt_remarks: $("#txt_remarks").val(),
      ddl_reason: $("#ddl_reason").val(),
      hd_enquiry_id: $("#enquiry").val(),
      add_unq_enquiry_submit: 'yes'
    },
    success: function(response) {
      alert(response);
      drawTable();
      $("#modal").removeClass("in");
      $(".modal-backdrop").remove();
      $("#modal").hide();
    }
  });
}

$(document).ready(function() {
  var columns = [{
      className: 'noVis',
      "orderable": false,
      "data": function() {
        return arguments[3].settings._iDisplayStart + arguments[3].row + 1;
      }
    },
    {
      "orderable": false,
      "data": "name"
    },
    {
      "orderable": false,
      "data": "cell"
    },
    {
      "orderable": false,
      "data": `enquiry_number`
    },
    {
      "orderable": false,
      "data": "project_name"
    }, {
      "orderable": false,
      data: function(data, type, full, meta) {
        var classname = columnsMapping[meta.col] + '_' + meta.row;
        return '<span class="' + classname + '">loading..</span>';
      }
    },
    {
      "orderable": false,
      "data": "enquiry_source_master_name"
    },

    {
      "orderable": true,
      "data": function(data) {
        return moment(data.added_on).format("DD/MM/YYYY");
      }
    }, {
      "orderable": false,
      data: function(data, type, full, meta) {
        var classname = columnsMapping[meta.col] + '_' + meta.row;
        return '<span class="' + classname + '">loading..</span>';
      }
    }, {
      "orderable": false,
      data: function(data, type, full, meta) {
        var classname = columnsMapping[meta.col] + '_' + meta.row;
        return '<span class="' + classname + '">loading..</span>';
      }
    }, {
      "orderable": false,
      "data": function(data, type, full) {
        if (type !== 'display') {
          return '';
        }
        if (data.walk_in == "1") {
          return 'Yes';
        }
        return 'No';
      }
    }, {
      "orderable": false,
      "data": function(data, type, full) {
        return createToolTip(data.user_name, 20);
      }
    },
    {
      "orderable": false,
      data: function(data, type, full, meta) {
        var classname = columnsMapping[meta.col] + '_' + meta.row;
        return '<span class="' + classname + '">loading..</span>';
      }
    }, {
      "orderable": false,
      data: function(data, type, full, meta) {
        var classname = columnsMapping[meta.col] + '_' + meta.row;
        return '<span class="' + classname + '">loading..</span>';
      }
    },
    {
      "orderable": false,
      "data": function(data, type, full, meta) {
        if (!window.permissions.edit) {
          return '***';
        }
        return `<a target="_blank" href="crm_enquiry_fup_list.php?enquiry=${data.enquiry_id}"><span class="glyphicon glyphicon-calendar"></span></a>`;
      }
    },
    {
      "orderable": false,
      "data": function(data, type, full) {
        return '<a href="#"><span id="enquiry" class="fas fa-users"></span></a>';
      }
    },
    {
      "orderable": false,
      "data": function(data, type, full) {
        return '<a  style="color:red" href="#"><span id="unqualified" class="glyphicon glyphicon-warning-sign"></span></a>';
      }
    },
  ];

  table = $('#example').DataTable({
    stateSave: true,
    serverSide: true,
    dataSrc: 'aaData',
    ajax: 'datatable/crm_enquiry_list_unq.php',
    pageLength: 10,
    scrollY: 600,
    processing: true,
    scrollCollapse: true,
    fixedHeader: true,
    searchDelay: 1200,
    fnCreatedRow: function(row, full, index) {
      $(row).attr('id', 'row_' + index);
      $.ajax({
        url: 'ajax/get_follow_up_data.php',
        data: "enquiry_id=" + full.enquiry_id,
        success: function(response) {
          response = JSON.parse(response);
          $('span.' + 'requested_date' + '_' + index).html(moment(response.follow_up_added_on).format("DD/MM/YYYY"));
          $('span.' + 'requested_by' + '_' + index).html(createToolTip(response.follow_up_added_by, 10));
          $('span.' + 'follow_up_count' + '_' + index).html(response.fup_count);
          $('span.' + 'site_count' + '_' + index).html(response.sv_count);
          $('span.' + 'reason' + '_' + index).html(createToolTip(response.remarks, 20));
        }
      });
    },
    "language": {
      "infoFiltered": " "
    },
    fnServerParams: function(aoData) {
      aoData.aaSorting = aoData.order;
      aoData.iDisplayLength = aoData.length;
      aoData.iDisplayStart = aoData.start;
      aoData.table = "project_crm_enquiry";
      aoData.assigned_to = $('#ddl_search_assigned_to').val();
      aoData.project_id = $('#ddl_project').val();
      aoData.search_cell = $('#cell').val();
      aoData.enquiry_number = $('#enquiry_number').val();
      aoData.search_source = $('#ddl_search_source').val();
      aoData.search_status = $('#ddl_search_int_status').val();

      if (aoData.search && aoData.search.value) {
        aoData.sSearch = aoData.search.value;
      }
    },

    buttons: [{
      extend: 'colvis',
      columns: ':not(.noVis)'
    }],
    fixedColumns: {
      leftColumns: 3,
      rightColumns: 3
    },
    scrollX: true,
    "columns": columns,
  });

  $('#example tbody').on('click', 'tr', function(event) {
    var rowData = table.row(this).data();
    if (event.target.tagName == 'SPAN' && event.target.id == 'enquiry') {
      openModal(rowData['enquiry_id']);
    } else if (event.target.tagName == 'SPAN' && event.target.id == 'unqualified') {
      mark_unqualified(rowData['enquiry_id']);
    }
  });
});