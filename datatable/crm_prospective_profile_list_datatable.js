var table;
var columnsMapping = {
  '12': 'site_count',
  '11': 'follow_up_count',
  '13': 'follow_up_date',
  '16': 'prospective_closure_date'
};

function drawTable() {
  table.draw();
}

function createToolTip(str, length) {
  var substr = (str.length <= 20) ? str : str.substr(0, 20) + '...';
  return '<abbr data-toggle="tooltip" data-placement="right" title="' + str + '">' + substr + '</abbr>'
}

function getCellValue() {
  var meta = arguments[3];
  var classname = columnsMapping[meta.col] + '_' + meta.row;
  if (arguments[1] === 'export') {
    return $("tr").find('span.' + classname).html();
  }
  return '<span class="' + classname + '">Loading..</span>';
}

$(document).ready(function() {
  var commonCellDefinition = {
    orderable: false,
    data: getCellValue,
  };
  $("#example_processing").css("z-index", "100");

  table = $('#example').DataTable({
    stateSave: true,
    serverSide: true,
    dataSrc: 'aaData',
    ajax: 'datatable/crm_prospective_profile_list.php',
    pageLength: 10,
    scrollY: 600,
    processing: true,
    scrollCollapse: true,
    fixedHeader: true,
    searchDelay: 1200,
    dom: 'lBfrtip',
    buttons: [{
      extend: 'excelHtml5',
      text: 'Export',
      exportOptions: {
        orthogonal: 'export'
      }
    }, ],
    lengthMenu: [
      [10, 25, 50, 100, -1],
      [10, 25, 50, 100, "All"]
    ],
    fnCreatedRow: function(row, full, index) {
      $(row).attr('id', 'row_' + index);
      $.ajax({
        url: 'ajax/get_follow_up_data.php',
        data: "enquiry_id=" + full.enquiry_id + "&prospective_profile_id=" + full.crm_prospective_profile_id,
        success: function(response) {
          response = JSON.parse(response);
          $('span.' + 'follow_up_date' + '_' + index).html(moment(response.follow_up_date).format("DD/MM/YYYY"));
          $('span.' + 'follow_up_count' + '_' + index).html(response.fup_count);
          $('span.' + 'site_count' + '_' + index).html(response.sv_count);
          $('span.' + 'prospective_closure_date' + '_' + index).html(moment(response.closure_date).format('DD/MM/YYYY'));
          if (window.permissions.edit) {
            $('span.' + 'prospective_closure_date' + '_' + index).append(`<a target="_blank"
            href=crm_update_prospective_profile.php?profile_id=${full.crm_prospective_profile_id}>
            <span class='glyphicon glyphicon-pencil'></span></a>`);
          }
        }
      });
    },
    "language": {
      "infoFiltered": " "
    },
    fnServerParams: function(aoData) {
      aoData.aaSorting = aoData.order;
      aoData.iDisplayLength = aoData.length;
      aoData.iDisplayStart = aoData.start;
      aoData.table = "prospective_profile";
      aoData.assigned_to = $('#ddl_search_assigned_to').val();
      aoData.project_id = $('#ddl_project').val();
      aoData.search_cell = $('#cell').val();
      aoData.search_source = $('#ddl_search_source').val();
      aoData.search_status = $('#ddl_search_int_status').val();
      aoData.start_date = $('#start_date').val();
      aoData.end_date = $('#end_date').val();

      if (aoData.search && aoData.search.value) {
        aoData.sSearch = aoData.search.value;
      }
    },
    "columns": [{
        "orderable": false,
        targets: [0],
        "data": function() {
          return arguments[3].settings._iDisplayStart + arguments[3].row + 1;
        }
      },
      {
        "orderable": false,
        targets: [1],
        "data": function(data, type) {
          if (type === 'export') {
            return data.name;
          }
          return createToolTip(data.name, 20);
        }
      },
      {
        "orderable": false,
        targets: [2],
        "data": "cell"
      },
      {
        "orderable": false,
        targets: [3],
        "data": function(data) {
          return data.enquiry_number;
        }
      },
      {
        "orderable": false,
        targets: [4],
        "data": function(data, type) {
          if (type === 'export') {
            return data.project_name;
          }
          return createToolTip(data.project_name, 20);
        }
      },
      {
        "orderable": false,
        targets: [5],
        "data": "enquiry_source_master_name"
      },
      {
        "orderable": false,
        targets: [6],
        "data": function(data, type) {
          var interest_status = data.crm_prospective_profile_buying_interest;
          interest_status = interest_status.split(",")
          var status = "";
          var status_val;
          console.log(interest_status);
          for (i = 0; i < interest_status.length; i++) {
            switch (interest_status[i]) {
              case "1":
                status_val = "20x30,";
                break;

              case "2":
                status_val = "30x40,";
                break;

              case "3":
                status_val = "40x60,";
                break;

              case "4":
                status_val = "Other,";
                break;

              case "5":
                status_val = "30x50,";
                break;

              default:
                status_val = "";
                break;
            }
            status += status_val;
          }
          if (type === 'export') {
            return status.replace(/,$/, "");
          }
          return createToolTip(status.replace(/,$/, ""), 20);
        }
      },
      {
        "orderable": false,
        targets: [6],
        "data": function(data, type, full) {
          if (data.walk_in == "1") {
            return 'Yes';
          }
          return 'No';
        }
      },
      {
        "orderable": false,
        targets: [7],
        "data": "crm_cust_interest_status_name"
      },
      {
        "orderable": true,
        targets: [8],
        "data": function(data) {
          return moment(data.crm_prospective_profile_added_on).format("DD/MM/YYYY");
        }
      },
      {
        "orderable": false,
        targets: [9],
        "data": function(data) {
          return moment(data.added_on).format("DD/MM/YYYY");
        }
      },
      commonCellDefinition,
      commonCellDefinition,
      commonCellDefinition,
      {
        "orderable": false,
        targets: [13],
        "data": function(data, type) {
          if (type === 'export') {
            return data.added_by;
          }
          return createToolTip(data.added_by, 20);
        }
      },
      {
        "orderable": false,
        targets: [14],
        "data": function(data, type) {
          if (type === 'export') {
            return data.stm;
          }
          return createToolTip(data.stm, 20);
        }
      },
      commonCellDefinition,
      {
        "orderable": false,
        targets: [16],
        "data": function(data, type, meta) {
          if (!window.permissions.edit) {
            return '***';
          }
          return '<a target="_blank" href="crm_add_enquiry_fup.php?enquiry=' + data.enquiry_id + '"><span class="glyphicon glyphicon-calendar"></span></a>';
        }
      },
      {
        "orderable": false,
        targets: [17],
        "data": function(data, type, meta) {
          if (!window.permissions.edit) {
            return '***';
          }
          return '<a target="_blank" href="crm_site_status_display.php?client=' + data.crm_prospective_profile_id + '"><span class="glyphicon glyphicon-share-alt"></span></a>';
        }
      },
    ],
    fixedColumns: {
      leftColumns: 3,
      rightColumns: 3
    },
    scrollX: true,
  });
});