<?php
  session_start();
  $base = $_SERVER['DOCUMENT_ROOT'];
  include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'connection.php');
  $dbConnection = get_conn_handle();

    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * Easy set variables
     */

    /* Array of database columns which should be read and sent back to DataTables. Use a space where
     * you want to insert a non-database field (for example a counter or static image)
     */
  $user = $_SESSION["loggedin_user"];
  $aColumns = array(
    'project_master_name',
    'project_process_master_name',
    'project_task_master_name',
    'budget_history_road_id',
    'project_site_location_mapping_master_name',
    'budget_history_process_id',
    'budget_history_task_id',
    'user_name',
    'budget_history_manpower',
    'budget_history_machine',
    'budget_history_contract',
    'budget_history_material',
    'budget_history_changed_on',
    'budget_history_file',
    'budget_history_project',
    'user_name',
    'budget_history_remarks',
    'project_process_master_id',
    'project_task_master_id',
  );

   /* Indexed column (used for fast and accurate table cardinality) */
   $sIndexColumn = "budget_history_id";

   /* DB table to use */
   $sTable = 'project_budget_history';

    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * If you just want to use the basic configuration for DataTables with PHP server-side, there is
     * no need to edit below this line
     */

    /*
     * Local functions
     */
    function fatal_error($sErrorMessage = '')
    {
        header($_SERVER['SERVER_PROTOCOL'] .' 500 Internal Server Error');
        die($sErrorMessage);
    }

    /*
     * Paging
     */
    $sLimit = "";
    if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
        $sLimit = "LIMIT ".intval($_GET['iDisplayStart']).", ".
            intval($_GET['iDisplayLength']);
    }

    /*
  	 * Ordering
  	 */
  	$sOrder = "";
  	if ( isset( $_GET['order'][0]["column"] ) )
  	{
  		$sOrder = "ORDER BY ";
  		for ( $i=0 ; $i<count($_GET['order']) ; $i++ )
  		{
  				$sOrder .= "`".$aColumns[ intval( $_GET['order'][$i]["column"] ) ]."` ".
  				($_GET['order'][$i]["dir"]=='asc' ? 'asc': 'desc') .", ";
  		}

  		$sOrder = substr_replace( $sOrder, "", -2 );
  		if ( $sOrder == "ORDER BY" )
  		{
  			$sOrder = "";
  		}
  	}

     // * Filtering
     // * NOTE this does not match the built-in DataTables filtering which does it
     // * word by word on any field. It's possible to do here, but concerned about efficiency
     // * on very large tables, and MySQL's regex functionality is very limited

     $sWhere = "";
     if($sWhere == "") {
       $sWhere .= "";
        }

      if (isset($_GET['sSearch']) && $_GET['sSearch'] != "") {
          $sWhere = "Where (";
          for ($i=0 ; $i<count($aColumns) ; $i++) {
              $sWhere .= "`".$aColumns[$i]."` LIKE '%".($_GET['sSearch'])."%' OR ";
          }
          $sWhere = substr_replace($sWhere, "", -3);
          $sWhere .= ") AND `budget_history_project` IN (select `project_user_mapping_project_id` from `project_user_mapping`
          where `project_user_mapping_user_id`= $user  and `project_user_mapping_active` = 1)";
      }

    if (isset($_GET['project_id']) && !empty($_GET['project_id'])) {
        if (!empty($sWhere)) {
            $sWhere .= " AND ";
        }
        $sWhere .= " Where budget_history_project = ". $_GET['project_id'];
    }

    if (isset($_GET['search_process']) && $_GET['search_process'] != '') {
        if (!empty($sWhere)) {
            $sWhere .= " AND ";
        }
        $sWhere .= "project_process_master_id = ". $_GET['search_process'];
    }

    if (isset($_GET['search_task']) && $_GET['search_task'] != '') {
        if (!empty($sWhere)) {
            $sWhere .= " AND ";
        }
        $sWhere .= "project_task_master_id = ". $_GET['search_task'];
    }
    /*
     * SQL queries
     * Get data to display
     */
    $sQuery = "
		SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))."
		FROM   $sTable
		$sWhere
    $sOrder
		$sLimit
		";

    $statement = $dbConnection->prepare($sQuery);
    $statement -> execute();
    $rResult = $statement -> fetchAll();

    /* Data set length after filtering */

    $sQuery = "SELECT FOUND_ROWS() as iFilteredTotal";
    $statement = $dbConnection->prepare($sQuery);
    $statement -> execute();
    $iFilteredTotal = $statement -> fetch()['iFilteredTotal'];
     /* Total data set length */
    $sQuery = "
		SELECT COUNT(".$sIndexColumn.") as iTotal
		FROM   $sTable
	";
    $statement = $dbConnection->prepare($sQuery);
    $statement -> execute();
    $iTotal = $statement -> fetch()['iTotal'];

    /*
     * Output
     */
     $output = array(
         "iTotalRecords" => $iTotal,
         "iTotalDisplayRecords" => $iFilteredTotal,
         "aaData" => $rResult,
         "where" => $sQuery,
     );
     echo json_encode($output);
