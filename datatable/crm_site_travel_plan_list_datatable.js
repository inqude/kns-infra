var table;

function tableDraw() {
  table.draw(false);
}

function openModal(rowData) {
  $('#exampleModal').modal({
    remote: 'crm_update_site_travel_plan.php?travel_plan_id=' + rowData.crm_site_travel_plan_id +
      '&enquiry=' + rowData.enquiry_number + '&name=' + window.encodeURIComponent(rowData.name) +
      '&cell=' + rowData.cell + '&project=' + window.encodeURIComponent(rowData.project_name) +
      '&planned_by=' + rowData.crm_site_visit_plan_added_by + '&assigned_to=' + rowData.assigned_to +
      '&travel_plan_svp=' + rowData.crm_site_travel_plan_svp + '&plan_drive=' + rowData.crm_site_visit_plan_drive +
      '&travel_plan_cab=' + rowData.crm_site_travel_plan_cab
  });

  $('#exampleModal').on('hidden.bs.modal', function(event) {
    $(this).find('.modal-content').empty();
    $(this).data('bs.modal', null);
  });
}

function submitPlan() {
  var status;
  if ($("#stxt_cab_number").val().trim() == '') {
    alert("Please fill all the mandatory fields");
    return false;
  }
  if ($('#ddl_status').val()) {
    status = $('#ddl_status').val();
  } else {
    status = $('#status').val();
  }
  $.ajax({
    url: 'crm_update_site_travel_plan.php',
    type: 'POST',
    data: {
      ddl_status: status,
      hd_travel_plan_id: $('#plan_id').val(),
      stxt_cab_number: $('#stxt_cab_number').val(),
      ddl_cab: $('#ddl_cab').val(),
      travel_plan_svp: $('#travel_plan_svp').val(),
      update_site_travel_submit: 'yes'
    },
    success: function(response) {
      $('#exampleModal').modal('hide');
      if (response.length > 6) {
        tableDraw();
      } else {
        tableDraw();
        var form = document.createElement("form");
        form.setAttribute("method", "get");
        form.setAttribute("action", "crm_add_prospective_profile.php");
        form.setAttribute("target", "blank");
        var hiddenField1 = document.createElement("input");
        hiddenField1.setAttribute("type", "hidden");
        hiddenField1.setAttribute("name", "enquiry");
        hiddenField1.setAttribute("value", response);
        form.appendChild(hiddenField1);
        document.body.appendChild(form);
        form.submit();
      }
    }
  })
}

function createToolTip(str, length) {
  var substr = (str.length <= 20) ? str : str.substr(0, 20) + '...';
  return '<abbr data-toggle="tooltip" data-placement="right" title="' + str + '">' + substr + '</abbr>'
}

$(document).ready(function() {
  var columns = [{
      className: 'noVis',
      "orderable": false,
      "data": function() {
        return arguments[3].settings._iDisplayStart + arguments[3].row + 1;
      }
    },
    {
      "orderable": true,
      "data": function(data) {
        return moment(data.crm_site_travel_plan_date).format("DD/MM/YYYY")
      }
    },
    {
      "orderable": false,
      "data": "project_name"
    },
    {
      "orderable": false,
      "data": "enquiry_number"
    },
    {
      "orderable": false,
      "data": "name"
    },
    {
      "orderable": false,
      "data": "cell"
    },
    {
      "orderable": false,
      "data": function(data) {
        if (data.crm_site_travel_plan_status == "1") {
          return 'Pending'
        } else if (data.crm_site_travel_plan_status == "2") {
          return 'Completed';
        } else if (data.crm_site_travel_plan_status == "3") {
          return 'Customer Cancelled';
        } else if (data.crm_site_travel_plan_status == "4") {
          return 'Driver did not come';
        }
        return 'Company Cancelled';
      }
    },
    {
      "orderable": false,
      "data": function(data) {
        if (data.crm_site_visit_plan_confirmation == "0") {
          return 'Tentative'
        }
        return 'Confirmed';
      }
    },
    {
      "orderable": false,
      "data": function(data) {
        if (data.crm_site_visit_plan_drive == "1") {
          return 'Self-Drive'
        }
        return data.crm_cab_travels;
      }
    },
    {
      "orderable": false,
      "data": function(data) {
        return createToolTip(data.crm_site_travel_actual_cab, 20);
      }
    },
    {
      "orderable": false,
      "data": "travel_plan_assigned_to"
    },
    {
      "orderable": false,
      "data": "travel_plan_added_by"
    },
    {
      "orderable": false,
      "data": "site_visit_planned_by"
    },
    {
      orderable: false,
      "data": function(data) {
        return '<a href="#"><span id="travel" class="glyphicon glyphicon-pencil"></span></a>';
      }
    }
  ];

  table = $('#example').DataTable({
    stateSave: true,
    serverSide: true,
    dataSrc: 'aaData',
    ajax: 'datatable/crm_site_travel_plan_list.php',
    pageLength: 10,
    scrollY: 600,
    processing: true,
    scrollCollapse: true,
    fixedHeader: true,
    searchDelay: 1200,
    "language": {
      "infoFiltered": " "
    },
    "order": [
      [1, "asc"]
    ],
    fnServerParams: function(aoData) {
      aoData.aaSorting = aoData.order;
      aoData.iDisplayLength = aoData.length;
      aoData.iDisplayStart = aoData.start;
      aoData.table = "crm_site_visit";
      aoData.project_id = $('#ddl_project').val();
      aoData.status = $('#ddl_status').val() || 1;
      aoData.planned_by = $('#ddl_search_planned_by').val();
      aoData.stm = $('#ddl_search_stm').val();
      aoData.start_date = $('#start_date').val();
      aoData.end_date = $('#end_date').val();

      if (aoData.search && aoData.search.value) {
        aoData.sSearch = aoData.search.value;
      }
    },
    buttons: [{
      extend: 'colvis',
      columns: ':not(.noVis)'
    }],
    fixedColumns: {
      leftColumns: 3,
      rightColumns: 2
    },
    scrollX: true,
    "columns": columns,
  });

  $('#example tbody').on('click', 'tr', function(event) {
    var rowData = table.row(this).data();
    if (event.target.tagName == 'SPAN' && event.target.id == 'travel') {
      openModal(rowData);
    }
  });
});