<?php
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * Easy set variables
	 */
     session_start();
     define('CRM_UNQUALIFIABLE_ENQUIRY_LIST_FUNC_ID', '111');
   $base = $_SERVER['DOCUMENT_ROOT'];
   include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
   include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'connection.php');
   $dbConnection = get_conn_handle();
	/* Array of database columns which should be read and sent back to DataTables. Use a space where
	 * you want to insert a non-database field (for example a counter or static image)
	 */
  $user = $_SESSION["loggedin_user"];
  $role = $_SESSION["loggedin_role"];
  $approve_perms_list = i_get_user_perms($user, '', CRM_UNQUALIFIABLE_ENQUIRY_LIST_FUNC_ID, '6', '1');
  $aColumns = array(
    'name',
    'project_name',
    'project_id',
    'email',
    'is_unqualified',
    'cell',
    'company',
    'assigned_on',
    'added_on',
    'enquiry_id',
    'assigned_to',
    'user_name',
    'added_by',
    'enquiry_number',
    'crm_cust_interest_status_id',
    'crm_cust_interest_status_name',
    'enquiry_source_master_name',
    'enquiry_source_master_id',
    'walk_in'
  );

   /* Indexed column (used for fast and accurate table cardinality) */
   $sIndexColumn = "enquiry_id";

   /* DB table to use */
   $sTable = $_GET['table'];

	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP server-side, there is
	 * no need to edit below this line
	 */

	/*
	 * Local functions
	 */
	function fatal_error ( $sErrorMessage = '' )
	{
		header( $_SERVER['SERVER_PROTOCOL'] .' 500 Internal Server Error' );
		die( $sErrorMessage );
	}

	/*
	 * Paging
	 */
	$sLimit = "";
	if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
	{
		$sLimit = "LIMIT ".intval( $_GET['iDisplayStart'] ).", ".
			intval( $_GET['iDisplayLength'] );
	}


	/*
	 * Ordering
	 */
   $sOrder = "";
 	if ( isset( $_GET['order'][0]["column"] ) )
 	{
 		$sOrder = "ORDER BY ";
 		for ( $i=0 ; $i<count($_GET['order']) ; $i++ )
 		{
 				$sOrder .= "`".$aColumns[ intval( $_GET['order'][$i]["column"] ) ]."` ".
 				($_GET['order'][$i]["dir"]=='asc' ? 'desc' : 'asc') .", ";
 		}

 		$sOrder = substr_replace( $sOrder, "", -2 );
 		if ( $sOrder == "ORDER BY" )
 		{
 			$sOrder = "";
 		}
 	}
	/*
	 * Filtering
	 * NOTE this does not match the built-in DataTables filtering which does it
	 * word by word on any field. It's possible to do here, but concerned about efficiency
	 * on very large tables, and MySQL's regex functionality is very limited
	 */
   $sWhere = "";
   /* Here approve permissions corresponds to crud operation
   where one can view all records that matches 'WHERE' condition
   irrespective of their roles except admin role which is 1
   */
   if($approve_perms_list["status"]==0 || $role==1) {
     $sWhere .= "WHERE `crm_cust_interest_status_id`=4 AND `is_unqualified`!='YES'";
     // if value of is_unqualified is "YES" then the record is permanently unqualified
    }
    else{
      $sWhere .= "Where assigned_to = $user AND `crm_cust_interest_status_id`=4 AND `is_unqualified`!='YES'";
    }

   if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
     {
       $sWhere.= "AND (";
       for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
         $sWhere .= "`".$aColumns[$i]."` LIKE '%".( $_GET['sSearch'] )."%' OR ";
        }
        $sWhere = substr_replace( $sWhere, "", -3 );
        $sWhere .= ")";
     	}

             	/* Individual column filtering */


    if(isset($_GET['project_id']) && $_GET['project_id'] != '') {
        $sWhere .= " AND `project_id` = ". $_GET['project_id'];
      }

    if(isset($_GET['search_cell']) && $_GET['search_cell'] != '') {
        $sWhere .= " AND `cell` = '". $_GET['search_cell']."'";
    }

    if(isset($_GET['search_source']) && $_GET['search_source'] != '') {
        $sWhere .= " AND `enquiry_source_master_id` = ". $_GET['search_source'];
      }

    if(isset($_GET['assigned_to']) && $_GET['assigned_to'] != '') {
        $sWhere .= " AND `user_id` = ". $_GET['assigned_to'];
    }

    if(isset($_GET['search_status']) && $_GET['search_status'] != '') {
        $sWhere .= " AND `crm_cust_interest_status_id` = '". $_GET['search_status']."'";
    }

    if(isset($_GET['enquiry_number']) && $_GET['enquiry_number'] != '') {
        $sWhere .= " AND `enquiry_number` = '". $_GET['enquiry_number']."'";
    }

	/*
	 * SQL queries
	 * Get data to display
	 */

   $sQuery = "
    SELECT SQL_CALC_FOUND_ROWS `".str_replace(" , ", " ", implode("`, `", $aColumns))."`
    FROM $sTable
    $sWhere
    $sOrder
    $sLimit
    ";
   $statement = $dbConnection->prepare($sQuery);
   $statement -> execute();
   $rResult = $statement -> fetchAll();
   $statement = $dbConnection->prepare("SELECT FOUND_ROWS() as iFilteredTotal");
   $statement -> execute();
   $iFilteredTotal = $statement -> fetch()['iFilteredTotal'];
  /*
   * Output
   */
  $output = array(
    "iTotalDisplayRecords" => $iFilteredTotal,
    "query"=>$sQuery,
    "aaData" => array(),
  );
   foreach($rResult as $aRow){
    $row = array();
    for ( $i=0 ; $i<count($aColumns) ; $i++ )
    {
      if ( $aColumns[$i] == "version" )
      {
        /* Special output formatting for 'version' column */
        $row[] = ($aRow[ $aColumns[$i] ]=="0") ? '-' : $aRow[ $aColumns[$i] ];
      }
      else if ( $aColumns[$i] != ' ' )
      {
        /* General output */
        $row[$aColumns[$i]] = $aRow[ $aColumns[$i] ];
      }
    }
    $output['aaData'][] = $row;
  }
  echo json_encode($output);
 ?>
