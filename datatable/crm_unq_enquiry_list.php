<?php
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * Easy set variables
	 */
     session_start();
     define('CRM_UNQ_ENQUIRY_LIST_FUNC_ID', '112');
   $base = $_SERVER['DOCUMENT_ROOT'];
   include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
   include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'connection.php');
   $user = $_SESSION["loggedin_user"];
   $role = $_SESSION["loggedin_role"];
   $dbConnection = get_conn_handle();
   $approve_perms_list = i_get_user_perms($user, '', CRM_UNQ_ENQUIRY_LIST_FUNC_ID, '6', '1');

	/* Array of database columns which should be read and sent back to DataTables. Use a space where
	 * you want to insert a non-database field (for example a counter or static image)
	 */
  $aColumns = array(
    'name',
    'cell',
    'walk_in',
    'email',
    'reason_id',
    'unqualified_assigned_to',
    'project_id',
    'enquiry_id',
    'project_name',
    'enquiry_number',
    'added_on',
    'assigned_to',
    'enquiry_source_master_id',
    'reason_name',
    'crm_unqualified_lead_id',
    'crm_unqualified_lead_added_on',
    'crm_unqualified_lead_added_by',
    'enquiry_source_master_name'
  );

   /* Indexed column (used for fast and accurate table cardinality) */
   $sIndexColumn = "enquiry_id";

   /* DB table to use */
   $sTable = $_GET['table'];

	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP server-side, there is
	 * no need to edit below this line
	 */

	/*
	 * Local functions
	 */
	function fatal_error ( $sErrorMessage = '' )
	{
		header( $_SERVER['SERVER_PROTOCOL'] .' 500 Internal Server Error' );
		die( $sErrorMessage );
	}

	/*
	 * Paging
	 */
	$sLimit = "";
	if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
	{
		$sLimit = "LIMIT ".intval( $_GET['iDisplayStart'] ).", ".
			intval( $_GET['iDisplayLength'] );
	}


	/*
	 * Ordering
	 */
   $sOrder = "";
 	if ( isset( $_GET['order'][0]["column"] ) )
 	{
 		$sOrder = "ORDER BY ";
 		for ( $i=0 ; $i<count($_GET['order']) ; $i++ )
 		{
 				$sOrder .= "`".$aColumns[ intval( $_GET['order'][$i]["column"] ) ]."` ".
 				($_GET['order'][$i]["dir"]=='asc' ? 'desc' : 'asc') .", ";
 		}

 		$sOrder = substr_replace( $sOrder, "", -2 );
 		if ( $sOrder == "ORDER BY" )
 		{
 			$sOrder = "";
 		}
 	}
	/*
	 * Filtering
	 * NOTE this does not match the built-in DataTables filtering which does it
	 * word by word on any field. It's possible to do here, but concerned about efficiency
	 * on very large tables, and MySQL's regex functionality is very limited
	 */
	$sWhere = "";
  /* Here approve permissions corresponds to crud operation
  where one can view all records that matches 'WHERE' condition
  irrespective of their roles except admin role which is 1
  */
  if($approve_perms_list["status"]==0 || $role==1) {
    $sWhere .= "";
   }
   else{
     $sWhere .= "Where assigned_to = $user";
   }

    if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" ){
      if($sWhere==""){
        $sWhere.="WHERE (";
      }
      else{
        $sWhere.= " AND (";
      }
       for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
          $sWhere .= "`".$aColumns[$i]."` LIKE '%".( $_GET['sSearch'] )."%' OR ";
        }
      $sWhere = substr_replace( $sWhere, "", -3 );
      $sWhere .= ")";
    	}

             	/* Individual column filtering */


    if(isset($_GET['project_id']) && $_GET['project_id'] != '') {
      if ($sWhere == "") {
        $sWhere .= " WHERE `project_id` = ". $_GET['project_id'];
      }
      else{
        $sWhere .= " AND `project_id` = ". $_GET['project_id'];
      }
    }

    if(isset($_GET['search_cell']) && $_GET['search_cell'] != '') {
      if ($sWhere == "") {
        $sWhere .= " WHERE `cell` = '". $_GET['search_cell']."'";
      }
      else{
      $sWhere .= " AND `cell` = '". $_GET['search_cell']."'";
    }
  }

    if(isset($_GET['search_source']) && $_GET['search_source'] != '') {
      if ($sWhere == "") {
        $sWhere .= " WHERE `enquiry_source_master_id` = ". $_GET['search_source'];
      }
      else{
        $sWhere .= " AND `enquiry_source_master_id` = ". $_GET['search_source'];
      }
    }

    if(isset($_GET['assigned_to']) && $_GET['assigned_to'] != '') {
      if ($sWhere == "") {
        $sWhere .= " WHERE  `assigned_to` = ". $_GET['assigned_to'];
      }
      else{
        $sWhere .= " AND `assigned_to` = ". $_GET['assigned_to'];
      }
    }

    if(isset($_GET['search_reason']) && $_GET['search_reason'] != '') {
      if ($sWhere == "") {
        $sWhere .= " WHERE  `reason_id` = ". $_GET['search_reason'];
      }
      else{
        $sWhere .= " AND `reason_id` = ". $_GET['search_reason'];
      }
    }

    if(isset($_GET['start_date']) && $_GET['start_date'] != '') {
      if ($sWhere == "") {
        $sWhere .= " WHERE `crm_unqualified_lead_added_on` >= '". $_GET['start_date']." 00:00:00'";
      }
      else{
      $sWhere .= " AND `crm_unqualified_lead_added_on` >= '". $_GET['start_date']." 00:00:00'";
    }
   }

    if(isset($_GET['end_date']) && $_GET['end_date'] != '') {
      if ($sWhere == "") {
        $sWhere .= " WHERE `crm_unqualified_lead_added_on` <= '". $_GET['end_date']." 23:59:00'";
      }
      else{
      $sWhere .= " AND `crm_unqualified_lead_added_on` <= '". $_GET['end_date']." 23:59:00'";
    }
   }

	/*
	 * SQL queries
	 * Get data to display
	 */

   $sQuery = "
    SELECT SQL_CALC_FOUND_ROWS `".str_replace(" , ", " ", implode("`, `", $aColumns))."`
    FROM $sTable
    $sWhere
    $sOrder
    $sLimit
    ";
   $statement = $dbConnection->prepare($sQuery);
   $statement -> execute();
   $rResult = $statement -> fetchAll();
   $statement = $dbConnection->prepare("SELECT FOUND_ROWS() as iFilteredTotal");
   $statement -> execute();
   $iFilteredTotal = $statement -> fetch()['iFilteredTotal'];
  /*
   * Output
   */
   // print_r($rResult);
  $output = array(
    "iTotalDisplayRecords" => $iFilteredTotal,
    "query"=>$sQuery,
    "aaData" => array(),
  );
   foreach($rResult as $aRow){
    $row = array();
    for ( $i=0 ; $i<count($aColumns) ; $i++ )
    {
      if ( $aColumns[$i] == "version" )
      {
        /* Special output formatting for 'version' column */
        $row[] = ($aRow[ $aColumns[$i] ]=="0") ? '-' : $aRow[ $aColumns[$i] ];
      }
      else if ( $aColumns[$i] != ' ' )
      {
        /* General output */
        $row[$aColumns[$i]] = $aRow[ $aColumns[$i] ];
      }
    }
    $output['aaData'][] = $row;
  }
  echo json_encode($output,JSON_PARTIAL_OUTPUT_ON_ERROR);
 ?>
