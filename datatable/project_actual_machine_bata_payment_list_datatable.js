var table;
var columnsMapping = {
  '2': 'project',
  '3': 'machine',
  '4': 'machine_name',
}

function tableDraw() {
  table.draw(false);
}

function createToolTip(str) {
  var substr = (str.length <= 15) ? str : str.substr(0, 10) + '...';
  return '<abbr id="tool_tip" data-toggle="tooltip" data-placement="right" title="' + str + '">' + substr + '</abbr>'
}

function machine_payment(vendor_id, bata_amount, payment_id) {
  $("#machine_vendor").val(vendor_id);
  $("#payment_id").val(payment_id);
  $("#bata_amount").val(bata_amount);
  $("#amount").val(bata_amount);
  $("#myModal").modal('show');
  $('#myModal').on('hide.bs.modal', function() {
    document.forms[0].reset();
  });
}

function machine_bata_payment() {
  if ($("#bata_amount").val() != $("#amount").val()) {
    alert("amount must be same as bata amount");
    return false;
  }
  var formData = {
    amount: $("#amount").val(),
    payment_id: $("#payment_id").val(),
    hd_machine_vendor: $("#machine_vendor").val(),
    payment_mode: $("#ddl_mode").val(),
    instrument_details: $("#instrument_details").val(),
    txt_remarks: $("#txt_remarks").val(),
  }
  $.ajax({
    url: "tools/machine_payment_list/addMachineBataIssuePayment.php",
    data: formData,
    type: 'POST',
    success: function(response) {
      if (response == 'SUCCESS') {
        $('#myModal').modal('hide');
        tableDraw();
      } else {
        alert("response")
      }
    }
  });
}

function getCellValue() {
  var meta = arguments[3];
  var classname = columnsMapping[meta.col] + '_' + meta.row;
  if (arguments[1] === 'export') {
    return $("tr").find('span.' + classname).children('abbr#tool_tip').attr('title');
  }
  return '<span class="' + classname + '">Loading..</span>';
}

$(document).ready(function() {
  var commonCellDefinition = {
    orderable: false,
    data: getCellValue
  }
  var columns = [{
      className: 'noVis',
      "orderable": false,
      "data": function() {
        return arguments[3].settings._iDisplayStart + arguments[3].row + 1;
      }
    },
    {
      "orderable": false,
      width: "1%",
      "data": "project_machine_vendor_master_name"
    },
    commonCellDefinition,
    commonCellDefinition,
    commonCellDefinition,
    {
      "orderable": false,
      "data": function(data, type) {
        return data.project_payment_machine_bata_bill_no;
      }
    },
    {
      "orderable": false,
      "data": function(data, type) {
        if (type === 'export') {
          return data.project_payment_machine_bata;
        }
        return `₹ ${data.project_payment_machine_bata}`
      }
    },
    {
      "orderable": false,
      "data": function(data, type) {
        if (type === 'export') {
          return data.bata_accepted_by;
        }
        return createToolTip(data.bata_accepted_by);
      }
    },
    {
      "orderable": true,
      data: function(data, type) {
        if (data.project_bata_payment_accepted_on == "0000-00-00") {
          return '00-00-0000';
        }
        return moment(data.project_bata_payment_accepted_on).
        format('DD-MM-YYYY');
      }
    },
    {
      "orderable": false,
      "data": function(data, type) {
        if (window.permissions.edit) {
          return `<a target = _blank href=project_machine_bata_print.php?machine_payment_id=${data.project_payment_machine_id}&bata_status=${data.project_bata_payment_machine_status}>
          <span class="glyphicon glyphicon-Print"></span></a>`;
        }
        return '***';
      }
    },
    {
      "orderable": false,
      "data": function(data, type) {
        if (window.permissions.edit) {
          return `<a  href="#" onclick=machine_payment(${data.project_payment_machine_vendor_id},${data.project_payment_machine_bata},${data.project_payment_machine_id})>
          <span style=font-size:16px>₹</span></a>`;
        }
        return '***';
      }
    },
  ];

  table = $('#example').DataTable({
    stateSave: true,
    serverSide: true,
    dataSrc: 'aaData',
    ajax: 'datatable/project_actual_machine_bata_payment_list.php',
    pageLength: 10,
    scrollY: 600,
    processing: true,
    scrollCollapse: true,
    fixedHeader: true,
    searchDelay: 1200,
    buttons: [{
      extend: 'excelHtml5',
      text: 'Export',
      exportOptions: {
        orthogonal: 'export'
      }
    }, ],
    dom: 'lBfrtip',
    lengthMenu: [
      [10, 25, 50, 100, -1],
      [10, 25, 50, 100, "All"]
    ],
    fnServerParams: function(aoData) {
      aoData.aaSorting = aoData.order;
      aoData.iDisplayLength = aoData.length;
      aoData.iDisplayStart = aoData.start;
      aoData.table = "weekly_machine_payment_list";
      aoData.search_vendor = $('#search_vendor').val();
      aoData.start_date = $('#start_date').val();
      aoData.end_date = $('#end_date').val();

      if (aoData.search && aoData.search.value) {
        aoData.sSearch = aoData.search.value;
      }
    },
    fnCreatedRow: function(row, data, index) {
      if ($('#search_vendor').val() != "" ||
        $('#start_date').val() != "" || $('#end_date').val() != "") {
        $("button.btn.btn-default.buttons-excel.buttons-html5").show();
      } else {
        $("button.btn.btn-default.buttons-excel.buttons-html5").hide();
      }
      $.ajax({
        url: 'ajax/get_machine_details.php',
        data: "payment_id=" + data.project_payment_machine_id,
        success: function(response) {
          response = JSON.parse(response);
          $('span.' + 'project' + '_' + index).html(createToolTip(response.project));
          $('span.' + 'machine' + '_' + index).html(createToolTip(response.machine));
          $('span.' + 'machine_name' + '_' + index).html(createToolTip(response.machine_name));
          $('span.' + 'bata_amount' + '_' + index).html(response.bata_amount);
        }
      });
    },
    fixedColumns: {
      leftColumns: 2,
      rightColumns: 2
    },
    scrollX: true,
    "columns": columns,
  });
});