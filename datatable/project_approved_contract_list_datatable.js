var table;

function redrawTable() {
  table.draw();
}

function createToolTip(str) {
  var substr = (str.length <= 20) ? str : str.substr(0, 20) + '...';
  return '<abbr data-toggle="tooltip" data-placement="right" title="' + str + '">' + substr + '</abbr>'
}

$(document).ready(function() {
  var columns = [{
      className: 'noVis',
      "orderable": false,
      "data": function() {
        return arguments[3].settings._iDisplayStart + arguments[3].row + 1;
      }
    },
    {
      "orderable": false,
      "data": "project_manpower_agency_name"
    },
    {
      "orderable": false,
      "data": function(data, type) {
        if (type === 'export') {
          return data.project_master_name;
        }
        return createToolTip(data.project_master_name);
      }
    },
    {
      "orderable": false,
      "data": function(data, type) {
        if (type === 'export') {
          return data.project_process_master_name;
        }
        return createToolTip(data.project_process_master_name);
      }
    },
    {
      "orderable": false,
      "data": function(data, type) {
        if (type === 'export') {
          return data.project_task_master_name;
        }
        return createToolTip(data.project_task_master_name);
      }
    },
    {
      "orderable": false,
      "data": "stock_unit_name"
    },
    {
      "orderable": false,
      "data": "project_task_actual_boq_work_type"
    },
    {
      "orderable": false,
      "data": function(data, type) {
        if (data.road_name == "No Roads") {
          return 'No Roads';
        }
        return data.road_name;
      }
    },
    {
      "orderable": false,
      "data": function(data, type) {
        if (type === 'export') {
          return data.project_contract_process_name;
        }
        return createToolTip(data.project_contract_process_name);
      }
    },
    {
      "orderable": false,
      "data": function(data, type) {
        if (type === 'export') {
          return data.project_contract_rate_master_work_task;
        }
        return createToolTip(data.project_contract_rate_master_work_task);
      }
    },
    {
      "orderable": false,
      "data": "location_name"
    },
    {
      "orderable": false,
      "data": "project_task_boq_actual_number"
    },
    {
      "orderable": false,
      "data": "project_task_actual_boq_length"
    },
    {
      "orderable": false,
      "data": "project_task_actual_boq_breadth"
    },
    {
      "orderable": false,
      "data": "project_task_actual_boq_depth"
    },
    {
      "orderable": false,
      "data": "project_task_actual_boq_total_measurement"
    },
    {
      "orderable": false,
      "data": function(data, type) {
        if (data.project_task_actual_boq_work_type === 'Regular') {
          return data.project_task_actual_boq_completion;
        } else {
          return data.project_task_actual_boq_rework_completion;
        }
      }
    },
    {
      "orderable": false,
      "data": function(data, type) {
        return Math.round(parseFloat(data.project_task_actual_boq_amount));
      }
    },
    {
      "orderable": false,
      "data": "project_task_actual_boq_lumpsum"
    },
    {
      "orderable": false,
      "data": function(data, type) {
        if (type === 'export') {
          return data.project_task_actual_boq_remarks;
        }
        return createToolTip(data.project_task_actual_boq_remarks);
      }
    },
    {
      "orderable": false,
      "data": "added_by"
    },
    {
      "orderable": true,
      "orderData": [21, 22],
      "data": function(data, type) {
        return moment(data.project_task_actual_boq_added_on).format('DD-MM-YYYY');
      }
    },
    {
      "orderable": true,
      "orderData": [22, 21],
      "data": function(data, type) {
        return moment(data.project_task_actual_boq_date).format('DD-MM-YYYY');
      }
    },
  ];

  table = $('#example').DataTable({
    stateSave: true,
    serverSide: true,
    dataSrc: 'aaData',
    ajax: 'datatable/project_approved_contract_list.php',
    pageLength: 10,
    scrollY: 600,
    processing: true,
    scrollCollapse: true,
    fixedHeader: true,
    searchDelay: 1200,
    buttons: [{
      extend: 'excelHtml5',
      text: 'Export',
      exportOptions: {
        orthogonal: 'export'
      }
    }, ],
    dom: 'lBfrtip',
    lengthMenu: [
      [10, 25, 50, 100, -1],
      [10, 25, 50, 100, "All"]
    ],
    fnServerParams: function(aoData) {
      aoData.aaSorting = aoData.order;
      aoData.iDisplayLength = aoData.length;
      aoData.iDisplayStart = aoData.start;
      aoData.table = "project_contract_work";
      aoData.search_project = $('#search_project').val();
      aoData.search_vendor = $('#search_vendor').val();
      aoData.search_contract = $('#search_contract').val();
      aoData.search_task = $('#search_task').val();
      aoData.start_date = $('#start_date').val();
      aoData.end_date = $('#end_date').val();

      if (aoData.search && aoData.search.value) {
        aoData.sSearch = aoData.search.value;
      }
    },
    fnCreatedRow: function(row, index, full) {
      if ($('#search_project').val() != "" || $('#search_vendor').val() != "" ||
        $('#start_date').val() != "" || $('#end_date').val() != "" ||
        $('#search_contract').val() != "" || $('#search_task').val() != "") {
        $("button.btn.btn-default.buttons-excel.buttons-html5").show();
      } else {
        $("button.btn.btn-default.buttons-excel.buttons-html5").hide();
      }
      $(row).attr('id', 'row_' + index);
    },
    fixedColumns: {
      leftColumns: 3,
      rightColumns: 3
    },
    scrollX: true,
    "columns": columns,
  });
});