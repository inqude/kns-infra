var table;
var columnsMapping = {
  '2': 'project',
  '7': 'machine',
  '8': 'machine_name',
  '14': 'issued_amount',
  '15': 'payment_mode',
  '16': 'remarks',
  '17': 'instrument_details',
  '18': 'payment_added_by',
  '19': 'payment_added_on'
}

function tableDraw() {
  table.draw();
}

function createToolTip(str) {
  var substr = (str.length <= 15) ? str : str.substr(0, 10) + '...';
  return '<abbr id="tool_tip" data-toggle="tooltip" data-placement="right" title="' + str + '">' + substr + '</abbr>'
}

function getCellValue() {
  var meta = arguments[3];
  var classname = columnsMapping[meta.col] + '_' + meta.row;
  if (arguments[1] === 'export') {
    if (columnsMapping[meta.col] == 'project' || columnsMapping[meta.col] == 'machine' ||
      columnsMapping[meta.col] == 'machine_name' ||
      columnsMapping[meta.col] == 'payment_added_on' ||
      columnsMapping[meta.col] == 'payment_added_by') {
      return $("tr").find('span.' + classname).children('abbr#tool_tip').attr('title');
    }
    return $("tr").find('span.' + classname).html();
  }
  return '<span class="' + classname + '">Loading..</span>';
}

$(document).ready(function() {
  var commonCellDefinition = {
    orderable: false,
    data: getCellValue
  }
  var columns = [{
      className: 'noVis',
      "orderable": false,
      "data": function() {
        return arguments[3].settings._iDisplayStart + arguments[3].row + 1;
      }
    },
    {
      "orderable": false,
      "data": "project_machine_vendor_master_name"
    },
    commonCellDefinition,
    {
      "orderable": false,
      "data": function(data, type) {
        return data.project_payment_machine_bill_no;
      }
    },
    {
      "orderable": false,
      "data": function(data, type) {
        if (type === 'export') {
          return data.stock_company_master_name;
        }
        if (data.stock_company_master_name == null) {
          return '';
        }
        return createToolTip(data.stock_company_master_name);
      }
    },
    {
      "orderable": false,
      data: function(data, type) {
        return moment(data.project_payment_machine_from_date).
        format('DD-MM-YYYY');
      }
    },
    {
      "orderable": false,
      data: function(data, type) {
        return moment(data.project_payment_machine_to_date).
        format('DD-MM-YYYY');
      }
    },
    commonCellDefinition,
    commonCellDefinition,
    {
      "orderable": false,
      "data": function(data, type) {
        if (type === 'export') {
          return data.project_payment_machine_bata;
        }
        return `₹ ${data.project_payment_machine_bata}`
      }
    },
    {
      "orderable": false,
      "data": function(data, type) {
        if (type === 'export') {
          return data.project_payment_machine_amount;
        }
        return `₹ ${data.project_payment_machine_amount}`
      }
    },
    {
      "orderable": false,
      "data": function(data, type) {
        return Math.round(data.project_payment_machine_tds) + ' % ';
      }
    },

    {
      "orderable": false,
      "data": function(data, type) {
        if (type === 'export') {
          return Math.round((data.project_payment_machine_tds / 100) * data.project_payment_machine_amount).toFixed(2);
        }
        return `₹ ${Math.round((data.project_payment_machine_tds / 100) * data.project_payment_machine_amount).toFixed(2)}`;
      }
    },
    {
      "orderable": false,
      "data": function(data, type) {
        var tds_amount = (data.project_payment_machine_tds / 100) * data.project_payment_machine_amount;
        if (type === 'export') {
          return Math.round(data.project_payment_machine_amount - tds_amount).toFixed(2);
        }
        return `₹ ${Math.round(data.project_payment_machine_amount - tds_amount).toFixed(2)}`;
      }
    },
    commonCellDefinition,
    commonCellDefinition,
    commonCellDefinition,
    commonCellDefinition,
    commonCellDefinition,
    commonCellDefinition,
    {
      "orderable": false,
      "data": function(data, type) {
        if (window.permissions.edit) {
          return `<a target = _blank href=project_machine_weekly_print.php?machine_payment_id=${data.project_payment_machine_id}&bata_status=${data.project_bata_payment_machine_status}>
          <span class="glyphicon glyphicon-Print"></span></a>`;
        }
        return '***';
      }
    },
  ];

  table = $('#example').DataTable({
    serverSide: true,
    dataSrc: 'aaData',
    ajax: 'datatable/project_issued_machine_payment_list.php',
    pageLength: 10,
    scrollY: 600,
    processing: true,
    scrollCollapse: true,
    fixedHeader: true,
    searchDelay: 1200,
    buttons: [{
      extend: 'excelHtml5',
      text: 'Export',
      exportOptions: {
        orthogonal: 'export'
      }
    }, ],
    dom: 'lBfrtip',
    lengthMenu: [
      [10, 25, 50, 100, -1],
      [10, 25, 50, 100, "All"]
    ],
    fnServerParams: function(aoData) {
      aoData.aaSorting = aoData.order;
      aoData.iDisplayLength = aoData.length;
      aoData.iDisplayStart = aoData.start;
      aoData.table = "weekly_machine_payment_list";
      aoData.search_vendor = $('#search_vendor').val();
      aoData.start_date = $('#start_date').val();
      aoData.end_date = $('#end_date').val();

      if (aoData.search && aoData.search.value) {
        aoData.sSearch = aoData.search.value;
      }
    },
    fnCreatedRow: function(row, data, index) {
      if ($('#search_vendor').val() != "" ||
        $('#start_date').val() != "" || $('#end_date').val() != "") {
        $("button.btn.btn-default.buttons-excel.buttons-html5").show();
      } else {
        $("button.btn.btn-default.buttons-excel.buttons-html5").hide();
      }
      $.ajax({
        url: 'ajax/get_machine_details.php',
        data: "payment_id=" + data.project_payment_machine_id,
        success: function(response) {
          response = JSON.parse(response);
          $('span.' + 'project' + '_' + index).html(createToolTip(response.project));
          $('span.' + 'machine' + '_' + index).html(createToolTip(response.machine));
          $('span.' + 'machine_name' + '_' + index).html(createToolTip(response.machine_name));
        }
      });

      $.ajax({
        url: 'ajax/get_payment_details.php',
        data: "payment_id=" + data.project_payment_machine_id,
        success: function(response) {
          response = JSON.parse(response);
          $('span.' + 'issued_amount' + '_' + index).html(Math.round(response.issued_amount));
          var tds_amount = Math.round((data.project_payment_machine_tds / 100) * data.project_payment_machine_amount);
          var payable = Math.round(data.project_payment_machine_amount - tds_amount).toFixed(2);
          $('span.' + 'payment_mode' + '_' + index).html(response.payment_mode);
          $('span.' + 'remarks' + '_' + index).html(createToolTip(response.remarks));
          $('span.' + 'payment_added_on' + '_' + index).html(createToolTip(response.payment_added_on));
          $('span.' + 'payment_added_by' + '_' + index).html(createToolTip(response.payment_added_by));
          $('span.' + 'instrument_details' + '_' + index).html(createToolTip(response.instrument_details));
        }
      });
    },
    fixedColumns: {
      leftColumns: 2,
      rightColumns: 3
    },
    scrollX: true,
    "columns": columns,
  });
});