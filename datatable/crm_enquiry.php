<?php
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * Easy set variables
	 */
     session_start();
   $base = $_SERVER['DOCUMENT_ROOT'];
   include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'connection.php');
   $dbConnection = get_conn_handle();
	/* Array of database columns which should be read and sent back to DataTables. Use a space where
	 * you want to insert a non-database field (for example a counter or static image)
	 */
  $user = $_SESSION["loggedin_user"];
  $role = $_SESSION["loggedin_role"];
  $aColumns = array(
    'name',
    'project_name',
    'project_id',
    'email',
    'cell',
    'company',
    'assigned_on',
    'added_on',
    'enquiry_id',
    'assigned_to',
    'user_name',
    'added_by',
    'enquiry_number',
    'crm_cust_interest_status_id',
    'crm_cust_interest_status_name',
    'enquiry_source_master_name',
    'enquiry_source_master_id',
    'walk_in'
  );

   /* Indexed column (used for fast and accurate table cardinality) */
   $sIndexColumn = "enquiry_id";

   /* DB table to use */
   $sTable = $_GET['table'];

	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP server-side, there is
	 * no need to edit below this line
	 */

	/*
	 * Local functions
	 */
	function fatal_error ( $sErrorMessage = '' )
	{
		header( $_SERVER['SERVER_PROTOCOL'] .' 500 Internal Server Error' );
		die( $sErrorMessage );
	}

	/*
	 * Paging
	 */
	$sLimit = "";
	if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
	{
		$sLimit = "LIMIT ".intval( $_GET['iDisplayStart'] ).", ".
			intval( $_GET['iDisplayLength'] );
	}


	/*
	 * Ordering
	 */
   $sOrder = "";
 	if ( isset( $_GET['order'][0]["column"] ) )
 	{
 		$sOrder = "ORDER BY ";
 		for ( $i=0 ; $i<count($_GET['order']) ; $i++ )
 		{
 				$sOrder .= "`".$aColumns[ intval( $_GET['order'][$i]["column"] ) ]."` ".
 				($_GET['order'][$i]["dir"]=='asc' ? 'desc' : 'asc') .", ";
 		}

 		$sOrder = substr_replace( $sOrder, "", -2 );
 		if ( $sOrder == "ORDER BY" )
 		{
 			$sOrder = "";
 		}
 	}
	/*
	 * Filtering
	 * NOTE this does not match the built-in DataTables filtering which does it
	 * word by word on any field. It's possible to do here, but concerned about efficiency
	 * on very large tables, and MySQL's regex functionality is very limited
	 */
	$sWhere = "";

   if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
     {
       $sWhere = "WHERE (";
       for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
         $sWhere .= "`".$aColumns[$i]."` LIKE '%".( $_GET['sSearch'] )."%' OR ";
        }
        $sWhere = substr_replace( $sWhere, "", -3 );
        $sWhere .= ")";
     	}

             	/* Individual column filtering */


    if(isset($_GET['project_id']) && $_GET['project_id'] != '') {
      if($sWhere==''){
        $sWhere .= " Where `project_id` = ". $_GET['project_id'];
      }
      else{
        $sWhere .= " AND `project_id` = ". $_GET['project_id'];
      }
    }

    if(isset($_GET['search_cell']) && $_GET['search_cell'] != '') {
      if($sWhere==''){
        $sWhere .= " Where `cell` ='". $_GET['search_cell']."'";
      }
      else{
        $sWhere .= " AND `cell` = '". $_GET['search_cell']."'";
      }
    }

    if(isset($_GET['search_source']) && $_GET['search_source'] != '') {
      if($sWhere==''){
        $sWhere .= " Where `enquiry_source_master_id` = ". $_GET['search_source'];
      }
      else{
        $sWhere .= " AND `enquiry_source_master_id` = ". $_GET['search_source'];
      }
    }

    if(isset($_GET['assigned_to']) && $_GET['assigned_to'] != '') {
      if($sWhere==''){
        $sWhere .= " Where `user_id` = ". $_GET['assigned_to'];
      }
      else{
        $sWhere .= " AND `user_id` = ". $_GET['assigned_to'];
      }
    }

    if(isset($_GET['search_status']) && $_GET['search_status'] != '') {
      if($sWhere==''){
        $sWhere .= " Where `crm_cust_interest_status_id` ='". $_GET['search_status']."'";
      }
      else{
        $sWhere .= " AND `crm_cust_interest_status_id` = '". $_GET['search_status']."'";
      }
    }

    if(isset($_GET['enquiry_number']) && $_GET['enquiry_number'] != '') {
      if($sWhere==''){
        $sWhere .= " Where `enquiry_number` ='". $_GET['enquiry_number']."'";
      }
      else{
        $sWhere .= " AND `enquiry_number` = '". $_GET['enquiry_number']."'";
      }
    }

    if(isset($_GET['start_date']) && $_GET['start_date'] != '') {
      if($sWhere==''){
        $sWhere .= " Where `added_on` >= '". $_GET['start_date']." 00:00:00'";
      }
      else{
      $sWhere .= " AND `added_on` >= '". $_GET['start_date']." 00:00:00'";
     }
   }

    if(isset($_GET['end_date']) && $_GET['end_date'] != '') {
      if($sWhere==''){
        $sWhere .= " Where `added_on` <= '". $_GET['end_date']." 23:59:00'";
      }
      else{
      $sWhere .= " AND `added_on` <= '". $_GET['end_date']." 23:59:00'";
     }
   }

	/*
	 * SQL queries
	 * Get data to display
	 */

   $sQuery = "
    SELECT SQL_CALC_FOUND_ROWS `".str_replace(" , ", " ", implode("`, `", $aColumns))."`
    FROM $sTable
    $sWhere
    $sOrder
    $sLimit
    ";
   $statement = $dbConnection->prepare($sQuery);
   $statement -> execute();
   $rResult = $statement -> fetchAll();
   $statement = $dbConnection->prepare("SELECT FOUND_ROWS() as iFilteredTotal");
   $statement -> execute();
   $iFilteredTotal = $statement -> fetch()['iFilteredTotal'];
  /*
   * Output
   */
   // print_r($rResult);
  $output = array(
    "iTotalDisplayRecords" => $iFilteredTotal,
    "query"=>$sQuery,
    "aaData" => array(),
  );
   foreach($rResult as $aRow){
    $row = array();
    for ( $i=0 ; $i<count($aColumns) ; $i++ )
    {
      if ( $aColumns[$i] == "version" )
      {
        /* Special output formatting for 'version' column */
        $row[] = ($aRow[ $aColumns[$i] ]=="0") ? '-' : $aRow[ $aColumns[$i] ];
      }
      else if ( $aColumns[$i] != ' ' )
      {
        /* General output */
        $row[$aColumns[$i]] = $aRow[ $aColumns[$i] ];
      }
    }
    $output['aaData'][] = $row;
  }
  echo json_encode($output,JSON_PARTIAL_OUTPUT_ON_ERROR);
 ?>
