var table;
var columnsMapping = {
  '2': 'total_amount',
  '3': 'issued_amount',
  '4': 'balance_amount',
  '5': 'total_contract_amount',
  '6': 'issued_contract_amount',
  '7': 'balance_contract_amount',
  '8': 'overall_amount',
  '9': 'overall_issued_amount',
  '10': 'overall_balance_amount'
}

function tableDraw() {
  table.fnDraw();
}

function createToolTip(str) {
  var substr = (str.length <= 15) ? str : str.substr(0, 10) + '...';
  return '<abbr id="tool_tip" data-toggle="tooltip" data-placement="right" title="' + str + '">' + substr + '</abbr>'
}

function getCellValue() {
  var meta = arguments[3];
  var classname = columnsMapping[meta.col] + '_' + meta.row;
  if (arguments[1] === 'export') {
    return $("tr").find('span.' + classname).html();
  }
  return '<span class="' + classname + '">Loading..</span>';
}

function colorizeCell() {
  if (arguments[4] < 5) {
    $(arguments[0]).addClass('blue');
  } else if (arguments[4] > 4 && arguments[4] < 8) {
    $(arguments[0]).addClass('red');
  } else {
    $(arguments[0]).addClass('green');
  }
}

$(document).ready(function() {
  var commonCellDefinition = {
    orderable: false,
    data: getCellValue,
    createdCell: colorizeCell,
  };

  var columns = [{
      className: 'noVis',
      "orderable": false,
      "width": "1%",
      "data": function() {
        return arguments[3].settings._iDisplayStart + arguments[3].row + 1;
      }
    },
    {
      "orderable": false,
      "data": "project_manpower_agency_name"
    },
    commonCellDefinition,
    commonCellDefinition,
    commonCellDefinition,
    commonCellDefinition,
    commonCellDefinition,
    commonCellDefinition,
    commonCellDefinition,
    commonCellDefinition,
    commonCellDefinition,
  ];

  table = $('#example').dataTable({
    stateSave: true,
    serverSide: true,
    dataSrc: 'aaData',
    ajax: 'datatable/project_master_overall_payment_list.php',
    pageLength: 10,
    scrollY: 600,
    processing: true,
    scrollCollapse: true,
    fixedHeader: true,
    searchDelay: 1200,
    fnCreatedRow: function(row, full, index) {
      var manpower_payment = $.ajax({
        url: 'ajax/get_overall_payment_details.php',
        data: "vendor_id=" + full.project_manpower_agency_id + "&type='Manpower'",
        success: function(response) {
          response = JSON.parse(response);
          $('span.' + 'total_amount' + '_' + index).html(response.total_amount.toFixed(2));
          $('span.' + 'issued_amount' + '_' + index).html(response.issued_amount.toFixed(2));
          $('span.' + 'balance_amount' + '_' + index).html(response.balance_amount.toFixed(2));

          var contract_payment = $.ajax({
            url: 'ajax/get_contract_overall_payment_details.php',
            data: "vendor_id=" + full.project_manpower_agency_id,
            success: function(contract_response) {
              contract_response = JSON.parse(contract_response);
              $('span.' + 'total_contract_amount' + '_' + index).html(contract_response.total_amount.toFixed(2));
              $('span.' + 'issued_contract_amount' + '_' + index).html(contract_response.issued_amount.toFixed(2));
              $('span.' + 'balance_contract_amount' + '_' + index).html(contract_response.balance_amount.toFixed(2));
              $('span.' + 'overall_amount' + '_' + index).html((response.total_amount + contract_response.total_amount).toFixed(2));
              $('span.' + 'overall_issued_amount' + '_' + index).html((response.issued_amount + contract_response.issued_amount).toFixed(2));
              $('span.' + 'overall_balance_amount' + '_' + index).html((response.balance_amount + contract_response.balance_amount).toFixed(2));
            }
          });
        }
      });
    },
    fnServerParams: function(aoData) {
      aoData.aaSorting = aoData.order;
      aoData.iDisplayLength = aoData.length;
      aoData.iDisplayStart = aoData.start;
      aoData.table = "project_manpower_agency_list";
      aoData.search_vendor = $('#search_vendor').val();

      if (aoData.search && aoData.search.value) {
        aoData.sSearch = aoData.search.value;
      }
    },
    buttons: [{
      extend: 'excelHtml5',
      text: 'Export',
      exportOptions: {
        orthogonal: 'export'
      }
    }, ],
    dom: 'lBfrtip',
    lengthMenu: [
      [10, 25, 50, 100, -1],
      [10, 25, 50, 100, "All"]
    ],
    scrollX: true,
    "columns": columns,
  });
});