var table;

function tableDraw() {
  table.fnDraw();
}

function createToolTip(str) {
  var substr = (str.length <= 15) ? str : str.substr(0, 10) + '...';
  return '<abbr id="tool_tip" data-toggle="tooltip" data-placement="right" title="' + str + '">' + substr + '</abbr>'
}


var columns = [{
    className: 'noVis',
    "orderable": false,
    "width": "1%",
    "data": function() {
      return arguments[3].settings._iDisplayStart + arguments[3].row + 1;
    }
  },
  {
    "orderable": false,
    "data": function(data, type) {
      if ($('#ddl_tds_type').val() == 'Machine') {
        return data.project_machine_vendor_master_name;
      }
      return data.project_manpower_agency_name;
    }
  },
  {
    "orderable": false,
    "data": "project_tds_deduction_master_type"
  },
  {
    "orderable": false,
    "data": "project_tds_deduction_master_deduction"
  },
  {
    "orderable": false,
    "data": function(data, type) {
      return data.project_tds_deduction_master_remarks;
    }
  },
  {
    "orderable": false,
    "data": function(data, type) {
      return data.user_name;
    }
  },
  {
    "orderable": false,
    "data": function(data, type) {
      return moment(data.project_tds_deduction_master_added_on).
      format('DD-MM-YYYY hh:mm A');
    }
  },
  {
    "orderable": false,
    "data": function(data, type) {
      if (data.project_tds_deduction_master_effective_date == '0000-00-00') {
        return '00-00-0000';
      }
      return moment(data.project_tds_deduction_master_effective_date).
      format('DD-MM-YYYY');
    }
  },
  {
    "orderable": false,
    "data": function(data, type) {
      if (window.permissions.edit) {
        return `<a target = _blank href=project_edit_tds_master.php?tds_master_id=${data.project_tds_deduction_master_id}&tds_master_type=${data.project_tds_deduction_master_type}&vendor=${data.project_manpower_agency_name}>
        <span class="glyphicon glyphicon-pencil"></span></a>`;
      }
      return '***';
    }
  },
];

$(document).ready(function() {
  table = $('#example').dataTable({
    stateSave: true,
    serverSide: true,
    dataSrc: 'aaData',
    ajax: 'datatable/project_tds_master_list.php',
    pageLength: 10,
    scrollY: 600,
    processing: true,
    scrollCollapse: true,
    fixedHeader: true,
    searchDelay: 1200,
    fnServerParams: function(aoData) {
      aoData.aaSorting = aoData.order;
      aoData.iDisplayLength = aoData.length;
      aoData.iDisplayStart = aoData.start;
      aoData.table = "tds_deduction_master";
      aoData.search_master_type = $('#ddl_tds_type').val();
      aoData.search_vendor = $('#search_vendor').val();
      aoData.search_machine_vendor = $('#search_machine_vendor').val();

      if (aoData.search && aoData.search.value) {
        aoData.sSearch = aoData.search.value;
      }
    },
    buttons: [{
      extend: 'excelHtml5',
      text: 'Export',
      exportOptions: {
        orthogonal: 'export'
      }
    }, ],
    dom: 'lBfrtip',
    lengthMenu: [
      [10, 25, 50, 100, -1],
      [10, 25, 50, 100, "All"]
    ],
    // fixedColumns: {
    //   leftColumns: 3,
    //   rightColumns: 3
    // },
    scrollX: true,
    "columns": columns,
  });
});