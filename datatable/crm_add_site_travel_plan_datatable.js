var table;
var columnsMapping = {
  '12': 'assigned_by'
};

function tableDraw() {
  table.draw();
}

function openModal(project_name, project_id, plan_id, enquiry_number, name, phone_no) {
  $('span#project').html(project_name);
  $('span#enquiry').html(enquiry_number);
  $('span#name').html(name);
  $('span#phone').html(phone_no);
  $('input#project_id').val(project_id);
  $('input#plan_id').val(plan_id);
  $('#exampleModal').modal('show');

  $('#exampleModal').on('hidden.bs.modal', function(e) {
    $('form#site_travel_plan')[0].reset();
  });
}

function submitPlan() {
  if ($('#date').val().trim() == '' || $("#ddl_cab").val().trim() == '') {
    alert("Please fill all the mandatory fields");
    return false;
  }
  $.ajax({
    url: 'ajax/project_add_travel_plan.php',
    type: 'POST',
    data: {
      dt_site_visit: $('#date').val(),
      cb_sv_plans: $('#plan_id').val(),
      ddl_project: $('#project_id').val(),
      ddl_cab: $("#ddl_cab").val(),
      add_stp_submit: 'yes'
    },
    success: function(response) {
      if (response == 1) {
        alert("Cab Plan entered successfully");
        $('#exampleModal').modal('hide');
        tableDraw();
      } else {
        $('#exampleModal').modal('hide');
        alert("Something Went Wrong");
      }
    }
  })
}

function createToolTip(str, length) {
  var substr = (str.length <= 20) ? str : str.substr(0, 20) + '...';
  return '<abbr data-toggle="tooltip" data-placement="right" title="' + str + '">' + substr + '</abbr>'
}

$(document).ready(function() {
  var columns = [{
      className: 'noVis',
      "orderable": false,
      "data": function() {
        return arguments[3].settings._iDisplayStart + arguments[3].row + 1;
      }
    },
    {
      "orderable": true,
      "data": function(data) {
        return moment(data.crm_site_visit_plan_date).format("DD/MM/YYYY")
      }
    },
    {
      "orderable": false,
      "data": "crm_site_visit_plan_time"
    },
    {
      "orderable": false,
      "data": "enquiry_number"
    },
    {
      "orderable": false,
      "data": "project_name"
    },
    {
      "orderable": false,
      "data": "name"
    },
    {
      "orderable": false,
      "data": "cell"
    },
    {
      "orderable": false,
      "data": "assignee"
    },
    {
      "orderable": false,
      "data": function(data) {
        return createToolTip(data.crm_site_visit_pickup_location, 20);
      }
    },
    {
      "orderable": false,
      "data": function(data) {
        if (data.crm_site_visit_plan_confirmation == "0") {
          return 'Tentative'
        } else if (data.crm_site_visit_plan_confirmation == "1") {
          return 'Confirmed';
        }
        return 'Cancelled';
      }
    },
    {
      "orderable": false,
      "data": function(data) {
        if (data.crm_site_visit_plan_drive == "0") {
          return 'KNS'
        }
        return 'Self-Drive';
      }
    },
    {
      "orderable": false,
      "data": "assigner"
    },
    {
      orderable: false,
      "data": function(data) {
        return '<a href="#"><span id="travel" class="fas fa-car"></span></a>';
      }
    }
  ];

  table = $('#example').DataTable({
    stateSave: true,
    serverSide: true,
    dataSrc: 'aaData',
    ajax: 'datatable/crm_add_site_travel_plan.php',
    pageLength: 10,
    scrollY: 600,
    processing: true,
    scrollCollapse: true,
    fixedHeader: true,
    searchDelay: 1200,
    "language": {
      "infoFiltered": " "
    },
    "order": [
      [1, "asc"]
    ],
    fnServerParams: function(aoData) {
      aoData.aaSorting = aoData.order;
      aoData.iDisplayLength = aoData.length;
      aoData.iDisplayStart = aoData.start;
      aoData.table = "crm_site_travel";
      aoData.project_id = $('#ddl_project').val();
      aoData.start_date = $('#start_date').val();
      aoData.end_date = $('#end_date').val();

      if (aoData.search && aoData.search.value) {
        aoData.sSearch = aoData.search.value;
      }
    },
    buttons: [{
      extend: 'colvis',
      columns: ':not(.noVis)'
    }],
    fixedColumns: {
      leftColumns: 3,
      rightColumns: 2
    },
    scrollX: true,
    "columns": columns,
  });

  $('#example tbody').on('click', 'tr', function(event) {
    var rowData = table.row(this).data();
    if (event.target.tagName == 'SPAN' && event.target.id == 'travel') {
      openModal(rowData.project_name, rowData.project_id, rowData.crm_site_visit_plan_id, rowData.enquiry_number, rowData.name, rowData.cell);
    }
  });
});