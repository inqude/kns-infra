<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: bd_file_list.php
CREATED ON	: 11-Nov-2015
CREATED BY	: Nitin Kashyap
PURPOSE     : List of BD files
*/

/* DEFINES - START */
define('BD_FILE_LIST_FUNC_ID','131');
define('LAND_STATUS_LIST_FUNC_ID','327');
define('DELAY_LIST_FUNC_ID','328');
define('FILE_PAYMENT_LIST_FUNC_ID','329');
define('FILE_DETAILS_LIST_FUNC_ID','330');
define('LAWYER_REPORT_PRINT_FUNC_ID','331');
/* DEFINES - END */


/*
TBD: 
1. Date display and calculation
2. Permission management
*/
$_SESSION['module'] = 'BD';

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_projects'.DIRECTORY_SEPARATOR.'bd_project_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_masters'.DIRECTORY_SEPARATOR.'bd_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'files'.DIRECTORY_SEPARATOR.'file_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];
	
	
	// Get permission settings for this user for this page
	$view_perms_list    = i_get_user_perms($user,'',BD_FILE_LIST_FUNC_ID,'2','1');
	$edit_perms_list    = i_get_user_perms($user,'',BD_FILE_LIST_FUNC_ID,'3','1');
	$delete_perms_list  = i_get_user_perms($user,'',BD_FILE_LIST_FUNC_ID,'4','1');
	
	$land_status_view_perms_list    = i_get_user_perms($user,'',LAND_STATUS_LIST_FUNC_ID,'2','1');
	$land_status_edit_perms_list    = i_get_user_perms($user,'',LAND_STATUS_LIST_FUNC_ID,'3','1');
	$land_status_delete_perms_list  = i_get_user_perms($user,'',LAND_STATUS_LIST_FUNC_ID,'4','1');
	
	$delay_list_view_perms_list    = i_get_user_perms($user,'',DELAY_LIST_FUNC_ID,'2','1');
	$delay_list_edit_perms_list    = i_get_user_perms($user,'',DELAY_LIST_FUNC_ID,'3','1');
	$delay_list_delete_perms_list  = i_get_user_perms($user,'',DELAY_LIST_FUNC_ID,'4','1');
	
	$file_payment_list_view_perms_list    = i_get_user_perms($user,'',FILE_PAYMENT_LIST_FUNC_ID,'2','1');
	$file_payment_list_edit_perms_list    = i_get_user_perms($user,'',FILE_PAYMENT_LIST_FUNC_ID,'3','1');
	$file_payment_list_delete_perms_list  = i_get_user_perms($user,'',FILE_PAYMENT_LIST_FUNC_ID,'4','1');
	
	$file_details_view_perms_list    = i_get_user_perms($user,'',FILE_DETAILS_LIST_FUNC_ID,'2','1');
	$file_details_edit_perms_list    = i_get_user_perms($user,'',FILE_DETAILS_LIST_FUNC_ID,'3','1');
	$file_details_delete_perms_list  = i_get_user_perms($user,'',FILE_DETAILS_LIST_FUNC_ID,'4','1');
	
	$lawyer_report_view_perms_list    = i_get_user_perms($user,'',LAWYER_REPORT_PRINT_FUNC_ID,'2','1');
	$lawyer_report_edit_perms_list    = i_get_user_perms($user,'',LAWYER_REPORT_PRINT_FUNC_ID,'3','1');
	$lawyer_report_delete_perms_list  = i_get_user_perms($user,'',LAWYER_REPORT_PRINT_FUNC_ID,'4','1');
	
	
	

	// Query String Data
	if(isset($_GET["msg"]))
	{
		$msg = $_GET["msg"];
	}
	else
	{
		$msg = "";
	}
	// Nothing here

	// Temp data
	$alert = "";

	$file_id        = "";
	$project_id     = "";
	$survey_no      = "";
	$owner          = "";
	$village        = "";
	$owner_status   = "";
	$process_status = "";		
	$is_not_started = "";

	// Search parameters
	if(isset($_POST["bd_file_search_submit"]))
	{		
		$project_id   = $_POST["ddl_project"];
		$survey_no    = $_POST["stxt_survey_no"];
		$village      = $_POST["ddl_village"];
		$owner_status = $_POST["ddl_owner_status"];
		
		if((isset($_POST['cb_is_not_started'])) && ($_POST['cb_is_not_started'] == '1'))
		{
			$is_not_started = '1';
		}
		else
		{
			$is_not_started = '';
		}
	}
	
	// Get list of files
	$bd_file_list = i_get_bd_files_list($file_id,$project_id,$survey_no,$owner,$village,$owner_status,$process_status,'1');

	if($bd_file_list["status"] == SUCCESS)
	{
		$bd_file_list_data = $bd_file_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$bd_file_list["data"];
	}	
	
	// Get list of villages
	$village_list = i_get_village_list('');
	if($village_list["status"] == SUCCESS)
	{
		$village_list_data = $village_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$village_list["data"];
	}	
	
	// Get list of BD projects
	$bd_project_list = i_get_bd_project_list('','','','','1');
	if($bd_project_list["status"] == SUCCESS)
	{
		$bd_project_list_data = $bd_project_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$bd_project_list["data"];
		$alert_type = 0; // Failure
	}
	
	// Get list of owner status
	$owner_status_list = i_get_owner_status_list('');
	if($owner_status_list["status"] == SUCCESS)
	{
		$owner_status_list_data = $owner_status_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$owner_status_list["data"];
		$alert_type = 0; // Failure
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>BD File List</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>    

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
		  
            <div class="widget-header" style="height:100px;"> <i class="icon-th-list"></i>
              <h3>BD File List&nbsp;&nbsp;&nbsp;Total Extent: <span id="total_extent_span"><i>Calculating</i></span> guntas&nbsp;&nbsp;&nbsp;Total Files: <span id="total_file_count"><i>Calculating</i></span>&nbsp;&nbsp;&nbsp;&nbsp;<a href="bd_file_live_export.php?file_id=<?php echo $file_id; ?>&project_id=<?php echo $project_id; ?>&survey=<?php echo $survey_no; ?>&owner=<?php echo $owner; ?>&village=<?php echo $village; ?>&owner_status=<?php echo $owner_status; ?>&process_status=<?php echo $process_status; ?>" target="_blank">Export Live to Excel</a></h3>
			  <h3>
			  <?php
			  $status_totals = array();
			  for($count = 0; $count < count($owner_status_list_data); $count++)
			  {
				  $status_totals[$owner_status_list_data[$count]['bd_file_owner_status_id']] = 0;
			  ?>
			  <span <?php if($owner_status_list_data[$count]['bd_file_owner_status_id'] == '8')
					 {
						?>						
						style="color:red;"
						<?php
					 } ?>>
			  <?php echo $owner_status_list_data[$count]['bd_file_owner_status_name']; ?></span>: <span id="extent_<?php echo $owner_status_list_data[$count]['bd_file_owner_status_id']; ?>"><i>Calculating</i></span> guntas&nbsp;&nbsp;&nbsp;
			  <?php
			  }
			  ?></h3>			  
            </div>
            <!-- /widget-header -->
			<?php
			if($view_perms_list['status'] == SUCCESS)
			{
			?>
			<div class="widget-header" style="height:80px; padding-top:10px;">               
			  <form method="post" id="file_search_form" action="bd_file_list.php">
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="ddl_village">
			  <option value="">- - Select Village - -</option>
			  <?php for($count = 0; $count < count($village_list_data); $count++)
			  {?>
			  <option value="<?php echo $village_list_data[$count]["village_id"]; ?>" <?php if($village_list_data[$count]["village_id"] == $village) { ?> selected="selected" <?php } ?>><?php echo $village_list_data[$count]["village_name"]; ?></option>
			  <?php
			  }?>
			  </select>
			  </span>
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="ddl_project">
			  <option value="">- - Select Project - -</option>
			  <?php for($count = 0; $count < count($bd_project_list_data); $count++)
			  {?>
			  <option value="<?php echo $bd_project_list_data[$count]["bd_project_id"]; ?>" <?php if($bd_project_list_data[$count]["bd_project_id"] == $project_id){ ?> selected="selected" <?php } ?>><?php echo $bd_project_list_data[$count]["bd_project_name"]; ?></option>
			  <?php
			  }?>
			  </select>
			  </span>
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="ddl_owner_status">
			  <option value="">- - Select Land Status - -</option>
			  <?php for($count = 0; $count < count($owner_status_list_data); $count++)
			  {?>
			  <option value="<?php echo $owner_status_list_data[$count]["bd_file_owner_status_id"]; ?>" <?php if($owner_status_list_data[$count]["bd_file_owner_status_id"] == $owner_status){ ?> selected="selected" <?php } ?>><?php echo $owner_status_list_data[$count]["bd_file_owner_status_name"]; ?></option>
			  <?php
			  }?>
			  </select>
			  </span>
			  <span style="padding-left:20px; padding-right:20px;">
			  <input type="text" name="stxt_survey_no" value="<?php echo $survey_no; ?>" placeholder="Search by survey number" />
			  </span>
			  <br />
			  <span style="padding-left:20px; padding-right:20px;">
			  <input type="checkbox" name="cb_is_not_started" <?php if($is_not_started == '1'){ ?> checked <?php } ?> value='1' />&nbsp;&nbsp;&nbsp;Process Not Started
			  </span>			  
			  <input type="submit" name="bd_file_search_submit" />
			  </form>			  
            </div>
			<?php
			}
			else
			{
				echo 'You are not authorized to view this page';
			}
			?>
            <!-- /widget-header -->
            <div class="widget-content">
			<?php echo $msg; ?>
			<div class="widget-content">
			<?php
			if($view_perms_list['status'] == SUCCESS)
			{
			?>
              <table class="table table-bordered" style="table-layout: fixed;">
                <thead>
                  <tr>				    
				    <th style="word-wrap:break-word;">SL No</th>
					<th style="word-wrap:break-word;">File id</th>
					<th style="word-wrap:break-word;">Survey No</th>
					<th style="word-wrap:break-word;">Survey Alias</th>
					<th style="word-wrap:break-word;">Survey Alias Date</th>
					<th style="word-wrap:break-word;">Project</th>
					<th style="word-wrap:break-word;">Land Owner</th>	
					<th style="word-wrap:break-word;">Village</th>						
					<th style="word-wrap:break-word;">Extent</th>
					<th style="word-wrap:break-word;">Land Status</th>					
					<th style="word-wrap:break-word;">Land Cost</th>					
					<th style="word-wrap:break-word;">Brokerage Amount</th>					
					<th style="word-wrap:break-word;">Paid Amount</th>					
					<th style="word-wrap:break-word;">Payable</th>					
					<th style="word-wrap:break-word;">Process Status</th>	
					<th style="word-wrap:break-word;">Account</th>	
					<th style="word-wrap:break-word;">JDA %</th>					
					<th style="word-wrap:break-word;">Added By</th>				
					<th style="word-wrap:break-word;">&nbsp;</th>				
					<th style="word-wrap:break-word;">&nbsp;</th>								
				</tr>
				</thead>
				<tbody>
				<?php
				$total_extent = 0;
				$file_count   = 0;
				$sl_no        = 0;
				
				if($bd_file_list["status"] == SUCCESS)
				{		
					for($count = 0; $count < count($bd_file_list_data); $count++)
					{	
						$paid        = 0;
						// Corresponding Legal File Details
						$legal_file_sresult = i_get_file_list('',$bd_file_list_data[$count]["bd_project_file_id"],'','','','','','','','','','');
						if($legal_file_sresult['status'] == SUCCESS)
						{
							$is_legal_file = true;
							$legal_file_no = $legal_file_sresult['data'][0]['file_number']; 
							$legal_file_id = $legal_file_sresult['data'][0]['file_id']; 
							
							// Get list of file payments
							$file_payment_list = i_get_file_payment_list($legal_file_id,'');

							$total = $bd_file_list_data[$count]["bd_file_land_cost"];
							if($file_payment_list["status"] == SUCCESS)
							{
								// Get specific file payment details
								$file_payment_list_data = $file_payment_list["data"];
								for($pay_count = 0 ; $pay_count < count($file_payment_list_data) ; $pay_count++)
								{
									$paid = $paid + $file_payment_list_data[$pay_count]["file_payment_amount"];
								}
								$balance_amount = $total - $paid ;
							}
							else
							{
								$alert = "Invalid File!";
								$paid = 0;
							}
						}
						else
						{
							$is_legal_file = false;
							$legal_file_no = 'NO LEGAL FILE';
							$legal_file_id = '';
							$paid = 0;
						}
						
						/*$file_handover_data = i_get_file_handover_details($legal_file_id);
						if($file_handover_data['status'] != SUCCESS)
						{*/
							if(($is_not_started == '') || ($is_legal_file == false))
							{
					?>
					<tr <?php
					if($bd_file_list_data[$count]["bd_file_owner_status"] == '8')
					{
					?>						
					style="color:red;"
					<?php
					}
					?>>					
					<?php
					$total_extent = $total_extent + $bd_file_list_data[$count]["bd_file_extent"];
					$file_count++;
					$sl_no = $sl_no + 1;
					
					// See which owner status this file is in
					$status_totals[$bd_file_list_data[$count]["bd_file_owner_status"]] = $status_totals[$bd_file_list_data[$count]["bd_file_owner_status"]] + $bd_file_list_data[$count]["bd_file_extent"];		
					// Check if this file is already in land bank
					$bd_land_bank_search_data = array('file_id'=>$bd_file_list_data[$count]["bd_project_file_id"],"sort"=>'1');
					$bd_land_bank_data = i_get_bd_land_bank_list($bd_land_bank_search_data);
					
					// Delay details for the file
					$delay_sresult = i_get_delay_details($bd_file_list_data[$count]["bd_project_file_id"]);
					if($delay_sresult['status'] == SUCCESS)
					{
						$delay_reason = $delay_sresult['data'][0]['bd_delay_reason_name'];
					}
					else
					{
						$delay_reason = 'NO DELAY';
					}
					?>
						<td style="word-wrap:break-word;"><?php echo $sl_no; ?></td>
						<td style="word-wrap:break-word;"><?php echo $bd_file_list_data[$count]["bd_project_file_id"]; ?><br /><br /><?php echo $legal_file_no; ?></td>
						<td style="word-wrap:break-word;"><?php echo $bd_file_list_data[$count]["bd_file_survey_no"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $bd_file_list_data[$count]["bd_file_survey_sale_deed"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $bd_file_list_data[$count]["bd_file_survey_sale_deed_date"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $bd_file_list_data[$count]["bd_project_name"]; ?></td>						
						<td style="word-wrap:break-word;"><?php echo $bd_file_list_data[$count]["bd_file_owner"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $bd_file_list_data[$count]["village_name"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $bd_file_list_data[$count]["bd_file_extent"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $bd_file_list_data[$count]["bd_file_owner_status_name"]; ?><br /></td>
						<td style="word-wrap:break-word;"><?php echo get_formatted_amount($total,"INDIA") ; ?></td>
						<td style="word-wrap:break-word;"><?php echo  $bd_file_list_data[$count]["bd_file_brokerage_amount"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo get_formatted_amount($paid,"INDIA") ; ?></td>
						<td style="word-wrap:break-word;"><?php echo get_formatted_amount($balance_amount,"INDIA") ;?></td>
						<td style="word-wrap:break-word;"><?php if($legal_file_sresult["status"] == SUCCESS)
						{
							echo $legal_file_sresult["data"][0]["process_name"];
						}
						else
						{
							echo $bd_file_list_data[$count]["process_name"];
						}?></td>
						<td style="word-wrap:break-word;"><?php echo $bd_file_list_data[$count]["bd_own_account_master_account_name"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $bd_file_list_data[$count]["bd_project_file_jda_share_percent"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $bd_file_list_data[$count]["user_name"]; ?></td>
					
						<td style="word-wrap:break-word;"><?php if($view_perms_list['status'] == SUCCESS)
					{
					?><a href="bd_file_details.php?file=<?php echo $bd_file_list_data[$count]["bd_project_file_id"]; ?>">Details</a><?php } ?></td>		
						
						<td style="word-wrap:break-word;"><?php												
						if($bd_land_bank_data['status'] != SUCCESS)
						{
							?><?php if($edit_perms_list['status'] == SUCCESS)
					{
					?><a href="#" onclick="return go_to_land_bank('<?php echo $bd_file_list_data[$count]["bd_project_file_id"]; ?>');">Move to Land Bank</a>
							<?php
						} }
						else
						{
							if($bd_land_bank_data['data'][0]['bd_land_bank_active'] == '0')
							{
								?><?php												
						if($bd_land_bank_data['status'] != SUCCESS)
						{
							?><a href="#" onclick="return go_to_land_bank('<?php echo $bd_file_list_data[$count]["bd_project_file_id"]; ?>');">Move to Land Bank</a>
								<?php
							} }
							else if($bd_land_bank_data['data'][0]['bd_land_bank_active'] == '1')
							{
								echo 'Already in land bank';
							}
							else if($bd_land_bank_data['data'][0]['bd_land_bank_active'] == '2')
							{
								echo 'Mortgage not possible';
							}
						}						
						?></td>
					</tr>
					<?php 
							}
						//}
					}
				}
				else
				{
					?>
					<td colspan="18">No Files added yet!</td>
					<?php					
				}
				 ?>	
				 <script>
				 document.getElementById('total_extent_span').innerHTML = <?php echo $total_extent; ?>;
				 document.getElementById('total_file_count').innerHTML = <?php echo $file_count; ?>;
				 </script>
				 <script>
				 <?php
				 for($count = 0; $count < count($owner_status_list_data); $count++)
				 {
					if($owner_status_list_data[$count]['bd_file_owner_status_id'] == '8')
					 {
						?>
						document.getElementById('extent_<?php echo $owner_status_list_data[$count]['bd_file_owner_status_id']; ?>').style.color = "red";
						<?php
					 }
				 ?>				 
				 document.getElementById('extent_<?php echo $owner_status_list_data[$count]['bd_file_owner_status_id']; ?>').innerHTML = <?php echo $status_totals[$owner_status_list_data[$count]['bd_file_owner_status_id']]; ?>;				 				 
				 <?php
				 }
				 ?>
				</script>
                </tbody>
              </table>
			   <?php
			}
			else
			{
				echo 'You are not authorized to view this page';
			}
			?>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>

<script>
function confirm_deletion(bd_file)
{
	var ok = confirm("Are you sure you want to delete?")
	{         
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					 window.location = "bd_file_list.php";
				}
			}

			xmlhttp.open("GET", "bd_file_delete.php?file=" + bd_file);   // file name where delete code is written
			xmlhttp.send();
		}
	}	
}

function go_to_land_bank(file_id)
{
	var form = document.createElement("form");
    form.setAttribute("method", "post");
    form.setAttribute("action", "bd_move_to_land_bank.php");
	
	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","file_id");
	hiddenField1.setAttribute("value",file_id);		
    
	form.appendChild(hiddenField1);	
	
	document.body.appendChild(form);
    form.submit();
}
</script>
<script>
/* Open the sidenav */
function openNav() {
    document.getElementById("mySidenav").style.width = "75%";
}

/* Close/hide the sidenav */
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}
</script>

</body>

</html>
