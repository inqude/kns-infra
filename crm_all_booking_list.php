<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: crm_all_booking_list.php
CREATED ON	: 22-Nov-2015
CREATED BY	: Nitin Kashyap
PURPOSE     : All Booking List
*/
/* DEFINES - START */define('CRM_ALL_BOOKING_LIST_FUNC_ID','131');/* DEFINES - END */
/*
TBD: 
*/$_SESSION['module'] = 'CRM Projects';

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_post_sales_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_sales_process.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_projects'.DIRECTORY_SEPARATOR.'crm_project_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];		// Get permission settings for this user for this page	$add_perms_list    = i_get_user_perms($user,'',CRM_ALL_BOOKING_LIST_FUNC_ID,'1','1');		$view_perms_list   = i_get_user_perms($user,'',CRM_ALL_BOOKING_LIST_FUNC_ID,'2','1');	$edit_perms_list   = i_get_user_perms($user,'',CRM_ALL_BOOKING_LIST_FUNC_ID,'3','1');	$delete_perms_list   = i_get_user_perms($user,'',CRM_ALL_BOOKING_LIST_FUNC_ID,'4','1');

	// Query String Data
	// Nothing

	// Temp data
	$alert = "";
	
	if(isset($_POST["search_all_booking"]))
	{
		$project_id      = $_POST["ddl_project"];	
		$approval_status = $_POST["ddl_approval_status"];
		if(($role == 1) || ($role == 10))
		{
			$added_by = $_POST["ddl_user"];		
		}
		else
		{
			$added_by = $user;
		}		
		$site_number  = $_POST["stxt_site_num"];
	}
	else
	{
		$project_id      = "-1";		
		if(($role == 1) || ($role == 10))
		{
			$added_by = "";		
		}
		else
		{
			$added_by = $user;
		}
		$approval_status = "";
		$site_number     = "";
	}

	$booking_list = i_get_site_booking('',$project_id,'','','',$approval_status,$added_by,'','','','','','','','','',$site_number);
	if($booking_list["status"] == SUCCESS)
	{
		$booking_list_data = $booking_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$booking_list["data"];
	}
	
	// Get project list	$crm_user_project_mapping_search_data =  array("user_id"=>$user,"active"=>'1',"project_active"=>'1');	$project_list =  i_get_crm_user_project_mapping($crm_user_project_mapping_search_data);	if($project_list["status"] == SUCCESS)	{		$project_list_data = $project_list["data"];	}	else	{		$alert = $alert."Alert: ".$project_list["data"];		$alert_type = 0; // Failure	}
	
	// User List
	$user_list = i_get_user_list('','','','','1');
	if($user_list["status"] == SUCCESS)
	{
		$user_list_data = $user_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$user_list["data"];
		$alert_type = 0; // Failure
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>All Bookings</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>
    

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Booking - Approved List</h3>
            </div>
			<div class="widget-header" style="height:80px; padding-top:10px;">               
			  <form method="post" id="all_booked_list" action="crm_all_booking_list.php">			  		  
			  <span style="padding-left:8px; padding-right:8px;">
			  <select name="ddl_project">
			  <option value="">- - Select Project - -</option>
			  <?php
				for($count = 0; $count < count($project_list_data); $count++)
				{
					?>
					<option value="<?php echo $project_list_data[$count]["project_id"]; ?>" <?php 
					if($project_id == $project_list_data[$count]["project_id"])
					{
					?>					
					selected="selected"
					<?php
					}?>><?php echo $project_list_data[$count]["project_name"]; ?></option>								
					<?php					
				}
      		  ?>
			  </select>
			  </span>
			  <span style="padding-left:8px; padding-right:8px;">
			  <input type="text" name="stxt_site_num" value="<?php echo $site_number; ?>" placeholder="Site Number" />
			  </span>
			  <span style="padding-left:8px; padding-right:8px;">
			  <select name="ddl_approval_status">
			  <option value="">- - Select Approval Status - -</option>
			  <option value="0" <?php if($approval_status == "0") {?> selected="selected" <?php } ?>>Pending</option>
			  <option value="1" <?php if($approval_status == "1") {?> selected="selected" <?php } ?>>Approved</option>
			  <option value="2" <?php if($approval_status == "2") {?> selected="selected" <?php } ?>>Rejected</option>
			  </select>
			  </span>
			  <?php if(($role == 1) || ($role == 5))
			  {
			  ?>
			  <span style="padding-left:8px; padding-right:8px;">
			  <select name="ddl_user">
			  <option value="">- - Select STM - -</option>
			  <?php
				for($count = 0; $count < count($user_list_data); $count++)
				{
					if(($user_list_data[$count]["user_role"] == "1") || ($user_list_data[$count]["user_role"] == "5") || ($user_list_data[$count]["user_role"] == "6") || ($user_list_data[$count]["user_role"] == "7") ||($user_list_data[$count]["user_role"] == "8"))
					{
					?>
					<option value="<?php echo $user_list_data[$count]["user_id"]; ?>" <?php 
					if($added_by == $user_list_data[$count]["user_id"])
					{
					?>												
					selected="selected"
					<?php
					}?>><?php echo $user_list_data[$count]["user_name"]; ?></option>								
					<?php
					}
				}
      		  ?>
			  </select>
			  </span>
			  <?php
			  }
			  ?>
			  <span style="padding-left:8px; padding-right:8px;">
			  <input type="submit" name="search_all_booking" />
			  </span>
			  </form>			  
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
			
              <table class="table table-bordered">
                <thead>
                  <tr>
					<th>SL No</th>
					<th>Enquiry No</th>
					<th>Project</th>
					<th>Site No.</th>					
					<th>Booked By</th>
					<th>Booked On</th>
					<th>Client Name</th>
					<th>Amount</th>
					<th>Rate per sq. ft</th>
					<th>Status</th>
					<th>Remarks</th>									
				</tr>
				</thead>
				<tbody>							
				<?php						
				if($booking_list["status"] == SUCCESS)
				{										
					$sl_no = 0;
					for($count = 0; $count < count($booking_list_data); $count++)
					{										
						$sl_no++;						
					?>
					<tr>
					<td><?php echo $sl_no; ?></td>
					<td><?php echo $booking_list_data[$count]["enquiry_number"]; ?></td>
					<td><?php echo $booking_list_data[$count]["project_name"]; ?></td>
					<td><?php echo $booking_list_data[$count]["crm_site_no"]; ?></td>					
					<td><?php echo $booking_list_data[$count]["user_name"]; ?></td>										
					<td><?php echo date("d-M-Y",strtotime($booking_list_data[$count]["crm_booking_added_on"])); ?></td>	
					<td><?php echo $booking_list_data[$count]["name"]; ?></td>		
					<td><?php echo $booking_list_data[$count]["crm_booking_initial_amount"]; ?></td>
					<td><?php echo $booking_list_data[$count]["crm_booking_rate_per_sq_ft"]; ?></td>					
					<td><?php switch($booking_list_data[$count]["crm_booking_status"])
					{
					case 0:
					echo "Pending Approval";
					break;
					
					case 1:
					echo "Approved";
					break;
					
					case 2:
					echo "Rejected";
					break;
					
					case 3:
					echo "Cancelled";
					break;
					
					default:
					echo "Pending Approval";
					break;
					}?></td>
					<td><?php echo $booking_list_data[$count]["crm_booking_remarks"]; ?></td>					
					</tr>
					<?php
					}
				}
				else
				{
				?>
				<td colspan="11">No site booked yet!</td>
				<?php
				}
				 ?>	

                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>/* Open the sidenav */function openNav() {    document.getElementById("mySidenav").style.width = "75%";}/* Close/hide the sidenav */function closeNav() {    document.getElementById("mySidenav").style.width = "0";}</script>

  </body>

</html>