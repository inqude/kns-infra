<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: task_list.php
CREATED ON	: 28-July-2015
CREATED BY	: Nitin Kashyap
PURPOSE     : Detailed Report of files
*/

/*
TBD: 
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'tasks'.DIRECTORY_SEPARATOR.'task_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'process'.DIRECTORY_SEPARATOR.'process_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'files'.DIRECTORY_SEPARATOR.'file_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');

if(isset($_GET["file"]))
{
	$file = $_GET["file"];
}
else
{
	echo "Please choose a file";
}

// Get file details
$file_details = i_get_file_details($file);

// Get file payment details
$file_payment_list = i_get_file_payment_list($file,'');
if($file_payment_list["status"] == SUCCESS)
{
	$file_paid = 0;
	for($count = 0; $count < count($file_payment_list["data"]); $count++)
	{
		$file_paid = $file_paid + $file_payment_list["data"][$count]["file_payment_amount"];
	}
}
else
{
	$file_paid = 0;
}

// Get process list and details
$process_list = i_get_legal_process_plan_list('',$file,'','','','','','','','','');

$process_incomplete = 0;
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Report</title>
	<script type="text/javascript">
        function PrintWindow()
        {                     
           window.print();            
           CheckWindowState(); 
        }
        
        function CheckWindowState()
        {            
            if(document.readyState=="complete")
            {
                window.close();  
            }
            else
            {            
                setTimeout("CheckWindowState()", 2000)
            }
        }    
        
       PrintWindow();
	</script>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>
    
<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <!-- /widget-header -->
            <div class="widget-content">
			<div style="padding-left:10px; padding-top:25px; padding-bottom:25px;">			
			<?php if($file_details["status"] == SUCCESS)
			{
			?>
			<table class="table">
			<tr>
			<td>File Number</td>
			<td><strong><?php echo $file_details["data"]["file_number"]; ?></strong></td>
			<td>Survey Number</td>
			<td><strong><?php echo $file_details["data"]["file_survey_number"]; ?></strong></td>
			<td>Village</td>
			<td><strong><?php echo $file_details["data"]["file_village"]; ?></strong></td>
			</tr>
			<tr>
			<td>Party Name</td>
			<td><strong><?php echo $file_details["data"]["file_land_owner"]; ?></strong></td>
			<td>Extent</td>
			<td><strong><?php echo $file_details["data"]["file_extent"]; ?> guntas</strong></td>
			<td>Start Date</td>
			<td><strong><?php echo date("d-M-Y",strtotime($file_details["data"]["file_start_date"])); ?></strong></td>
			</tr>
			<tr>
			<td>Total Amount</td>
			<td><strong><?php echo get_formatted_amount($file_details["data"]["file_deal_amount"],"INDIA"); ?></strong></td>
			<td>Paid Amount</td>
			<td><strong><?php echo get_formatted_amount($file_paid,"INDIA"); ?></strong></td>
			<td>Balance Amount</td>
			<td><strong><?php echo get_formatted_amount($file_details["data"]["file_deal_amount"] - $file_paid,"INDIA"); ?></strong></td>
			</tr>
			</table>			
			<?php
			}
			?>
			</div>			
			<?php
			if($process_list["status"] == SUCCESS)
			{
				for($count = 0; $count < count($process_list["data"]); $count++)
				{
				// Get start date and actual end date
				$task_list = i_get_task_plan_list('','',$process_list["data"][$count]["process_plan_legal_id"],'','','1','','slno');
				if($task_list["status"] == SUCCESS)
				{
					$end_date = "1969-12-31";
					$process_lead_time = 0;
					for($task_count = 0;$task_count < count($task_list["data"]); $task_count++)
					{
						// Latest End Date
						if($end_date < $task_list["data"][$task_count]["task_plan_actual_end_date"])
						{
							$end_date = $task_list["data"][$task_count]["task_plan_actual_end_date"];
						}												
						
						// Biggest Lead Time
						if($process_lead_time < $task_list["data"][$task_count]["task_type_maximum_expected_duration"])
						{
							$process_lead_time = $task_list["data"][$task_count]["task_type_maximum_expected_duration"];
						}
						
						// If task is not completed
						if($task_list["data"][$task_count]["task_plan_actual_end_date"] == "0000-00-00")
						{
							$process_incomplete = 1;
						}
					}					
				}
				else
				{					
					$end_date   = "0000-00-00";
						
					$actual_date = $end_date;
				}
				
				if($process_incomplete == 1)
				{
					$end_date = date("Y-m-d");
				}
				$start_date = $process_list["data"][$count]["process_plan_legal_start_date"];
				?>
				  
				  <table class="table table-bordered">
					<thead>
					  <tr>
						<th style="font-size:14px; color:green;">Process: <?php echo $process_list["data"][$count]["process_name"]; ?></th>
						<th style="font-size:14px; color:green;">Start Date: <?php echo date("d-M-Y",strtotime($process_list["data"][$count]["process_plan_legal_start_date"])); ?></th>
						<th style="font-size:14px; color:green;">End Date: <?php if($process_incomplete == 1)
						{
							echo "";
						}
						else
						{
							echo date("d-M-Y",strtotime($end_date));
						}?></th>							
						<th style="font-size:14px; color:green;">Lead Time: <?php $variance = get_date_diff($start_date,$end_date); echo $variance["data"]; ?></th>	
						<th style="font-size:14px; color:green;">Planned Lead Time: <?php echo $process_lead_time; ?></th>
					  </tr>
					</thead>
					<tbody>
					  <tr>
						<td><strong>Task</strong></td>
						<td><strong>Start Date</strong></td>
						<td><strong>End Date</strong></td>	
						<td><strong>Lead Time</strong></td>	
						<td><strong>Planned Lead Time</strong></td>	
					  </tr>
				  <?php				  
				  for($task_count = 0;$task_count < count($task_list["data"]); $task_count++)
				  {
				  ?>					  
					  <tr>
					  <td><?php echo $task_list["data"][$task_count]["task_type_name"]; ?></td>
					  <td><?php echo date("d-M-Y",strtotime($task_list["data"][$task_count]["task_plan_actual_start_date"])); ?></td>
					  <td><?php if(($task_list["data"][$task_count]["task_plan_actual_end_date"] == "0000-00-00") || ($task_list["data"][$task_count]["task_plan_actual_end_date"] == "1970-01-01") || ($task_list["data"][$task_count]["task_plan_actual_end_date"] == "1969-12-31"))
					  {
						$task_end_date = "";
					  }
					  else
					  {
						$task_end_date = date("d-M-Y",strtotime($task_list["data"][$task_count]["task_plan_actual_end_date"]));
					  }
					  echo $task_end_date; ?></td>
					  <td><?php if(($task_list["data"][$task_count]["task_plan_actual_end_date"] == "0000-00-00") || ($task_list["data"][$task_count]["task_plan_actual_end_date"] == "1970-01-01") || ($task_list["data"][$task_count]["task_plan_actual_end_date"] == "1969-12-31"))
					  {
						$task_end_date = date("Y-m-d");
					  }
					  else
					  {
						$task_end_date = $task_list["data"][$task_count]["task_plan_actual_end_date"];
					  }
					  $variance = get_date_diff($task_list["data"][$task_count]["task_plan_actual_start_date"],$task_end_date); echo $variance["data"];
					  ?></td>
					  <td><?php echo $task_list["data"][$task_count]["task_type_maximum_expected_duration"] ?></td>
					  </tr>
				  <?php
				  }
				  ?>
					</tbody>
				  </table>
				  <br /><br />
				<?php
				}
			}
			?>	
			<span style="padding-left:15px;"><strong>PAYMENT DETAILS</strong></span><br /><br />
			<table class="table table-bordered">
				<thead>
				  <tr>
					<th style="font-size:14px;">Date</th>
					<th style="font-size:14px;">Amount</th>
					<th style="font-size:14px;">Done To</th>	
					<th style="font-size:14px;">Remarks</th>	
				  </tr>
				</thead>
				<tbody>
			<?php
			if($file_payment_list["status"] == SUCCESS)
			{
			?>			
				<?php 
				for($count = 0; $count < count($file_payment_list["data"]); $count++)
				{
				?>
				  <tr>
					<td><?php echo date("d-M-Y",strtotime($file_payment_list["data"][$count]["file_payment_date"])); ?></td>
					<td><?php echo $file_payment_list["data"][$count]["file_payment_amount"]; ?></td>
					<td><?php echo $file_payment_list["data"][$count]["file_payment_done_to"]; ?></td>	
					<td><?php echo $file_payment_list["data"][$count]["file_payment_remarks"]; ?></td>							
				  </tr>
				<?php
				}
				?>				
			<?php
			}
			else
			{
			?>
			<tr>
			<td colspan="4">No Payment done</td>
			</tr>
			<?php
			}
			?>
			</tbody>
			</table>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>

<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>