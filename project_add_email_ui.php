<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 24-Aug-2017
// LAST UPDATED BY: Lakshmi
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];


include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_masters'.DIRECTORY_SEPARATOR.'crm_masters_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');


if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */

	if(isset($_POST["project_id"]))
	{
		$project_id = $_POST["project_id"];
	}
	else {
			$project_id = $_REQUEST["project_id"];
	}


	// Capture the form data
	if(isset($_POST["add_user_project_mapping_submit"]))
	{
		$user_id       = $_POST["ddl_user_id"];
		$project_id    = $_POST["ddl_project_id"];
		$remarks       = $_POST["txt_remarks"];

		// Check for mandatory fields
		if(($user_id != "") && ($project_id != ""))
		{
			$user_project_mapping_iresult =  i_add_crm_user_project_mapping($user_id,$project_id,$remarks,$user);

			if($user_project_mapping_iresult["status"] == SUCCESS)
			{

				header("location:crm_user_project_mapping_list.php");
			}
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}

	// Get Users modes already added
	$user_list = i_get_user_list('','','','','1');
	if($user_list['status'] == SUCCESS)
	{
		$user_list_data = $user_list['data'];
	}
	else
	{
		$alert = $user_list["data"];
		$alert_type = 0;
	}

  // Project data
  $project_management_master_search_data = array("active"=>'1', "user_id"=>$user);
  $project_management_master_list = i_get_project_management_master_list($project_management_master_search_data);
  if($project_management_master_list["status"] == SUCCESS)
  {
    $project_management_master_list_data = $project_management_master_list["data"];
  }
  else
  {
    $alert = $alert."Alert: ".$project_management_master_list["data"];
  }
}
else
{
	header("location:login.php");
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
	<style type="text/css">
    .btn-group.open .dropdown-menu {
        max-height: 200px !important;
    }
</style>

    <meta charset="utf-8">
    <title>User - Add  Project Mappin</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">

    <link href="css/style.css" rel="stylesheet">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css" />



    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">

	<div class="main-inner">

	    <div class="container">

	      <div class="row">

	      	<div class="span12">

	      		<div class="widget ">

	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>User - Add  Project Mapping</h3>
	  				</div> <!-- /widget-header -->

					<div class="widget-content">



						<div class="tabbable">

						<br>
							<div class="control-group">
								<div class="controls">
								<?php
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>

								<?php
								if($alert_type == 1) // Success
								{
								?>
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <?php echo $alert; ?>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form method="post" id="framework_form">
									<fieldset>
                    <div class="control-group">
                        <label class="control-label" for="ddl_project_id">Project*</label>
                        <div class="controls">
                        <select name="ddl_project_id" style="max-height:100px;" id="ddl_project_id" required>
                        <?php
                        for($count = 0; $count < count($project_management_master_list_data); $count++)
                        {
                        ?>
                        <option value="<?php echo $project_management_master_list_data[$count]["project_management_master_id"]; ?>,<?php echo $project_management_master_list_data[$count]["project_master_name"]; ?>"<?php if($project_id == $project_management_master_list_data[$count]["project_management_master_id"]) { ?> selected="selected" <?php } ?>><?php echo $project_management_master_list_data[$count]["project_master_name"]; ?></option>
                        <?php
                        }
                        ?>
                        </select>
                        </div> <!-- /controls -->
                        </div> <!-- /control-group -->

                        <div class="control-group">
                          <label class="control-label" for="email_to">To</label>
                          <div class="controls">
                            <input type="email" style="width:220px;"  id="email_to" name="email_to" placeholder="Email Id">
                          </div> <!-- /controls -->
                        </div> <!-- /control-group -->

											<div class="control-group">
											<label class="control-label" for="framework">Cc*</label>
											<div class="controls">
												<select id="framework" name="framework[]" multiple class="form-control">
												<?php
												for($count = 0; $count < count($user_list_data); $count++)
												{
												?>
												<option value="<?php echo $user_list_data[$count]["user_email_id"]; ?>"><?php echo $user_list_data[$count]["user_email_id"]; ?></option>
												<?php
												}
												?>
												</select>
											</div> <!-- /controls -->
										</div> <!-- /control-group -->

                    <div class="control-group">
                    <label class="control-label" for="ddl_user_id">BCC*</label>
                    <div class="controls">
                      <select id="email_bcc" name="email_bcc[]" multiple class="form-control" >
                      <?php
                      for($count = 0; $count < count($user_list_data); $count++)
                      {
                      ?>
                      <option value="<?php echo $user_list_data[$count]["user_email_id"]; ?>"><?php echo $user_list_data[$count]["user_email_id"]; ?></option>
                      <?php
                      }
                      ?>
                      </select>
                    </div> <!-- /controls -->
                  </div> <!-- /control-group -->

										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>

							</div>

					</div> <!-- /widget-content -->

				</div> <!-- /widget -->

		    </div> <!-- /span8 -->




	      </div> <!-- /row -->

	    </div> <!-- /container -->

	</div> <!-- /main-inner -->

</div> <!-- /main -->




<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">

                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->




<div class="footer">

	<div class="footer-inner">

		<div class="container">

			<div class="row">

    			<div class="span12">
    				&copy; 2015 .
    			</div> <!-- /span12 -->

    		</div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /footer-inner -->

</div> <!-- /footer -->



<script src="js/jquery-1.7.2.min.js"></script>

<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script src="js/jquery.min1.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.2/bootstrap3-typeahead.min.js"></script>
<script src="js/bootstrap.min1.js"></script>
<script src="js/bootstrap-multiselect.js"></script>

<script>
$(document).ready(function(){
 $('#framework').multiselect({
  nonSelectedText: '--Select CC--',
  enableFiltering: true,
  enableCaseInsensitiveFiltering: true,
  buttonWidth:'220px'
 });
 $('#email_bcc').multiselect({
  nonSelectedText: '--Select BCC--',
  enableFiltering: true,
  enableCaseInsensitiveFiltering: true,
  buttonWidth:'220px'
 });
 $('#ddl_project_id').multiselect({
  nonSelectedText: '--Select Project--',
  enableFiltering: true,
  enableCaseInsensitiveFiltering: true,
  buttonWidth:'220px'
 });

 $('#framework_form').on('submit', function(event){
  event.preventDefault();
  var form_data = $(this).serialize();
  $.ajax({
   url:"project_insert_email.php",
   method:"POST",
   data:form_data,
   success:function(data)
   {
    $('#framework option:selected').each(function(){
     $(this).prop('selected', false);
    });
    $('#framework').multiselect('refresh');

		$('#email_bcc option:selected').each(function(){
     $(this).prop('selected', false);
    });
    $('#email_bcc').multiselect('refresh');
		 $("#email_to").val("");
		 $('#deselectall').click(function() {
     $('select#ddl_project_id option').removeAttr("selected");
 });
    alert(data);
   }
  });
 });

 var valArr = [101,102],
    i = 0, size = valArr.length,
    $options = $('#data option');

for(i; i < size; i++){
    $options.filter('[value="'+valArr[i]+'"]').prop('selected', true);
}
 $("#ddl_project_id").change(function() {
	 var project_data = $(this).val();
	 project_id = project_data.split(',')[0]
	 console.log('project_id ', project_id);
	 if(project_id == 0) {
		 $("#email_to").empty();
		 return false;
	 }

	 $.ajax({
		 url: 'project_get_email_list.php',
		 data: {project_id: project_id},
		 dataType: 'json',
		 success: function(response) {
			 $("#email_to").empty();
			 for(var i=0; i< response.length; i++) {
				 var cc = response[i]['project_email_details_cc'];
				 var bcc = response[i]['project_email_details_bcc'];
				 var email_to = response[i]['project_email_details_to'];
				 var cc_string = cc.split(',');
				 var bcc_string = bcc.split(',');
				 $("#email_to").val(email_to);
					cc_selected = 0, cc_size = cc_string.length;
					for(cc_selected; cc_selected < cc_size; cc_selected++){
						$("#framework").multiselect().find(":checkbox[value= '"+cc_string[cc_selected]+"']").attr("checked","checked");
					 $("#framework option[value='" +cc_string[cc_selected]+ "']").attr("selected", true);
					 $("#framework").multiselect("refresh");
					}
					bcc_selected = 0, bcc_size = bcc_string.length;
					for(bcc_selected; bcc_selected < bcc_size; bcc_selected++){
					  $("#email_bcc").multiselect().find(":checkbox[value= '"+bcc_string[bcc_selected]+"']").attr("checked","checked");
					  $("#email_bcc option[value='" +bcc_string[bcc_selected]+ "']").attr("selected", true);
					  $("#email_bcc").multiselect("refresh");
					}
			 }
		 }
	 })
 })
});
</script>
  </body>

</html>
