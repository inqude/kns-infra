<?php

/* SESSION INITIATE - START */

session_start();

/* SESSION INITIATE - END */



/* FILE HEADER - START */

// LAST UPDATED ON: 5th Sep 2015

// LAST UPDATED BY: Nitin Kashyap

/* FILE HEADER - END */



/* TBD - START */

/* TBD - END */



/* DEFINES - START */

define('QUOTATION_APP_FUNC_ID','177');

/* DEFINES - END */



/* INCLUDES - START */

$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock'.DIRECTORY_SEPARATOR.'stock_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_indent_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_quotation_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

/* INCLUDES - END */



if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))

{

	// Session Data

	$user 		   = $_SESSION["loggedin_user"];

	$role 		   = $_SESSION["loggedin_role"];

	$loggedin_name = $_SESSION["loggedin_user_name"];

	

	// Get permissions

	$add_perms_list    = i_get_user_perms($user,'',QUOTATION_APP_FUNC_ID,'1','1');	



	/* DATA INITIALIZATION - START */

	$alert_type = -1;

	$alert = "";

	/* DATA INITIALIZATION - END */

	/* QUERY STRING - START */

	if(isset($_REQUEST["indent_id"]))

	{

		$indent_id = $_REQUEST["indent_id"];

	}

	else

	{

		$indent_id = "";

	}

	if(isset($_POST["status"]))

	{

		$indent_status = $_POST["status"];

	}

	else

	{

		$indent_status = "";

	}

	if(isset($_POST["ii_search_submit"]))

	{

		$project      = $_POST["ddl_project"];

	}

	else

	{

		$project      = '-1';

	}

	//Get Uom List

	$stock_unit_search_data = array();

	$uom_list = i_get_stock_unit_measure_list($stock_unit_search_data);

	if($uom_list["status"] == SUCCESS)

	{

		$uom_list_data = $uom_list["data"];

	}

	else

	{

		$alert = $uom_list["data"];

		$alert_type = 0;

	}

	

	// Get Material Details

	$stock_material_search_data = array();

	$material_list = i_get_stock_material_master_list($stock_material_search_data);

	if($material_list["status"] == SUCCESS)

	{

		$material_list_data = $material_list["data"];

	}

	else

	{

		$alert = $material_list["data"];

		$alert_type = 0;

	}

	

	// Get Indent Item Details		

	$stock_indent_search_data = array("status"=>'Approved',"active"=>'1','approved_on'=>'1','project'=>$project);	

	$indent_item_list = i_get_indent_sum_of_items_list($stock_indent_search_data);		

	

	if($indent_item_list["status"] == SUCCESS)

	{

		$indent_item_list_data = $indent_item_list["data"];

	}

	else

	{

		$alert = $alert."Alert: ".$indent_item_list["data"];

	}

	

	//Get Indent List

	$stock_indent_search_data = array("indent_id"=>$indent_id);

	$indent_list = i_get_stock_indent_list($stock_indent_search_data);

	if($indent_list["status"] == SUCCESS)

	{

		$indent_list_data = $indent_list["data"];

	}

	else

	{

		$alert = $indent_list["data"];

		$alert_type = 0;

	}

	

	// Get Project List
	
	$stock_project_search_data = array();
	
	$project_result_list = i_get_project_list($stock_project_search_data);
	
	if($project_result_list['status'] == SUCCESS)
		
	{
		
		$project_result_list_data = $project_result_list['data'];		
		
	}	
	
	else
		
	{
		
		$alert = $project_result_list["data"];
		
		$alert_type = 0;		
		
	}
}

else

{

	header("location:login.php");

}	

?>



<!DOCTYPE html>

<html lang="en">

  

<head>

    <meta charset="utf-8">

    <title>Quotation Process</title>

    

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <meta name="apple-mobile-web-app-capable" content="yes">    

    

    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">

    

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">

    <link href="css/font-awesome.css" rel="stylesheet">

    

    <link href="css/style.css" rel="stylesheet">

   





    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->

    <!--[if lt IE 9]>

      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>

    <![endif]-->



  </head>



<body>



<?php

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');

?>    



<div class="main">

	

	<div class="main-inner">



	    <div class="container">

	

	      <div class="row">

	      	

	      	<div class="span12">      		

	      		

	      		<div class="widget ">

	      		

					

					<div class="widget-content">

						

	

								

								

			 <div class="widget widget-table action-table">

            <div class="widget-header"> <i class="icon-th-list"></i>

              <h3>Quotation process</h3>

            </div>

			<div class="widget-header" style="height:50px; padding-top:10px;">               

			  <form method="post" id="file_search_form" action="stock_quotation_for_approval.php">

			  <span style="padding-right:20px;">
			  
			  <select name="ddl_project">
			  
				<option value="">- - Select Project - -</option>
				
				<?php
				
				for($count = 0; $count < count($project_result_list_data); $count++)
					
				{
				?>
				
				<option value="<?php echo $project_result_list_data[$count]["stock_project_id"]; ?>"<?php if($project_result_list_data[$count]["stock_project_id"] == $project){?> selected="selected" <?php } ?>><?php echo $project_result_list_data[$count]["stock_project_name"]; ?></option>
				
				<?php
				
				}
				
				?>
				
			  </select>
			  
			  </span>

			<input type="submit" name="ii_search_submit" />

			  </form>			  

            </div>

            <!-- /widget-header -->

            <div class="widget-content">

			<form action="task_list.php" method="post" id="task_update_form">	

			<input type="hidden" name="hd_indent_id" value="<?php echo $indent_id; ?>" />			

			<input type="hidden" name="indent_status" value="<?php echo $indent_status; ?>" />			

			

              <table class="table table-bordered">

                <thead>

                  <tr>

					<th>Sl No</th>

					<th>Material</th>

					<th>Material Code</th>										<th>Project</th>

					<th>Total Indent qty</th>

					<th>Stock Qty</th>

					<th>Status</th>

					<th>Requested By</th>

					<th colspan="3" style="text-align:center;">Actions</th>	

				</tr>

				</thead>

				<tbody>

				 <?php

				 if($indent_item_list["status"] == SUCCESS)

				 {

					 $sl_no = 0;

						for($count = 0; $count < count($indent_item_list_data); $count++)

						{		

							// Get reset date

							$stock_quote_reset_search_data = array();

							$quote_reset_data = i_get_stock_quote_reset($stock_quote_reset_search_data);

					

							// Get Stock list

							$sl_no++;

							$stock_quotation_compare_search_data = array("indent_id"=>$indent_item_list_data[$count]["stock_indent_item_material_id"],"status"=>'Waiting',"location"=>$location);

							$quotation_list = i_get_stock_quotation_compare_list($stock_quotation_compare_search_data);

							

							if($quotation_list["status"] == SUCCESS)

							{

								$quotation_list_data = $quotation_list["data"];

								$quotation_status = $quotation_list_data[0]["stock_quotation_status"];

							}

							else

							{

								$quotation_status = "";

							}

							$material_stock_search_data = array("material_id"=>$indent_item_list_data[$count]["stock_indent_item_material_id"]);

							$stock_material = i_get_material_stock($material_stock_search_data);

							if($stock_material["status"] == SUCCESS)

							{

								$stock_material_data = $stock_material["data"];

								$qunatity = $stock_material_data[0]["material_stock_quantity"];

								$material = $stock_material_data[0]["material_id"];

							}

							else

							{

								$alert = $alert."Alert: ".$stock_material["data"];

								$qunatity = "0";

								

							}

							// Get Stock Issue List

							$issued_qty ="0";

							$stock_issue_search_data = array("indent_item_id"=>$indent_item_list_data[$count]["stock_indent_item_id"],"indent_id"=>$indent_id);

							$stock_issue_list = i_get_stock_issue($stock_issue_search_data);

							if($stock_issue_list["status"] ==  SUCCESS)

							{

								for($qty_count = 0 ; $qty_count < count($stock_issue_list["data"]) ; $qty_count++)

								{

								$stock_issue_list_data = $stock_issue_list["data"];

								$issued_qty = $issued_qty + $stock_issue_list_data[$qty_count]["stock_issue_qty"];

								}

							}

							else

							{

								$issued_qty = "0";

							}

							// Get Returnable Type from material master

							$stock_material_search_data = array("material_id"=>$indent_item_list_data[$count]["stock_indent_item_material_id"]);

							$material_master_list = i_get_stock_material_master_list($stock_material_search_data);

							if($material_master_list["status"] == SUCCESS)

							{

								$returnable_type = $material_master_list["data"][0]["stock_material_type"];

							}

							

						?>				

						<tr>

							<?php							

							if(($quotation_status != ""))

							{ ?>

							<td><?php echo $sl_no; ?></td>

							<td><?php echo $indent_item_list_data[$count]["stock_material_name"]; ?></td>

							<td><?php echo $indent_item_list_data[$count]["stock_material_code"]; ?></td>														<td><?php echo $indent_item_list_data[$count]["stock_project_name"]; ?></td>

							<td><?php echo $required_qunatity = $indent_item_list_data[$count]["total_stock_indent_item_quantity"]; ?></td>

							<td <?php if( $qunatity > $required_qunatity ) { ?> style="color:#00FF00;" <?php } else { ?> style="color:#FF0000;" <?php } ?>><strong><?php echo $qunatity ; ?></strong></td>

							<td> &nbsp;   </td>

							<td> <?php echo $indent_item_list_data[$count]["user_name"]; ?></td>														

							<td><a href="stock_master_vendor_item_mapping_list.php?indent_id=<?php echo $indent_item_list_data[$count]["stock_indent_id"]; ?>&indent_item_id=<?php echo $indent_item_list_data[$count]["stock_indent_item_material_id"]; ?>" target="_blank">View Vendors</a></td>

							<td><a href="stock_add_quotation_compare.php?indent_id=<?php echo $indent_item_list_data[$count]["stock_indent_id"]; ?>&indent_item_id=<?php echo $indent_item_list_data[$count]["stock_indent_item_material_id"];?>&project=<?php echo $indent_item_list_data[$count]["stock_indent_project"];; ?>">Add Quotations</a></td>							

							<td><a href="stock_quotation_compare_list.php?indent_item_id=<?php echo $indent_item_list_data[$count]["stock_indent_item_material_id"];?>&project=<?php echo $indent_item_list_data[$count]["stock_indent_project"]; ?>">View Quotations</a></td>

							<?php

							}

							?>

							

						</tr>

						<?php 		

					}

				}

				else

				{

				?>

				<td colspan="9">No Items added</td>

				<?php

				}

				 ?>	



                </tbody>

				</table>

				<br/>		

				<div class="modal-body">

			    <div class="row">

				  </div>

				  </div>			  

			</form>

            </div>

            <!-- /widget-content --> 

          </div>

								</div> <!-- /controls -->	                                                

							</div> <!-- /control-group -->

  

						  

					</div> <!-- /widget-content -->

						

				</div> <!-- /widget -->

	      		

		    </div> <!-- /span8 -->

	      	

	      </div> <!-- /row -->

	

	    </div> <!-- /container -->

	    

	</div> <!-- /main-inner -->

    

</div> <!-- /main -->

 

<div class="extra">



	<div class="extra-inner">



		<div class="container">



			<div class="row">

                    

                </div> <!-- /row -->



		</div> <!-- /container -->



	</div> <!-- /extra-inner -->



</div> <!-- /extra -->  

    

<div class="footer">

	

	<div class="footer-inner">

		

		<div class="container">

			

			<div class="row">

				

    			<div class="span12">

    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.

    			</div> <!-- /span12 -->

    			

    		</div> <!-- /row -->

    		

		</div> <!-- /container -->

		

	</div> <!-- /footer-inner -->

	

</div> <!-- /footer -->

    





<script src="js/jquery-1.7.2.min.js"></script>

	

<script src="js/bootstrap.js"></script>

<script src="js/base.js"></script>

<script>

function get_products(count,material)

{			

	product_type = document.getElementById("ddl_material_type").value;			

	if (window.XMLHttpRequest)

	{// code for IE7+, Firefox, Chrome, Opera, Safari

		xmlhttp = new XMLHttpRequest();

	}

	else

	{// code for IE6, IE5

		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");

	}



	xmlhttp.onreadystatechange = function()

	{				

		if (xmlhttp.readyState == 4 && xmlhttp.status == 200)

		{																	

			document.getElementById("product_area").innerHTML = xmlhttp.responseText;					

		}

	}			

	

	xmlhttp.open("POST", "select_material.php");   // file name where delete code is written

	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

	xmlhttp.send("product_type=" + product_type);

}



function go_to_issue_item(indent_id,indent_item_id,returnable_type)

{		

	var form = document.createElement("form");

    form.setAttribute("method", "post");

    form.setAttribute("action", "stock_add_issue.php");

	

	var hiddenField2 = document.createElement("input");

	hiddenField2.setAttribute("type","hidden");

	hiddenField2.setAttribute("name","indent_id");

	hiddenField2.setAttribute("value",indent_id);

	

	var hiddenField3 = document.createElement("input");

	hiddenField3.setAttribute("type","hidden");

	hiddenField3.setAttribute("name","indent_item_id");

	hiddenField3.setAttribute("value",indent_item_id);

	

	var hiddenField4 = document.createElement("input");

	hiddenField4.setAttribute("type","hidden");

	hiddenField4.setAttribute("name","returnable_type");

	hiddenField4.setAttribute("value",returnable_type);

    	

	form.appendChild(hiddenField2);	

	form.appendChild(hiddenField3);	

	form.appendChild(hiddenField4);	

	

	document.body.appendChild(form);

    form.submit();

}

function go_to_view_quotation(indent_id,indent_item_id)

{		

	var form = document.createElement("form");

    form.setAttribute("method", "post");

    form.setAttribute("action", "stock_quotation_compare_list.php");

	

	var hiddenField2 = document.createElement("input");

	hiddenField2.setAttribute("type","hidden");

	hiddenField2.setAttribute("name","indent_id");

	hiddenField2.setAttribute("value",indent_id);

	

	var hiddenField3 = document.createElement("input");

	hiddenField3.setAttribute("type","hidden");

	hiddenField3.setAttribute("name","indent_item_id");

	hiddenField3.setAttribute("value",indent_item_id);

    	

	form.appendChild(hiddenField2);	

	form.appendChild(hiddenField3);	

	

	document.body.appendChild(form);

    form.submit();

}

function go_to_view_vendor(indent_id,indent_item_id)

{		

	var form = document.createElement("form");

    form.setAttribute("method", "post");

    form.setAttribute("action", "stock_vendor_item_mapping_list.php");

	

	var hiddenField2 = document.createElement("input");

	hiddenField2.setAttribute("type","hidden");

	hiddenField2.setAttribute("name","indent_id");

	hiddenField2.setAttribute("value",indent_id);

	

	var hiddenField3 = document.createElement("input");

	hiddenField3.setAttribute("type","hidden");

	hiddenField3.setAttribute("name","indent_item_id");

	hiddenField3.setAttribute("value",indent_item_id);

    	

	form.appendChild(hiddenField2);	

	form.appendChild(hiddenField3);	

	

	document.body.appendChild(form);

    form.submit();

}function go_to_quotation(indent_id,indent_item_id)

{		

	var form = document.createElement("form");

    form.setAttribute("method", "post");

    form.setAttribute("action", "stock_add_quotation_compare.php");

	

	var hiddenField2 = document.createElement("input");

	hiddenField2.setAttribute("type","hidden");

	hiddenField2.setAttribute("name","indent_id");

	hiddenField2.setAttribute("value",indent_id);

	

	var hiddenField3 = document.createElement("input");

	hiddenField3.setAttribute("type","hidden");

	hiddenField3.setAttribute("name","indent_item_id");

	hiddenField3.setAttribute("value",indent_item_id);

    	

	form.appendChild(hiddenField2);	

	form.appendChild(hiddenField3);	

	

	document.body.appendChild(form);

    form.submit();

}

function delete_indent_item(indent_item_id,indent_id)

{

	var ok = confirm("Are you sure you want to Delete?")

	{         

		if (ok)

		{



			if (window.XMLHttpRequest)

			{// code for IE7+, Firefox, Chrome, Opera, Safari

				xmlhttp = new XMLHttpRequest();

			}

			else

			{// code for IE6, IE5

				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");

			}



			xmlhttp.onreadystatechange = function()

			{

				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)

				{

					if(xmlhttp.responseText != "SUCCESS")

					{

					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;

					 document.getElementById("span_msg").style.color = "red";

					}

					else					

					{

						var url = "http://<?php echo HOST; ?>/kns/Legal/stock_indent_items_approved.php";

						window.location = url;

					}

				}

			}



			xmlhttp.open("POST", "delete_indent_item.php");   // file name where delete code is written

			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

			xmlhttp.send("indent_item_id=" + indent_item_id + "indent_id=" + indent_id + "&action=0");

		}

	}	

}

</script>

</body>

</html>

