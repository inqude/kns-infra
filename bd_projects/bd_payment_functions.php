<?php

/**

 * @author Nitin kashyap

 * @copyright 2015

 */



$base = $_SERVER["DOCUMENT_ROOT"];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'status_codes.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'da_bd_payments.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_projects'.DIRECTORY_SEPARATOR.'bd_project_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_projects'.DIRECTORY_SEPARATOR.'bd_config.php');



/*

PURPOSE : To add BD payment request

INPUT 	: File ID, Amount, Reason, Remarks, Payable To, Added By

OUTPUT 	: Message, success or failure message

BY 		: Punith

*/

function i_add_bd_payment_request($file_id,$amount,$reason,$remarks,$payable_to,$added_by)

{

	/* Check if the planned amount is being exceeded- Start */

	$planned_amount          = 0;

	$approved_amount         = 0;

	$approval_pending_amount = 0;

	

	// Get planned amount

	$planned_payment_sresult = i_get_bd_files_list($file_id,'','','','','','','');

	if($planned_payment_sresult['status'] == SUCCESS)

	{

		$planned_amount = $planned_amount + $planned_payment_sresult['data'][0]['bd_file_land_cost'];

	}

	else

	{

		// Invalid file

		$planned_amount = 0;

	}

	

	// Get already approved amount for this file

	$payment_request_data = array('file_id'=>$file_id,'status'=>'1');

	$approved_payment_sresult = db_get_bd_payment_request($payment_request_data);

	if($approved_payment_sresult['status'] == DB_RECORD_ALREADY_EXISTS)

	{

		for($count = 0; $count < count($approved_payment_sresult['data']); $count++)

		{

			$approved_amount = $approved_amount + $approved_payment_sresult['data'][$count]['bd_payment_requests_amount'];

		}

	}

	

	// Get approval pending amount for this file

	$payment_request_data = array('file_id'=>$file_id,'status'=>'0');

	$app_pending_payment_sresult = db_get_bd_payment_request($payment_request_data);	

	if($app_pending_payment_sresult['status'] == DB_RECORD_ALREADY_EXISTS)

	{

		for($count = 0; $count < count($app_pending_payment_sresult['data']); $count++)

		{

			$approval_pending_amount = $approval_pending_amount + $app_pending_payment_sresult['data'][$count]['bd_payment_requests_amount'];

		}

	}

	

	/* Check if the planned amount is being exceeded- End */

	if($planned_amount >= ($approved_amount + $approval_pending_amount + $amount))

	{

		$bd_payment_iresult = db_add_bd_payment_request($file_id,$amount,$reason,$remarks,$payable_to,$added_by);

		

		if($bd_payment_iresult['status'] == SUCCESS)

		{

			$return["data"]   = $bd_payment_iresult['data'];

			$return["status"] = SUCCESS;							

		}

		else

		{

			$return["data"]   = "Internal Error. Please try again later";

			$return["status"] = FAILURE;

		}

	}

	else

	{

		$return["data"]   = "Total amount request exceeds total planned amount for this file";

		$return["status"] = FAILURE;

	}

		

	return $return;

}



/*

PURPOSE : To get BD payment request list

INPUT 	: Payment Request data: File ID, Amount, Reason, Status, Added By, Start Date, End Date

OUTPUT 	: BD payment List or Error Details, success or failure message

BY 		: Punith

*/

function i_get_bd_payment_list($payment_request_data)

{

	$bd_payment_sresult = db_get_bd_payment_request($payment_request_data);

	

	if($bd_payment_sresult['status'] == DB_RECORD_ALREADY_EXISTS)

    {

		$return["status"] = SUCCESS;

        $return["data"]   = $bd_payment_sresult["data"]; 

    }

    else

    {

	    $return["status"] = FAILURE;

        $return["data"]   = "No payments added yet!"; 

    }

	

	return $return;

}



/*

PURPOSE : To update BD payment

INPUT 	: Payment Request ID, Payment Request Details for updating

OUTPUT 	: Message, success or failure message

BY 		: Nitin Kashyap

*/

function i_update_bd_payment($bd_payment_request_id,$bd_payment_request_data)

{

	$bd_payment_uresult = db_update_bd_payment_request($bd_payment_request_id,$bd_payment_request_data);

	

	if($bd_payment_uresult['status'] == SUCCESS)

	{

		$return["data"]   = "BD payment Successfully updated";

		$return["status"] = SUCCESS;							

	}

	else

	{

		$return["data"]   = "Internal Error. Please try again later";

		$return["status"] = FAILURE;

	}

		

	return $return;

}



/*

PURPOSE : To add BD payment history

INPUT 	: Payment History data: Request ID, Status, Remarks, Added By

OUTPUT 	: BD History ID

BY 		: Nitin

*/

function i_add_bd_payment_history($request_id,$status,$remarks,$added_by)

{

	$bd_payment_history_iresult = db_add_bd_payment_request_history($request_id,$status,$remarks,$added_by);

	

	if($bd_payment_history_iresult['status'] == SUCCESS)

    {

		$return["status"] = SUCCESS;

        $return["data"]   = $bd_payment_history_iresult["data"]; 

    }

    else

    {

	    $return["status"] = FAILURE;

        $return["data"]   = "Internal Error. Please try later"; 

    }

	

	return $return;

}



/*

PURPOSE : To get BD payment history list

INPUT 	: Payment Request data: File ID, Amount, Reason, Status, Added By, Start Date, End Date

OUTPUT 	: BD payment List or Error Details, success or failure message

BY 		: Punith

*/

function i_get_bd_payment_history_list($pay_history_data)

{

	$bd_payment_history_sresult = db_get_bd_payment_request_history($pay_history_data);

	

	if($bd_payment_history_sresult['status'] == DB_RECORD_ALREADY_EXISTS)

    {

		$return["status"] = SUCCESS;

        $return["data"]   = $bd_payment_history_sresult["data"]; 

    }

    else

    {

	    $return["status"] = FAILURE;

        $return["data"]   = "No payment updates added yet!"; 

    }

	

	return $return;

}



/*

PURPOSE : To add BD borrow payment request

INPUT 	: borrow ID, Amount, Reason, Remarks, Payable To, Added By

OUTPUT 	: Message, success or failure message

BY 		: Sonakshi

*/

function i_add_bd_borrow_payment_request($borrow_id,$amount,$reason,$remarks,$payable_to,$added_by)

{

	$bd_borrow_payment_iresult = db_add_bd_borrow_payment_request($borrow_id,$amount,$reason,$remarks,$payable_to,$added_by);

	

	if($bd_borrow_payment_iresult['status'] == SUCCESS)

	{

		$return["data"]   = $bd_borrow_payment_iresult['data'];

		$return["status"] = SUCCESS;							

	}

	else

	{

		$return["data"]   = "Internal Error. Please try again later";

		$return["status"] = FAILURE;

	}

	

	return $return;

}



/*

PURPOSE : To get borrow BD payment request list

INPUT 	: Payment Request data: borrow ID, Amount, Reason, Status, Added By, Start Date, End Date

OUTPUT 	: BD payment List or Error Details, success or failure message

BY 		: Sonakshi

*/

function i_get_borrow_bd_payment_list($payment_request_data)

{

	$bd_borrow_payment_sresult = db_get_bd_borrow_payment_request($payment_request_data);

	

	if($bd_borrow_payment_sresult['status'] == DB_RECORD_ALREADY_EXISTS)

    {

		$return["status"] = SUCCESS;

        $return["data"]   = $bd_borrow_payment_sresult["data"]; 

    }

    else

    {

	    $return["status"] = FAILURE;

        $return["data"]   = "No payments added yet!"; 

    }

	

	return $return;

}



?>