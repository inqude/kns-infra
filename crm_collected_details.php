<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: crm_approved_booking_list.php
CREATED ON	: 04-Nov-2015
CREATED BY	: Nitin Kashyap
PURPOSE     : Approved Booking List
*/

/*
TBD: 
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_post_sales_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_sales_process.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_projects'.DIRECTORY_SEPARATOR.'crm_project_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Query String Data
	if(isset($_GET["project"]))
	{
		$project = $_GET["project"];
	}
	else
	{
		$project = "";
	}
	
	if(isset($_GET["projectname"]))
	{
		$project_name = $_GET["projectname"];
	}
	else
	{
		$project_name = "";
	}
	
	if(isset($_GET["start"]))
	{
		$start_date = $_GET["start"];
	}
	else
	{
		$start_date = "";
	}
	
	if(isset($_GET["end"]))
	{
		$end_date = $_GET["end"];
	}
	else
	{
		$end_date = "";
	}

	$payment_sresult = i_get_payment('','','',$start_date,$end_date,$project);
	if($payment_sresult['status'] == SUCCESS)
	{
		$payment_list_data = $payment_sresult['data'];
	}
	else
	{
		// Nothing here
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>CRM  Payment Collected Details</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>
    

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header" style="height:50px;">
              <h3>Collection Completed Report&nbsp;&nbsp;&nbsp;Project: <?php echo $project_name; ?>&nbsp;&nbsp;&nbsp;Between: <?php echo date('d-M-Y',strtotime($start_date)); ?> and <?php echo date('d-M-Y',strtotime($end_date)); ?>&nbsp;&nbsp;&nbsp;Total Collected: <span id="total_collected">0</span></h3>
            </div>			
            <!-- /widget-header -->
            <div class="widget-content">
			
              <table class="table table-bordered" style="table-layout: fixed;">
                <thead>
                  <tr>
					<th>SL No</th>					
					<th>Project</th>
					<th>Plot No.</th>									
					<th>Party Name</th>
					<th>Amount Collected</th>
					<th>Balance</th>													
				</tr>
				</thead>
				<tbody>							
				<?php				
				$total_collected = 0;
				if($payment_sresult['status'] == SUCCESS)
				{										
					$sl_no = 0;										
					for($count = 0; $count < count($payment_list_data); $count++)
					{																	
						$sl_no++;
						
						// Total cost for this plot
						if($payment_list_data[$count]["crm_booking_consideration_area"] != "0")
						{
							$consideration_area = $payment_list_data[$count]["crm_booking_consideration_area"];
						}
						else
						{
							$consideration_area = $payment_list_data[$count]["crm_site_area"];
						}
						
						$total_payable = round($payment_list_data[$count]["crm_booking_rate_per_sq_ft"] * $consideration_area);
						
						// Already paid total
						$payment_done = 0;
						$payments_done_sresult = i_get_payment('',$payment_list_data[$count]["crm_payment_booking_id"],'','','');											
						if($payments_done_sresult["status"] == SUCCESS)
						{
							for($payment_count = 0; $payment_count < count($payments_done_sresult["data"]); $payment_count++)
							{
								$payment_done = $payment_done + $payments_done_sresult["data"][$payment_count]["crm_payment_amount"];
							}
						}
						else
						{
							$payment_done = 0;
						}		
						$payment_done = round($payment_done);
						
						// Pending payment						
						$pending_payment = $total_payable - $payment_done;
						
						// Customer Name
						if($payment_list_data[$count]['crm_customer_name_one'] != '')
						{
							$party_name = $payment_list_data[$count]['crm_customer_name_one'];
						}
						else
						{
							$party_name = $payment_list_data[$count]["name"];
						}
						
						$total_collected = $total_collected + $payment_list_data[$count]["crm_payment_amount"];
						?>
						<tr>
						<td style="word-wrap:break-word;"><?php echo $sl_no; ?></td>							
						<td style="word-wrap:break-word;"><?php echo $payment_list_data[$count]["project_name"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $payment_list_data[$count]["crm_site_no"]; ?> (<?php echo $payment_list_data[$count]["crm_site_area"].' sq. ft'; ?>)</td>							
						<td style="word-wrap:break-word;"><?php echo $party_name; ?><br />							
						<td style="word-wrap:break-word;"><?php echo $payment_list_data[$count]["crm_payment_amount"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $pending_payment; ?></td>							
						</tr>
						<?php													
					}
				}
				else
				{
				?>
				<td colspan="6">No payment collected!</td>
				<?php
				}
				?>					
                </tbody>
              </table>
			  <script>
			  document.getElementById('total_collected').innerHTML = <?php echo $total_collected; ?>;
			  </script>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>