<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: user_list.php
CREATED ON	: 05-July-2015
CREATED BY	: Nitin Kashyap
PURPOSE     : List of Users
*/

/*
TBD: 
*/$_SESSION['module'] = 'Legal Masters';

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'tasks'.DIRECTORY_SEPARATOR.'task_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Query String Data
	// Nothing here

	// Get list of reasons from master
	$reason_list = i_get_reason_list('','','','','');

	if($reason_list["status"] == SUCCESS)
	{
		$reason_list_data = $reason_list["data"];
		$reason_count = count($reason_list_data);
	}
	else
	{
		$alert = $alert."Alert: ".$reason_list["data"];
		$process_count = 0;
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Reason List</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>     

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Reason List (Count: <?php echo $reason_count; ?>)</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-bordered">
                <thead>
                  <tr>
				    <th>Reason Name</th>					
					<th>Active</th>
					<th>Search Priority</th>
					<th>Added By</th>	
					<th>Added On</th>
					<th>&nbsp;</th>					
				</tr>
				</thead>
				<tbody>
				<?php
				if($reason_list["status"] == SUCCESS)
				{				
					for($count = 0; $count < count($reason_list_data); $count++)
					{
						if($reason_list_data[$count]["reason_master_active"] == 1)
						{
							$action = 0;
							$text   = "Disable";
							$status = "Enabled";
						}
						else
						{
							$action = 1;
							$text   = "Enable";
							$status = "Disabled";
						}
						
						if($reason_list_data[$count]["reason_priority"] == 1)
						{
							$search_priority = 'YES';
						}
						else
						{
							$search_priority = 'NO';
						}
					?>					
					<tr>
						<td><?php echo $reason_list_data[$count]["reason"]; ?></td>						
						<td><?php echo $status; ?></td>
						<td><?php echo $search_priority; ?></td>
						<td><?php echo $reason_list_data[$count]["user_name"]; ?></td>	
						<td><?php echo date("d-M-Y",strtotime($reason_list_data[$count]["reason_master_added_on"])); ?></td>
						<td><a href="reason_enable_disable.php?reason=<?php echo $reason_list_data[$count]["reason_master_id"]; ?>&action=<?php echo $action; ?>"><?php echo $text; ?></a></td>
					</tr>
					<?php 
					}
				}
				else
				{
				?>
				<tr>
				<td colspan="5">No reason added yet</td>
				</tr>
				<?php
				}
				?>	

                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>/* Open the sidenav */function openNav() {    document.getElementById("mySidenav").style.width = "75%";}/* Close/hide the sidenav */function closeNav() {    document.getElementById("mySidenav").style.width = "0";}</script>

  </body>

</html>
