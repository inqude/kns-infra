<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 22nd March 2016
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'tasks'.DIRECTORY_SEPARATOR.'task_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'process'.DIRECTORY_SEPARATOR.'process_functions.php');
/* INCLUDES - END */

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	
	/* QUERY STRING - START */
	if(isset($_GET["task"]))
	{
		$task_id = $_GET["task"];
	}
	else
	{
		$task_id = "";
	}
	/* QUERY STRING - END */

	// Capture the form data
	if(isset($_POST["add_pay_request_submit"]))
	{
		$task_id = $_POST["hd_task"];
		$amount  = $_POST["num_amount"];
		$reason  = $_POST["ddl_pay_reason"];
		$remarks = $_POST["txt_remarks"];
		
		// Check for mandatory fields
		if(($task_id !="") && ($amount != "") && ($reason != ""))
		{
			$pay_request_iresult = i_add_pay_request_legal($task_id,$amount,$reason,$remarks,$user);
			
			if($pay_request_iresult["status"] == SUCCESS)
			{
				$alert_type = 1;
				$alert = "Payment Request sent to HODs approval";
			}
			else
			{
				$alert_type = 0;
				$alert = $pay_request_iresult["data"];
			}
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}

	// Get process type list
	$reason_list = i_get_pay_request_reason_list('','','1','','','');
	if($reason_list["status"] == SUCCESS)
	{
		$reason_list_data = $reason_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$reason_list["data"]."<br />";
		$alert_type = -1;
	}
	
	// Get already paid amount to this task
	$pay_request_data = array("task"=>$task_id);
	$pay_request_list = i_get_pay_request_list($pay_request_data);
	if($pay_request_list["status"] == SUCCESS)
	{
		$pay_request_list_data = $pay_request_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$pay_request_list["data"]."<br />";
		$alert_type = -1;
	}
	
	// Get task details
	$task_data = i_get_task_plan_list($task_id,'','','','','','',''); // Get task plan for this task plan ID
	if($task_data["status"] == SUCCESS)
	{
		$task_data_details = $task_data["data"];
		
		if($task_data_details[0]['process_is_bulk'] == '1')
		{
			$legal_bulk_search_data = array('bulk_process_id'=>$task_data_details[0]["task_plan_legal_bulk_process_plan_id"]);
			$process_data = i_get_bulk_legal_process_plan_details($legal_bulk_search_data);
			$process_name_display = $process_data['data'][0]['process_name'];
			$process_file_display = '<a href="bulk_file_list.php?bprocess='.$process_data['data'][0]["legal_bulk_process_id"].'" target="_blank">Click</a>';
		}
		else
		{
			$process_data = i_get_legal_process_plan_details($task_data_details[0]["task_plan_legal_process_plan_id"]);		
			$process_name_display = $process_data["data"]["process_name"];
			$process_file_display = $process_data["data"]["file_number"];
		}	
	}
	else
	{
		$alert = $alert."Alert: ".$task_data["data"];
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Request for payment</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3><?php
						if($task_data["status"] == SUCCESS)
						{
							?>
							Task: <?php echo $task_data_details[0]["task_type_name"]; ?>&nbsp;&nbsp;&nbsp;Process: <?php echo $process_name_display; ?>&nbsp;&nbsp;&nbsp;File No: <?php echo $process_file_display; ?>&nbsp;&nbsp;&nbsp;Amount Paid: <span id="amount_paid_already"><i>Calculating</i></span>
							<?php
						}
						else
						{
							echo "Invalid task";
						}
						?></h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Make Payment Request</a>
						  </li>						  
						</ul>
						
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="add_task_type_form" class="form-horizontal" method="post" action="add_payment_request.php">
									<fieldset>
										<input type="hidden" name="hd_task" value="<?php echo $task_id; ?>" />
																				
										<div class="control-group">											
											<label class="control-label" for="num_amount">Amount</label>
											<div class="controls">
												<input type="number" class="span6" name="num_amount" placeholder="Amount needed" required="required">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="role">Purpose</label>
											<div class="controls">
											    <select name="ddl_pay_reason" required>
												<option value="">- - Select Purpose - -</option>
												<?php
												for($count = 0; $count < count($reason_list_data); $count++)
												{
												?>
												<option value="<?php echo $reason_list_data[$count]["payment_request_reason_id"]; ?>"><?php echo $reason_list_data[$count]["payment_request_reason"]; ?></option>					
												<?php
												}
												?>
												</select>
												<p class="help-block">For what purpose is the amount needed</p>
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="txt_remarks">Remarks</label>
											<div class="controls">
												<textarea name="txt_remarks" class="span6"></textarea>
												<p class="help-block">Details of why the money is needed</p>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
                                                                                                                                                               										 <br />
										
											
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="add_pay_request_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>																
								
							</div>
						  
							<table class="table table-bordered" style="table-layout: fixed;">
								<thead>
								  <tr>
									<th>Amount</th>
									<th>Request Date</th>					
									<th>Status</th>
									<th>Reason</th>
									<th>Remarks</th>					
									<th>Added By</th>														
								</tr>
								</thead>
								<tbody>							
								<?php
								$total_paid = 0;
								if($pay_request_list["status"] == SUCCESS)
								{									
									for($count = 0; $count < count($pay_request_list_data); $count++)
									{
										if($pay_request_list_data[$count]["legal_payment_request_status"] == "4") // Released Payment
										{
											$total_paid = $total_paid + $pay_request_list_data[$count]["legal_payment_request_amount"];
										}
										
										switch($pay_request_list_data[$count]["legal_payment_request_status"])
										{
										case "1":
										$pay_status = "HOD Approval Pending";
										break;
										
										case "2":
										$pay_status = "HOD Approved. Pending with Finance";
										break;
										
										case "3":
										$pay_status = "HOD Rejected";
										break;
										
										case "4":
										$pay_status = "Payment Released";
										break;
										
										case "5":
										$pay_status = "Finance Rejected";
										break;
										
										case "6":
										$pay_status = "Finance Held";
										break;
										
										default:
										$pay_status = "HOD Approval Pending";
										break;
										}
									?>
									<tr>
									<td style="word-wrap:break-word;"><?php echo $pay_request_list_data[$count]["legal_payment_request_amount"]; ?></td>
									<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($pay_request_list_data[$count]["legal_payment_request_added_on"])); ?></td>
									<td style="word-wrap:break-word;"><?php echo $pay_status; ?></td>					
									<td style="word-wrap:break-word;"><?php echo $pay_request_list_data[$count]["payment_request_reason"]; ?></td>
									<td style="word-wrap:break-word;"><?php echo $pay_request_list_data[$count]["legal_payment_request_remarks"]; ?></td>
									<td style="word-wrap:break-word;"><?php echo $pay_request_list_data[$count]["user_name"]; ?></td>
									</tr>
									<?php									
									}
								}
								else
								{
								?>
								<td colspan="6">No payments for this task yet!</td>
								<?php
								}	
								?>	
								</tbody>
							  </table>
							  <script>
							  document.getElementById("amount_paid_already").innerHTML = <?php echo $total_paid; ?>;
							  </script>
						</div>
						
						
						
						
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
