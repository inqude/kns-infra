<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 29th Dec 2015
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'files'.DIRECTORY_SEPARATOR.'file_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'process'.DIRECTORY_SEPARATOR.'process_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'tasks'.DIRECTORY_SEPARATOR.'task_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');

/* INCLUDES - END */

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	
	/* QUERY STRING - START */
	if(isset($_GET["file"]))
	{
		$file_id = $_GET["file"];
	}
	else
	{
		$file_id = "";
	}		
	/* QUERY STRING - END */
	
	// Temp data
	$alert = "";	
	
	// Get list of files
	$legal_file_list = i_get_file_list('','','','','','','','','','',$file_id);

	if($legal_file_list["status"] == SUCCESS)
	{
		$legal_file_list_data = $legal_file_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$legal_file_list["data"];
	}	
	
	// Get list of process types
	$process_type_list = i_get_process_type_list('','');

	if($process_type_list["status"] == SUCCESS)
	{
		$process_type_list_data = $process_type_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$process_type_list["data"];
	}	
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>File Details</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>     

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>File Details</h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">						
						
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">							
								<div class="tab-pane active" id="formcontrols">
									<div class="widget-header">
									<i class="icon-user"></i>
									<h3>File Details</h3>
									</div> <!-- /widget-header -->
									<br />									
									<fieldset>	
										<table class="table table-bordered" style="table-layout: fixed;">
										  <thead>
											<?php if($legal_file_list["status"] == SUCCESS)
											{?>
											  <tr>
											  <td width="25%">File No</td>
											  <td width="70%"><?php echo $legal_file_list_data[0]["file_number"]; ?> </td>
											  </tr>											 
											  <tr>
											  <td>Survey No</td>										  
											  <td><?php echo $legal_file_list_data[0]["file_survey_number"]; ?></td>	
											  </tr>
											  <tr>
											  <td>Land Owner</td>										  
											  <td><?php echo $legal_file_list_data[0]["file_land_owner"]; ?></td>
											  </tr>
											  <tr>
											  <td>PAN No</td>										  
											  <td><?php echo $legal_file_list_data[0]["file_pan_number"]; ?></td>
											  </tr>
											  <tr>
											  <td>File Type</td>										  
											  <td><?php echo $legal_file_list_data[0]["legal_file_type_name"]; ?></td>
											  </tr>
											  <tr>
											  <td>Extent</td>
											  <td style="word-wrap:break-word;"><?php $extent_array = get_acre_from_guntas($legal_file_list_data[0]["file_extent"]);
											  echo $extent_array["acres"]." acres ".$extent_array["guntas"]." guntas"; ?></td>	
											 </tr>
											  <tr>
											  <td>Village</td>										  
											  <td style="word-wrap:break-word;"><?php if($legal_file_list_data[0]["village_name"] != "")
												{
													echo $legal_file_list_data[0]["village_name"]; 
												}
												else
												{
													echo $legal_file_list_data[0]["file_village"]; 
												}?></td>
											  </tr>											
											  <tr>
											  <td>Mobile Number</td>										  
											 <td style="word-wrap:break-word;"><?php echo $legal_file_list_data[0]["bd_file_owner_phone_no"]; ?></td>	
											  </tr>
											  </tbody>
											</table>
									<div>
										<table class="table table-bordered" style="table-layout: fixed;">
										  <tbody>
										    <?php
											$total_files = 0;
												// Get list of completed process plan
												$completed_process_plan_list = i_get_completed_process_plan_list($legal_file_list_data[0]["file_id"],'','1');

												if($completed_process_plan_list["status"] == SUCCESS)
												{
													$completed_process_plan_list_data = $completed_process_plan_list["data"];
												}
												else
												{
													$alert = $alert."Alert: ".$completed_process_plan_list["data"];
												}	
												
												$file_handover_data = i_get_file_handover_details($legal_file_list_data[0]["file_id"]);
												?>	
												<tr>
												<?php
												if((($role == 1) || ($role == 2)) && ($file_handover_data["status"] != SUCCESS))
												{?>
												<td><a href="file_project_handover.php?file=<?php echo $legal_file_list_data[0]["file_id"]; ?>">Handover to project</a></td>
												<?php
												}
												?>
												<?php if($role == 1)
												{?>
												<td><a href="edit_file.php?file=<?php echo $legal_file_list_data[0]["file_id"]; ?>">Edit File</a>
												</td>
												<td><a href="#" onclick="return confirm_deletion(<?php echo $legal_file_list_data[0]["file_id"]; ?>);">Delete File</a></td>													
												<?php
												}
												?>
												<td>
												<?php if(($role == 1) || ($role == 2) || ($role == 3))
												{?>
												<span id="completed_status_span">
													<?php if(($completed_process_plan_list["status"] != SUCCESS) && ($file_handover_data["status"] != SUCCESS))
													{
													?>
													<a href="#" onclick="return complete_process_plan(<?php echo $legal_file_list_data[0]["file_id"]; ?>);">Complete Processes</a>
													<?php 
													}
													else
													{?>
														<?php echo "Completed"; ?>
													<?php 
													}
													?>
												</span>
												<?php
												}?></td>		
												<td style="word-wrap:break-word;">
												<?php if(($role == 1) || ($role == 2))
												{
												?>
													<?php if($file_handover_data["status"] != SUCCESS)
													{?><a href="create_project_plan.php?file=<?php echo $legal_file_list_data[0]["file_id"]; ?>">Add Plan</a>
													<?php
													}
													else
													{
													?>
													<strong>Can't add Plan</strong>
													<?php
													}
													?>
												<?php
												}
												?></td>
												<td><a href="pending_project_list.php?file=<?php echo $legal_file_list_data[0]["file_id"]; ?>">View Processes</a></td>
												<td><a href="pending_bulk_project_list.php?file=<?php echo $legal_file_list_data[0]["file_id"]; ?>">View Bulk Processes</a></td>
												<td>							
												<?php
												if($legal_file_list_data[0]["file_doc"] != '')
												{
												?>
												<a href="documents/<?php echo $legal_file_list_data[0]["file_doc"]; ?>" target="_blank">Download Documents</a>
												<?php
												}
												else
												{
													echo 'NO DOCUMENTS';
												}
												?>
												</td>
												</tr>
											<?php																									
											}
											else
											{
												echo "No Records";
											}
												
												?>
										 
										  </tbody>
										</table>	
									</div>
									</fieldset>									
								</div>																
								
							</div>
						  						  
						</div>
	
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function confirm_deletion(file_id)
{
	var ok = confirm("Are you sure you want to delete?")
	{         
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					window.location = "pending_file_list.php";
				}
			}

			xmlhttp.open("GET", "legal_file_delete.php?file=" + file_id);   // file name where delete code is written
			xmlhttp.send();
		}
	}	
}

function complete_process_plan(file_id)
{
	var ok = confirm("Are you sure you want to Complete?")
	{         
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{					
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else					
					{
					     document.getElementById('completed_status_span').innerHTML = 'Completed';
					}
				}
			}

			xmlhttp.open("POST", "add_legal_process_plan.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("process_id=" + file_id + "&action=1");
		}
	}	
}
</script>


  </body>

</html>
