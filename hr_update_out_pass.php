<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD:
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'hr'.DIRECTORY_SEPARATOR.'hr_attendance_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Update attendance details
	$out_pass_id = $_POST["outpass"];
	$action      = $_POST["action"];
	
	$out_pass_data = array("status"=>$action,"approver"=>$user,"approved_on"=>date("Y-m-d H:i:s"));
	$update_out_pass_result = i_update_out_pass($out_pass_id,$out_pass_data);
	
	if($update_out_pass_result["status"] == FAILURE)
	{
		echo $update_out_pass_result["data"];
	}
	else
	{
		echo "SUCCESS";
	}
}
else
{
	header("location:login.php");
}
?>