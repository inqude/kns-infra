<?php
  session_start();
  $_SESSION['module'] = 'PM Masters';

  /* DEFINES - START */
  define('PROJECT_MASTER_PROJECT_PROCESS_FUNC_ID', '359');
  /* DEFINES - END */

  // Includes
  $base = $_SERVER["DOCUMENT_ROOT"];
  require dirname(__FILE__) . '/utilities/PHPExcel-1.8/Classes/PHPExcel.php';
  include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
  include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
  include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

  if ((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != "")) {
    // Session Data
    $user 		   = $_SESSION["loggedin_user"];
    $role 		   = $_SESSION["loggedin_role"];
    $loggedin_name = $_SESSION["loggedin_user_name"];

    // Get permission settings for this user for this page
    $view_perms_list   = i_get_user_perms($user, '', PROJECT_MASTER_PROJECT_PROCESS_FUNC_ID, '2', '1');
    $edit_perms_list   = i_get_user_perms($user, '', PROJECT_MASTER_PROJECT_PROCESS_FUNC_ID, '3', '1');
    $delete_perms_list = i_get_user_perms($user, '', PROJECT_MASTER_PROJECT_PROCESS_FUNC_ID, '4', '1');
    $add_perms_list    = i_get_user_perms($user, '', PROJECT_MASTER_PROJECT_PROCESS_FUNC_ID, '1', '1');

    // Query String Data
    // Nothing

    // Temp data
    $alert_type = -1;
    $alert 	    = "";

    if (isset($_REQUEST['hd_project_id'])) {
      $project_id = $_REQUEST['hd_project_id'];
    } else {
      $project_id = "";
    }

    if (isset($_REQUEST['hd_status'])) {
      $status = $_REQUEST['hd_status'];
    } else {
      $status = "";
    }

    // Get Already added Object Output
    $stock_indent_search_data = array("active"=>'1',"project"=>$project_id,"status"=>$status);
    $indent_item_list = i_get_indent_items_list($stock_indent_search_data);
  	if($indent_item_list["status"] == SUCCESS)
  	{
  		$indent_item_list_data = $indent_item_list["data"];
  	}
  	else
  	{
  		$alert = $alert."Alert: ".$indent_item_list["data"];
      exit;
  	}

    // print_r($indent_item_list); exit;
    $project_name = $indent_item_list_data[0]['stock_project_name'];
    $filename = $project_name."-exported-data_".date("m-d-Y-h-m-s").".xls";

    // Create new PHPExcel object
    $objPHPExcel = new PHPExcel();

    // PRINT HEADERS
    $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'Project')
                ->setCellValue('B1', 'Indent Date')
                ->setCellValue('C1', 'Material')
                ->setCellValue('D1', 'Material Code')
                ->setCellValue('E1', 'UOM')
                ->setCellValue('F1', 'Quantity')
                ->setCellValue('G1', 'Status')
                ->setCellValue('H1', 'Remarks')
                ->setCellValue('I1', 'Requested By')
                ->setCellValue('J1', 'Required Date')
                ->setCellValue('K1', 'Stock Quantity')
                ->setCellValue('L1', 'Indent No');

                for ($count = 0; $count < count($indent_item_list_data); $count++) {
                $material_stock_search_data=array( "material_id"=>$indent_item_list_data[$count]['stock_material_id'],"project"=>$project_id);
              	$stock_material = i_get_material_stock($material_stock_search_data);
              	if($stock_material["status"] == SUCCESS)
              	{
              		$quantity = 0;
              		for($stock_qty = 0; $stock_qty< count($stock_material[ "data"]) ; $stock_qty++)
              		{
              			$stock_material_data = $stock_material[ "data"];
              			$stock_quantity = $quantity + $stock_material_data[$stock_qty][ "material_stock_quantity"];
              		}
              	} else
              	{
              		$stock_quantity = "0" ;
              		}

                $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A'.($count+2), $indent_item_list_data[$count]["stock_project_name"])
                    ->setCellValue('B'.($count+2), $indent_item_list_data[$count]["stock_indent_item_added_on"])
                    ->setCellValue('C'.($count+2), $indent_item_list_data[$count]["stock_material_name"])
                    ->setCellValue('D'.($count+2), $indent_item_list_data[$count]["stock_material_code"])
                    ->setCellValue('E'.($count+2), $indent_item_list_data[$count]["stock_unit_name"])
                    ->setCellValue('F'.($count+2), $indent_item_list_data[$count]["stock_indent_item_quantity"])
                    ->setCellValue('G'.($count+2), $indent_item_list_data[$count]["stock_indent_item_status"])
                    ->setCellValue('H'.($count+2), $indent_item_list_data[$count]["stock_indent_item_remarks"])
                    ->setCellValue('I'.($count+2), $indent_item_list_data[$count]["user_name"])
                    ->setCellValue('J'.($count+2), $indent_item_list_data[$count]["stock_indent_added_on"])
                    ->setCellValue('K'.($count+2), $stock_quantity)
                    ->setCellValue('L'.($count+2), $indent_item_list_data[$count]["stock_indent_no"]);
    }

    // Rename worksheet
    $objPHPExcel->getActiveSheet()->setTitle($project_name);

    // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $objPHPExcel->setActiveSheetIndex(0);

    // Redirect output to a client’s web browser (Excel5)
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="'.$filename.'"');
    header('Cache-Control: max-age=0');

    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header ('Pragma: public'); // HTTP/1.0
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output');
  } else {
    header("location:login.php");
  }
