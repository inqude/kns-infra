<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 08-Nov-2016
// LAST UPDATED BY: Lakshmi
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */

	if(isset($_GET["agency_id"]))
	{
		$agency_id = $_GET["agency_id"];
	}
	else
	{
		$agency_id = "";
	}

	// Capture the form data
	if(isset($_POST["edit_project_manpower_agency_submit"]))
	{
		    $agency_id      = $_POST["hd_agency_id"];
        $name           = $_POST["name"];
				$contact_name   = $_POST["contact_name"];
				$contact_number = $_POST["contact_number"];
				$address        = $_POST["address"];
				$email          = $_POST["email"];
				$pan_number     = $_POST["pan_number"];
				$tin_number     = $_POST["tin_number"];
				$gst_number 		= $_POST["gst_number"];
				$acc_name 			=	$_POST["acc_name"];
				$acc_bank 			= $_POST["acc_bank"];
				$acc_branch 		= $_POST["acc_branch"];
				$acc_no 				= $_POST["acc_no"];
				$ifsc_code 			= $_POST["ifsc_code"];
				$sec_acc_name 	= $_POST["sec_acc_name"];
				$sec_acc_bank 	= $_POST["sec_acc_bank"];
				$sec_acc_branch = $_POST["sec_acc_branch"];
				$sec_acc_no 		= $_POST["sec_acc_no"];
				$sec_ifsc_code 	= $_POST["sec_ifsc_code"];
		    $remarks 	    	= $_POST["txt_remarks"];

		// Check for mandatory fields
		if(($name != ""))
		{
			$project_manpower_agency_update_data =  array('name'=>$name,'contact_name'=>$contact_name,'contact_number'=>$contact_number,'address'=>$address,'email'=>$email,'pan_number'=>$pan_number,'tin_number'=>$tin_number,'gst_number'=>$gst_number,'acc_name'=>$acc_name,'acc_bank'=>$acc_bank,'acc_branch'=>$acc_branch,'acc_no'=>$acc_no,'ifsc_code'=>$ifsc_code,'sec_acc_name'=>$sec_acc_name,
			'sec_acc_bank'=>$sec_acc_bank,'sec_acc_branch'=>$sec_acc_branch,'sec_acc_no'=>$sec_acc_no,'sec_ifsc_code'=>$sec_ifsc_code,'remarks'=>$remarks,'updated_by'=>$user,'updated_on'=>date("Y-m-d H:i:s"));
			$project_manpower_agency_iresult = i_update_project_manpower_agency($agency_id,$project_manpower_agency_update_data);

			if($project_manpower_agency_iresult["status"] == SUCCESS)

			{
			$alert_type = 1;

			  header("location:project_master_manpower_agency_list.php");
			}
			else
			{
			   $alert = "Project Already Exists";
			  $alert_type = 0;
			}

			$alert = $project_manpower_agency_iresult["data"];
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}


	// Get Project manpower_agency Master modes already added
	$project_manpower_agency_search_data = array("agency_id"=>$agency_id);
	$project_manpower_agency_list = i_get_project_manpower_agency($project_manpower_agency_search_data);
	if($project_manpower_agency_list['status'] == SUCCESS)
	{
		$project_manpower_agency_list_data = $project_manpower_agency_list['data'];
	}
}
else
{
	header("location:login.php");
}
?>

  <!DOCTYPE html>
  <html lang="en">

  <head>
    <meta charset="utf-8">
    <title>Project Master Edit Manpower Agency</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">

    <link href="css/style.css" rel="stylesheet">



    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

  <body>

    <?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

    <div class="main">

      <div class="main-inner">

        <div class="container">

          <div class="row">

            <div class="span12">

              <div class="widget ">

                <div class="widget-header">
                  <i class="icon-user"></i>
                  <h3>Project Master - Edit Manpower Agency</h3>
                </div>
                <!-- /widget-header -->

                <div class="widget-content">



                  <div class="tabbable">
                    <ul class="nav nav-tabs">
                      <li>
                        <a href="#formcontrols" data-toggle="tab">Edit Manpower Agency</a>
                      </li>
                    </ul>
                    <br>
                    <div class="control-group">
                      <div class="controls">
                        <?php
								if($alert_type == 0) // Failure
								{
								?>
                          <div class="alert">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <strong><?php echo $alert; ?></strong>
                          </div>
                          <?php
								}
								?>

                          <?php
								if($alert_type == 1) // Success
								{
								?>
                            <div class="alert alert-success">
                              <button type="button" class="close" data-dismiss="alert">&times;</button>
                              <strong><?php echo $alert; ?></strong>
                            </div>
                            <?php
								}
								?>
                      </div>
                      <!-- /controls -->
                    </div>
                    <!-- /control-group -->
                    <div class="tab-content">
                      <div class="tab-pane active" id="formcontrols">
                        <form id="project_master_edit_manpower_agency_form" class="form-horizontal" method="post" action="project_master_edit_manpower_agency.php">
                          <input type="hidden" name="hd_agency_id" value="<?php echo $agency_id; ?>" />
                          <fieldset>

                            <div class="control-group">
                              <label class="control-label" for="name">Name*</label>
                              <div class="controls">
                                <input type="text" class="span6" name="name" placeholder="Name" value="<?php echo $project_manpower_agency_list_data[0]["project_manpower_agency_name"] ;?>" required="required">
                              </div>
                              <!-- /controls -->
                            </div>
                            <!-- /control-group -->

                            <div class="control-group">
                              <label class="control-label" for="contact_name">Contact Person Name</label>
                              <div class="controls">
                                <input type="text" class="span6" name="contact_name" placeholder="Contact Name" value="<?php echo $project_manpower_agency_list_data[0]["project_manpower_agency_contact_person"] ;?>">
                              </div>
                              <!-- /controls -->
                            </div>
                            <!-- /control-group -->

                            <div class="control-group">
                              <label class="control-label" for="contact_number">Contact Person Number</label>
                              <div class="controls">
                                <input type="text" pattern="[0-9]*" class="span6" name="contact_number" value="<?php echo $project_manpower_agency_list_data[0]["project_manpower_agency_contact_number"] ;?>" placeholder="10 digit mobile no." required="required" step="1" min="0">
                              </div>
                              <!-- /controls -->
                            </div>
                            <!-- /control-group -->

                            <div class="control-group">
                              <label class="control-label" for="address">Contact Person Address</label>
                              <div class="controls">
                                <input type="text" class="span6" name="address" placeholder="Contact Person Address" value="<?php echo $project_manpower_agency_list_data[0]["project_manpower_agency_address"] ;?>" required="required">
                              </div>
                              <!-- /controls -->
                            </div>
                            <!-- /control-group -->

                            <div class="control-group">
                              <label class="control-label" for="email">Email</label>
                              <div class="controls">
                                <input type="email" class="span6" name="email" placeholder="Valid Customer Email ID" value="<?php echo $project_manpower_agency_list_data[0]["project_manpower_agency_email"] ;?>">
                              </div>
                              <!-- /controls -->
                            </div>
                            <!-- /control-group -->

                            <div class="control-group">
                              <label class="control-label" for="pan_number">Pan Number</label>
                              <div class="controls">
                                <input type="text" class="span6" name="pan_number" placeholder="Valid Pan Number" value="<?php echo $project_manpower_agency_list_data[0]["project_manpower_agency_pan_number"] ;?>">
                              </div>
                              <!-- /controls -->
                            </div>
                            <!-- /control-group -->

                            <div class="control-group">
                              <label class="control-label" for="tin_number">Tin Number</label>
                              <div class="controls">
                                <input type="text" class="span6" name="tin_number" placeholder="Valid Tin Number" value="<?php echo $project_manpower_agency_list_data[0]["project_manpower_agency_tin_number"] ;?>">
                              </div>
                              <!-- /controls -->
                            </div>
                            <!-- /control-group -->

                            <div class="control-group">
                              <label class="control-label" for="gst_number">Gst Number</label>
                              <div class="controls">
                                <input type="text" class="span6" name="gst_number" placeholder="Valid Customer Gst Number" value="<?php echo $project_manpower_agency_list_data[0]["project_manpower_agency_gst_number"] ;?>">
                              </div>
                              <!-- /controls -->
                            </div>
                            <!-- /control-group -->

                            <div class="control-group">
                              <label class="control-label" for="acc_name">Account Holder Name</label>
                              <div class="controls">
                                <input type="text" class="span6" name="acc_name" placeholder="Account Holder Name" value="<?php echo $project_manpower_agency_list_data[0]["project_manpower_agency_account_holder_name"] ;?>">
                              </div>
                              <!-- /controls -->
                            </div>
                            <!-- /control-group -->

                            <div class="control-group">
                              <label class="control-label" for="acc_bank">Account Holder Bank</label>
                              <div class="controls">
                                <input type="text" class="span6" name="acc_bank" placeholder="Account Holder Bank Name" value="<?php echo $project_manpower_agency_list_data[0]["project_manpower_agency_bank_name"] ;?>">
                              </div>
                              <!-- /controls -->
                            </div>
                            <!-- /control-group -->

                            <div class="control-group">
                              <label class="control-label" for="acc_branch">Account Holder Branch</label>
                              <div class="controls">
                                <input type="text" class="span6" name="acc_branch" placeholder="Account Holder Branch Name" value="<?php echo $project_manpower_agency_list_data[0]["project_manpower_agency_branch"] ;?>">
                              </div>
                              <!-- /controls -->
                            </div>
                            <!-- /control-group -->

                            <div class="control-group">
                              <label class="control-label" for="acc_no">Account Number</label>
                              <div class="controls">
                                <input type="text" class="span6" name="acc_no" placeholder="Account Number" value="<?php echo $project_manpower_agency_list_data[0]["project_manpower_agency_account_number"] ;?>">
                              </div>
                              <!-- /controls -->
                            </div>
                            <!-- /control-group -->

                            <div class="control-group">
                              <label class="control-label" for="ifsc_code">IFSC Code</label>
                              <div class="controls">
                                <input type="text" class="span6" name="ifsc_code" placeholder="IFSC Code" value="<?php echo $project_manpower_agency_list_data[0]["project_manpower_agency_ifsc_code"] ;?>">
                              </div>
                              <!-- /controls -->
                            </div>
                            <!-- /control-group -->

                            <div class="control-group">
                              <label class="control-label" for="sec_acc_name">Secondary Account Holder Name</label>
                              <div class="controls">
                                <input type="text" class="span6" name="sec_acc_name" placeholder="Secondary Account Holder Name" value="<?php echo $project_manpower_agency_list_data[0]["project_manpower_agency_secondary_acc_holder_name"] ;?>">
                              </div>
                              <!-- /controls -->
                            </div>
                            <!-- /control-group -->

                            <div class="control-group">
                              <label class="control-label" for="sec_acc_bank">Secondary Account Holder Bank</label>
                              <div class="controls">
                                <input type="text" class="span6" name="sec_acc_bank" placeholder="Secondary Account Holder Bank Name" value="<?php echo $project_manpower_agency_list_data[0]["project_manpower_agency_secondary_acc_bank"] ;?>">
                              </div>
                              <!-- /controls -->
                            </div>
                            <!-- /control-group -->

                            <div class="control-group">
                              <label class="control-label" for="sec_acc_branch">Secondary Account Holder Branch</label>
                              <div class="controls">
                                <input type="text" class="span6" name="sec_acc_branch" placeholder="Secondary Account Holder Branch" value="<?php echo $project_manpower_agency_list_data[0]["project_manpower_agency_secondary_acc_branch"] ;?>">
                              </div>
                              <!-- /controls -->
                            </div>
                            <!-- /control-group -->

                            <div class="control-group">
                              <label class="control-label" for="sec_acc_no">Secondary Account Number</label>
                              <div class="controls">
                                <input type="text" class="span6" name="sec_acc_no" placeholder="Secondary Account Number" value="<?php echo $project_manpower_agency_list_data[0]["project_manpower_agency_secondary_acc_number"] ;?>">
                              </div>
                              <!-- /controls -->
                            </div>
                            <!-- /control-group -->

                            <div class="control-group">
                              <label class="control-label" for="sec_ifsc_code">IFSC Code</label>
                              <div class="controls">
                                <input type="text" class="span6" name="sec_ifsc_code" placeholder="Secondary Account IFSC Code" value="<?php echo $project_manpower_agency_list_data[0]["project_manpower_agency_secondary_acc_ifsc_code"] ;?>">
                              </div>
                              <!-- /controls -->
                            </div>
                            <!-- /control-group -->

                            <div class="control-group">
                              <label class="control-label" for="txt_remarks">Remarks</label>
                              <div class="controls">
                                <input type="text" class="span6" name="txt_remarks" placeholder="Remarks" value="<?php echo $project_manpower_agency_list_data[0]["project_manpower_agency_remarks"] ;?>">
                              </div>
                              <!-- /controls -->
                            </div>
                            <!-- /control-group -->
                            <br />


                            <div class="form-actions">
                              <input type="submit" class="btn btn-primary" name="edit_project_manpower_agency_submit" value="Submit" />
                              <button type="reset" class="btn">Cancel</button>
                            </div>
                            <!-- /form-actions -->
                          </fieldset>
                        </form>
                      </div>

                    </div>

                  </div>
                  <!-- /widget-content -->

                </div>
                <!-- /widget -->

              </div>
              <!-- /span8 -->




            </div>
            <!-- /row -->

          </div>
          <!-- /container -->

        </div>
        <!-- /main-inner -->

      </div>
      <!-- /main -->




      <div class="extra">

        <div class="extra-inner">

          <div class="container">

            <div class="row">

            </div>
            <!-- /row -->

          </div>
          <!-- /container -->

        </div>
        <!-- /extra-inner -->

      </div>
      <!-- /extra -->




      <div class="footer">

        <div class="footer-inner">

          <div class="container">

            <div class="row">

              <div class="span12">
                &copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
              </div>
              <!-- /span12 -->

            </div>
            <!-- /row -->

          </div>
          <!-- /container -->

        </div>
        <!-- /footer-inner -->

      </div>
      <!-- /footer -->



      <script src="js/jquery-1.7.2.min.js"></script>

      <script src="js/bootstrap.js"></script>
      <script src="js/base.js"></script>


  </body>

  </html>
