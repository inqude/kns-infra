<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: apf_project_master_list.php
CREATED ON	: 10-Jan-2017
CREATED BY	: Lakshmi
PURPOSE     : List of project for customer withdrawals
*/

/* DEFINES - START */
define('SURVEY_MASTER_SURVEY_LIST_FUNC_ID','341');
/* DEFINES - END */

/*
TBD: 
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'survey'.DIRECTORY_SEPARATOR.'survey_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'survey'.DIRECTORY_SEPARATOR.'survey_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_masters'.DIRECTORY_SEPARATOR.'bd_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_projects'.DIRECTORY_SEPARATOR.'bd_project_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

	$alert_type = -1;
	$alert = "";
if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];
	
	// Get permission settings for this user for this page
	$add_perms_list    = i_get_user_perms($user,'',SURVEY_MASTER_SURVEY_LIST_FUNC_ID,'1','1');
	$view_perms_list   = i_get_user_perms($user,'',SURVEY_MASTER_SURVEY_LIST_FUNC_ID,'2','1');
	$edit_perms_list   = i_get_user_perms($user,'',SURVEY_MASTER_SURVEY_LIST_FUNC_ID,'3','1');
	$delete_perms_list = i_get_user_perms($user,'',SURVEY_MASTER_SURVEY_LIST_FUNC_ID,'4','1');

	// Query String Data
	// Nothing
	
	$search_village   = "";
	
	if(isset($_POST['file_search_submit']))
	{
		$search_village    = $_POST["search_village"];
	}
	
	 // Get Village Master modes already added
	$village_master_list = i_get_village_list('');
	if($village_master_list['status'] == SUCCESS)
	{
		$village_master_list_data = $village_master_list["data"];
	}
	else
	{
		$alert = $village_master_list["data"];
		$alert_type = 0;
	}

	// Get Survey already added
	$survey_master_search_data = array("active"=>'1',"village"=>$search_village);
	$survey_master_list = i_get_survey_master($survey_master_search_data);
	if($survey_master_list['status'] == SUCCESS)
	{
		$survey_master_list_data = $survey_master_list['data'];
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Survey Master List</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>
    

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Survey Master List&nbsp;&nbsp;&nbsp;&nbsp;Extent:<i>&nbsp;&nbsp;</i>&nbsp;&nbsp;&nbsp;<span id="total_extent" style="float:right; padding-right:720px;"></h3><a href="survey_master_add_survey.php">Add Survey</a></span>
            </div>
            <!-- /widget-header -->
			<div class="widget-header" style="height:50px; padding-top:10px;">
			<form method="post" id="file_search_form" action="survey_master_survey_list.php">
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_village">
			  <option value="">- - Select Village - -</option>
			  <?php
			  for($village_count = 0; $village_count < count($village_master_list_data); $village_count++)
			  {
			  ?>
			  <option value="<?php echo $village_master_list_data[$village_count]["village_id"]; ?>" <?php if($search_village == $village_master_list_data[$village_count]["village_id"]) { ?> selected="selected" <?php } ?>><?php echo $village_master_list_data[$village_count]["village_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>
			  <input type="submit" name="file_search_submit" />
			  </form>
			  </div>
            <div class="widget-content">
			<?php
			if($view_perms_list['status'] == SUCCESS)
			{
			?>
              <table class="table table-bordered">
                <thead>
                  <tr>
				    <th>SL No</th>
					<th>Village</th>
					<th>Parent Survey No</th>
					<th>Survey No</th>
					<th>Land Owner</th>
					<th>Extent</th>
					<th>Remarks</th>
					<th>Added By</th>					
					<th>Added On</th>									
					<th colspan="3" style="text-align:center;">Actions</th>
    					
				</tr>
				</thead>
				<tbody>							
				<?php
				if($survey_master_list["status"] == SUCCESS)
				{
					$sl_no = 0;
					$total_extent = 0;
					for($count = 0; $count < count($survey_master_list_data); $count++)
					{
						$sl_no++;
						
						$total_extent = $total_extent + $survey_master_list_data[$count]["extent"];
					?>
					<tr>
					<td><?php echo $sl_no; ?></td>
					<td><?php echo $survey_master_list_data[$count]["village_name"]; ?></td>
					<td><?php echo $survey_master_list_data[$count]["parent_survey"]; ?></td>
					<td><?php echo $survey_master_list_data[$count]["survey_no"]; ?></td>
					<td><?php echo $survey_master_list_data[$count]["land_owner"]; ?></td>
					<td><?php echo $survey_master_list_data[$count]["extent"]; ?></td>
					<td><?php echo $survey_master_list_data[$count]["remarks"]; ?></td>
					<td><?php echo $survey_master_list_data[$count]["user_name"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($survey_master_list_data[$count][
					"added_on"])); ?></td>
					<td style="word-wrap:break-word;"><?php if($edit_perms_list['status'] == SUCCESS){?><a style="padding-right:10px" href="#" onclick="return go_to_edit_survey_master('<?php echo $survey_master_list_data[$count]["survey_master_id"]; ?>');">Edit </a><?php } ?></td>
					<td><?php if(($survey_master_list_data[$count]["survey_master_active"] == "1") && ($delete_perms_list['status'] == SUCCESS)){?><a href="#" onclick="return delete_survey_master(<?php echo $survey_master_list_data[$count]["survey_master_id"]; ?>);">Delete</a><?php } ?></td>
					<td style="word-wrap:break-word;"><?php if($view_perms_list['status'] == SUCCESS){?><a style="padding-right:10px" href="#" onclick="return go_to_survey_single_process('<?php echo $survey_master_list_data[$count]["survey_master_id"]; ?>');">Single Process</a><?php } ?></td>
					</tr>
					<?php
					}
					
				}
				else
				{
				?>
				<td colspan="6">No Survey Master added yet!</td>
				
				<?php
				}
				 ?>	

                </tbody>
              </table>
			  
			   <script>

			  document.getElementById('total_extent').innerHTML = <?php echo $total_extent; ?>;

			  </script>
			<?php
			}
			else
			{
				echo 'You are not authorized to view this page';
			}
			?>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function delete_survey_master(survey_id)
{
	var ok = confirm("Are you sure you want to Delete?")
	{         
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else					
					{
					 window.location = "survey_master_survey_list.php";
					}
				}
			}

			xmlhttp.open("POST", "ajax/survey_master_delete_survey.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("survey_id=" + survey_id + "&action=0");
		}
	}	
}
function go_to_edit_survey_master(survey_id)
{		
	var form = document.createElement("form");
    form.setAttribute("method", "GET");
    form.setAttribute("action", "survey_master_edit_survey.php");
	
	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","survey_id");
	hiddenField1.setAttribute("value",survey_id);
	
	form.appendChild(hiddenField1);
	
	document.body.appendChild(form);
    form.submit();
}

function go_to_survey_single_process(survey_id)
{		
	var form = document.createElement("form");
    form.setAttribute("method", "GET");
    form.setAttribute("action", "survey_add_single_process.php");
	
	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","survey_id");
	hiddenField1.setAttribute("value",survey_id);
	
	form.appendChild(hiddenField1);
	
	document.body.appendChild(form);
    form.submit();
}
</script>

  </body>

</html>