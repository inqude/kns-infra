<?php
/* Database connection information */

function filter_array_data($array, $key){
  $temp_array = array();
  $i = 0;
  $key_array = array();

  foreach($array as $val) {
      if (!in_array($val[$key], $key_array)) {
          $key_array[$i] = $val[$key];
          $temp_array[$i] = $val;
      }
      $i++;
  }
  return $temp_array;
}

function create_json_file($project_id,$json)
{
  $dir = "/kns/Legal/gantchart_json" ;
  $filename = $_SERVER['DOCUMENT_ROOT'].$dir. '/Project_'.$project_id.'.json';
  $myfile = fopen($filename, "w") or die("Unable to open file!");
  fwrite($myfile, $json);
  fclose($myfile);
}
function get_conn_handle()
{
  $db     = 'kns_legal';
	$dbhost = '192.168.0.150:3307';
	$dbuser = 'mysql_user';
	$dbpass = 'mysql_pass';

	try
	{

		$db_conn_handle = new PDO('mysql:host='.$dbhost.';dbname='.$db, $dbuser, $dbpass);
  }
    catch (PDOException $e)
	{
		$db_conn_handle = DB_CONN_FAILURE;
		$error = $e->getMessage();
	}
  return $db_conn_handle;
}
$mysqli = get_conn_handle();

//Get project list
// Project data
$gantchart_data = array();

// $time_start = microtime(true);
$query_for_project = 'SELECT * from project_management_project_master where project_master_active =1';
$query_for_project_results = $mysqli->query($query_for_project);
$query_for_project_data = $query_for_project_results->fetchAll();
$total_work_to_be_completed_for_project = 0;
$gantchart_data  =array();
for($project_count = 0 ;$project_count<count($query_for_project_data); $project_count++)
{
  array_push($gantchart_data, $query_for_project_data[$project_count]["project_management_master_id"]);
    // create_json_file($query_for_project_data[$project_count]["project_management_master_id"],json_encode($gantchart_data, JSON_PRETTY_PRINT));
}
?>
