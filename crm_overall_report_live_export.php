<?php

/* SESSION INITIATE - START */

session_start();

/* SESSION INITIATE - END */



/*

FILE		: Attendance Report

CREATED ON	: 26-Mar-2016

CREATED BY	: Nitin Kashyap

PURPOSE     : Attendance Report

*/



/*

TBD: 

*/



// Includes

$base = $_SERVER["DOCUMENT_ROOT"];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_post_sales_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_masters'.DIRECTORY_SEPARATOR.'crm_masters_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_sales_process.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_projects'.DIRECTORY_SEPARATOR.'crm_project_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'PHPExcel-1.8'.DIRECTORY_SEPARATOR.'Classes'.DIRECTORY_SEPARATOR.'PHPExcel.php');



if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))

{

	// Session Data

	$user 		   = $_SESSION["loggedin_user"];

	$role 		   = $_SESSION["loggedin_role"];

	$loggedin_name = $_SESSION["loggedin_user_name"];





	// Temp data

	$alert      = "";

	$alert_type = "";		
	
	$is_not_started = "";
	// Query String
	
	if(isset($_GET["project_id"]))

	{

		$project_id = $_GET["project_id"];

	}

	else

	{

		$project_id = "";

	}

	if(isset($_GET["added_by"]))

	{

		$added_by = $_GET["added_by"];

	}

	else

	{

		$added_by = "";

	}

	

	if(isset($_GET["start_date"]))

	{

		$start_date = $_GET["start_date"];

	}

	else

	{

		$start_date = "";

	}

	

	if(isset($_GET["end_date"]))

	{

		$end_date = $_GET["end_date"];

	}

	else

	{

		$end_date = "";

	}
	
	if(isset($_GET["site_no"]))

	{

		$site_no = $_GET["site_no"];

	}

	else

	{

		$site_no = "";

	}
	
	if(isset($_GET["status"]))

	{

		$status = $_GET["status"];

	}

	else

	{

		$status = "";

	}
	
	if(isset($_GET["is_no_date"]))

	{

		$is_no_date = $_GET["is_no_date"];

	}

	else

	{

		$is_no_date = "";

	}
	
	if(isset($_GET["pending_amount"]))

	{

		$pending_amount = $_GET["pending_amount"];

	}

	else

	{

		$pending_amount = "";

	}
	
	if(isset($_GET["others_pending_amount"]))

	{

		$others_pending_amount = $_GET["others_pending_amount"];

	}

	else

	{

		$others_pending_amount = "";

	}
	
	if(isset($_GET["delay_rsn"]))

	{

		$delay_rsn = $_GET["delay_rsn"];

	}

	else

	{

		$delay_rsn = "";

	}

	
	

	// Get list of files
	if($is_no_date == '1')

	{

		$booking_list = i_get_no_date_list($project_id,$status,$added_by,$start_date,$end_date,$site_no);

		if($booking_list["status"] == SUCCESS)

		{

			$booking_list_data = $booking_list["data"];

		}

	}

	else

	{

		switch($status)

		{

		case "1": // Booking

		$booking_list = i_get_site_booking('',$project_id,'','','','1',$added_by,'','','','','','','',$start_date,$end_date,$site_no);

		if($booking_list["status"] == SUCCESS)

		{

			$booking_list_data = $booking_list["data"];

		}

		break;

		

		case "2": // Agreement

		$booking_list = i_get_agreement_list('','',$project_id,'','1',$start_date,$end_date,$added_by,'','','',$site_no);

		if($booking_list["status"] == SUCCESS)

		{

			$booking_list_data = $booking_list["data"];

		}

		break;

		

		case "3": // Registration

		$booking_list = i_get_registration_list('','',$project_id,'','1',$start_date,$end_date,$added_by,'','','',$site_no);

		if($booking_list["status"] == SUCCESS)

		{

			$booking_list_data = $booking_list["data"];

		}

		break;

		

		case "4": // Katha Transfer

		$booking_list = i_get_katha_transfer_list('','',$project_id,'','1',$start_date,$end_date,$added_by,'','','',$site_no);

		if($booking_list["status"] == SUCCESS)

		{

			$booking_list_data = $booking_list["data"];

		}

		break;

		

		default:

		$booking_list = i_get_site_booking('',$project_id,'','','','1',$added_by,'','','','','','','',$start_date,$end_date,$site_no);

		if($booking_list["status"] == SUCCESS)

		{

			$booking_list_data = $booking_list["data"];

		}

		break;

		}

	}

	

	/* Create excel sheet and write the column headers - START */

	// Instantiate a new PHPExcel object

	$objPHPExcel = new PHPExcel(); 

	// Set the active Excel worksheet to sheet 0

	$objPHPExcel->setActiveSheetIndex(0); 

	// Initialise the Excel row number

	$row_count = 1; 

	// Excel column identifier array

	$column_array = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL');

	

	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[0].$row_count, "ENQUIRY NO");

	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[1].$row_count, "MORTGAGE BANK"); 

	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[2].$row_count, "BANK LOAN/SELF"); 

	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[3].$row_count, "PROJECT"); 

	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[4].$row_count, "SITE NO"); 

	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[5].$row_count, "TOTAL AREA"); 

	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[6].$row_count, "CLIENT"); 	

	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[7].$row_count, "PHONE NO"); 

	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[8].$row_count, "EMAIL ID"); 

	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[9].$row_count, "ESTIMATED PRICE"); 

	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[10].$row_count, "BASIC AMOUNT"); 

	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[11].$row_count, "RECEIVED AMOUNT"); 

	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[12].$row_count, "BOOKED AMOUNT"); 

	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[13].$row_count, "APPROVED BOOKED DATE"); 
	
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[14].$row_count, "CRM BOOKED DATE");
	
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[15].$row_count, "BOOKED DELAY DAYS");
	
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[16].$row_count, "AGREEMENT PLANNED AMOUNT"); 
	
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[17].$row_count, "AGREEMENT PLANNED DATE"); 
	
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[18].$row_count, "AGREEMENT ACTUAL AMOUNT RECEIVED"); 
	
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[19].$row_count, "AGREEMENT DATE"); 
	
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[20].$row_count, "AGREEMENT BALANCE AMOUNT"); 
	
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[21].$row_count, "AGREEMENT DELAY DAYS"); 
	
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[22].$row_count, "REGISTRATION PLANNED AMOUNT"); 
	
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[23].$row_count, "REGISTRATION PLANNED DATE"); 
	
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[24].$row_count, "REGISTRATION ACTUAL AMOUNT RECEIVED"); 
	
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[25].$row_count, "REGISTRATION DATE"); 
	
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[26].$row_count, "REGISTRATION DELAY DAYS"); 
	
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[27].$row_count, "KATHA TRANSFER PLANNED DATE");
	
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[28].$row_count, "KATHA TRANSFER DATE"); 
	
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[29].$row_count, "KATHA TRANSFER LEAD TIME");	
	
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[30].$row_count, "BOOKED BY");	
	
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[31].$row_count, "SOURCE");	
	
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[32].$row_count, "DELAY");
	
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[33].$row_count, "SALES CLOSURE LEAD TIME");	
	
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[34].$row_count, "OTHERS - AMOUNT");
	
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[35].$row_count, "OTHERS - RECEIVED AMOUNT");	
	
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[36].$row_count, "OTHERS - PENDING AMOUNT");
	
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[37].$row_count, "STATUS");	
	


	$style_array = array('font' => array('bold' => true));

	$objPHPExcel->getActiveSheet()->getStyle($column_array[0].$row_count.':'.$column_array[37].$row_count)->applyFromArray($style_array);

	$row_count++;
	
	$total_area                = 0;

	$sum_amount                = 0;

	$sum_received_amount       = 0;

	$sum_pending_amount        = 0;

	$sum_other_amount          = 0;

	$sum_other_received_amount = 0;

	$sum_other_pending_amount  = 0;

	if($booking_list["status"] == SUCCESS)

	{

		$booking_list_data = $booking_list['data'];

		$sl_no           = 0;

		

		for($crm_file_count = 0; $crm_file_count < count($booking_list_data); $crm_file_count++)

		{

			$sl_no++;
			// Get customer profile details, if entered

			$cust_details = i_get_cust_profile_list('',$booking_list_data[$crm_file_count]["crm_booking_id"],'','','','','','','','');

			if($cust_details["status"] == SUCCESS)

			{

				if($cust_details["data"][0]["crm_customer_funding_type"] == '2')

				{

					$bank_loan = $cust_details["data"][0]["crm_bank_name"];

				}

				else if($cust_details["data"][0]["crm_customer_funding_type"] == '3')

				{

					$bank_loan = "Waiting for info";

				}

				else

				{

					$bank_loan = "Self Funding";

				}

			}

			else

			{

				$bank_loan = "";

			}

		

			// Current Status Data										

			if($booking_list_data[$crm_file_count]["crm_booking_date"] != "0000-00-00")

			{

				$booking_date = date("d-M-Y",strtotime($booking_list_data[$crm_file_count]["crm_booking_date"])); 
				$process_status = "BOOKED";

			}

			else

			{

				$booking_date = "NA. Approved Date: ".date("d-M-Y",strtotime($booking_list_data[$crm_file_count]["crm_booking_approved_on"]));
				$process_status = "";

			}												

									

			$agreement_list = i_get_agreement_list('',$booking_list_data[$crm_file_count]["crm_booking_id"],'','','','','','','','','');

			if($agreement_list["status"] == SUCCESS)

			{

				$agreement_date = date("d-M-Y",strtotime($agreement_list["data"][0]["crm_agreement_date"])); 
				$process_status = "AGREEMENT";

			}

			else

			{

				$agreement_date = "" ;
				$process_status = "" ;

			}											
			
			$registration_list = i_get_registration_list('',$booking_list_data[$crm_file_count]["crm_booking_id"],'','','','','','','','','');

			if($registration_list["status"] == SUCCESS)

			{

				$registration_date = date("d-M-Y",strtotime($registration_list["data"][0]["crm_registration_date"])); 
				$process_status = "REGISTERED";

			}

			else

			{

				$registration_date = "";
				$process_status = "";

			}						

									

			$kt_list = i_get_katha_transfer_list('',$booking_list_data[$crm_file_count]["crm_booking_id"],'','','','','','','','','','');

			if($kt_list["status"] == SUCCESS)

			{

				$kt_date = date("d-M-Y",strtotime($kt_list["data"][0]["crm_katha_transfer_date"])); 

			}

			else

			{

				$kt_date = "";

			}												

			

			// Payment Data

			$total_payment_done = 0;
			$total_booking_amount = 0;
			$total_agreement_amount = 0;
			$registration_amount = 0;
			$total_registration_amount = 0;

			$payment_details = i_get_payment('',$booking_list_data[$crm_file_count]["crm_booking_id"],'','','');

			if($payment_details["status"] == SUCCESS)

			{

				for($pay_count = 0; $pay_count < count($payment_details["data"]); $pay_count++)

				{
					if($payment_details["data"][$pay_count]["crm_payment_milestone"] == 'BOOKING')
					{
						$total_booking_amount = $total_booking_amount + $payment_details["data"][$pay_count]["crm_payment_amount"];
					}
					elseif($payment_details["data"][$pay_count]["crm_payment_milestone"] == 'AGREEMENT')
					{
						$total_agreement_amount = $total_agreement_amount + $payment_details["data"][$pay_count]["crm_payment_amount"];
					}
					elseif($payment_details["data"][$pay_count]["crm_payment_milestone"] == 'REGISTRATION')
					{
						$total_registration_amount = $total_registration_amount + $payment_details["data"][$pay_count]["crm_payment_amount"];
					}
					else
					{
						$total_booking_amount = 0;
						$total_agreement_amount = 0;
						$total_registration_amount = 0;
					}
					$total_payment_done = $total_payment_done + $payment_details["data"][$pay_count]["crm_payment_amount"];
				}

			}

			else						

			{
				$total_payment_done = 0;
				$booking_amount = 0;
				$agreement_amount = 0;
				$registration_amount = 0;
			}
			
			$booking_schedule_date = "0000-00-00";
			$registration_schedule_date = "0000-00-00";
			$khatha_schedule_date = "0000-00-00";
			$agreement_schedule_date = "0000-00-00";
			$total_registration_scheduled_amount = "";
			$total_agreement_scheduled_amount = "";
			
			$payment_schedule_details_list = i_get_pay_schedule($booking_list_data[$crm_file_count]["crm_booking_id"],'','','','','');
			if($payment_schedule_details_list["status"] == SUCCESS)
			{
				$payment_schedule_details_list_data = $payment_schedule_details_list["data"];
				for($pay_schedule = 0 ; $pay_schedule < count($payment_schedule_details_list_data); $pay_schedule++)
				{
					if($payment_schedule_details_list["data"][$pay_schedule]["crm_payment_schedule_milestone"] == "BOOKING")
					{
						$booking_schedule_date = date("d-M-y",strtotime($payment_schedule_details_list["data"][$pay_schedule]["crm_payment_schedule_date"]));
					}
					elseif($payment_schedule_details_list["data"][$pay_schedule]["crm_payment_schedule_milestone"] == "REGISTRATION")
					{
						$registration_schedule_date = date("d-M-y",strtotime($payment_schedule_details_list["data"][$pay_schedule]["crm_payment_schedule_date"]));
						$total_registration_scheduled_amount = $total_registration_scheduled_amount + $payment_schedule_details_list["data"][$pay_schedule]["crm_payment_schedule_amount"];
					}
					elseif($payment_schedule_details_list["data"][$pay_schedule]["crm_payment_schedule_milestone"] == "AGREEMENT")
					{
						$agreement_schedule_date = date("d-M-y",strtotime($payment_schedule_details_list["data"][$pay_schedule]["crm_payment_schedule_date"]));
						$total_agreement_scheduled_amount = $total_agreement_scheduled_amount + $payment_schedule_details_list["data"][$pay_schedule]["crm_payment_schedule_amount"];
					}
					elseif($payment_schedule_details_list["data"][$pay_schedule]["crm_payment_schedule_milestone"] == "KHATHA TRANSFER")
					{
						$khatha_schedule_date = date("d-M-y",strtotime($payment_schedule_details_list["data"][$pay_schedule]["crm_payment_schedule_date"]));
						$total_agreement_scheduled_amount = $total_agreement_scheduled_amount + $payment_schedule_details_list["data"][$pay_schedule]["crm_payment_schedule_amount"];
					}
					else
					{
						
					}
				}
			}
			else
			{
				$booking_schedule_date = "0000-00-00";
				$registration_schedule_date = "0000-00-00";
				$khatha_schedule_date = "0000-00-00";
				$agreement_schedule_date = "0000-00-00";
				$total_registration_scheduled_amount = "";
				$total_agreement_scheduled_amount = "";
			}

			// Other Payment - To be paid data

			$other_total_payment = 0;

			$other_payment_details = i_get_payment_terms($booking_list_data[$crm_file_count]["crm_booking_id"]);

			if($other_payment_details["status"] == SUCCESS)

			{

				$other_total_payment = $other_payment_details["data"][0]["crm_pt_layout_maintenance_charges"] + $other_payment_details["data"][0]["crm_pt_water_charges"] + $other_payment_details["data"][0]["crm_pt_club_house_charges"] + $other_payment_details["data"][0]["crm_pt_documentation_charges"] + $other_payment_details["data"][0]["crm_pt_other_charges"];

			}

			else						

			{

				$other_total_payment = 0;

			}

			

			// Other Payment Data						

			$other_total_payment_done_data = i_get_other_payment_total($booking_list_data[$crm_file_count]["crm_booking_id"],'');						

			if(($other_total_payment_done_data["data"] == NULL) || ($other_total_payment_done_data["data"] == "NULL"))

			{

				$other_total_payment_done = 0;

			}

			else

			{

				$other_total_payment_done = $other_total_payment_done_data["data"];

			}

			
			
			// Get site list	

			$site_list = i_get_site_list('','','','','','','',$booking_list_data[$crm_file_count]["crm_booking_site_id"]);

			if($site_list["status"] == SUCCESS)

			{

				$site_list_data      = $site_list["data"];
				$mortgage_bank_name  = $site_list["data"][0]["crm_bank_name"];

			}

			else

			{

				$alert = $alert."Alert: ".$site_list["data"];
				$mortgage_bank_name = "";
			}

			// Get delay reason data

			$delay_reason_list = i_get_delay_reason_list($booking_list_data[$crm_file_count]["crm_booking_id"],'','1');

			if($delay_reason_list["status"] == SUCCESS)

			{

				$latest_delay_reason    = strtoupper($delay_reason_list["data"][0]["crm_delay_reason_master_name"]);

				$latest_delay_reason_id = $delay_reason_list["data"][0]["crm_delay_reason_master_id"];

			}

			else

			{

				$latest_delay_reason    = 'NO REASON';

				$latest_delay_reason_id = '0';

			}

			if($booking_list_data[$crm_file_count]["crm_booking_consideration_area"] != "0")

			{

				$consideration_area = $booking_list_data[$crm_file_count]["crm_booking_consideration_area"];						

			}

			else

				
			{

				$consideration_area = $booking_list_data[$crm_file_count]["crm_site_area"];

			}

			

			$pending_payment	   = ($consideration_area * $booking_list_data[$crm_file_count]["crm_booking_rate_per_sq_ft"]) - $total_payment_done;

			$other_pending_payment = ($other_total_payment - $other_total_payment_done);

			

			if($booking_list_data[$crm_file_count]["crm_booking_status"] == "1")

			{

				if(($delay_rsn == '') || ($latest_delay_reason_id == $delay_rsn))

				{

					if(($pending_amount == '') || ($pending_payment > 0))

					{

						if(($others_pending_amount == '') || ($other_pending_payment > 0))

						{

							$total_area = $total_area + $booking_list_data[$crm_file_count]["crm_site_area"];
							
							$rate_per_sqft = $booking_list_data[$crm_file_count]["crm_booking_rate_per_sq_ft"];
							$dimension = $booking_list_data[$crm_file_count]["crm_booking_consideration_area"];
							
							$basic_cost = $rate_per_sqft * $dimension;
							
							$booking_approved_date = date("d-M-Y",strtotime($booking_list_data[$crm_file_count]["crm_booking_approved_on"]));
										
							if($booking_list_data[$crm_file_count]["crm_booking_date"] == "0000-00-00")
							{
								
								$booking_delay_days = get_date_diff($booking_approved_date,date("d-M-Y"));
							}
							else
							{
								$booking_delay_days = get_date_diff($booking_list_data[$crm_file_count]["crm_booking_date"],$booking_approved_date
								);
							}
							
							if($booking_list_data[$crm_file_count]["crm_booking_consideration_area"] != "0")

							{

								$consideration_area = $booking_list_data[$crm_file_count]["crm_booking_consideration_area"];

							}

							else

							{

								$consideration_area = $booking_list_data[$crm_file_count]["crm_site_area"];

							}

							$total_payment_to_be_done = ($consideration_area * $booking_list_data[$crm_file_count]["crm_booking_rate_per_sq_ft"]);
							$sum_amount = $sum_amount + $total_payment_to_be_done;
							
							$sum_pending_amount = $sum_amount - $sum_received_amount;
							$agreement_delay_days = get_date_diff($agreement_schedule_date,$agreement_date);
							$registration_delay_days = get_date_diff($registration_schedule_date,$registration_date);
							
							if(($khatha_schedule_date == "0000-00-00") && ($kt_date == "0000-00-00"))
							{
								$khatha_delay_days = get_date_diff($registration_date,date("d-M-y"));
								$khatha_delay_days_data = $khatha_delay_days["data"] + 1;
								
							}
							elseif($khatha_schedule_date != "0000-00-00")
							{
								$khatha_delay_days = get_date_diff($khatha_schedule_date,date("d-M-y"));
								$khatha_delay_days_data = $khatha_delay_days["data"] + 1;
							}
							elseif(($khatha_schedule_date == "0000-00-00") && ($kt_date != "0000-00-00"))
							{
								$khatha_delay_days = get_date_diff($registration_date,$kt_date);
								$khatha_delay_days_data = $khatha_delay_days["data"] + 1;
							}
							elseif(($kt_date != "0000-00-00"))
							{
								$khatha_delay_days = get_date_diff($khatha_schedule_date,$kt_date);
								$khatha_delay_days_data = $khatha_delay_days["data"] + 1;
							}
							else
							{
								$khatha_delay_days = "";
							}
							
							if($khatha_delay_days["status"] != "2") 
							{
								
								$katha_delay_data = $khatha_delay_days["data"] + 1 ;
							
							}
							else 
							{ 
								$katha_delay_data = $khatha_delay_days["data"]; 
							}
							
							if($kt_date != "")
							{
								$total_lead_time = get_date_diff($booking_approved_date,$kt_date);
							}
							else
							{
								$total_lead_time = get_date_diff($booking_approved_date,date("d-M-y"));
							}
							$balance_agreement_amount = $total_agreement_scheduled_amount - $total_agreement_amount;

	
							$objPHPExcel->getActiveSheet()->SetCellValue($column_array[0].$row_count, $booking_list_data[$crm_file_count]["enquiry_number"]); 

							$objPHPExcel->getActiveSheet()->SetCellValue($column_array[1].$row_count, $mortgage_bank_name); 

							$objPHPExcel->getActiveSheet()->SetCellValue($column_array[2].$row_count, $bank_loan); 

							$objPHPExcel->getActiveSheet()->SetCellValue($column_array[3].$row_count, $booking_list_data[$crm_file_count]["project_name"]); 

							$objPHPExcel->getActiveSheet()->SetCellValue($column_array[4].$row_count, $booking_list_data[$crm_file_count]["crm_site_no"]); 

							$objPHPExcel->getActiveSheet()->SetCellValue($column_array[5].$row_count, $booking_list_data[$crm_file_count]["crm_site_area"].' sq. ft'); 			

							$objPHPExcel->getActiveSheet()->SetCellValue($column_array[6].$row_count, $booking_list_data[$crm_file_count]["name"]);

							$objPHPExcel->getActiveSheet()->SetCellValue($column_array[7].$row_count, $booking_list_data[$crm_file_count]["cell"]); 

							$objPHPExcel->getActiveSheet()->SetCellValue($column_array[8].$row_count,$booking_list_data[$crm_file_count]["email"]); 

							$objPHPExcel->getActiveSheet()->SetCellValue($column_array[9].$row_count, $booking_list_data[$crm_file_count]["crm_booking_rate_per_sq_ft"]); 	

							$objPHPExcel->getActiveSheet()->SetCellValue($column_array[10].$row_count, $total_payment_to_be_done); 	
							
							$objPHPExcel->getActiveSheet()->SetCellValue($column_array[11].$row_count, $total_payment_done); 			

							$objPHPExcel->getActiveSheet()->SetCellValue($column_array[12].$row_count, $total_booking_amount); 

							$objPHPExcel->getActiveSheet()->SetCellValue($column_array[13].$row_count, $booking_approved_date);
							
							$objPHPExcel->getActiveSheet()->SetCellValue($column_array[14].$row_count, $booking_list_data[$crm_file_count]["crm_booking_date"]);
							
							$objPHPExcel->getActiveSheet()->SetCellValue($column_array[15].$row_count, $booking_delay_days["data"]);
							
							$objPHPExcel->getActiveSheet()->SetCellValue($column_array[16].$row_count, $total_agreement_scheduled_amount);
							
							$objPHPExcel->getActiveSheet()->SetCellValue($column_array[17].$row_count, $agreement_schedule_date);
							
							$objPHPExcel->getActiveSheet()->SetCellValue($column_array[18].$row_count, $total_agreement_amount);
							
							$objPHPExcel->getActiveSheet()->SetCellValue($column_array[19].$row_count, $agreement_date);
							
							$objPHPExcel->getActiveSheet()->SetCellValue($column_array[20].$row_count, $balance_agreement_amount);
							
							$objPHPExcel->getActiveSheet()->SetCellValue($column_array[21].$row_count, $agreement_delay_days["data"]);
							
							$objPHPExcel->getActiveSheet()->SetCellValue($column_array[22].$row_count, $total_registration_scheduled_amount);
							
							$objPHPExcel->getActiveSheet()->SetCellValue($column_array[23].$row_count, $registration_schedule_date);
							
							$objPHPExcel->getActiveSheet()->SetCellValue($column_array[24].$row_count, $total_registration_amount);
							
							$objPHPExcel->getActiveSheet()->SetCellValue($column_array[25].$row_count, $registration_date);
							
							$objPHPExcel->getActiveSheet()->SetCellValue($column_array[26].$row_count, $registration_delay_days["data"]);
							
							$objPHPExcel->getActiveSheet()->SetCellValue($column_array[27].$row_count, $khatha_schedule_date);
							
							$objPHPExcel->getActiveSheet()->SetCellValue($column_array[28].$row_count, $kt_date);
							
							$objPHPExcel->getActiveSheet()->SetCellValue($column_array[29].$row_count, $katha_delay_data);
							
							$objPHPExcel->getActiveSheet()->SetCellValue($column_array[30].$row_count, $booking_list_data[$crm_file_count]["stm"]);
							
							$objPHPExcel->getActiveSheet()->SetCellValue($column_array[31].$row_count, $booking_list_data[$crm_file_count]["enquiry_source_master_name"]);
							
							$objPHPExcel->getActiveSheet()->SetCellValue($column_array[32].$row_count, $latest_delay_reason);
							
							$objPHPExcel->getActiveSheet()->SetCellValue($column_array[33].$row_count, $total_lead_time["data"]);
							
							$objPHPExcel->getActiveSheet()->SetCellValue($column_array[34].$row_count, $other_total_payment);
							
							$objPHPExcel->getActiveSheet()->SetCellValue($column_array[35].$row_count, $other_total_payment_done);
							
							$objPHPExcel->getActiveSheet()->SetCellValue($column_array[36].$row_count, ($other_total_payment - $other_total_payment_done));
							
							$objPHPExcel->getActiveSheet()->SetCellValue($column_array[37].$row_count, $process_status);

							$row_count++;
						}
					}
				}
			}
		}
	}

	$objPHPExcel->getActiveSheet()->getStyle($column_array[0].'0'.':'.$column_array[37].$row_count)->getAlignment()->setWrapText(true); 	

	

	$objPHPExcel->getActiveSheet()->mergeCells($column_array[0].$row_count.':'.$column_array[37].$row_count);

	$row_count++;



	$objPHPExcel->getActiveSheet()->getStyle($column_array[0].'1'.':'.$column_array[37].($row_count - 1))->getAlignment()->setWrapText(true); 	

	/* Create excel sheet and write the column headers - END */





	header('Content-Type: application/vnd.ms-excel'); 

	header('Content-Disposition: attachment;filename="CRM - Live.xls"'); 

	header('Cache-Control: max-age=0'); 

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5'); 

	$objWriter->save('php://output');

}		

else

{

	header("location:login.php");

}	

?>