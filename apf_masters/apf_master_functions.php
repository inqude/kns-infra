<?php
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'da_apf_project.php');

/*
PURPOSE : To add new APF Details
INPUT 	: Project, Village, Planned Start Date, Planned End Date, Remarks, Added By
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_add_apf_details($project,$village,$bank,$planned_start_date,$planned_end_date,$remarks,$added_by)
{
	$apf_details_search_data = array("project"=>$project,"bank"=>$bank,"active"=>'1');
	$apf_details_sresult = db_get_apf_details($apf_details_search_data);
	
	if($apf_details_sresult["status"] == DB_NO_RECORD)
	{
		$apf_details_iresult = db_add_apf_details($project,$village,$bank,$planned_start_date,$planned_end_date,$remarks,$added_by);
		
		if($apf_details_iresult['status'] == SUCCESS)
		{
			$return["data"]   = $apf_details_iresult["data"];
			$return["status"] = SUCCESS;	
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "APF Details already exists!";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To get APF Details List
INPUT 	: APF ID, Project, Village, Bank, Planned Start Date, Planned End Date, Remarks, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of APF Details, success or failure message
BY 		: Lakshmi
*/
function i_get_apf_details($apf_details_search_data)
{
	$apf_details_sresult = db_get_apf_details($apf_details_search_data);
	
	if($apf_details_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   =$apf_details_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No  APF Details Added. Please Contact The System Admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To update APF Deatils
INPUT 	: APF ID, APF  Details Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_update_apf_details($apf_id,$apf_details_update_data)
{
	$apf_details_search_data = array("project"=>$apf_details_update_data['project'],"bank"=>$apf_details_update_data['bank'],"active"=>'1');
	$apf_details_sresult = db_get_apf_details($apf_details_search_data);
	
	$allow_update = false;
	if($apf_details_sresult["status"] == DB_NO_RECORD)
	{
		$allow_update = true;
	}
	else if($apf_details_sresult["status"] == DB_RECORD_ALREADY_EXISTS)
	{
		if($apf_details_sresult['data'][0]['apf_details_id'] == $apf_id)
		{
			$allow_update = true;
		}
	}
	
	if($allow_update == true)
	{
		$apf_details_sresult = db_update_apf_details($apf_id,$apf_details_update_data);
		
		if($apf_details_sresult['status'] == SUCCESS)
		{
			$return["data"]   = "APF Details Successfully Updated";
			$return["status"] = SUCCESS;							
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "APF Details Already Exists";
		$return["status"] = FAILURE;
	}
	
	return $return;
}

/*
PURPOSE : To Delete APF Details 
INPUT 	: APF ID, APF  Details Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/

function i_delete_apf_details($apf_id,$apf_details_update_data)
{       
	$apf_delete_sresult = db_update_apf_details($apf_id,$apf_details_update_data);
	
	if($apf_delete_sresult['status'] == SUCCESS)
	{
		$apf_process_update_data = array("apf_id"=>$apf_id);
		$apf_delete_iresult = i_delete_all_file_process($apf_process_update_data);
		
		if($apf_delete_iresult['status'] == SUCCESS)
		{
			$return["data"]   = "APF All Details successfully added";
			$return["status"] = SUCCESS;	
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}	
	}
	else
	{
		$return["data"]   = "The APF Details already exists";
		$return["status"] = FAILURE;
	}
	
	return $return;
}
/*
PURPOSE : To add new APF Process
INPUT 	: APF ID, Process ID, Start Date, End Date, Remarks, Added By
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_add_apf_process($apf_id,$process_id,$process_start_date,$process_end_date,$remarks,$added_by)
{   
	$apf_process_iresult =  db_add_apf_process($apf_id,$process_id,$process_start_date,$process_end_date,$remarks,$added_by);
	
	if($apf_process_iresult['status'] == SUCCESS)
	{
			$return["data"]   = $apf_process_iresult['data'] ;
			$return["status"] = SUCCESS;	
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	
	return $return;
}

/*
PURPOSE : To get APF Process List
INPUT 	: APF Process ID, APF ID, Process ID, Start Date, End Date, Remarks, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of APF Process, success or failure message
BY 		: Lakshmi
*/
function i_get_apf_process($apf_process_search_data)
{
	$apf_process_sresult = db_get_apf_process($apf_process_search_data);
	
	if($apf_process_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   =$apf_process_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No  APF Process Added. Please Contact The System Admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To update APF Process 
INPUT 	: APF Process ID, APF Process Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/

function i_update_apf_process($apf_process_id,$apf_process_update_data)
{       
		$apf_process_sresult = db_update_apf_process($apf_process_id,$apf_process_update_data);
		
		if($apf_process_sresult['status'] == SUCCESS)
		{
			$return["data"]   = "APF Process Successfully Added";
			$return["status"] = SUCCESS;							
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	
	return $return;
}

/*
PURPOSE : To add new APF File
INPUT 	: BD File ID, APF ID, Remarks, Added By
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_add_apf_file($bd_file_id,$apf_id,$remarks,$added_by)
{   
	$apf_file_iresult =  db_add_apf_file($bd_file_id,$apf_id,$remarks,$added_by);
	
	if($apf_file_iresult['status'] == SUCCESS)
	{
			$return["data"]   = $apf_file_iresult['data'] ;
			$return["status"] = SUCCESS;	
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	
	return $return;
}

/*
PURPOSE : To get APF File list
INPUT 	: File ID, BD File ID, APF ID, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of APF File, success or failure message
BY 		: Lakshmi
*/
function i_get_apf_file($apf_file_search_data)
{
	$apf_file_sresult = db_get_apf_file($apf_file_search_data);
	
	if($apf_file_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   =$apf_file_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No  APF File Added. Please Contact The System Admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To update APF File 
INPUT 	: File ID, APF File Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/

function i_update_apf_file($file_id,$apf_file_update_data)
{       
	$apf_file_sresult = db_update_apf_file($file_id,$apf_file_update_data);
	
	if($apf_file_sresult['status'] == SUCCESS)
	{
		$return["data"]   = "APF File Successfully Added";
		$return["status"] = SUCCESS;							
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}
	
	return $return;
}

/*
PURPOSE : To add new APF Document
INPUT 	: Process ID, File Path ID, Remarks, Added By
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_add_apf_document($process_id,$file_path_id,$remarks,$added_by)
{   
	$apf_document_iresult =  db_add_apf_document($process_id,$file_path_id,$remarks,$added_by);
	
	if($apf_document_iresult['status'] == SUCCESS)
	{
			$return["data"]   = $apf_document_iresult['data'] ;
			$return["status"] = SUCCESS;	
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	
	return $return;
}

/*
PURPOSE : To get APF Document list
INPUT 	: Document ID, Process ID, File Path ID, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of APF Document, success or failure message
BY 		: Lakshmi
*/
function i_get_apf_document($apf_document_search_data)
{
	$apf_document_sresult = db_get_apf_document($apf_document_search_data);
	
	if($apf_document_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   =$apf_document_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No  APF Document Added. Please Contact The System Admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To update APF Document 
INPUT 	: Document ID, APF Document Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/

function i_update_apf_document($document_id,$apf_document_update_data)
{       
	$apf_document_sresult = db_update_apf_document($document_id,$apf_document_update_data);
	
	if($apf_document_sresult['status'] == SUCCESS)
	{
		$return["data"]   = "APF Document Successfully Added";
		$return["status"] = SUCCESS;							
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}
	
	return $return;
}

/*
PURPOSE : To add new APF Query
INPUT 	: Process ID, Query, Query By, Remarks, Added By
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_add_apf_query($process_id,$query,$query_by,$remarks,$added_by)
{   
	$apf_query_iresult =  db_add_apf_query($process_id,$query,$query_by,$remarks,$added_by);
	
	if($apf_query_iresult['status'] == SUCCESS)
	{
			$return["data"]   = $apf_query_iresult['data'] ;
			$return["status"] = SUCCESS;	
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	
	return $return;
}

/*
PURPOSE : To get APF Query list
INPUT 	: Query ID, Process ID, Query, Query By, Query Date, Remarks, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of APF Query
BY 		: Lakshmi
*/
function i_get_apf_query($apf_query_search_data)
{
	$apf_query_sresult = db_get_apf_query($apf_query_search_data);
	
	if($apf_query_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   =$apf_query_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No  APF Query Added. Please Contact The System Admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To update APF Query 
INPUT 	: Query ID, APF Query Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/

function i_update_apf_query($query_id,$apf_query_update_data)
{       
	$apf_query_response_sresult = db_update_apf_query($query_id,$apf_query_update_data);
	
	if($apf_query_response_sresult['status'] == SUCCESS)
	{
		$return["data"]   = "APF Query Successfully Added";
		$return["status"] = SUCCESS;							
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}
	
	return $return;
}

/*
PURPOSE : To add new APF Process Master
INPUT 	: Name, Remarks, Added By
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/

function i_add_apf_process_master($name,$remarks,$added_by)
{
	$apf_process_master_search_data = array("name"=>$name,"process_name_check"=>'1',"active"=>'1');
	$apf_process_master_sresult = db_get_apf_process_master($apf_process_master_search_data);
	
	if($apf_process_master_sresult["status"] == DB_NO_RECORD)
	{
		$apf_process_master_iresult = db_add_apf_process_master($name,$remarks,$added_by);
		
		if($apf_process_master_iresult['status'] == SUCCESS)
		{
			$return["data"]   = "APF Process Successfully Added";
			$return["status"] = SUCCESS;	
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "Survey name already exists!";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To get APF Process Master List
INPUT 	: Process ID, Name, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of APF Process Master, success or failure message
BY 		: Lakshmi
*/
function i_get_apf_process_master($apf_process_master_search_data)
{
	$apf_process_master_sresult = db_get_apf_process_master($apf_process_master_search_data);
	
	if($apf_process_master_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   =$apf_process_master_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No APF Process Master Added. Please Contact The System Admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To update APF Process Master 
INPUT 	: Process ID, APF Process Master Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/

function i_update_apf_process_master($process_id,$apf_process_master_update_data)
{
	$apf_process_master_search_data = array("name"=>$apf_process_master_update_data['name'],"process_name_check"=>'1',"active"=>'1');
	$apf_process_master_sresult = db_get_apf_process_master($apf_process_master_search_data);
	
	$allow_update = false;
	if($apf_process_master_sresult["status"] == DB_NO_RECORD)
	{
		$allow_update = true;
	}
	else if($apf_process_master_sresult["status"] == DB_RECORD_ALREADY_EXISTS)
	{
		if($apf_process_master_sresult['data'][0]['apf_process_master_id'] == $process_id)
		{
			$allow_update = true;
		}
	}
	
	if($allow_update == true)
	{
		$apf_process_master_sresult = db_update_apf_process_master($process_id,$apf_process_master_update_data);
		
		if($apf_process_master_sresult['status'] == SUCCESS)
		{
			$return["data"]   = "APF Process Details Successfully Updated";
			$return["status"] = SUCCESS;							
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "APF Name Already Exists";
		$return["status"] = FAILURE;
	}
	
	return $return;
}

/*
PURPOSE : To Delete APF Process Master 
INPUT 	: Process ID, APF Process Master Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/

function i_delete_apf_process_master($process_id,$apf_process_master_update_data)
{   
    $apf_process_master_update_data = array('active'=>'0');
	$apf_process_master_sresult = db_update_apf_process_master($process_id,$apf_process_master_update_data);
	
	if($apf_process_master_sresult['status'] == SUCCESS)
	{
		$return["data"]   = "APF Process Master Successfully Added";
		$return["status"] = SUCCESS;							
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}
	
	return $return;
}

/*
PURPOSE : To add new APF Bank Master
INPUT 	: Name, Remarks, Added By
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/

function i_add_apf_bank_master($name,$remarks,$added_by)
{
	$apf_bank_master_search_data = array("name"=>$name,"bank_name_check"=>'1',"active"=>'1');
	$apf_bank_master_sresult = db_get_apf_bank_master($apf_bank_master_search_data);
	
	if($apf_bank_master_sresult["status"] == DB_NO_RECORD)
	{
		$apf_bank_master_iresult = db_add_apf_bank_master($name,$remarks,$added_by);
		
		if($apf_bank_master_iresult['status'] == SUCCESS)
		{
			$return["data"]   = "APF Bank Successfully Added";
			$return["status"] = SUCCESS;	
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "Bank name already exists!";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To get APF Bank Master List
INPUT 	: Bank ID, Name, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of APF Bank Master, success or failure message
BY 		: Lakshmi
*/
function i_get_apf_bank_master($apf_bank_master_search_data)
{
	$apf_bank_master_sresult = db_get_apf_bank_master($apf_bank_master_search_data);
	
	if($apf_bank_master_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   =$apf_bank_master_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No APF Bank Master Added. Please Contact The System Admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To update APF Process Master 
INPUT 	: Process ID, APF Process Master Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/

function i_update_apf_bank_master($bank_id,$apf_bank_master_update_data)
{
	$apf_bank_master_search_data = array("name"=>$apf_bank_master_update_data['name'],"bank_name_check"=>'1',"active"=>'1');
	$apf_bank_master_sresult = db_get_apf_bank_master($apf_bank_master_search_data);
	
	$allow_update = false;
	if($apf_bank_master_sresult["status"] == DB_NO_RECORD)
	{
		$allow_update = true;
	}
	else if($apf_bank_master_sresult["status"] == DB_RECORD_ALREADY_EXISTS)
	{
		if($apf_bank_master_sresult['data'][0]['apf_bank_master_id'] == $bank_id)
		{
			$allow_update = true;
		}
	}
	
	if($allow_update == true)
	{
		$apf_bank_master_sresult = db_update_apf_bank_master($bank_id,$apf_bank_master_update_data);
		
		if($apf_bank_master_sresult['status'] == SUCCESS)
		{
			$return["data"]   = "APF Bank Details Successfully Updated";
			$return["status"] = SUCCESS;							
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "Bank Name Already Exists";
		$return["status"] = FAILURE;
	}
	
	return $return;
}

/*
PURPOSE : To Delete APF Bank Master 
INPUT 	: Bank ID, APF Bank Master Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/

function i_delete_apf_bank_master($bank_id,$apf_bank_master_update_data)
{   
    $apf_bank_master_update_data = array('active'=>'0');
	$apf_bank_master_sresult = db_update_apf_bank_master($bank_id,$apf_bank_master_update_data);
	
	if($apf_bank_master_sresult['status'] == SUCCESS)
	{
		$return["data"]   = "APF Bank Master Successfully Added";
		$return["status"] = SUCCESS;							
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}
	
	return $return;
}

/*
PURPOSE : To add new APF Project Master
INPUT 	: Name, Remarks, Added By
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/

function i_add_apf_project_master($name,$remarks,$added_by)
{
	$apf_project_master_search_data = array("name"=>$name,"project_name_check"=>'1',"active"=>'1');
	$apf_project_master_sresult = db_get_apf_project_master($apf_project_master_search_data);
	
	if($apf_project_master_sresult["status"] == DB_NO_RECORD)
	{
		$apf_project_master_iresult = db_add_apf_project_master($name,$remarks,$added_by);
		
		if($apf_project_master_iresult['status'] == SUCCESS)
		{
			$return["data"]   = "APF Project Successfully Added";
			$return["status"] = SUCCESS;	
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "Survey name already exists!";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To get APF Project Master List
INPUT 	: Project ID, Name, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of APF Project Master, success or failure message
BY 		: Lakshmi
*/
function i_get_apf_project_master($apf_project_master_search_data)
{
	$apf_project_master_sresult = db_get_apf_project_master($apf_project_master_search_data);
	
	if($apf_project_master_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   =$apf_project_master_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No APF Project Master Added. Please Contact The System Admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To update APF Project Master 
INPUT 	: Project ID, APF Project Master Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/

function i_update_apf_project_master($project_id,$apf_project_master_update_data)
{
	$apf_project_master_search_data = array("name"=>$apf_project_master_update_data['name'],"project_name_check"=>'1',"active"=>'1');
	$apf_project_master_sresult = db_get_apf_project_master($apf_project_master_search_data);
	
	$allow_update = false;
	if($apf_project_master_sresult["status"] == DB_NO_RECORD)
	{
		$allow_update = true;
	}
	else if($apf_project_master_sresult["status"] == DB_RECORD_ALREADY_EXISTS)
	{
		if($apf_project_master_sresult['data'][0]['apf_project_master_id'] == $project_id)
		{
			$allow_update = true;
		}
	}
	
	if($allow_update == true)
	{
		$apf_project_master_sresult = db_update_apf_project_master($project_id,$apf_project_master_update_data);
		
		if($apf_project_master_sresult['status'] == SUCCESS)
		{
			$return["data"]   = "APF Project Details Successfully Updated";
			$return["status"] = SUCCESS;							
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "APF Project Name Already Exists";
		$return["status"] = FAILURE;
	}
	
	return $return;
}

/*
PURPOSE : To Delete APF Project Master 
INPUT 	: Project ID, APF Project Master Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/

function i_delete_apf_project_master($project_id,$apf_project_master_update_data)
{   
    $apf_project_master_update_data = array('active'=>'0');
	$apf_project_master_sresult = db_update_apf_project_master($project_id,$apf_project_master_update_data);
	
	if($apf_project_master_sresult['status'] == SUCCESS)
	{
		$return["data"]   = "APF Project Master Successfully Added";
		$return["status"] = SUCCESS;							
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}
	
	return $return;
}

/*
PURPOSE : To add new APF Survey Master
INPUT 	: Project, Survey No, Land Owner, Extent, Remarks, Added By
OUTPUT 	: Survey ID, success or failure message
BY 		: Lakshmi
*/
function i_add_apf_survey_master($project,$survey_no,$land_owner,$extent,$remarks,$added_by)
{   
	$apf_survey_master_iresult =  db_add_apf_survey_master($project,$survey_no,$land_owner,$extent,$remarks,$added_by);
	
	if($apf_survey_master_iresult['status'] == SUCCESS)
	{
			$return["data"]   = $apf_survey_master_iresult['data'] ;
			$return["status"] = SUCCESS;	
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	
	return $return;
}

/*
PURPOSE : To get APF Survey Master list
INPUT 	: Survey ID, Project, Survey No, Remarks, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of APF Survey Master
BY 		: Lakshmi
*/
function i_get_apf_survey_master($apf_survey_master_search_data)
{
	$apf_survey_master_sresult = db_get_apf_survey_master($apf_survey_master_search_data);
	
	if($apf_survey_master_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   =$apf_survey_master_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No  APF Survey Master Added. Please Contact The System Admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To update APF Query Response
INPUT 	: Response ID, APF Query Response Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/

function i_update_apf_survey_master($survey_id,$apf_survey_master_update_data)
{       
	$apf_survey_master_sresult = db_update_apf_survey_master($survey_id,$apf_survey_master_update_data);
	
	if($apf_survey_master_sresult['status'] == SUCCESS)
	{
		$return["data"]   = "APF Survey Master Successfully Added";
		$return["status"] = SUCCESS;							
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}
	
	return $return;
}

/*
PURPOSE : To get APF Dashboard Pending list
INPUT 	: APF ID, Process End Date
OUTPUT 	: List of APF Dashboard
BY 		: Lakshmi
*/
function i_get_apf_dashboard_pending($apf_dashboard_pending_search_data)
{
	$apf_dashboard_sresult = db_get_apf_dashboard_pending($apf_dashboard_pending_search_data);
	
	if($apf_dashboard_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $apf_dashboard_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No  APF Dashboard Added. Please Contact The System Admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To add new APF Response
INPUT 	: Query ID, response, Follow up Date, Status, Remarks, Added By
OUTPUT 	: Response ID, success or failure message
BY 		: Lakshmi
*/
function i_add_apf_response($query_id,$response,$follow_up_date,$status,$remarks,$added_by)
{   
	$apf_response_iresult =  db_add_apf_response($query_id,$response,$follow_up_date,$status,$remarks,$added_by);
	
	if($apf_response_iresult['status'] == SUCCESS)
	{
			$return["data"]   = $apf_response_iresult['data'] ;
			$return["status"] = SUCCESS;	
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	
	return $return;
}

/*
PURPOSE : To get APF Response list
INPUT 	: Response ID, Query ID, response, Follow Up Date, Remarks, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of APF response
BY 		: Lakshmi
*/
function i_get_apf_response($apf_response_search_data)
{
	$apf_response_sresult = db_get_apf_response($apf_response_search_data);
	
	if($apf_response_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   =$apf_response_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No  APF Response Added. Please Contact The System Admin"; 
    }
	
	return $return;
}

  /*
PURPOSE : To update APF Response 
INPUT 	: Response ID, APF Response Update Array
OUTPUT 	: Response ID; Message of success or failure
BY 		: Lakshmi
*/

function i_update_apf_response($response_id,$apf_response_update_data)
{       
	$apf_response_response_sresult = db_update_apf_response($response_id,$apf_response_update_data);
	
	if($apf_response_response_sresult['status'] == SUCCESS)
	{
		$return["data"]   = "APF Response Successfully Added";
		$return["status"] = SUCCESS;							
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}
	
	return $return;
}

/*
PURPOSE : To Delete All File Process 
INPUT 	: APF ID, APF File Process Update Array
OUTPUT 	: APF ID; Message of success or failure
BY 		: Lakshmi
*/
function i_delete_all_file_process($apf_process_update_data)
{
	$apf_process_update_data = array("apf_id"=>$apf_process_update_data['apf_id']);
	$apf_process_delete_uresult = db_delete_all_process($apf_process_update_data);
	
	$apf_file_update_data = array("apf_id"=>$apf_process_update_data['apf_id']);
	$apf_file_delete_iresult = db_delete_all_apf_file($apf_file_update_data);
	
	if(($apf_file_delete_iresult['status'] == SUCCESS) && ($apf_process_delete_uresult['status'] == SUCCESS))
	{
		$return["data"]   = "APF All Details successfully added";
		$return["status"] = SUCCESS;	
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}
		
	return $return;
}
?>