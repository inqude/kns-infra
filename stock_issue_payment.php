<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 20th Sep 2016
// LAST UPDATED BY: Lakshmi
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* DEFINES - START */
define('STOCK_ISSUE_PAYMENT_FUNC_ID','213');
/* DEFINES - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock'.DIRECTORY_SEPARATOR.'stock_grn_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_masters'.DIRECTORY_SEPARATOR.'crm_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_purchase_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
/* INCLUDES - END */

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];
	
	// Get permission settings for this user for this page
	$view_perms_list   = i_get_user_perms($user,'',STOCK_ISSUE_PAYMENT_FUNC_ID,'2','1');
	$add_perms_list    = i_get_user_perms($user,'',STOCK_ISSUE_PAYMENT_FUNC_ID,'1','1');

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */

	// Query String Data
	if(isset($_REQUEST["payment_release_id"]))
	{
		$payment_release_id = $_REQUEST["payment_release_id"];
	}
	else
	{
		$payment_release_id = "-1";
	} 
	
	if(isset($_REQUEST["po_id"]))
	{
		$po_id = $_REQUEST["po_id"];
	}
	else
	{
		$po_id = "-1";
	}
	
	// Get Released Payment List
	$stock_payment_release_search_data = array('payment_release_id'=>$payment_release_id);
	$released_payment_list =  i_get_stock_payment_release_list($stock_payment_release_search_data);
	if($released_payment_list['status'] == SUCCESS)
	{
		$released_payment_list_data = $released_payment_list['data'];
		$released_amount = $released_payment_list_data[0]["stock_release_payment_amount"];
	}	
	else
	{
		$alert = $released_payment_list["data"];
		$alert_type = 0;		
	}
	//Get purchase Order List
	$stock_purchase_order_search_data = array("order_id"=>$po_id);
	$stock_purchase_order_list = i_get_stock_purchase_order_list($stock_purchase_order_search_data);
	if($stock_purchase_order_list["status"] == SUCCESS)
	{
		$vendor      = $stock_purchase_order_list["data"][0]["stock_purchase_order_vendor"];
		$vendor_name = $stock_purchase_order_list["data"][0]["stock_vendor_name"];
		$po_no       = $stock_purchase_order_list["data"][0]["stock_purchase_order_number"];
	}
	else
	{
		$vendor = "";
	}
	// Capture the form data
	if(isset($_POST["add_payment_submit"]))
	{
		$po_id				= $_POST["hd_po_id"];
		$payment_release_id	= $_POST["hd_payment_release_id"];
		$vendor_id			= $_POST["hd_vendor_id"];
		$amount		        = $_POST["released_amount"];
		$mode		        = $_POST["ddl_mode"];
		$instrument_details = $_POST["txt_details"];
		$remarks     	  	= $_POST["txt_remarks"];
		
		
		// Check for mandatory fields
		if($amount !="")
		{
			$payment_issue_iresult = i_add_stock_issue_payment($payment_release_id,$vendor_id,$amount,$mode,$instrument_details,$remarks,$user);
			if($payment_issue_iresult["status"] == SUCCESS)
			{
				$alert_type = 1;
			}
			else
			{
				$alert_type = 0;
			}		
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}
	//Get Payment Mode 
	$payment_mode_list =  i_get_payment_mode_list('','1');
	if($payment_mode_list['status'] == SUCCESS)
	{
		$payment_mode_list_data = $payment_mode_list['data'];		
	}	
	else
	{
		$alert = $payment_mode_list["data"];
		$alert_type = 0;		
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Payment Issue - Stock Masters</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>    

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
					<?php
					if($view_perms_list['status'] == SUCCESS)
					{
					?>
	      				<i class="icon-user"></i>
	      				<h3>Payment Issue </h3><strong>&nbsp;&nbsp;&nbsp;&nbsp; PO No :<?php echo $po_no ;?>&nbsp;&nbsp;&nbsp;&nbsp; Vendor :<?php echo $vendor_name ;?> &nbsp;&nbsp;&nbsp;&nbsp; Released Amount:<?php echo $released_amount ;?> </strong>
					<?php
					}
					else
					{
						echo 'You are not authorized to view this page';
					}
					?>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Payment Issue </a>
						  </li>						  
						</ul>
						
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="add_unit_form" class="form-horizontal" method="post" action="stock_issue_payment.php">
								<input type="hidden" name="hd_payment_release_id" value="<?php echo $payment_release_id; ?>" />
								<input type="hidden" name="hd_po_id" value="<?php echo $po_id; ?>" />
								<input type="hidden" name="released_amount" value="<?php echo $released_amount; ?>" />
								<input type="hidden" name="hd_vendor_id" value="<?php echo $vendor; ?>" />
									<fieldset>										
													
										<div class="control-group">											
											<label class="control-label" for="ddl_mode">Payment Mode*</label>
											<div class="controls">
												<select name="ddl_mode" required>
												<option>- - -Select Mode- - -</option>
												<?php
												for($count = 0; $count < count($payment_mode_list_data); $count++)
												{
													?>
												<option value="<?php echo $payment_mode_list_data[$count]["payment_mode_id"]; ?>"><?php echo $payment_mode_list_data[$count]["payment_mode_name"]; ?></option>
												<?php
												}
												?>
												</select>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="txt_details">Instrument Details</label>
											<div class="controls">
												<input type="text" class="span6" name="txt_details">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="txt_remarks">Remarks</label>
											<div class="controls">
												<input type="text" class="span6" name="txt_remarks">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
                                                                                                                                                               										 <br />
																					
										<div class="form-actions">
										<?php			  
										if($add_perms_list['status'] == SUCCESS)
										{
										?>
											<input type="submit" class="btn btn-primary" name="add_payment_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
									    <?php
										}
										else
										{
											echo 'You are not authorized to view this page';
										}
										?>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>																
								
							</div>
						  
						</div>					
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->  
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
?>