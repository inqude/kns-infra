<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 4th August 2016
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

/* TBD - START */
// 
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_projects'.DIRECTORY_SEPARATOR.'bd_project_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_masters'.DIRECTORY_SEPARATOR.'bd_masters_functions.php');
/* INCLUDES - END */
if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert = "";
	$alert_type = -1; // No alert
	/* DATA INITIALIZATION - END */
	
	/* QUERY STRING - START */
	if(isset($_POST["project"]))
	{
		$project_id = $_POST["project"];
	}
	else
	{
		$project_id = "";
	}
	/* QUERY STRING - END */

	// Capture the form data
	if(isset($_POST["bd_edit_prof_calc_submit"]))
	{
		$project_id       = $_POST["hd_project"];
		$expected_rate    = $_POST["stxt_expected_rate"];
		$stamp_duty       = $_POST["stxt_stamp_duty"];
		$dev_cost         = $_POST["stxt_dev_cost_rate"];
		$admin_charges    = $_POST["stxt_admin_charge_percent"];
		$finance_rate     = $_POST["stxt_finance_cost_percent"];
		
		// Check for mandatory fields
		if(($project_id !="") && ($expected_rate !="") && ($stamp_duty !="") && ($dev_cost !="") && ($admin_charges !="") && ($finance_rate !=""))
		{
			$finance_working_data = array('rate'=>$expected_rate,'stamp_duty'=>$stamp_duty,'dev_cost'=>$dev_cost,'admin_charges'=>$admin_charges,'finance_rate'=>$finance_rate);
			$edit_bd_prof_calc_result = i_update_bd_prof_calc_settings($project_id,$finance_working_data);
			
			if($edit_bd_prof_calc_result["status"] == SUCCESS)
			{
				$alert = $edit_bd_prof_calc_result["data"];
				$alert_type = 1;
				
				header('location:bd_project_list.php');
			}
			else
			{
				$alert = $edit_bd_prof_calc_result["data"];
				$alert_type = 0;
			}	
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}

	// Get list of BD projects
	$bd_project_list = i_get_bd_project_list($project_id,'','','');
	if($bd_project_list["status"] == SUCCESS)
	{
		$bd_project_list_data = $bd_project_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$bd_project_list["data"];
		$alert_type = 0; // Failure
	}
	
	// Get profitability calculation settings
	$bd_prof_calc_sresult = i_get_bd_prof_calc_settings('',$project_id);
	if($bd_prof_calc_sresult['status'] == SUCCESS)
	{
		$bd_prof_calc_data = $bd_prof_calc_sresult['data'];
	}
	else
	{
		header("location:bd_project_list.php");
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Edit BD Profitability Calculation Settings</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Edit BD Profitability Calculation Settings - <?php echo $bd_prof_calc_data[0]['bd_project_name']; ?></h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Edit Settings</a>
						  </li>						  
						</ul>
						
						<br>
						    <div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="edit_bd_prof_calc_settings" class="form-horizontal" method="post" action="bd_edit_prof_calc_settings.php">
								<input type="hidden" name="hd_project" value="<?php echo $project_id; ?>";
									<fieldset>																				
										
										<div class="control-group">											
											<label class="control-label" for="stxt_expected_rate">Expected Rate *</label>
											<div class="controls">
												<input type="text" class="span6" name="stxt_expected_rate" placeholder="Rate expected per sq. ft" required="required" value="<?php echo $bd_prof_calc_data[0]['bd_profitability_calc_setting_expected_rate']; ?>">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->																														
										<div class="control-group">											
											<label class="control-label" for="stxt_stamp_duty">Registration & Other Charges *</label>
											<div class="controls">
												<input type="text" class="span6" name="stxt_stamp_duty" required="required" value="<?php echo $bd_prof_calc_data[0]['bd_profitability_calc_setting_stamp_duty_charges']; ?>">
												<p class="help-block">Includes Registration, Stamp Duty, Plan Approval, Conversion etc.</p>
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="stxt_dev_cost_rate">Development Cost *</label>
											<div class="controls">
												<input type="text" class="span6" name="stxt_dev_cost_rate" required="required" value="<?php echo $bd_prof_calc_data[0]['bd_profitability_calc_setting_dev_cost_rate']; ?>">
											</div> <!-- /controls -->
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="stxt_admin_charge_percent">Admin, Marketing & Others *</label>
											<div class="controls">
												<input type="number" class="span6" name="stxt_admin_charge_percent" required="required" min="0" step="0.01" value="<?php echo $bd_prof_calc_data[0]['bd_profitability_calc_setting_admin_charges']; ?>">
												<p class="help-block">DONT include % symbol. Only mention the number</p>
											</div> <!-- /controls -->												
										</div> <!-- /control-group -->																											
										<div class="control-group">											
											<label class="control-label" for="stxt_finance_cost_percent">Finance Rate*</label>
											<div class="controls">
												<input type="text" class="span6" name="stxt_finance_cost_percent" value="<?php echo $bd_prof_calc_data[0]['bd_profitability_calc_setting_fianance_rate']; ?>">
												<p class="help-block">DONT include % symbol. Only mention the number</p>
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
                                                                                                                                                               										 <br />
										
											
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="bd_edit_prof_calc_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>																
								
							</div>
						  
						  
						</div>
						
						
						
						
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>