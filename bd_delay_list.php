<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: file_payment_list.php
CREATED ON	: 05-June-2015
CREATED BY	: Nitin Kashyap
PURPOSE     : List of Task Plans for a particular process ID
*/

/*
TBD: 
1. Date display and calculation
2. Session management
3. Linking Tasks
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_projects'.DIRECTORY_SEPARATOR.'bd_project_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Query String Data
	if(isset($_GET["file"]))
	{
		$delay_reasons_file_id = $_GET["file"];
	}
	else	
	{
		$delay_reasons_file_id = "";
	}

	// Temp data
	$alert = "";

	// Get list of delay
	$delay_list = i_get_delay_details($delay_reasons_file_id);

	if($delay_list["status"] == SUCCESS)
	{
		// Get specific delay details
		$delay_list_data = $delay_list["data"];
	}
	else
	{
		$alert = "Invalid File!";
	}	
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Delay Details List</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>    

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Delay Details List</h3><span style="float:right; padding-right:30px;"><a href="bd_add_delay_reason.php?file=<?php echo $delay_reasons_file_id; ?>">Add Delay</a></span>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">			
			<span style="padding-left:10px;">
			
			</span>
			
              <table class="table table-bordered">
                <thead>
                  <tr>					
					<th>File ID</th>
					<th>Reason Id</th>
					<th>Remarks</th>
					<th>Added By</th>										
					<th>Added on</th>
					<th>&nbsp;</th>
				</tr>
				</thead>
				<tbody>
				 <?php				
				if($delay_list["status"] == SUCCESS)
				{
					for($count = 0; $count < count($delay_list_data); $count++)
					{
					?>	
					<tr>
						<td><?php echo $delay_list_data[$count]["bd_delay_reasons_file_id"]; ?></td>						
						<td><?php echo $delay_list_data[$count]["bd_delay_reason_name"]; ?></td>
						<td><?php echo $delay_list_data[$count]["bd_delay_remarks"]; ?></td>
						<td><?php echo $delay_list_data[$count]["user_name"]; ?></td>
						<td><?php echo date('d-M-Y H:i',strtotime($delay_list_data[$count]["bd_delay_added_on"])); ?></td>
						<td><?php
						if($delay_list_data[$count]["bd_delay_reason_is_task"] == '1')
						{
							?>
							<a href="#" onclick="return complete_sub_task(<?php echo $delay_list_data[$count]["bd_delay_reasons_id"]; ?>,<?php echo $delay_reasons_file_id; ?>);">COMPLETE</a>
							<?php
						}
						else if($delay_list_data[$count]["bd_delay_reason_is_task"] == '2')
						{
							echo 'Sub Task Completed';
						}
						else
						{
							echo 'No sub task';
						}
						?></td>
					</tr>
					<?php 
					}
					?>	
				<?php
				
				}				
				else
				{
					?>
					<td colspan="6">No Delay Details for this File!</td>
					<?php
				}
				?>	
                </tbody>
              </table>
			  <br />
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function complete_sub_task(delay_id,file)
{
	var ok = confirm("Are you sure you want to Complete?")
	{         
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{					
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else					
					{
					 window.location = "bd_delay_list.php?file=" + file;
					}
				}
			}

			xmlhttp.open("POST", "update_bd_delay_task.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("delay_id=" + delay_id + "&action=2");
		}
	}	
}
</script>

  </body>

</html>