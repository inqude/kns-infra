 <link href="bootstrap_aku.min.css" rel="stylesheet" type="text/css">
  <link href="https://use.fontawesome.com/releases/v4.7.0/css/font-awesome-css.min.css" rel="stylesheet">

<script src="js/jquery-1.7.2.min1.js"></script>

  <style type="text/css">

.table th {

  position:relative;
  z-index:999;
	/*min-width:100px;*/
}
.table td {
	min-width: 100px;
	}
	.table {
		background:#FFF;
	}
@media only screen and (max-width:1199px)
	{
.table {
	width:auto !important;

	}
	}

.dropdown-menu{

    overflow-y: auto !important;
	`}
  </style>
  <?php
    $base = $_SERVER["DOCUMENT_ROOT"];
    include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_header.php');
  ?>

<script src="js/jquery.dataTables.min.js"></script>
<script src="js/dataTables.bootstrap.min.js"></script>

<script type='text/javascript'>
function hide_th(number){

       $('.table').find("th:nth-child("+number+")").toggle();
  $('.table').find("td:nth-child("+number+")").toggle();

	}
//<![CDATA[
$(window).load(function(){

setTimeout(function(){ $(document).ready(function () {
	var rowCount = $('table tr').length;

$('#loader-wrapper').fadeOut('slow');
if(rowCount>1){

	$(".dropdown-menu").css('max-height',($(window).height()-80)+'px');

	$(".widget-header").height('auto');
	 var cellText='<div class="dropdown col-sm-1"><button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-th" aria-hidden="true"></i><span class="caret"></span></button><div class="column-list dropdown-menu " ></div></div>';

	$( cellText ).insertBefore( ".widget-header form" );
(function ( $ ) {
    'use strict';
     var path = window.location.pathname;
var page = path.split("/").pop();

    $.fn.columnFilter = function( options ) {
        var settings = $.extend({
            columnCheckboxsContainer: '.column-list',
            localStorageNamespace: page,
            headerCell: 'TH'
        }, options );

        var columnList = '';
        var that = this;
        var checked='';
        $(that).find('* > tr').each(function(){
            $(this).children().each(function(index){
                if( localStorage.getItem(settings.localStorageNamespace + index) == 'hide' ) {
                    $(this).hide();
                    checked='';
                } else {
                    $(this).show();
                    checked='checked';
                }
                if( $(this).context.nodeName == settings.headerCell ) {
                    var label = $.trim($(this).text());
                    var checkbox = '<input type="checkbox" '+ checked +' data-label="'+ label +'">';
                    columnList = $.fn.columnFilter.format( checkbox, label );
                    $(settings.columnCheckboxsContainer).append('<div class="col-sm-12">'+columnList+'</div>');
                }
            });
        });

		var options = [];

  $( '.widget-header .dropdown-menu .col-sm-12' ).on( 'click', function( event ) {

   var $target = $( event.currentTarget ),
       val = $target.find( 'input' ).val(),
       $inp = $target.find( 'input' ),
       idx;

   if ( ( idx = options.indexOf( val ) ) > -1 ) {
      options.splice( idx, 1 );
      setTimeout( function() { $inp.prop( 'checked', false ) }, 0);
   } else {
      options.push( val );
      setTimeout( function() { $inp.prop( 'checked', true ) }, 0);
   }

   $( event.target ).blur();
   return false;
  });

  $(settings.columnCheckboxsContainer).find('input').bind('click', function(){
      var label = $(this).attr('data-label');

      var index = $(that).find('thead tr th').filter(function() {
          return $.trim($(this).text()) === label;
      }).index();

      if( !$(this).is(':checked') ) {
          localStorage.setItem(settings.localStorageNamespace + index, 'hide');
          $(that).find('thead').children().each(function(i){
              $(this).children().eq(index).hide();
          });

          $(that).find('tbody').children().each(function(i){
              $(this).children().eq(index).hide();
          });
      } else {
          localStorage.setItem(settings.localStorageNamespace + index, 'show');
          $(that).find('thead').children().each(function(i){
              $(this).children().eq(index).show();
          });

          $(that).find('tbody').children().each(function(i){
              $(this).children().eq(index).show();
          });
      }
  });
  return;
};

    $.fn.columnFilter.format = function(checkbox,column) {
        return checkbox + '<label>' + column + '</label>';
    };
}( jQuery ));



$('table').columnFilter();
    var lastScrollLeft = 0;
    $(window).scroll(function () {
		var scroll = $(window).scrollTop();

		  if (scroll <= $('.navbar-header').height()) {
        var x = $(window).scrollTop();
        $('.table th').css({
            top: x,
        });
		  }else{
			  var x = $(window).scrollTop()-$('.table').offset().top+$('.navbar-header').height();
        $('.table th').css({
            top: x,
        });
			  }
    });
	}


});}, 500);

});//]]>

</script>
</div>
<style>

.column-list input{
	float:left;
	margin-right:10px;
	}
	.column-list label{
	float:left;
	margin-right:0px;
	font-size:11px;
	}
	.column-list{
		width:800px !important;
		column-count: 3;
		}
	@charset "utf-8";
.chromeframe {
	margin: 0.2em 0;
	background: #ccc;
	color: #000;
	padding: 0.2em 0;
}
p {
	line-height: 1.33em;
	color: #7E7E7E;
}
#loader-wrapper {
	position: fixed;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	z-index: 1000;
	background-color: #fff;
}
#loader {
	display: block;
	position: relative;
	left: 50%;
	top: 50%;
	width: 150px;
	height: 150px;
	margin: -75px 0 0 -75px;
	border-radius: 50%;
	border: 3px solid transparent;
	border-top-color: #3498db;
	-webkit-animation: spin 2s linear infinite;
	animation: spin 2s linear infinite;
}
#loader:before {
	content: "";
	position: absolute;
	top: 5px;
	left: 5px;
	right: 5px;
	bottom: 5px;
	border-radius: 50%;
	border: 3px solid transparent;
	border-top-color: #e74c3c;
	-webkit-animation: spin 3s linear infinite;
	animation: spin 3s linear infinite;
}
#loader:after {
	content: "";
	position: absolute;
	top: 15px;
	left: 15px;
	right: 15px;
	bottom: 15px;
	border-radius: 50%;
	border: 3px solid transparent;
	border-top-color: #f9c922;
	-webkit-animation: spin 1.5s linear infinite;
	animation: spin 1.5s linear infinite;
}
@-webkit-keyframes spin {
0% {
-webkit-transform:rotate(0deg);
-ms-transform:rotate(0deg);
transform:rotate(0deg);
}
100% {
-webkit-transform:rotate(360deg);
-ms-transform:rotate(360deg);
transform:rotate(360deg);
}
}
@keyframes spin {
0% {
-webkit-transform:rotate(0deg);
-ms-transform:rotate(0deg);
transform:rotate(0deg);
}
100% {
-webkit-transform:rotate(360deg);
-ms-transform:rotate(360deg);
transform:rotate(360deg);
}
}
</style>

<!-- <div id="loader-wrapper">
  <div id="loader"></div>
</div> -->

<!-- /navbar -->
<div style=" margin:50px auto;"></div>
<script type="text/javascript" src="https://use.fontawesome.com/1c608bc16d.js"></script>
