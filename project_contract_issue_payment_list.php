<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 09-May-2017
// LAST UPDATED BY: Ashwini
/* FILE HEADER - END */

/* DEFINES - START */
define('PROJECT_CONTRACT_ISSUE_PAYMENT_LIST_FUNC_ID','252');
/* DEFINES - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

$alert_type = -1;
$alert = "";

if(isset($_GET['payment_contract_id']))
{
	$payment_id = $_GET['payment_contract_id'];
}
else
{
	$payment_id = "";
}

if(isset($_GET['page_status']))
{
	$page_status = $_GET['page_status'];
}
else
{
	$page_status = "";
}

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];
	
	// Get permission settings for this user for this page
	$add_perms_list    = i_get_user_perms($user,'',PROJECT_CONTRACT_ISSUE_PAYMENT_LIST_FUNC_ID,'1','1');
	$view_perms_list   = i_get_user_perms($user,'',PROJECT_CONTRACT_ISSUE_PAYMENT_LIST_FUNC_ID,'2','1');
	$edit_perms_list   = i_get_user_perms($user,'',PROJECT_CONTRACT_ISSUE_PAYMENT_LIST_FUNC_ID,'3','1');
	$delete_perms_list = i_get_user_perms($user,'',PROJECT_CONTRACT_ISSUE_PAYMENT_LIST_FUNC_ID,'4','1');

	//Get Project Contract Issue Payment List
	$project_contract_issue_payment_search_data = array("active"=>'1',"contract_id"=>$payment_id);
	$project_contract_issue_payment_list = i_get_project_contract_issue_payment($project_contract_issue_payment_search_data);
	if($project_contract_issue_payment_list["status"] == SUCCESS)
	{
		$project_contract_issue_payment_list_data = $project_contract_issue_payment_list["data"];
	}
	else
	{
		$alert = $project_contract_issue_payment_list["data"];
		$alert_type = 0;
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Project Contract Issue Payment List</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>
    

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Project Contract Issue Payment List</h3>
            </div>
            <!-- /widget-header -->
			
            <div class="widget-content">
			<?php
			if($view_perms_list['status'] == SUCCESS)
			{
			?>
              <table class="table table-bordered" style="table-layout: fixed;">
								<thead>
								  <tr>
									<th>Sl no</th>
									<th>Vendor</th>
									<th>Total Amount</th>
									<th>Issued Amount</th>
									<th>Deduction</th>
									<th>Payment Mode</th>
									<th>Instrument Details</th>
									<th>Remarks</th>		
									<th>Added By</th>		
									<th>Added On</th>
                                    <th colspan="1" style="text-align:center;">Action</th>									
								</tr>
								</thead>
								<tbody>							
								<?php
								if($project_contract_issue_payment_list["status"] == SUCCESS)
								{			
									$sl_no = 0;
									
									if($page_status == "Issued")
									{
										$status_change = "Payment Issued";
									}
									else
									{
										$status_change = "Accepted";
									}
									for($count = 0; $count < count($project_contract_issue_payment_list_data); $count++)
									{
										$sl_no++;
										
										// Get Project  Payment ManPower modes already added
										$project_actual_contract_payment_search_data = array("active"=>'1',"status"=>$status_change,"contract_payment_id"=>$project_contract_issue_payment_list_data[$count]["project_contract_issue_payment_contract_id"]);
										$project_actual_contract_payment_list = i_get_project_actual_contract_payment($project_actual_contract_payment_search_data);
										if($project_actual_contract_payment_list['status'] == SUCCESS)
										{
											$project_actual_contract_payment_list_data = $project_actual_contract_payment_list['data'];
											$amount = $project_actual_contract_payment_list_data[0]["project_actual_contract_payment_amount"];
											$vendor = $project_actual_contract_payment_list_data[0]["project_manpower_agency_name"];
										}
										else
										{
											$amount ="";
											$vendor ="";
											$vendor ="";
											
										}
									?>
									<tr>
									<td><?php echo $sl_no;?>
									<td style="word-wrap:break-word;"><?php echo $project_contract_issue_payment_list_data[$count]["project_manpower_agency_name"]; ?></td>
									<td style="word-wrap:break-word;"><?php echo $amount; ?></td>
									<td style="word-wrap:break-word;"><?php echo $project_contract_issue_payment_list_data[$count]["project_contract_issue_payment_amount"]; ?></td>
									<td style="word-wrap:break-word;"><?php echo $project_contract_issue_payment_list_data[$count]["project_contract_issue_payment_deduction"]; ?></td>
									<td style="word-wrap:break-word;"><?php echo $project_contract_issue_payment_list_data[$count]["payment_mode_name"]; ?></td>
									<td style="word-wrap:break-word;"><?php echo $project_contract_issue_payment_list_data[$count]["project_contract_issue_payment_instrument_details"]; ?></td>
									<td style="word-wrap:break-word;"><?php echo $project_contract_issue_payment_list_data[$count]["project_contract_issue_payment_remarks"]; ?></td>
									<td style="word-wrap:break-word;"><?php echo $project_contract_issue_payment_list_data[$count]["user_name"]; ?></td>	
									<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($project_contract_issue_payment_list_data[$count]
									["project_contract_issue_payment_added_on"])); ?></td>	
                                    <td><?php if(($edit_perms_list['status'] == SUCCESS) && ($status_change != "Issued")){?><a href="project_edit_contract_issue_payment.php?payment_id=<?php echo $project_contract_issue_payment_list_data[$count]["project_contract_issue_payment_id"]; ?>">Edit</a><?php } ?></td>
									
									</tr>
									<?php									
									}
								}
								else
								{
								?>
								<td colspan="5">No Project Man Power Issue Payment data added yet!</td>
								<?php
								}	
								?>	
								</tbody>
							  </table>
							    <?php
								}
								else
								{
									echo 'You are not authorized to view this page';
								}
								?>	
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function delete_project_contract_issue_payment(payment_id)
{
	var ok = confirm("Are you sure you want to Delete?")
	{         
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					alert(xmlhttp.responseText);
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else					
					{
					 window.location = "project_contract_issue_payment_list.php";
					}
				}
			}

			xmlhttp.open("POST", "project_delete_contract_issue_payment.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("payment_id=" + payment_id + "&action=0");
		}
	}	
}

</script>

  </body>

</html>