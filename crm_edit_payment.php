<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 25th Feb 2016
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_masters'.DIRECTORY_SEPARATOR.'crm_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_post_sales_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_sales_process.php');
/* INCLUDES - END */

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	
	/* QUERY STRING - START */
	if(isset($_POST["hd_nav_booking"]))
	{
		$booking_id = $_POST["hd_nav_booking"];
	}
	else
	{
		$booking_id = "";
	}
	if(isset($_POST["hd_nav_payment"]))
	{
		$payment_id = $_POST["hd_nav_payment"];
	}
	else
	{
		$payment_id = "";
	}
	/* QUERY STRING - END */

	// Capture the form data
	if(isset($_POST["edit_payment_submit"]))
	{
		$booking_id   = $_POST["hd_booking_id"];	
		$payment_id   = $_POST["hd_payment_id"];
		$amount       = $_POST["num_amount"];						$milestone    = $_POST["ddl_milestone"];		
		$mode         = $_POST["ddl_mode"];
		$instru_date  = $_POST["dt_instru_date"];
		$bank         = $_POST["stxt_bank"];
		$branch       = $_POST["stxt_branch"];
		$rec_date     = $_POST["dt_rec_date"];
		$remarks      = $_POST["txt_remarks"];
		
		$payment_due  = $_POST["hd_total_due_payment"];
		$payment_done = $_POST["hd_total_payment_done"];
		
		// Check for mandatory fields
		if(($booking_id !="") && ($amount !="") && ($mode !=""))
		{
			if((($mode != "4") && ($bank != "") && ($branch != "")) || ($mode == "4"))
			{
				/* Check how much payment is due */
				$total_payment_pending = $payment_due - $payment_done;
			
				if($total_payment_pending >= $amount)
				{
					$payment_data = array("amount"=>$amount,"milestone"=>$milestone,"mode"=>$mode,"instru_date"=>$instru_date,"bank"=>$bank,"branch"=>$branch,"receipt_date"=>$rec_date,"remarks"=>$remarks);					var_dump($payment_data);
					$edit_pay_result = i_edit_payment_details($payment_id,$payment_data);
					
					if($edit_pay_result["status"] == SUCCESS)
					{
						$alert_type = 1;
						$alert      = $edit_pay_result["data"];
						
						header("location:crm_view_payments.php?booking=".$booking_id);
					}
					else
					{
						$alert_type = 0;
						$alert      = $edit_pay_result["data"];
					}
				}
				else
				{
					$alert_type = 0;
					$alert      = "The entered amount is greater than the pending amount. Please check!";
				}
			}
			else			
			{
				$alert_type = 0;
				$alert      = "Please fill bank and branch details";
			}
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}
	
	// Get the payment details
	$pay_details = i_get_payment($payment_id,'','','','');
	if($pay_details["status"] == SUCCESS)
	{
		$pay_details_list = $pay_details["data"];
	}
	else
	{
		$alert = $pay_details["data"];
		$alert_type = 0;
	}
	
	// Get the customer details as captured already
	$cust_details = i_get_cust_profile_list('',$booking_id,'','','','','','','','');
	if($cust_details["status"] == SUCCESS)
	{
		$cust_details_list = $cust_details["data"];
	}
	else
	{
		$alert = $cust_details["data"];
		$alert_type = 0;
	}
	
	// Get booking details
	$booking_list = i_get_site_booking($booking_id,'','','','','','','','','','','');
	if($booking_list["status"] == SUCCESS)
	{
		$booking_list_data = $booking_list["data"];
		
		// Get the total amount to be paid
		if($booking_list_data[0]["crm_booking_consideration_area"] != "0")
		{
			$consideration_area = $booking_list_data[0]["crm_booking_consideration_area"];
		}
		else
		{
			$consideration_area = $booking_list_data[0]["crm_site_area"];
		}
		$total_payment_to_be_done = ($consideration_area * $booking_list_data[0]["crm_booking_rate_per_sq_ft"]);
	}
	else
	{
		$alert = $alert."Alert: ".$booking_list["data"];
		$total_payment_to_be_done = "";
	}

	// Get the payment details
	$payment_details = i_get_payment('',$booking_id,'','','');
	$total_payment_done = 0;
	if($payment_details["status"] == SUCCESS)
	{
		$payment_details_list = $payment_details["data"];
		
		for($count = 0; $count < count($payment_details_list); $count++)
		{
			$total_payment_done = $total_payment_done + $payment_details_list[$count]["crm_payment_amount"];
		}
	}
	else
	{
		$alert = $alert."Alert: ".$payment_details["data"];
	}	
	$total_payment_done = $total_payment_done - $pay_details_list[0]["crm_payment_amount"];
	
	// Get payment mode list
	$payment_mode_list = i_get_payment_mode_list('','1');
	if($payment_mode_list["status"] == SUCCESS)
	{
		$payment_mode_list_data = $payment_mode_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$payment_mode_list["data"];
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Edit Payment</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>     

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
						<?php 
						if($cust_details["status"] == SUCCESS)
						{
						?>
						<h3>Project: <?php echo $cust_details_list[0]["project_name"]; ?>&nbsp;&nbsp;&nbsp;Site No: <?php echo $cust_details_list[0]["crm_site_no"]; ?>&nbsp;&nbsp;&nbsp;Customer: <?php echo $cust_details_list[0]["crm_customer_name_one"]." (".$cust_details_list[0]["crm_customer_contact_no_one"].")"; ?>&nbsp;&nbsp;&nbsp;</h3>
						<?php
						}
						else
						{
							$alert_type = 0;
							$alert      = "Customer Profile not added!";?>
	      				<h3>Project: <?php echo $booking_list_data[0]["project_name"]; ?>&nbsp;&nbsp;&nbsp;Site No: <?php echo $booking_list_data[0]["crm_site_no"]; ?>&nbsp;&nbsp;&nbsp;Customer: <?php echo $booking_list_data[0]["name"]." (".$booking_list_data[0]["cell"].")"; ?>&nbsp;&nbsp;&nbsp;</h3>
						<?php
						}?>
						&nbsp;&nbsp;&nbsp;
						<h3>Total Value: <?php echo $total_payment_to_be_done; ?>&nbsp;&nbsp;&nbsp;Total Recd: <?php echo $total_payment_done; ?>&nbsp;&nbsp;&nbsp;Total Due: <?php echo ($total_payment_to_be_done - $total_payment_done); ?></h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Edit Payment</a>
						  </li>						  
						</ul>
						
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="edit_payment" class="form-horizontal" method="post" action="crm_edit_payment.php">
								<input type="hidden" name="hd_booking_id" value="<?php echo $booking_id; ?>" />
								<input type="hidden" name="hd_payment_id" value="<?php echo $payment_id; ?>" />
								<input type="hidden" name="hd_total_due_payment" value="<?php echo $total_payment_to_be_done; ?>" />
								<input type="hidden" name="hd_total_payment_done" value="<?php echo $total_payment_done; ?>" />
									<fieldset>										
												
										<div class="control-group">											
											<label class="control-label" for="num_amount">Amount*</label>
											<div class="controls">
												<input type="number" name="num_amount" id="num_amount" min="1" step="0.01" class="span6" required onfocusout="return disable_entry(<?php echo $total_payment_to_be_done; ?>,<?php echo $total_payment_done; ?>);" value="<?php echo $pay_details_list[0]["crm_payment_amount"]; ?>" />
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->											<div class="control-group">																						<label class="control-label" for="ddl_milestone">Milestone*</label>											<div class="controls">												<select name="ddl_milestone" required>												<option value="BOOKING"<?php if ($pay_details_list[0]["crm_payment_milestone"] == "BOOKING" ){ ?> selected <?php } ?>>BOOKING</option>																								<option value="AGREEMENT" <?php if($pay_details_list[0]["crm_payment_milestone"] == "AGREEMENT") {?> selected  <?php } ?>>AGREEMENT</option>																								<option value="REGISTRATION"<?php if($pay_details_list[0]["crm_payment_milestone"] == REGISTRATION) {?> selected  <?php } ?>>REGISTRATION</option>																								<option value="KHATHA TRANSFER" <?php if($pay_details_list[0]["crm_payment_milestone"] == "KATHA TRANSFER") {?> selected  <?php } ?>>KHATHA TRANSFER</option>												</select>											</div> <!-- /controls -->															</div> <!-- /control-group -->	
										<div class="control-group">											
											<label class="control-label" for="num_tds_amount">Payment Mode*</label>
											<div class="controls">
												<select class="span6" name="ddl_mode" id="ddl_mode">
												<option value="">- - Select payment Mode - -</option>
												<?php
												for($count = 0; $count < count($payment_mode_list_data); $count++)
												{
												?>
												<option value="<?php echo $payment_mode_list_data[$count]["payment_mode_id"]; ?>" <?php if($payment_mode_list_data[$count]["payment_mode_id"] == $pay_details_list[0]["crm_payment_payment_mode"]) { ?> selected <?php } ?>><?php echo $payment_mode_list_data[$count]["payment_mode_name"]; ?></option>
												<?php
												}
												?>
												</select>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										<div class="control-group">											
											<label class="control-label" for="dt_instru_date">Instrument Date*</label>
											<div class="controls">
												<input type="date" name="dt_instru_date" id="dt_instru_date" class="span6" required value="<?php echo $pay_details_list[0]["crm_payment_instrument_date"]; ?>" />
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										<div class="control-group">											
											<label class="control-label" for="stxt_bank">Bank</label>
											<div class="controls">
												<input type="text" name="stxt_bank" id="stxt_bank" class="span6" value="<?php echo $pay_details_list[0]["crm_payment_bank"]; ?>" />
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										<div class="control-group">											
											<label class="control-label" for="stxt_branch">Branch</label>
											<div class="controls">
												<input type="text" name="stxt_branch" id="stxt_branch" class="span6" value="<?php echo $pay_details_list[0]["crm_payment_branch"]; ?>" />
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										<div class="control-group">											
											<label class="control-label" for="dt_rec_date">Receipt Date</label>
											<div class="controls">
												<input type="date" name="dt_rec_date" id="dt_rec_date" class="span6" required value="<?php echo $pay_details_list[0]["crm_payment_receipt_date"]; ?>" />
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										<div class="control-group">											
											<label class="control-label" for="txt_remarks">Remarks</label>
											<div class="controls">
												<textarea name="txt_remarks" id="txt_remarks"><?php echo $pay_details_list[0]["crm_payment_remarks"]; ?></textarea>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->																				
                                                                                                                                                               										 <br />
										
											
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="edit_payment_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>																
								
							</div>
						  
						  
						</div>
						
						
						
						
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function disable_entry(pay_due,pay_done)
{
	amount = document.getElementById("num_amount").value;

	if(amount > (pay_due - pay_done))
	{
		document.getElementById("ddl_mode").disabled = true;
		document.getElementById("dt_instru_date").disabled = true;
		document.getElementById("stxt_bank").disabled = true;
		document.getElementById("stxt_branch").disabled = true;
		document.getElementById("dt_rec_date").disabled = true;
		document.getElementById("txt_remarks").disabled = true;
	}
	else
	{
		document.getElementById("ddl_mode").disabled = false;
		document.getElementById("dt_instru_date").disabled = false;
		document.getElementById("stxt_bank").disabled = false;
		document.getElementById("stxt_branch").disabled = false;
		document.getElementById("dt_rec_date").disabled = false;
		document.getElementById("txt_remarks").disabled = false;
	}
}
</script>

  </body>

</html>
