<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: task_list.php
CREATED ON	: 01-April-2017
CREATED BY	: Lakshmi
PURPOSE     : List of Task Plans for a particular process ID
*/
/*
TBD:
1. Date display and calculation
2. Session management
3. Linking Tasks
*/
$_SESSION['module'] = 'Projectmgmnt';

/* DEFINES - START */
define('PROJECT_PENDING_TASK_LIST_FUNC_ID','252');
/* DEFINES - END */

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Get permission settings for this user for this page
	$add_perms_list    = i_get_user_perms($user,'',PROJECT_PENDING_TASK_LIST_FUNC_ID,'1','1');
	$view_perms_list   = i_get_user_perms($user,'',PROJECT_PENDING_TASK_LIST_FUNC_ID,'2','1');

	if(isset($_GET['process_id']))
	{
		$process_id = $_GET['process_id'];
	}
	else
	{
		$process_id = "";
	}

	if(isset($_GET['source']))
	{
		$source = $_GET['source'];
	}
	else
	{
		$source = "-1";
	}

	$search_process   	 = "";

	if(isset($_POST["file_search_submit"]))
	{
		$search_process   = $_POST["search_process"];
	}
	else
	{
		if($process_id == "")
		{
			$search_process = "-1";
		}
		else
		{
			$search_process = "";
		}
	}

	$search_project   	 = "-1";

	if(isset($_POST["file_search_submit"]))
	{
		$search_project   = $_POST["search_project"];
	}
	else
	{
		if($process_id == "")
		{
			$search_project = "-1";
		}
		else
		{
			$search_project = "";
		}
	}

	if($role == 1)
	{
		if(isset($_POST["file_search_submit"]))
		{
			$search_user   = $_POST["search_user"];
		}
		else
		{
			$search_user = "";
		}
	}
	else
	{
		$search_user = $user;
	}

	$search_task   	 = "";

	if(isset($_POST["file_search_submit"]))
	{
		$search_task   = $_POST["search_task"];
	}

	// Get Already added process
	$project_process_master_search_data = array("active"=>'1');
	$project_process_master_list = i_get_project_process_master($project_process_master_search_data);
	if($project_process_master_list["status"] == SUCCESS)
	{
		$project_process_master_list_data = $project_process_master_list["data"];
	}
	else
	{
		$alert = $project_process_master_list["data"];
		$alert_type = 0;
	}

	// Get Project Management Master modes already added
	$project_management_master_search_data = array("active"=>'1', "user_id"=>$user);
	$project_management_master_list = i_get_project_management_master_list($project_management_master_search_data);
	if($project_management_master_list['status'] == SUCCESS)
	{
		$project_management_master_list_data = $project_management_master_list['data'];
	}

	else
	{
		$alert = $project_management_master_list["data"];
		$alert_type = 0;
	}

	 // Get Users modes already added
	$user_list = i_get_user_list('','','','1');
	if($user_list['status'] == SUCCESS)
	{
		$user_list_data = $user_list['data'];
	}
	else
	{
		$alert = $user_list["data"];
		$alert_type = 0;
	}

	// Get Project Task Master modes already added
	$project_task_master_search_data = array("active"=>'1',"process"=>$search_process);
	$project_task_master_list = i_get_project_task_master($project_task_master_search_data);
	if($project_task_master_list['status'] == SUCCESS)
	{
		$project_task_master_list_data = $project_task_master_list["data"];
	}
	else
	{
		$alert = $project_task_master_list["data"];
		$alert_type = 0;
	}

	// Get list of task plans for this process plan
	$project_process_task_search_data = array("active"=>'1',"process_id"=>$process_id,"task_type"=>$search_task,"project_id"=>$search_project,"process_master_id"=>$search_process);
	$project_process_task_list = i_get_project_process_task($project_process_task_search_data);
	if($project_process_task_list["status"] == SUCCESS)
	{
		$project_process_task_list_data = $project_process_task_list["data"];
	}
	else
	{

	}
}
else
{
	header("location:login.php");
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Pending Task List</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">

    <link href="css/style.css" rel="stylesheet">



    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">

          <div class="span6" style="width:100%;">

          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Pending Task List &nbsp;&nbsp;&nbsp;&nbsp; Average Summary: <span id="grand_total"><i>Calculating</i></span>%</h3>
            </div>
            <!-- /widget-header -->

				<div class="widget-header" style="height:80px; padding-top:10px;">
			  <form method="post" id="file_search_form" action="project_pending_task_list.php">
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_project">
			  <option value="-1">- - Select Project - -</option>
			  <?php
			  for($project_count = 0; $project_count < count($project_management_master_list_data); $project_count++)
			  {
			  ?>
			  <option value="<?php echo $project_management_master_list_data[$project_count]["project_management_master_id"]; ?>" <?php if($search_project == $project_management_master_list_data[$project_count]["project_management_master_id"]) { ?> selected="selected" <?php } ?>><?php echo $project_management_master_list_data[$project_count]["project_master_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>

			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_process">
			  <option value="">- - Select Process - -</option>
			  <?php
			  for($process_count = 0; $process_count < count($project_process_master_list_data); $process_count++)
			  {
			  ?>
			  <option value="<?php echo $project_process_master_list_data[$process_count]["project_process_master_id"]; ?>" <?php if($search_process == $project_process_master_list_data[$process_count]["project_process_master_id"]) { ?> selected="selected" <?php } ?>><?php echo $project_process_master_list_data[$process_count]["project_process_master_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>

			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_task">
			  <option value="">- - Select Task - -</option>
			  <?php
			  for($task_count = 0; $task_count < count($project_task_master_list_data); $task_count++)
			  {
			  ?>
			  <option value="<?php echo $project_task_master_list_data[$task_count]["project_task_master_id"]; ?>" <?php if($search_task == $project_task_master_list_data[$task_count]["project_task_master_id"]) { ?> selected="selected" <?php } ?>><?php echo $project_task_master_list_data[$task_count]["project_task_master_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>

			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_user">
			  <option value="">- - Select Assigned To - -</option>
			  <?php
			  for($user_count = 0; $user_count < count($user_list_data); $user_count++)
			  {
			  ?>
			  <option value="<?php echo $user_list_data[$user_count]["user_id"]; ?>" <?php if($search_user == $user_list_data[$user_count]["user_id"]) { ?> selected="selected" <?php } ?>><?php echo $user_list_data[$user_count]["user_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>

			  <input type="submit" name="file_search_submit" />
			  </form>
            </div>

            <div class="widget-content">
              <table class="table table-bordered">
                <thead>
                  <tr>
					<th>SL No</th>
					<th>Project</th>
					<th>Process Name</th>
					<th>Task</th>
					<th>Actual Start Date</th>
					<th>Actual End Date</th>
					<th>Days</th>
					<th>Actual %</th>
					<th>Completion Percentage</th>
					<th>Assigned To</th>
					<?php if($source != "view")
					{
					?>
					<th colspan="4" style="align:center">Action</th>
					<?php
					}
					?>
				</tr>
				</thead>
				<tbody>
				 <?php
				if($project_process_task_list["status"] == SUCCESS)
				{

					$sl_count = 0;

					for($count = 0; $count < count($project_process_task_list_data); $count++)
					{
						$sl_count++;
						$avg_count = 0;
						$no_of_task = count($project_process_task_list_data);
						if(get_formatted_date($project_process_task_list_data[$count]["project_process_actual_end_date"],"Y-m-d") == "0000-00-00")
						{
							$actual_end_date = date("Y-m-d");
						}
						else
						{
							$actual_end_date = $project_process_task_list_data[$count]["project_process_actual_end_date"];
						}

						   $actual_start_date = $project_process_task_list_data[$count]["project_process_actual_start_date"];

						$variance = get_date_diff($actual_start_date,$actual_end_date);
						if($variance["status"] == 1)
						{
							if((get_formatted_date($project_process_task_list_data[$count]["project_process_actual_end_date"],"Y-m-d") == "0000-00-00") || (get_formatted_date($project_process_task_list_data[$count]["project_process_actual_end_date"],"Y-m-d") == "1969-12-31"))
							{
								$css_class = "#FF0000";
							}
							else
							{
								$css_class = "#FFA500";
							}
						}
						else
						{
							if((get_formatted_date($project_process_task_list_data[$count]["project_process_actual_end_date"],"Y-m-d") == "0000-00-00") || (get_formatted_date($project_process_task_list_data[$count]["project_process_actual_end_date"],"Y-m-d") == "1969-12-31"))
							{
								$css_class = "#000000";
							}
							else
							{
								$css_class = "#00FF00";
							}
						}

						//$total_hrs = $men_hrs + $women_hrs + $mason_hrs;
						//$no_of_people = $total_hrs/8;

						$project_man_power_estimate_search_data = array("task_id"=>$project_process_task_list_data[$count]["project_process_task_id"]);
						$estimate_list = i_get_project_man_power_estimate($project_man_power_estimate_search_data);
						if($estimate_list['status'] == SUCCESS)
						{
							$estimate_list_data = $estimate_list['data'];
							$estimate_id = $estimate_list_data[0]["project_man_power_estimate_id"];
						}
						else
						{
							$estimate_id = '-1';
						}
						//Get actual man power List
						$man_power_search_data = array("active"=>'1',"max_percentage"=>'1',"task_id"=>$project_process_task_list_data[$count]["project_process_task_id"],"work_type"=>"Regular");
						$man_power_list = i_get_man_power_list($man_power_search_data);
						if($man_power_list["status"] == SUCCESS)
						{
							$man_power_list_data = $man_power_list["data"];
							$manpower_percentage = $man_power_list_data[0]["max_percentage"];
						}
						else
						{
							$manpower_percentage = 0;
						}

						//Get Actual Machine List
						$actual_machine_plan_search_data = array("active"=>'1',"max_machine_percentage"=>'1',"task_id"=>$project_process_task_list_data[$count]["project_process_task_id"],"work_type"=>"Regular");
						$actual_machine_plan_list = i_get_machine_planning_list($actual_machine_plan_search_data);
						if($actual_machine_plan_list["status"] == SUCCESS)
						{
							$actual_machine_plan_list_data = $actual_machine_plan_list["data"];
							$machine_percentage = $actual_machine_plan_list_data[0]["max_mpercentage"];
						}
						else
						{
							$machine_percentage = "0";
						}
						//Get contract List data
						$project_task_boq_actual_search_data = array("active"=>'1',"max_contract_percentage"=>'1',"task_id"=>$project_process_task_list_data[$count]["project_process_task_id"],"work_type"=>"Regular");
						$project_task_boq_actual_list = i_get_project_task_boq_actual($project_task_boq_actual_search_data);
						if($project_task_boq_actual_list['status'] == SUCCESS)
						{
							$project_task_boq_actual_list_data = $project_task_boq_actual_list['data'];
							$contract_percentage = $project_task_boq_actual_list_data[0]["max_cpercentage"];
						}
						else
						{
							$contract_percentage = 0;
						}

						// Actual Manpower data
						$man_power_search_data = array("active"=>'1',"task_id"=>$project_process_task_list_data[$count]["project_process_task_id"],"work_type"=>"Regular");
						$man_power_list = i_get_man_power_list($man_power_search_data);
						if($man_power_list["status"] == SUCCESS)
						{
							$avg_count = $avg_count + 1;
						}
						else
						{
							// Get Project Man Power Estimate modes already added
							$project_man_power_estimate_search_data = array("active"=>'1',"task_id"=>$project_process_task_list_data[$count]["project_process_task_id"]);
							$project_man_power_estimate_list = i_get_project_man_power_estimate($project_man_power_estimate_search_data);
							if($project_man_power_estimate_list['status'] == SUCCESS)
							{
								$avg_count = $avg_count + 1;
							}
							else
							{
								//Do not Inncrement
							}
						}

						// Machine Planning data
						$actual_machine_plan_search_data = array("active"=>'1',"task_id"=>$project_process_task_list_data[$count]["project_process_task_id"],"work_type"=>"Regular");
						$actual_machine_plan_list = i_get_machine_planning_list($actual_machine_plan_search_data);
						if($actual_machine_plan_list["status"] == SUCCESS)
						{
							$avg_count = $avg_count + 1;
						}
						else
						{
							//Machine Planning
							$project_machine_planning_search_data = array("active"=>'1',"task_id"=>$project_process_task_list_data[$count]["project_process_task_id"]);
							$project_machine_planning_list = i_get_project_machine_planning($project_machine_planning_search_data);
							if($project_machine_planning_list["status"] == SUCCESS)
							{
								$avg_count = $avg_count + 1;
							}
							else
							{
								//Do not Increment
							}
						}

						// Actual Contract  Data
						$project_task_boq_actual_search_data = array("active"=>'1',"task_id"=>$project_process_task_list_data[$count]["project_process_task_id"],"work_type"=>"Regular");
						$project_task_boq_actual_list = i_get_project_task_boq_actual($project_task_boq_actual_search_data);
						if($project_task_boq_actual_list['status'] == SUCCESS)
						{
							$avg_count = $avg_count + 1;
						}
						else
						{
							$project_task_boq_search_data = array("active"=>'1',"task_id"=>$project_process_task_list_data[$count]["project_process_task_id"]);
							$project_task_boq_list = i_get_project_task_boq($project_task_boq_search_data);
							if($project_task_boq_list['status'] == SUCCESS)
							{
								$avg_count = $avg_count + 1;
							}
							else
							{
								///Do not Increment
							}
						}

						$total_percentage = $manpower_percentage + $machine_percentage + $contract_percentage ;
						if($avg_count != 0)
						{
							$avg_percentage = $total_percentage/$avg_count;
						}
						else
						{
							$avg_percentage = 0;
						}
						$total_avg_percentage = $total_avg_percentage + $avg_percentage;
						$overall_percentage = $total_avg_percentage/$no_of_task
					?>
					<!--<tr style="color:<?php echo $css_class; ?>">-->
						<td style="word-wrap:break-word;"><?php echo $sl_count; ?></td>
						<td style="word-wrap:break-word;"><?php echo $project_process_task_list_data[$count]["project_master_name"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $project_process_task_list_data[$count]["project_process_master_name"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $project_process_task_list_data[$count]["project_task_master_name"]; ?></td>

						<td style="word-wrap:break-word;"><?php echo get_formatted_date($project_process_task_list_data[$count]["project_process_actual_start_date"],"d-M-Y"); ?></td>
						<td style="word-wrap:break-word;"><?php echo get_formatted_date($project_process_task_list_data[$count]["project_process_actual_end_date"],"d-M-Y"); ?></td>

						<td style="word-wrap:break-word;"><?php if($project_process_task_list_data[$count]["project_process_actual_start_date"] != '0000-00-00'){
							echo $variance["data"] + 1;
						}
						else
						{
							echo '';
						}?></td>
						<td><?php echo round($avg_percentage,2) ;?></td>
						<td style="word-wrap:break-word;">Man Power : <a href="project_task_actual_manpower_list.php?task_id=<?php echo $project_process_task_list_data[$count]["project_process_task_id"] ;?>" target="_blank"><?php echo $manpower_percentage ;?>% </a><br>Machine : <a href="project_actual_machine_planning_list.php?task_id=<?php echo $project_process_task_list_data[$count]["project_process_task_id"] ;?>" target="_blank" ><?php echo $machine_percentage ;?>%</a><br> Contract :<a href="project_task_boq_actuals_list.php?task_id=<?php echo $project_process_task_list_data[$count]["project_process_task_id"] ;?>" target ="_blank"> <?php echo $contract_percentage ; ?>%</a></td>
						<td style="word-wrap:break-word;"><?php echo $project_process_task_list_data[$count]["user_name"]; ?></td>
						<?php if($source != "view")
						{
						?>
						<!-- <td>*<?php if($add_perms_list['status'] == SUCCESS){?><a href="#" data-toggle="modal" data-id="<?php echo $project_process_task_list_data[$count]["project_process_task_id"]; ?>" data-estimate="<?php echo $estimate_id ; ?>" data-pause-status="<?php echo $project_process_task_list_data[$count]["project_process_task_status"]; ?>" data-target="#myModal" class="open_task" id="task_id_link_<?php echo $project_process_task_list_data[$count]["project_process_task_id"]; ?>"><strong>Actuals</strong></a><?php  } ?></td> -->

						<td><?php if($add_perms_list['status'] == SUCCESS){?><a href="#" data-toggle="modal" data-id="<?php echo $project_process_task_list_data[$count]["project_process_task_id"]; ?>" data-estimate="<?php echo $estimate_id ; ?>" data-target="#myModal1" class="open_task" id="task_id_link_<?php echo $project_process_task_list_data[$count]["project_process_task_id"]; ?>"><strong>Plan</strong></a><?php } ?></td>

						<?php
						if($project_process_task_list_data[$count]["project_process_task_status"]== "Pause")
						{
							$status = "Release";
						}
						else
						{
							$status = "Pause";
						}
						?>
					<td style="word-wrap:break-word;"><?php if($add_perms_list['status'] == SUCCESS){ ?><a style="padding-right:10px" href="#" onclick="return go_to_delay_reason_master('<?php echo $project_process_task_list_data[$count]["project_process_task_id"]; ?>','<?php echo $project_process_task_list_data[$count]["project_process_id"]; ?>','<?php echo $status ;?>');"><?php echo $status ; ?></a><?php  } ?></div></td>
						<?php
					}
					?>
					</tr>
					<?php
						}
					}
				else
				{
				?>
				<td colspan="9">No tasks added yet!</td>
				<?php
				}
				 ?>
                </tbody>
              </table>
			  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog" role="document"  style="padding:20px;">
                     <div class="modal-content">
					 <table class="table table-bordered" id="actuals_table_released">
					 <tr>

						<td><?php if($add_perms_list['status'] == SUCCESS){?><a style="padding-right:10px" href="#" onclick="return go_to_project_task_actual();">Add Actual Man Power</a><?php } ?></td>

						<td><?php if($add_perms_list['status'] == SUCCESS){?><a style="padding-right:10px" href="#" onclick="return go_to_project_task_actual_machine_planning();">Add Actual Machine</a><?php } ?></td>


						<td><?php if($add_perms_list['status'] == SUCCESS){?><a style="padding-right:10px" href="#" onclick="return go_to_project_task_boq_actuals();">Add Contract Work</a><?php } ?> </td>
						</tr>
						</table>
						<table class="table table-bordered" id="actuals_table_paused" style="display:none;">
						<tr><td>This task is paused. Please release before adding actuals</td></tr>
						</table>
						</div>
                           </form>
                     </div>
                  </div>
				  <div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog" role="document"  style="padding:20px;">
                     <div class="modal-content">

                       <input type="hidden" name="hd_task_id" id="hd_task_id" value="" />
					   <table class="table table-bordered" id="allow_plan_table">
						<tr>

						<td id="man_power_plan"><?php if($add_perms_list['status'] == SUCCESS){?><a style="padding-right:10px" href="#" onclick="return go_to_add_manpower_planning();">Add Man Power Plan</a><?php } ?> </td>

						<td><?php if($add_perms_list['status'] == SUCCESS){?><a style="padding-right:10px" href="#" onclick="return go_to_project_machine_planning_list();">Add Machine Plan</a><?php } ?>  </td>

						<td><?php if($add_perms_list['status'] == SUCCESS){?><a style="padding-right:10px" href="#" onclick="return go_to_project_task_boq();">Add Contract</a><?php } ?></td>

						<td><?php if($add_perms_list['status'] == SUCCESS){?><a style="padding-right:10px" href="#" onclick="return go_to_add_method_planning();">Method Plan</a><?php } ?> </td>

						<td><?php if($add_perms_list['status'] == SUCCESS){?><a style="padding-right:10px" href="#" onclick="return go_to_add_material();">Material Plan</a><?php } ?> </td>
						</tr>
						</table>


						</div>
			  <br />
            </div>
            <!-- /widget-content -->
               <script>
				  document.getElementById('grand_total').innerHTML = '<?php echo round($overall_percentage,2); ?>';
		    </script>
          </div>
          <!-- /widget -->

          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 -->
      </div>
      <!-- /row -->
    </div>
    <!-- /container -->
  </div>
  <!-- /main-inner -->
</div>




<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">

                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->




<div class="footer">

	<div class="footer-inner">

		<div class="container">

			<div class="row">

    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->

    		</div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /footer-inner -->

</div> <!-- /footer -->



<script src="js/jquery-1.7.2.min.js"></script>

<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
$(document).ready(function(){
   $(".open_task").click(function(){
     $("#hd_task_id").val($(this).data('id'));
	 var estimate_id = ($(this).data('estimate'));

	 if($(this).data('pause-status'))
	 {
		 if($(this).data('pause-status') == 'Pause')
		 {
			 $("#actuals_table_paused").show();
			 $("#actuals_table_released").hide();
		 }
		 else
		 {
			 $("#actuals_table_paused").hide();
			 $("#actuals_table_released").show();
		 }
	 }
   });
});
function go_to_delay_reason_master(project_process_task_id,process_id,status)
{
	var form = document.createElement("form");
    form.setAttribute("method", "GET");
    form.setAttribute("action", "project_update_delay_reason_master.php");

	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","project_process_task_id");
	hiddenField1.setAttribute("value",project_process_task_id);

	var hiddenField2 = document.createElement("input");
	hiddenField2.setAttribute("type","hidden");
	hiddenField2.setAttribute("name","process_id");
	hiddenField2.setAttribute("value",process_id);

	var hiddenField3 = document.createElement("input");
	hiddenField3.setAttribute("type","hidden");
	hiddenField3.setAttribute("name","status");
	hiddenField3.setAttribute("value",status);

	form.appendChild(hiddenField1);
	form.appendChild(hiddenField2);
	form.appendChild(hiddenField3);

	document.body.appendChild(form);
    form.submit();
}
function go_to_project_task_boq()
{
	var form = document.createElement("form");
    form.setAttribute("method", "GET");
    form.setAttribute("action", "project_add_task_boq.php");

	project_process_task_id=document.getElementById("hd_task_id").value;

	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","project_process_task_id");
	hiddenField1.setAttribute("value",project_process_task_id);

	form.appendChild(hiddenField1);

	document.body.appendChild(form);
    form.submit();
}

function go_to_release_reason_master(project_process_task_id)
{
	var form = document.createElement("form");
    form.setAttribute("method", "GET");
    form.setAttribute("action", "project_release_task_list.php");

	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","project_process_task_id");
	hiddenField1.setAttribute("value",project_process_task_id);

	form.appendChild(hiddenField1);

	document.body.appendChild(form);
    form.submit();
}

function go_to_project_task_actual()
{
	project_process_task_id = document.getElementById("hd_task_id").value;
	var form = document.createElement("form");
    form.setAttribute("method", "GET");
    form.setAttribute("action", "project_task_actual_manpower.php");

	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","project_process_task_id");
	hiddenField1.setAttribute("value",project_process_task_id);

	form.appendChild(hiddenField1);

	document.body.appendChild(form);
    form.submit();
}
function go_to_project_task_actual_machine_planning()
{
	var form = document.createElement("form");
    form.setAttribute("method", "GET");
    form.setAttribute("action", "project_task_actual_machine_plan.php");

	project_process_task_id=document.getElementById("hd_task_id").value;

	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","project_process_task_id");
	hiddenField1.setAttribute("value",project_process_task_id);

	form.appendChild(hiddenField1);

	document.body.appendChild(form);
    form.submit();
}

function go_to_project_machine_planning_list()
{
	var form = document.createElement("form");
    form.setAttribute("method", "GET");
    form.setAttribute("action", "project_add_machine_planning.php");

	project_process_task_id=document.getElementById("hd_task_id").value;

	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","project_process_task_id");
	hiddenField1.setAttribute("value",project_process_task_id);

	form.appendChild(hiddenField1);

	document.body.appendChild(form);
    form.submit();
}

function go_to_project_task_boq_actuals()
{
	var form = document.createElement("form");
    form.setAttribute("method", "GET");
    form.setAttribute("action", "project_add_task_boq_actuals.php");

	project_process_task_id=document.getElementById("hd_task_id").value;

	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","project_process_task_id");
	hiddenField1.setAttribute("value",project_process_task_id);

	form.appendChild(hiddenField1);

	document.body.appendChild(form);
    form.submit();
}
function go_to_add_method_planning()
{
	var form = document.createElement("form");
    form.setAttribute("method", "GET");
    form.setAttribute("action", "project_upload_documents.php");

	task_id=document.getElementById("hd_task_id").value;

	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","task_id");
	hiddenField1.setAttribute("value",task_id);

	form.appendChild(hiddenField1);
	document.body.appendChild(form);
    form.submit();
}

function go_to_add_material()
{
	var form = document.createElement("form");
    form.setAttribute("method", "GET");
    form.setAttribute("action", "project_add_task_required_item.php");

	task_id=document.getElementById("hd_task_id").value;

	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","task_id");
	hiddenField1.setAttribute("value",task_id);

	form.appendChild(hiddenField1);
	document.body.appendChild(form);
    form.submit();
}

function go_to_add_manpower_planning()
{
	var form = document.createElement("form");
    form.setAttribute("method", "GET");
    form.setAttribute("action", "project_add_man_power_estimate.php");

	task_id=document.getElementById("hd_task_id").value;

	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","task_id");
	hiddenField1.setAttribute("value",task_id);

	form.appendChild(hiddenField1);
	document.body.appendChild(form);
    form.submit();
}
</script>
<script>
/* Open the sidenav */
function openNav() {
    document.getElementById("mySidenav").style.width = "75%";
}

/* Close/hide the sidenav */
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}
</script>

  </body>

</html>
