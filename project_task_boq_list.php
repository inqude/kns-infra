<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: project_task_actual_manpower_list.php
CREATED ON	: 05-Dec-2016
CREATED BY	: Lakshmi
PURPOSE     : List of project for customer withdrawals
*/

/*
TBD:
*/

/* DEFINES - START */
define('PROJECT_TASK_PROJECT_BOQ_FUNC_ID','263');

/* DEFINES - END */

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

	$alert_type = -1;
	$alert = "";
if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Get permission settings for this user for this page
	$view_perms_list   = i_get_user_perms($user,'',PROJECT_TASK_PROJECT_BOQ_FUNC_ID,'2','1');
	$edit_perms_list   = i_get_user_perms($user,'',PROJECT_TASK_PROJECT_BOQ_FUNC_ID,'3','1');
	$delete_perms_list = i_get_user_perms($user,'',PROJECT_TASK_PROJECT_BOQ_FUNC_ID,'4','1');

	// Query String Data
	// Nothing

	$search_project = "";
	$search_process = "";
	$search_task	= "";
	$search_contract_process = "";
	$search_contract_task	 = "";

	if(isset($_POST["boq_search_submit"]))
	{
		$search_project = $_POST["search_project"];
		$search_process = $_POST["search_process"];
		$search_task    = $_POST["search_task"];
		$search_contract_process   = $_POST["search_contract_process"];
		$search_contract_task      = $_POST["search_contract_task"];
	}
	else if(isset($_POST["search_process"]))
	{
		$search_project = $_POST["search_project"];
		$search_process = $_POST["search_process"];
	}

	// Get Project Task BOQ modes already added
	$project_task_boq_search_data = array("active"=>'1',"project"=>$search_project,"process_master"=>$search_process,"task_master"=>$search_task,"contract_task"=>$search_contract_task,"process"=>$search_contract_process);
	$project_task_boq_list = i_get_project_task_boq($project_task_boq_search_data);
	if($project_task_boq_list['status'] == SUCCESS)
	{
		$project_task_boq_list_data = $project_task_boq_list['data'];
	}
	else
	{
		$alert = $alert."Alert: ".$project_task_boq_list["data"];
	}

	// Project data
	$project_management_master_search_data = array("active"=>'1', "user_id"=>$user);
	$project_management_master_list = i_get_project_management_master_list($project_management_master_search_data);
	if($project_management_master_list["status"] == SUCCESS)
	{
		$project_management_master_list_data = $project_management_master_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$project_management_master_list["data"];
	}

	// Process Master
	$project_process_master_search_data = array("active"=>'1');
	$project_process_master_list = i_get_project_process_master($project_process_master_search_data);
	if($project_process_master_list["status"] == SUCCESS)
	{
		$project_process_master_list_data = $project_process_master_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$project_process_master_list["data"];
	}

	// Task Master
	$project_task_master_search_data = array("active"=>'1',"process"=>$search_process);
	$project_task_master_list = i_get_project_task_master($project_task_master_search_data);
	if($project_task_master_list["status"] == SUCCESS)
	{
		$project_task_master_list_data = $project_task_master_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$project_task_master_list["data"];
	}

	// Project Contract Rate data
	$project_contract_rate_master_search_data = array("active"=>'1',"process"=>$search_contract_process);
	$project_contract_rate_master_list = i_get_project_contract_rate_master($project_contract_rate_master_search_data);
	if($project_contract_rate_master_list["status"] == SUCCESS)
	{
		$project_contract_rate_master_list_data = $project_contract_rate_master_list["data"];
	}
	else
	{
		$alert = $project_contract_rate_master_list["data"];
		$alert_type = 0;
	}

	// Get Project Contract Process modes already added
	$project_contract_process_search_data = array("active"=>'1');
	$project_contract_process_list = i_get_project_contract_process($project_contract_process_search_data);
	if($project_contract_process_list['status'] == SUCCESS)
	{
		$project_contract_process_list_data = $project_contract_process_list["data"];
	}
	else
	{
		$alert = $project_contract_process_list["data"];
		$alert_type = 0;
	}
}
else
{
	header("location:login.php");
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Project Task BOQ List</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">

    <link href="css/style.css" rel="stylesheet">



    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>


<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">

          <div class="span6" style="width:100%;">

          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Project Task BOQ List &nbsp;&nbsp;&nbsp;&nbsp; Total Amount: <span id="total_amount"><i>Calculating</i></span>&nbsp;&nbsp;&nbsp;&nbsp; Total Measurement: <span id="measurement"><i>Calculating</i></span></h3>
            </div>

			<div class="widget-header" style="height:80px; padding-top:10px;">
			  <form method="post" id="file_search_form" action="project_task_boq_list.php">

			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_project">
			  <option value="">- - Select Project - -</option>
			  <?php
			  for($project_count = 0; $project_count < count($project_management_master_list_data); $project_count++)
			  {
			  ?>
			  <option value="<?php echo $project_management_master_list_data[$project_count]["project_management_master_id"]; ?>" <?php if($search_project == $project_management_master_list_data[$project_count]["project_management_master_id"]) { ?> selected="selected" <?php } ?>><?php echo $project_management_master_list_data[$project_count]["project_master_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>

			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_process" onchange="this.form.submit();">
			  <option value="">- - Select Process - -</option>
			  <?php
			  for($process_count = 0; $process_count < count($project_process_master_list_data); $process_count++)
			  {
			  ?>
			  <option value="<?php echo $project_process_master_list_data[$process_count]["project_process_master_id"]; ?>" <?php if($search_process == $project_process_master_list_data[$process_count]["project_process_master_id"]) { ?> selected="selected" <?php } ?>><?php echo $project_process_master_list_data[$process_count]["project_process_master_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>

			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_task">
			  <option value="">- - Select Task - -</option>
			  <?php
			  for($task_count = 0; $task_count < count($project_task_master_list_data); $task_count++)
			  {
			  ?>
			  <option value="<?php echo $project_task_master_list_data[$task_count]["project_task_master_id"]; ?>" <?php if($search_task == $project_task_master_list_data[$task_count]["project_task_master_id"]) { ?> selected="selected" <?php } ?>><?php echo $project_task_master_list_data[$task_count]["project_task_master_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>

			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_contract_process">
			  <option value="">- - Select Contract Process - -</option>
			  <?php
			  for($process_count = 0; $process_count < count($project_contract_process_list_data); $process_count++)
			  {
			  ?>
			  <option value="<?php echo $project_contract_process_list_data[$process_count]["project_contract_process_id"]; ?>" <?php if($search_contract_process == $project_contract_process_list_data[$process_count]["project_contract_process_id"]) { ?> selected="selected" <?php } ?>><?php echo $project_contract_process_list_data[$process_count]["project_contract_process_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>

			   <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_contract_task">
			  <option value="">- - Select Contract Task - -</option>
			  <?php
			  for($work_count = 0; $work_count < count($project_contract_rate_master_list_data); $work_count++)
			  {
			  ?>
			  <option value="<?php echo $project_contract_rate_master_list_data[$work_count]["project_contract_rate_master_id"]; ?>" <?php if($search_contract_task == $project_contract_rate_master_list_data[$work_count]["project_contract_rate_master_id"]) { ?> selected="selected" <?php } ?>><?php echo $project_contract_rate_master_list_data[$work_count]["project_contract_rate_master_work_task"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>


			  <input type="submit" name="boq_search_submit" />
			  </form>

            </div>
            <!-- /widget-header -->
            <div class="widget-content">
			<?php if($view_perms_list['status'] == SUCCESS)
			{
			?>
              <table class="table table-bordered" style="table-layout: fixed;">
                <thead>
                  <tr>
				    <th style="word-wrap:break-word;">SL No</th>
					<th style="word-wrap:break-word;">Project</th>
				    <th style="word-wrap:break-word;">Process</th>
					<th style="word-wrap:break-word;">Task</th>
					<th style="word-wrap:break-word;">Contract Process</th>
					<th style="word-wrap:break-word;">Contract Task</th>
					<th style="word-wrap:break-word;">UOM</th>
					<th style="word-wrap:break-word;">Location</th>
					<th style="word-wrap:break-word;">Number</th>
					<th style="word-wrap:break-word;">Length</th>
					<th style="word-wrap:break-word;">Breadth</th>
					<th style="word-wrap:break-word;">Depth</th>
					<th style="word-wrap:break-word;">Total Measurement</th>
					<th style="word-wrap:break-word;">Rate</th>
					<th style="word-wrap:break-word;">Total</th>
					<th style="word-wrap:break-word;">Remarks</th>
					<th style="word-wrap:break-word;">Added By</th>
					<th style="word-wrap:break-word;">Added On</th>
					<th colspan="3" style="text-align:center;word-wrap:break-word;">Actions</th>

				</tr>
				</thead>
				<tbody>
				<?php
				if($project_task_boq_list["status"] == SUCCESS)
				{
					$sl_no = 0;
					$total_amount = 0;

					for($count = 0; $count < count($project_task_boq_list_data); $count++)
					{
						$sl_no++;
						$measurement = $project_task_boq_list_data[$count]["project_task_boq_total_measurement"];
						$rate = $project_task_boq_list_data[$count]["project_task_boq_amount"];
						$total = $measurement * $rate;
						$total_amount = $total_amount + $total;
					?>
					<tr>
					<td style="word-wrap:break-word;"><?php echo $sl_no; ?></td>
					<td style="word-wrap:break-word;"><?php echo $project_task_boq_list_data[$count]["project_master_name"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo $project_task_boq_list_data[$count]["project_process_master_name"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo $project_task_boq_list_data[$count]["project_task_master_name"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo $project_task_boq_list_data[$count]["project_contract_process_name"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo $project_task_boq_list_data[$count]["project_contract_rate_master_work_task"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo $project_task_boq_list_data[$count]["stock_unit_name"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo $project_task_boq_list_data[$count]["project_site_location_mapping_master_name"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo $project_task_boq_list_data[$count]["project_task_boq_number"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo $project_task_boq_list_data[$count]["project_task_boq_length"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo $project_task_boq_list_data[$count]["project_task_boq_breadth"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo $project_task_boq_list_data[$count]["project_task_boq_depth"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo $project_task_boq_list_data[$count]["project_task_boq_total_measurement"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo $project_task_boq_list_data[$count]["project_task_boq_amount"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo round($total) ; ?></td>
					<td style="word-wrap:break-word;"><?php echo $project_task_boq_list_data[$count]["project_task_boq_remarks"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo $project_task_boq_list_data[$count]["user_name"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($project_task_boq_list_data[$count][
					"project_task_boq_added_on"])); ?></td>
					<td style="word-wrap:break-word;"><?php if($edit_perms_list['status'] == SUCCESS){ ?><a style="padding-right:10px" href="#" onclick="return go_to_project_edit_task_boq('<?php echo $project_task_boq_list_data[$count]["project_task_boq_id"]; ?>');">Edit </a></div><?php } ?></td>
					<td style="word-wrap:break-word;"><?php if($delete_perms_list['status'] == SUCCESS){ ?><?php if(($project_task_boq_list_data[$count]["project_task_boq_active"] == "1")){?><a href="#" onclick="return project_delete_task_boq(<?php echo $project_task_boq_list_data[$count]["project_task_boq_id"]; ?>);">Delete</a><?php } ?><?php } ?></td>
					<td style="word-wrap:break-word;"><?php if($edit_perms_list['status'] == SUCCESS){ ?><a style="padding-right:10px" href="#" onclick="return go_to_project_plan_cancelation('<?php echo $project_task_boq_list_data[$count]["project_task_id"]; ?>','contract');">Cancel Plan </a></div><?php } ?></td>
					</tr>
					<?php
					}

				}
				else
				{
				?>
				<td colspan="20">No BOQ plan added yet!</td>

				<?php
				}
				 ?>

                </tbody>
              </table>
		  <?php
		    }
			?>
			   <script>
			  document.getElementById('total_amount').innerHTML = '<?php echo round($total_amount); ?>';
			   document.getElementById('measurement').innerHTML = '<?php echo $measurement; ?>';
			   </script>
            </div>
            <!-- /widget-content -->
          </div>
          <!-- /widget -->

          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 -->
      </div>
      <!-- /row -->
    </div>
    <!-- /container -->
  </div>
  <!-- /main-inner -->
</div>




<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">

                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->




<div class="footer">

	<div class="footer-inner">

		<div class="container">

			<div class="row">

    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->

    		</div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /footer-inner -->

</div> <!-- /footer -->



<script src="js/jquery-1.7.2.min.js"></script>

<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function project_delete_task_boq(boq_id)
{
	var ok = confirm("Are you sure you want to Delete?")
	{
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					alert(xmlhttp.responseText);
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else
					{
					 window.location = "project_task_boq_list.php";
					}
				}
			}

			xmlhttp.open("POST", "ajax/project_delete_task_boq.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("boq_id=" + boq_id + "&action=0");
		}
	}
}
function go_to_project_edit_task_boq(boq_id)
{
	var form = document.createElement("form");
    form.setAttribute("method", "get");
    form.setAttribute("action", "project_edit_task_boq.php");

	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","boq_id");
	hiddenField1.setAttribute("value",boq_id);

	form.appendChild(hiddenField1);

	document.body.appendChild(form);
    form.submit();
}

function go_to_project_plan_cancelation(task_id,source)
{
	var form = document.createElement("form");
    form.setAttribute("method", "get");
    form.setAttribute("action", "project_plan_add_cancelation.php");

	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","task_id");
	hiddenField1.setAttribute("value",task_id);

	var hiddenField2 = document.createElement("input");
	hiddenField2.setAttribute("type","hidden");
	hiddenField2.setAttribute("name","source");
	hiddenField2.setAttribute("value",source);

	form.appendChild(hiddenField1);
	form.appendChild(hiddenField2);

	document.body.appendChild(form);
    form.submit();
}
</script>

  </body>

</html>
