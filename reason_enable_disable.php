<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 8th July 2015
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'tasks'.DIRECTORY_SEPARATOR.'task_functions.php');
/* INCLUDES - END */

// Session Data
$user = $_SESSION["loggedin_user"];
$role = $_SESSION["loggedin_role"];

/* QUERY STRING DATA - START */
$reason = $_GET["reason"];
$action = $_GET["action"];
/* QUERY STRING DATA - END */

// Enable or disable user according to the command
$reason_data = array("reason_id"=>$reason,"status"=>$action);
$reason_update_result = i_update_reason_master($reason,$reason_data);

header("location:reason_master_list.php");
?>