<?php

/* SESSION INITIATE - START */

session_start();

/* SESSION INITIATE - END */



// Includes

$base = $_SERVER["DOCUMENT_ROOT"];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_purchase_functions.php');	

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock'.DIRECTORY_SEPARATOR.'stock_grn_functions.php');	

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'pdf_operations.php');	

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');	



// Session Data

$user 		   = $_SESSION["loggedin_user"];

$role 		   = $_SESSION["loggedin_role"];

$loggedin_name = $_SESSION["loggedin_user_name"];



// Temp Data

$alert_type = -1;

$alert = "";



$po_id = $_REQUEST["po_id"];

$stock_purchase_order_items_search_data = array("order_id"=>$po_id,"active"=>'1');

$po_items_list = i_get_stock_purchase_order_items_list($stock_purchase_order_items_search_data); 
if($po_items_list['status'] == SUCCESS)
{

	$po_items_list_data = $po_items_list['data'];

	$po_no 			    = $po_items_list_data[0]["stock_purchase_order_number"];

	$po_date 		    = $po_items_list_data[0]["stock_purchase_order_added_on"];

	$credit_duration    = $po_items_list_data[0]["stock_purchase_order_credit_duration"];

	$delivery_date      = date("d-M-Y",strtotime($po_items_list_data[0]["stock_purchase_order_due_date"]));
	
	$project		    = $po_items_list_data[0]["stock_project_name"];
	
	$trans_charges		= $po_items_list_data[0]["stock_purchase_order_transportation_charges"];
	
	$vendor 		    = $po_items_list_data[0]["stock_vendor_name"];
	
	$vendor_id 		    = $po_items_list_data[0]["stock_purchase_order_vendor"];
	
	$vendor 		    = $po_items_list_data[0]["stock_vendor_name"];

	$vendor_address     = $po_items_list_data[0]["stock_vendor_address"];

	$vendor_contact_no  = $po_items_list_data[0]["stock_vendor_contact_number"];

	$quotation_no 	    = $po_items_list_data[0]["stock_quotation_no"];	

	$po_terms    	    = $po_items_list_data[0]["stock_purchase_order_terms"];	

	$company_name       = $po_items_list_data[0]["stock_company_master_name"];

	$company_contact    = $po_items_list_data[0]["stock_company_master_contact_person"];	

	$company_address    = $po_items_list_data[0]["stock_company_master_address"];

	$company_tin        = $po_items_list_data[0]["stock_company_master_tin_no"];	

	$location_name      = $po_items_list_data[0]["stock_location_name"];	

	$location_address   = $po_items_list_data[0]["stock_location_address"];	

}	

else

{

	// Do nothing

}



$po_print_format = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<title>Purchase Order</title>

</head>



<body>

<table width="100%" border="1" cellpadding="3" style="border-collapse:collapse; border:1px solid #333;">

  <tr>

    <td width="50%" rowspan="3" valign="top"><img src="kns-logo.png" width="186" height="46" /><br /></td>

    <td colspan="2" align="right" valign="middle"><h3>Purchase Order</h3></td>

  </tr>

  <tr>

    <td width="25%" align="right" valign="middle">PO NO.</td>

    <td width="25%" align="left" valign="middle">'.$po_no.'</td>

  </tr>

  <tr>

    <td align="right" valign="middle">Date</td>

    <td align="left" valign="middle">'.date("d-M-Y",strtotime($po_date)).'</td>

  </tr>

  <tr>

    <td style="border-top:1px solid #fff;" width="50%" rowspan="3" valign="top">'.$company_name.',<br />

'.$company_contact.',<br />

'.$company_address.'</td>

    <td align="right" valign="middle">Quotation Ref</td>

    <td align="left" valign="middle">'.$quotation_no.'</td>

  </tr>

  <tr>

    <td align="right" valign="middle">Delivery Date</td>

    <td align="left" valign="middle">'.$delivery_date.'</td>

  </tr>

  <tr>

    <td align="right" valign="middle">GSTIN</td>

    <td align="left" valign="middle">'.$company_tin.'</td>

  </tr>

  <tr>

    <td valign="top">Vendor<br />

      '.$vendor.'<br />

   Address: '.$vendor_address.'<br/>

	Contact No:'.$vendor_contact_no.'</td>

    <td colspan="2">Delivery Address:<br />
      '.$location_name.' (for '.$project.')<br />
      '.$location_address.'</td>

  </tr>

</table><br/>

<table width="100%" border="1" cellpadding="3" style="border-collapse:collapse; border:1px solid #333;">

  <tr>

    <td width="4%" align="center">Sl.No</td>

    <td width="40%" align="center">Material Description</td>
	
    <td width="10%" align="center">HSN Code</td>
	
	<td width="5%" align="center">UOM</td>

    <td width="10%" align="center">Quantity</td>

    <td width="9%" align="center">Rate</td>
	
    <td width="9%" align="center">Value</td>
	
	<td width="9%" align="center">Transportation Charges</td>
	
	<td width="9%" align="center">CGST</td>

	<td width="9%" align="center">SGST</td>
	
	<td width="9%" align="center">IGST</td>
	
    <td width="14%" align="center">Total</td>

  </tr>';

		

$grand_total = 0;
$grand_total1 = 0;
$total_cgst_value = 0;
$total_sgst_value = 0;
$total_igst_value = 0;

for($count = 0; $count < count($po_items_list["data"]); $count++)

{

	$qty   = $po_items_list_data[$count]["stock_purchase_order_item_quantity"];

	$value = $po_items_list_data[$count]["stock_purchase_order_item_cost"];
	
	$sub_total   = ($qty * $value) + $trans_charges; 
	
	if($po_items_list_data[$count]["stock_tax_type_master_jurisdiction"] == 0)
	{
		$cgst = (($po_items_list_data[$count]["stock_tax_type_master_value"])/2);
		$cgst_value = ($cgst/100)* $sub_total;
		$sgst = (($po_items_list_data[$count]["stock_tax_type_master_value"])/2);
		$sgst_value = ($sgst/100)* $sub_total;
		$igst = 0;
		$igst_value = 0;
	}
	elseif($po_items_list_data[$count]["stock_tax_type_master_jurisdiction"] == 1)
	{
		$igst = $po_items_list_data[$count]["stock_tax_type_master_value"];
		$igst_value = ($igst/100)* $sub_total;
		$cgst = 0;
		$cgst_value = 0;
		$sgst  = 0;
		$sgst_value  = 0;
	}
	
	$total_cgst_value = $total_cgst_value + $cgst_value;
	$total_sgst_value = $total_sgst_value + $sgst_value;
	$total_igst_value = $total_igst_value + $igst_value;
	
	$total       = ($qty * $value) + $cgst_value + $igst_value + $sgst_value; 

	$grand_total = $grand_total + $total;

	if($vendor_id == 27)
	{
		$grand_total1 = $grand_total1 + $total + $trans_charges;
	}

	$po_print_format = $po_print_format.'<tr>

              <td style="height:30px;">'.($count + 1).'</td>

              <td>'.$po_items_list_data[$count]["stock_material_name"].''.'-'.''.$po_items_list_data[$count]["stock_material_code"].'</td>
			  
			  <td>'.$po_items_list_data[$count]["stock_material_manufacturer_part_number"].'</td>
			  
			  <td>'.$po_items_list_data[$count]["stock_unit_name"].'</td>

			  <td>'. $qty.'</td>

              <td>'. get_formatted_amount(round($value,2),INDIA).'</td>
			  
              <td>'.($qty * $value).'</td>
			   
			   <td>'.$trans_charges.'</td>
			  
			  <td>'.round($cgst_value,2).''.'('.' '.$cgst.''.'%'.''.')'.'</td>

			  <td>'.round($sgst_value,2).''.'('.' '.$sgst.''.'%'.''.')'.'</td>
			  
			  <td>'.round($igst_value,2).''.'('.' '.$igst.''.'%'.''.')'.'</td>

              <td>'.get_formatted_amount(round($total,2),INDIA).'</td>

            </tr>';

}
if($vendor_id == 27)
{
	$overall_total = round($grand_total1,2);
}
else
{
	$overall_total = $grand_total + $trans_charges;
}
$po_print_format = $po_print_format.'<tr>
	  
    <td colspan="8" style="height:30px;"border-top:none;""></td>
	  
	  
    <td>'.$total_cgst_value.'</td>
	
    <td>'.$total_sgst_value.'</td>
	
    <td>'.$total_igst_value.'</td>
	
    <td>'.'Rs.'.''.round($overall_total ).'</td>
	</tr>';

$po_print_format = $po_print_format.'<tr>

	  <<tr>
	  
	  <td colspan="10" rowspan="4" style="height:30px;"border-top:none;""></td>
	  
	  <td><strong>Grand Total</strong></td>
	  
	  <td>'.'Rs.'.''.round($overall_total).'</td>
	  
	</tr>';
	
$po_print_format = $po_print_format.'



</table>

<br />

<table width="100%" border="0">

  <tr>

    <td colspan="3" align="left"><strong>Terms and Conditions</strong></td>

  </tr>

  <tr>

    <td colspan="3" align="left"><ol>

    <li> Please send two copies of your invoice.</li>

       <li> Payment against delivery within '. $credit_duration.' days from the date of delivery</li>

        <li> Delivery Terms</li>

        <li> Delivery date/ Schedule : As per site requirement</li>

       <li> The quality of material will be checked by our QC/Engineers</li>

       <li> Quote our PO no in all future correspondence and documents</li>

        <li> Specify on every package - 1. Our PO No. 2. Gross, Net &amp; Tare weights 3. If any other (as per UOM)</li>

      <li> Above mentioned rates are inclusive Transportation</li>

	  <li>Other Terms: '.$po_terms.'</li></ol></td>

  </tr>

  <tr>

    <td width="4%" align="left">&nbsp;</td>

    <td width="47%" align="left">CC<br />

Finance<br />

Project<br />

Site/Store</td>

    <td width="49%" align="center"><br />

    <br />

    <br />

    (Purchase Manager)</td>

  </tr>



</table>

</body>

</html>';
//echo $po_print_format;
$mpdf = new mPDF();
$mpdf->WriteHTML($po_print_format);
$mpdf->Output('invoice_'.$invoice_no.'.pdf','I');

?>