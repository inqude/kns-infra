<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: crm_approved_booking_list.php
CREATED ON	: 04-Nov-2015
CREATED BY	: Nitin Kashyap
PURPOSE     : Approved Booking List
*/

/*
TBD: 
*/$_SESSION['module'] = 'CRM Reports';

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_post_sales_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_sales_process.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_projects'.DIRECTORY_SEPARATOR.'crm_project_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Query String Data
	// Nothing

	// Temp data
	$alert = "";
	
	if(isset($_POST["site_summary_filter_submit"]))
	{
		$project_id = $_POST["ddl_project"];		
	}
	else
	{
		$project_id = "";		
	}
	
	// Project List
	$project_list = i_get_project_list('','1');
	if($project_list["status"] == SUCCESS)
	{
		$project_list_data = $project_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$project_list["data"];
		$alert_type = 0; // Failure
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Booking Approved Site List</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>
    

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Site Summary Report</h3>
            </div>			
            <!-- /widget-header -->
            <div class="widget-content">
			
              <table class="table table-bordered">
                <thead>
                  <tr>
					<th>SL No</th>
					<th>Project</th>
					<th colspan="2">Total</th>
					<th colspan="3">Available</th>
					<th colspan="3">Blocked</th>	
					<th colspan="3">Management Blocked</th>
					<th colspan="3">Sold</th>													
				  </tr>
				  <tr>
					<th>&nbsp;</th>
					<th>&nbsp;</th>
					<th>Sites</th>
					<th>Area (in sq.ft)</th>						
					<th>Sites</th>
					<th>Area (in sq.ft)</th>
					<th>%</th>	
					<th>Sites</th>
					<th>Area (in sq.ft)</th>
					<th>%</th>	
					<th>Sites</th>
					<th>Area (in sq.ft)</th>
					<th>%</th>	
					<th>Sites</th>
					<th>Area (in sq.ft)</th>
					<th>%</th>														
				  </tr>
				</thead>
				<tbody>							
				<?php						
				if($project_list["status"] == SUCCESS)
				{									
					$sl_no = 0;
					$cancelled_count = 0;
					for($count = 0; $count < count($project_list_data); $count++)
					{		
						$total_sites = 0;
						$total_sq_ft = 0;
						$av_sites    = 0;
						$av_sq_ft    = 0;
						$mgmt_sites  = 0;
						$mgmt_sq_ft  = 0;
						$blk_sites   = 0;
						$blk_sq_ft   = 0;
					
						$sl_no++;
					
						$site_summary_array = array("project"=>$project_list_data[$count]["project_id"],"delete_status"=>'0');
						$tot_site_list = i_get_site_summary($site_summary_array);
						if($tot_site_list["status"] == SUCCESS)
						{
							$tot_sites = $tot_site_list["data"][0]["site_count"];
							$tot_sq_ft = $tot_site_list["data"][0]["area"];
						}
						
						$site_summary_array = array("project"=>$project_list_data[$count]["project_id"],"status"=>AVAILABLE_STATUS,"delete_status"=>'0');
						$av_site_list = i_get_site_summary($site_summary_array);
						if($av_site_list["status"] == SUCCESS)
						{
							$av_sites = $av_site_list["data"][0]["site_count"];
							$av_sq_ft = $av_site_list["data"][0]["area"];
						}
						
						$site_summary_array = array("project"=>$project_list_data[$count]["project_id"],"status"=>BLOCKED_STATUS,"delete_status"=>'0');
						$bl_site_list = i_get_site_summary($site_summary_array);
						if($av_site_list["status"] == SUCCESS)
						{
							$bl_sites = $bl_site_list["data"][0]["site_count"];
							$bl_sq_ft = $bl_site_list["data"][0]["area"];
						}
						
						$site_summary_array = array("project"=>$project_list_data[$count]["project_id"],"status"=>MGMT_BLOCKED_STATUS,"delete_status"=>'0');
						$mgbl_site_list = i_get_site_summary($site_summary_array);
						if($mgbl_site_list["status"] == SUCCESS)
						{
							$mgbl_sites = $mgbl_site_list["data"][0]["site_count"];
							$mgbl_sq_ft = $mgbl_site_list["data"][0]["area"];
						}
						
						$sold_sites = $tot_sites - $mgbl_sites - $bl_sites - $av_sites;
						$sold_sq_ft = $tot_sq_ft - $mgbl_sq_ft - $bl_sq_ft - $av_sq_ft;
						
						?>
						<tr>
						<td><?php echo $sl_no; ?></td>
						<td><?php echo $project_list_data[$count]["project_name"]; ?></td>
						<td><?php echo $tot_sites; ?></td>
						<td><?php echo $tot_sq_ft; ?></td>						
						<td><?php echo $av_sites; ?></td>
						<td><?php echo $av_sq_ft; ?></td>
						<td><?php if($tot_sites != 0)
						{ 
							echo round((($av_sites/$tot_sites)*100),2); 
						}
						else
						{
							echo "N.A";
						}?></td>
						<td><?php echo $bl_sites; ?></td>
						<td><?php echo $bl_sq_ft; ?></td>
						<td><?php if($tot_sites != 0)
						{
							echo round((($bl_sites/$tot_sites)*100),2);
						}
						else
						{
							echo "N.A";
						}?></td>
						<td><?php echo $mgbl_sites; ?></td>
						<td><?php echo $mgbl_sq_ft; ?></td>
						<td><?php if($tot_sites != 0)
						{
							echo round((($mgbl_sites/$tot_sites)*100),2); 
						}
						else
						{
							echo "N.A";
						}?></td>
						<td><?php echo $sold_sites; ?></td>
						<td><?php echo $sold_sq_ft; ?></td>
						<td><?php if($tot_sites != 0)
						{
							echo round((($sold_sites/$tot_sites)*100),2); 
						}
						else
						{
							echo "N.A";
						}?></td>
						</tr>
						<?php
					}
				}
				else
				{
				?>
				<td colspan="17">No site booked yet!</td>
				<?php
				}
				?>					
                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>/* Open the sidenav */function openNav() {    document.getElementById("mySidenav").style.width = "75%";}/* Close/hide the sidenav */function closeNav() {    document.getElementById("mySidenav").style.width = "0";}</script>

  </body>

</html>