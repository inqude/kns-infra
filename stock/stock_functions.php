<?php

$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'da_stock.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_indent_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock'.DIRECTORY_SEPARATOR.'stock_history_function.php');



/*

PURPOSE : To add Material Stock

INPUT 	: Material Id, Quantity, Uom, Location, Min Qty, Max Qty, Re-Order Qty, Re-Order level,Remarks, Last Updated By

OUTPUT 	: Message, success or failure message

BY 		: Lakshmi

*/
function i_add_material_stock($material_id,$quantity,$uom,$location,$project,$min_qty,$max_qty,$re_order_qty,$re_order_level,$remarks,$last_updated_by)
{
	$material_stock_search_data = array("material_id"=>$material_id,"project"=>$project);
	$material_stock_sresult = db_get_material_stock($material_stock_search_data);

	if($material_stock_sresult["status"] == DB_NO_RECORD)

	{
		$material_stock_iresult =  db_add_material_stock($material_id,$quantity,$uom,$location,$project,$min_qty,$max_qty,$re_order_qty,$re_order_level,$remarks,$last_updated_by);
		

		if($material_stock_iresult['status'] == SUCCESS)

		{

			$return["data"]   = "material stock Successfully Added";

			$return["status"] = SUCCESS;	

		}

		else

		{

			$return["data"]   = "Internal Error. Please try again later";

			$return["status"] = FAILURE;

		}

	}

	else

	{

		$return["data"]   = "Material Already Exists";

		$return["status"] = FAILURE;

	}

		

	return $return;

}

	

/*

PURPOSE : To get Material Stock list

INPUT 	: Stock ID, Material Id, Quantity, Uom, Location, Active

OUTPUT 	: Material Stock List or Error Details, success or failure message

BY 		: Lakshmi

*/

function i_get_material_stock($material_stock_search_data)

{

	$material_stock_sresult = db_get_material_stock($material_stock_search_data);

	

	if($material_stock_sresult['status'] == DB_RECORD_ALREADY_EXISTS)

    {

		$return["status"] = SUCCESS;

        $return["data"]   =$material_stock_sresult["data"]; 

    }

    else

    {

	    $return["status"] = FAILURE;

        $return["data"]   = "No material stock added. Please contact the system admin"; 

    }

	

	return $return;

}



/*

PURPOSE : To update Material Stock

INPUT 	: Material Stock ID, Material Stock Update Array

OUTPUT 	: Message, success or failure message

BY 		: Lakshmi

*/
function i_update_material_stock($material_id,$project,$material_stock_update_data)
{
		$material_stock_sresult = db_update_material_stock($material_id,$project,$material_stock_update_data);
		

		if($material_stock_sresult['status'] == SUCCESS)

		{

			$return["data"]   = $material_stock_sresult['data'];

			$return["status"] = SUCCESS;					

		}

		else

		{

			$return["data"]   = "Internal Error. Please try again later";

			$return["status"] = FAILURE;

		}

		

	return $return;

}



/*

PURPOSE : To add Stock Issue

INPUT 	: Indent Item ID, Indent ID, Returnable On, Remarks, Added By

OUTPUT 	: Message, success or failure message

BY 		: Lakshmi

*/

function i_add_stock_issue($indent_item_id,$indent_id,$returnable_on,$qty,$remarks,$issued_by,$added_by)

{

	//Get Issue Data

	$issued_qty ="0";

	$stock_issue_search_data = array("indent_item_id"=>$indent_item_id,"indent_id"=>$indent_id);

	$stock_issue_list = i_get_stock_issue($stock_issue_search_data);

	if($stock_issue_list["status"] ==  SUCCESS)

	{

		for($qty_count = 0 ; $qty_count < count($stock_issue_list["data"]) ; $qty_count++)

		{

		$stock_issue_list_data = $stock_issue_list["data"];

		$issued_qty = $issued_qty + $stock_issue_list_data[$qty_count]["stock_issue_qty"];

		}

	}

	else

	{

		$issued_qty = "0";

	}

	//Get Indent Data

	$stock_indent_items_search_data = array("item_id"=>$indent_item_id);

	$stock_indent_item_list = i_get_indent_items_list($stock_indent_items_search_data);;

	if($stock_indent_item_list["status"] == SUCCESS)

	{

		$indent_qty = $stock_indent_item_list["data"][0]["stock_indent_item_quantity"];

	}

	else

	{

		$indent_qty = "0";

	}

	$total_issued_qty = $issued_qty + $qty;

	$issue_no = p_generate_issue_no();

	if($total_issued_qty <= $indent_qty)

	{

		$stock_issue_iresult = db_add_stock_issue($issue_no,$indent_item_id,$indent_id,$returnable_on,$qty,$remarks,$issued_by,$added_by);

		

		if($stock_issue_iresult['status'] == SUCCESS)

		{

			$return["data"]   = $stock_issue_iresult['data'] ;

			$return["status"] = SUCCESS;	

		}

		else

		{

			$return["data"]   = "Internal Error. Please try again later";

			$return["status"] = FAILURE;

		}

	}

	else

	{

		$return["data"]   = "Quantity Exceeds Than Indent Quantity";

		$return["status"] = FAILURE;

		

	}

	return $return;

}

	



/*

PURPOSE : To get Stock Issue list

INPUT 	: Issue Id,Indent Item ID, Indent ID, Returnable On, Active

OUTPUT 	: Stock Issue List or Error Details, success or failure message

BY 		: Lakshmi

*/

function i_get_stock_issue($stock_issue_search_data)

{

	$stock_issue_sresult = db_get_stock_issue($stock_issue_search_data);

	

	if($stock_issue_sresult['status'] == DB_RECORD_ALREADY_EXISTS)

    {

		$return["status"] = SUCCESS;

        $return["data"]   =$stock_issue_sresult["data"]; 

    }

    else

    {

	    $return["status"] = FAILURE;

        $return["data"]   = "No stock issue added. Please contact the system admin"; 

    }

	

	return $return;

}



/*

PURPOSE : To update Stock Issue

INPUT 	: Stock Issue ID, Stock Issue Update Array

OUTPUT 	: Message, success or failure message

BY 		: Lakshmi

*/



function i_update_stock_issue($issue_id,$stock_issue_update_data)

{

		$stock_issue_sresult = db_update_stock_issue($issue_id,$stock_issue_update_data);

		

		if($stock_issue_sresult['status'] == SUCCESS)

		{

			$return["data"]   = "stock issue Successfully added";

			$return["status"] = SUCCESS;							

		}

		else

		{

			$return["data"]   = "Internal Error. Please try again later";

			$return["status"] = FAILURE;

		}

		

	return $return;

}

/*

PURPOSE : To add stock Issue Item

INPUT 	: Issue ID, Material ID, Qty, Remarks, Issued By

OUTPUT 	: Message, success or failure message

BY 		: Lakshmi

*/
function i_add_stock_issue_item($issue_id,$material_id,$qty,$machine_details,$project,$remarks,$issued_by)
{
	$stock_issue_item_iresult = db_add_stock_issue_item($issue_id,$material_id,$qty,$machine_details,$project,$remarks,$issued_by);
	

	if($stock_issue_item_iresult['status'] == SUCCESS)

	{

		$return["data"]   = $stock_issue_item_iresult['data'];
		$return["status"] = SUCCESS;	

	}

	else

	{

		$return["data"]   = "Internal Error. Please try again later";

		$return["status"] = FAILURE;

	}

		

	return $return;

}

	



/*

PURPOSE : To get Stock Issue Item list

INPUT 	: Issue Item ID, Issue ID, Material ID, Qty, Active, Issued By, Start Date(for added on), End Date(for added on)

OUTPUT 	: Stock Issue Item List or Error Details, success or failure message

BY 		: Lakshmi

*/

function i_get_stock_issue_item($stock_issue_item_search_data)

{

	$stock_issue_item_sresult = db_get_stock_issue_item($stock_issue_item_search_data);

	

	if($stock_issue_item_sresult['status'] == DB_RECORD_ALREADY_EXISTS)

    {

		$return["status"] = SUCCESS;

        $return["data"]   =$stock_issue_item_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No Stock Issue Item added. Please contact the system admin"; 
    }
	
	return $return;
}


/*
PURPOSE : To get Stock Issued Item list
INPUT 	: Issue Item ID, Issue ID, Material ID, Qty, Active, Issued By, Start Date(for added on), End Date(for added on)
OUTPUT 	: Stock Issue Item List or Error Details, success or failure message
BY 		: Sonakshi
*/
function i_get_stock_issued_item_without_sum($stock_issue_item_search_data)
{
	$stock_issued_item_sresult = db_get_stock_issue_items_without_sum($stock_issue_item_search_data);
	
	if($stock_issued_item_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   =$stock_issued_item_sresult["data"]; 
    }

    else

    {

	    $return["status"] = FAILURE;

        $return["data"]   = "No Stock Issue Item added. Please contact the system admin"; 

    }

	

	return $return;

}

/*
PURPOSE : To get Stock Issued Item list
INPUT 	: Issue Item ID, Issue ID, Material ID, Qty, Active, Issued By, Start Date(for added on), End Date(for added on)
OUTPUT 	: Stock Issue Item List or Error Details, success or failure message
BY 		: Sonakshi
*/
function i_get_stock_issued_item($stock_issue_item_search_data)
{
	$stock_issued_item_sresult = db_get_stock_issued_item_list($stock_issue_item_search_data);
	
	if($stock_issued_item_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   =$stock_issued_item_sresult["data"]; 
    }

    else

    {

	    $return["status"] = FAILURE;

        $return["data"]   = "No Stock Issue Item added. Please contact the system admin"; 

    }

	

	return $return;

}



/*

PURPOSE : To update Stock Issue Item

INPUT 	: Issue Item ID, Stock Issue Item Update Array

OUTPUT 	: Message, success or failure message

BY 		: Lakshmi

*/



function i_update_stock_issue_item($issue_item_id,$stock_issue_item_update_data)

{

		$stock_issue_item_sresult = db_update_stock_issue_item($issue_item_id,$stock_issue_item_update_data);

		

		if($stock_issue_item_sresult['status'] == SUCCESS)

		{

			$return["data"]   = "Stock Issue Item Successfully added";

			$return["status"] = SUCCESS;							

		}

		else

		{

			$return["data"]   = "Internal Error. Please try again later";

			$return["status"] = FAILURE;

		}

		

	return $return;

}



/*

PURPOSE : To add new Stock Transfer

INPUT 	: Material ID, Quantity, Source Location, Destination Location, Remarks, Added By

OUTPUT 	: Message, success or failure message

BY 		: Lakshmi

*/
function i_add_stock_transfer($material_id,$quantity,$source_project,$destination_project,$remarks,$added_by)
{
	if($source_project != $destination_project)
	{
		$stock_transfer_iresult = db_add_stock_transfer($material_id,$quantity,$source_project,$destination_project,$remarks,$added_by);
		$stock_transfer_id = $stock_transfer_iresult["data"];

		if($stock_transfer_iresult['status'] == SUCCESS)

		{

			$return["data"]   = "Stock Transfer Successfully Added";

			$return["status"] = SUCCESS;	

		}

		else

		{

			$return["data"]   = "Internal Error. Please try again later";

			$return["status"] = FAILURE;

		}

	}

	else

	{

		    $return["data"]   = "Source and Destination Location should not be same";

			$return["status"] = FAILURE;

	}

		

	return $return;

}

	

/*

PURPOSE : To get Stock Transfer list

INPUT 	: Transfer ID, Material ID, Quantity, Source Location, Destination Location, Active, Added By, Start Date(for added on), End Date(for added on)

OUTPUT 	: Stock Transfer List or Error Details, success or failure message

BY 		: Lakshmi

*/

function i_get_stock_transfer($stock_transfer_search_data)

{

	$stock_transfer_sresult = db_get_stock_transfer($stock_transfer_search_data);

	

	if($stock_transfer_sresult['status'] == DB_RECORD_ALREADY_EXISTS)

    {

		$return["status"] = SUCCESS;

        $return["data"]   =$stock_transfer_sresult["data"]; 

    }

    else

    {

	    $return["status"] = FAILURE;

        $return["data"]   = "No Stock Transfer added. Please contact the system admin"; 

    }

	

	return $return;

}



/*

PURPOSE : To update Stock Transfer

INPUT 	: Transfer ID, Stock Transfer Update Array

OUTPUT 	: Message, success or failure message

BY 		: Lakshmi

*/

function i_update_stock_transfer($transfer_id,$stock_transfer_update_data)

{

	$stock_transfer_sresult = db_update_stock_transfer($transfer_id,$stock_transfer_update_data);

		

	if($stock_transfer_sresult['status'] == SUCCESS)

	{

		$return["data"]   = "Stock Transfer Successfully added";

		$return["status"] = SUCCESS;							

	}

	else

	{

		$return["data"]   = "Internal Error. Please try again later";

		$return["status"] = FAILURE;

	}

		

	return $return;
}

/*

PURPOSE : To update Stock Transfer

INPUT 	: Transfer ID, Stock Transfer Update Array

OUTPUT 	: Message, success or failure message

BY 		: Lakshmi

*/

function i_transfer_stock($transfer_id,$stock_transfer_update_data)
{
	$stock_transfer_sresult = db_update_stock_transfer($transfer_id,$stock_transfer_update_data);
	if($stock_transfer_sresult['status'] == SUCCESS)
	{

		$return["data"]   = "Stock Transfer Successfully added";

		$return["status"] = SUCCESS;

		//$stock_transfer_id = $stock_transfer_iresult["data"];
		//Get Source Location Qty
		$source_project 	 = $stock_transfer_update_data["source_project"];
		$destination_project = $stock_transfer_update_data["destination_project"];
		$material_id 		 = $stock_transfer_update_data["material_id"];
		$quantity 		     = $stock_transfer_update_data["quantity"];
		$accepted_by 		 = $stock_transfer_update_data["accepted_by"];
		
		$material_stock_search_data = array("project"=>$source_project,"material_id"=>$material_id);
		$stock_data_result = i_get_material_stock($material_stock_search_data);
		if($stock_data_result["status"] == SUCCESS)
		{
			$stock_data_result_data = $stock_data_result["data"];
			$stock_source_project_qty = $stock_data_result_data[0]["material_stock_quantity"];
			$sproject_qty = (string)($stock_source_project_qty - $quantity);
		}
		
		//Update Source Location Qty
		$material_stock_update_data = array("quantity"=>$sproject_qty);
		$update_material_stock = i_update_material_stock($material_id,$source_project,$material_stock_update_data);
		if($update_material_stock["status"] == SUCCESS)
		{	
			
			// Update stock history
			$stock_history_iresult = i_add_stock_history($material_id,'Transfer Out',$source_project,$quantity,"",$accepted_by,$transfer_id);
		}

		

		//Get Destination Location Qty
		$material_stock_search_data = array("project"=>$destination_project,"material_id"=>$material_id);
		$stock_data_result = i_get_material_stock($material_stock_search_data);
		if($stock_data_result["status"] == SUCCESS)
		{
			$stock_data_result_data = $stock_data_result["data"];
			$stock_destination_project_qty = $stock_data_result_data[0]["material_stock_quantity"];
			$dproject_qty = ($stock_destination_project_qty + $quantity);
		}
		
		//Update Destination Location Qty
		$material_stock_update_data = array("quantity"=>$dproject_qty);
		$update_material_stock = i_update_material_stock($material_id,$destination_project,$material_stock_update_data);
		if($update_material_stock["status"] == SUCCESS)
		{

			// Update stock history
			$stock_history_iresult = i_add_stock_history($material_id,'Transfer In',$destination_project,$quantity,"",$accepted_by,$transfer_id);				
		}	

	}
	else
	{

		$return["data"]   = "Internal Error. Please try again later";

		$return["status"] = FAILURE;

	}

		

	return $return;
}
/*
PURPOSE : To get Material Stock list
INPUT 	: Stock ID, Material Id, Quantity, Uom, Location, Active
OUTPUT 	: Material Stock List or Error Details, success or failure message
BY 		: Lakshmi
*/
function i_get_sum_material_stock($material_stock_search_data)
{
	$material_stock_sum_sresult = db_get_sum_material_stock($material_stock_search_data);
	
	if($material_stock_sum_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   =$material_stock_sum_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No material stock added. Please contact the system admin"; 
    }
	
	return $return;
}

/* PRIVATE FUNCTIONS - START */

function p_generate_issue_no()

{

	// Get the last invoice no

	$stock_issue_search_data = array("sort"=>'1');

	$issue_no_data = db_get_stock_issue($stock_issue_search_data);

	

	if($issue_no_data["status"] == DB_RECORD_ALREADY_EXISTS)

	{

		$invoice_numeric_value_data = $issue_no_data["data"][0]["stock_issue_no"];

		$invoice_numeric_value_array = explode(INV_NO_DELIMITER,$invoice_numeric_value_data);

		

		$invoice_numeric_value = ($invoice_numeric_value_array[2]) + 1;

	}

	else

	{

		$invoice_numeric_value = 1;

	}

	

	return INV_NO_ROOT.INV_NO_DELIMITER.INV_NO_ISSUE.INV_NO_DELIMITER.$invoice_numeric_value;

}

/* PRIVATE FUNCTIONS - END */

?>