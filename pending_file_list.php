<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: process_list.php
CREATED ON	: 05-June-2015
CREATED BY	: Nitin Kashyap
PURPOSE     : List of Process Plans
*/

/*
TBD: 
1. Date display and calculation
2. Permission management
*/

/* DEFINES - START */
define('PENDING_LEGAL_FILES_LIST_FUNC_ID','2');
/* DEFINES - END */

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'files'.DIRECTORY_SEPARATOR.'file_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'process'.DIRECTORY_SEPARATOR.'process_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'tasks'.DIRECTORY_SEPARATOR.'task_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Query String Data
	// Nothing here
	
	// Get permission settings for this user for this page
	$view_perms_list   = i_get_user_perms($user,'',PENDING_LEGAL_FILES_LIST_FUNC_ID,'2','1');
	$edit_perms_list   = i_get_user_perms($user,'',PENDING_LEGAL_FILES_LIST_FUNC_ID,'3','1');
	$delete_perms_list = i_get_user_perms($user,'',PENDING_LEGAL_FILES_LIST_FUNC_ID,'4','1');

	// Temp data
	$alert = "";

	$file_number   = "";
	$file_type     = "";
	$survey_number = "";
	$land_owner    = "";
	$pan_number    = "";
	$extent        = "";
	$village       = "";
	
	$search_process = "";

	// Search parameters
	if(isset($_POST["file_search_submit"]))
	{
		$file_number    = $_POST["search_file_no"];
		$survey_number  = $_POST["search_survey_no"];
		$search_process = $_POST["search_process"];
	}
	
	// Get list of files
	$legal_file_list = i_get_file_list($file_number,'',$file_type,$survey_number,$land_owner,$pan_number,$extent,$village,'','');

	if($legal_file_list["status"] == SUCCESS)
	{
		$legal_file_list_data = $legal_file_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$legal_file_list["data"];
	}	
	
	// Get list of process types
	$process_type_list = i_get_process_type_list('','');

	if($process_type_list["status"] == SUCCESS)
	{
		$process_type_list_data = $process_type_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$process_type_list["data"];
	}	
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Project File List</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>     

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>File List&nbsp;&nbsp;&nbsp;Total Files: <span id="total_files_count_area"><i>Calculating</i></span></h3>			  
            </div>
            <!-- /widget-header -->
			
			<div class="widget-header" style="height:50px; padding-top:10px;">               
			  <form method="post" id="file_search_form" action="pending_file_list.php">
			  <span style="padding-left:20px; padding-right:20px;">
			  <input type="text" name="search_file_no" value="" placeholder="Search by file number" />
			  </span>
			  <span style="padding-left:20px; padding-right:20px;">
			  <input type="text" name="search_survey_no" value="" placeholder="Search by survey number" />
			  </span>
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_process">
			  <option value="">- - Select Process - -</option>
			  <?php
			  for($process_count = 0; $process_count < count($process_type_list_data); $process_count++)
			  {
			  ?>
			  <option value="<?php echo $process_type_list_data[$process_count]["process_master_id"]; ?>" <?php if($search_process == $process_type_list_data[$process_count]["process_master_id"]) { ?> selected="selected" <?php } ?>><?php echo $process_type_list_data[$process_count]["process_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>
			  <input type="submit" name="file_search_submit" />
			  </form>			  
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-bordered" style="table-layout: fixed;">
                <thead>
                  <tr>
				    <th>File Number</th>
					<th>Total Lead Time (In Days)</th>
					<th>Pending Time (In Days)</th>
				    <th>Survey Number</th>
					<th>Land Owner</th>	
					<th>PAN No.</th>
					<th>File Type</th>	
					<th>Extent</th>
					<th>Village</th>					
					<th>Process Type</th>					
					<th>Process Lead Time</th>	
					<th>Task</th>					
					<th>Task Lead Time</th>
					<th>Bulk Process Type</th>					
					<th>Bulk Process Lead Time</th>	
					<th>Bulk Task</th>					
					<th>Bulk Task Lead Time</th>
					<th>Details</th>
				</tr>
				</thead>
				<tbody>
				 <?php
				 $total_files = 0;
				if($legal_file_list["status"] == SUCCESS)
				{				
					for($count = 0; $count < count($legal_file_list_data); $count++)
					{
						
						// Get list of completed process plan
						$completed_process_plan_list = i_get_completed_process_plan_list($legal_file_list_data[$count]["file_id"],'','1');

						if($completed_process_plan_list["status"] == SUCCESS)
						{
							$completed_process_plan_list_data = $completed_process_plan_list["data"];
							$completed = true;
						}
						else
						{
							$alert = $alert."Alert: ".$completed_process_plan_list["data"];
							$completed = false;
						}

						// Get list of process plans for this module	
						$legal_bulk_search_data = array("file_no"=>$legal_file_list_data[$count]["file_number"],"completed"=>'0');						
						$legal_bulk_process_plan_list = i_get_bulk_legal_process_plan_details($legal_bulk_search_data);
						if($legal_bulk_process_plan_list["status"] == SUCCESS)
						{
							$legal_process_plan_list_data = $legal_bulk_process_plan_list["data"];
							$bulk_process_type = $legal_process_plan_list_data[0]['process_name'];
							
							$btask_plan_result = i_get_task_plan_list('','','','','','','','slno',$legal_process_plan_list_data[0]["legal_bulk_process_id"]);
							$bprocess_start_date = '';
							$bulk_task_lead      = '';
							$bulk_task_type      = '';
							for($btask_count = 0; $btask_count < count($btask_plan_result['data']); $btask_count++)
							{
								if($btask_count == 0)
								{
									$bprocess_start_date = $btask_plan_result['data'][$btask_count]['task_plan_actual_start_date'];
								}
								
								if($btask_plan_result['data'][$btask_count]['task_plan_actual_end_date'] == '0000-00-00')
								{
									$bulk_task_date    = $btask_plan_result['data'][0]['task_plan_actual_start_date'];
									$btvariance = get_date_diff($bulk_task_date,date('Y-m-d'));
									$bulk_task_lead    = $btvariance['data'];
									
									$bulk_task_type = $btask_plan_result['data'][0]['task_type_name'];
									break;
								}															
							}
							
							if($bprocess_start_date != '')
							{
								$bpvariance = get_date_diff($bprocess_start_date,date('Y-m-d'));
								$bulk_process_lead = $bpvariance['data'];
							}							
							else
							{
								$bulk_process_lead = '';
							}
						}
						else
						{
							$bulk_process_type = '';
							$bulk_process_lead = '';
							$bulk_task_type    = '';
							$bulk_task_lead    = '';
						}
						
						$file_handover_data = i_get_file_handover_details($legal_file_list_data[$count]["file_id"]);
							
						if($file_handover_data["status"] != SUCCESS)
						{
							if(($search_process == "") || (($search_process != "") && ($legal_file_list_data[$count]["process_master_id"] == $search_process)))
							{
							$total_files++;
					?>
					<tr>
						<td style="word-wrap:break-word;"><?php echo $legal_file_list_data[$count]["file_number"]; ?> </td>
						<td style="word-wrap:break-word;"><?php $lead_time = get_date_diff(date("Y-m-d",strtotime($legal_file_list_data[$count]["file_start_date"])),date("Y-m-d"));
						echo $lead_time["data"];?></td>
						<td style="word-wrap:break-word;"><?php $pending_time = get_date_diff(date("Y-m-d"),date("Y-m-d",strtotime($legal_file_list_data[$count]["file_start_date"]." + ".$legal_file_list_data[$count]["file_num_days"]." days")));
						echo $pending_time["data"];?></td>
						<td style="word-wrap:break-word;"><?php echo $legal_file_list_data[$count]["file_survey_number"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $legal_file_list_data[$count]["file_land_owner"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $legal_file_list_data[$count]["file_pan_number"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $legal_file_list_data[$count]["legal_file_type_name"]; ?></td>					
						<td style="word-wrap:break-word;"><?php $extent_array = get_acre_from_guntas($legal_file_list_data[$count]["file_extent"]);
								  echo $extent_array["acres"]." acres ".$extent_array["guntas"]." guntas"; ?></td>	
						<td style="word-wrap:break-word;"><?php if($legal_file_list_data[$count]["village_name"] != "")
						{
							echo $legal_file_list_data[$count]["village_name"]; 
						}
						else
						{
							echo $legal_file_list_data[$count]["file_village"]; 
						}?></td>									
						<td style="word-wrap:break-word;"><?php if($completed == false){
							echo $legal_file_list_data[$count]["process_name"];
							} ?></td>								
						<td style="word-wrap:break-word;"><?php if($completed == false)
						{
							$process_variance = get_date_diff($legal_file_list_data[$count]["process_plan_legal_start_date"],date("Y-m-d"));
							if($process_variance["status"] != 2)
							{
								echo $process_variance["data"];
							}
							else
							{
								echo "";
							}
						}?></td>	
						<td style="word-wrap:break-word;"><?php if($completed == false)
						{
							echo $legal_file_list_data[$count]["task_type_name"]; 
						}?></td>								
						<td style="word-wrap:break-word;"><?php if($completed == false)
						{
							$task_variance = get_date_diff($legal_file_list_data[$count]["task_plan_actual_start_date"],date("Y-m-d"));
							if($task_variance["status"] != 2)
							{
								echo $task_variance["data"];
							}
							else
							{
								echo "";
							}
						}?></td>
						<td style="word-wrap:break-word;"><?php echo $bulk_process_type; ?></td>								
						<td style="word-wrap:break-word;"><?php echo $bulk_process_lead; ?></td>	
						<td style="word-wrap:break-word;"><?php echo $bulk_task_type; ?></td>								
						<td style="word-wrap:break-word;"><?php echo $bulk_task_lead; ?></td>
						<td style="word-wrap:break-word;"><a href="file_details.php?file=<?php echo $legal_file_list_data[$count]["file_id"]; ?>">Details</a></td>
					</tr>
					<?php 
						}
						}
					}
				}
				else
				{
				?>
				<td colspan="16">No Files added yet!</td>
				<?php
				}
				 ?>	
				 <script>
				 document.getElementById("total_files_count_area").innerHTML = <?php echo $total_files; ?>;
				 </script>

                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function confirm_deletion(file_id)
{
	var ok = confirm("Are you sure you want to delete?")
	{         
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					 window.location = "pending_file_list.php";
				}
			}

			xmlhttp.open("GET", "legal_file_delete.php?file=" + file_id);   // file name where delete code is written
			xmlhttp.send();
		}
	}	
}

function complete_process_plan(file_id)
{
	var ok = confirm("Are you sure you want to Complete?")
	{         
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else					
					{
					 window.location = "pending_file_list.php";
					}
				}
			}

			xmlhttp.open("POST", "add_legal_process_plan.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("process_id=" + file_id + "&action=1");
		}
	}	
}
</script>

  </body>

</html>
