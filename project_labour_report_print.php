<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];	
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'pdf_operations.php');	
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');	

// Session Data
$user 		   = $_SESSION["loggedin_user"];
$role 		   = $_SESSION["loggedin_role"];
$loggedin_name = $_SESSION["loggedin_user_name"];

// Temp Data
$alert_type = -1;
$alert = "";

$manpower_id = $_REQUEST["manpower_id"];
// Temp data
$man_power_search_data = array("man_power_id"=>$manpower_id,"work_type"=>"Regular");
$man_power_list = i_get_man_power_list($man_power_search_data);

if($man_power_list["status"] == SUCCESS)
{
	$man_power_list_data = $man_power_list["data"];
	$date = $man_power_list_data[0]["project_task_actual_manpower_date"];
	$men_hrs = $man_power_list_data[0]["project_task_actual_manpower_no_of_men"];
	$women_hrs = $man_power_list_data[0]["project_task_actual_manpower_no_of_women"];
	$mason_hrs = $man_power_list_data[0]["project_task_actual_manpower_no_of_mason"];
	$project = $man_power_list_data[0]["project_master_name"];
	$process = $man_power_list_data[0]["project_process_master_name"];
	$task = $man_power_list_data[0]["project_task_master_name"];
	$vendor = $man_power_list_data[0]["project_manpower_agency_name"];
	$engineer = $man_power_list_data[0]["checked_by"];
	$no_of_men = $men_hrs/8;
	$no_of_women = $women_hrs/8;
	$no_of_mason = $mason_hrs/8; 
}
else
{
	$alert = $alert."Alert: ".$man_power_list["data"];
}

$po_print_format = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Untitled Document</title>
</head>

<body>
<table width="100%" border="1" cellpadding="5" style="border-collapse:collapse; border:1px solid #333;">
  <tr>
    <td colspan="10" align="center">Labour Report</td>
  </tr>
  <tr>
    <td colspan="6" rowspan="3" align="left">Name of project: '.$project.'</td>
    <td align="center">Date</td>
    <td width="0%"></td>
    <td width="27%" colspan="2">'.$date.'</td>
  </tr>
  <tr>
    <td align="center">Engineer</td>
    <td></td>
    <td colspan="2">'.$engineer.'</td>
  </tr>
  <tr>
    <td align="center">Contractor</td>
    <td></td>
    <td colspan="2">'.$vendor.'</td>
  </tr>
  <tr>
    <td width="7%" align="center">Sl.no</td>
    <td colspan="4" align="center">Date</td>
    <td align="center">Work of Description</td>
    <td colspan="2" align="center">Mason</td>
    <td align="center">Male</td>
    <td align="center">Female</td>
  </tr>
			
  <tr>
    <td align="center">1</td>
    <td colspan="4" align="center">'.$date.'</td>
    <td align="center">Process : '.$process.' <br> Task : '.$task.'</td>
    <td colspan="2" align="center">Men Hrs : '.$men_hrs.' <br> No of men : '.$no_of_mason.'</td>
    <td align="center">Women Hrs : '.$women_hrs.' <br> No of women : '.$no_of_men.'</td>
    <td align="center">Mason Hrs : '.$mason_hrs.' <br> No_of_mason : '.$no_of_women.'</td>
  </tr>
  <tr style="border:none">
  <td>&nbsp;</td>
  </tr>
  <tr>
  <tr style="border:none">
  <td>&nbsp;</td>
  </tr>
  <tr>
  <td colspan="6" align="center">(prepared by)</td>
  <td colspan="5	" align="center">(checked by)</td>
  </tr>
</table>
</body>
</html>';
$mpdf = new mPDF();
$mpdf->WriteHTML($po_print_format);
$mpdf->Output('invoice_'.$invoice_no.'.pdf','I');
