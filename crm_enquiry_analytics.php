<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD:
*/
$_SESSION['module'] = 'CRM Transactions';

/* DEFINES - START */
define('CRM_ENQUIRY_FOLLOW_UP_LIST_FUNC_ID', '102');
/* DEFINES - END */

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_masters'.DIRECTORY_SEPARATOR.'crm_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_transaction_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_post_sales_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if ((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != "")) {
    // Session Data
    $user 		   = $_SESSION["loggedin_user"];
    $role 		   = $_SESSION["loggedin_role"];
    $loggedin_name = $_SESSION["loggedin_user_name"];

    // Get permission settings for this user for this page
    $view_perms_list    = i_get_user_perms($user, '', CRM_ENQUIRY_FOLLOW_UP_LIST_FUNC_ID, '2', '1');
    $edit_perms_list    = i_get_user_perms($user, '', CRM_ENQUIRY_FOLLOW_UP_LIST_FUNC_ID, '3', '1');
    $delete_perms_list  = i_get_user_perms($user, '', CRM_ENQUIRY_FOLLOW_UP_LIST_FUNC_ID, '4', '1');
    $ok_perms_list   	= i_get_user_perms($user, '', CRM_ENQUIRY_FOLLOW_UP_LIST_FUNC_ID, '5', '1');
    $approve_perms_list = i_get_user_perms($user, '', CRM_ENQUIRY_FOLLOW_UP_LIST_FUNC_ID, '6', '1');

?>
<script>
  window.permissions = {
    view: <?php echo ($view_perms_list['status'] == 0)? 'true' : 'false'; ?>,
    edit: <?php echo ($edit_perms_list['status'] == 0)? 'true' : 'false'; ?>,
    ok: <?php echo ($ok_perms_list['status'] == 0)? 'true' : 'false'; ?>,
    delete: <?php echo ($delete_perms_list['status'] == 0)? 'true' : 'false'; ?>,
    aprove: <?php echo ($approve_perms_list['status'] == 0)? 'true' : 'false'; ?>,
  }
</script>
<?php
$user_list = db_get_user_list('','','','','','','1','1','','1');
if($user_list["status"] == DB_RECORD_ALREADY_EXISTS)
{
  $user_list_data = $user_list["data"];
}
} else {
    header("location:login.php");
}
?>

<style>
.modal-body {
    max-height: calc(100vh - 210px);
    overflow-x: auto;
}
</style>

<html>
  <head>
    <meta charset="utf-8">
    <title>Crm Enquiry Follow Up Analytics</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <link href="./css/style.css" rel="stylesheet">
    <link href="./js_devel/datatables-1.10.16/datatables.min.css" rel="stylesheet">
    <link href="./js_devel/datatables-1.10.16/bootstrap-3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
    <link href="./bootstrap_aku.min.css" rel="stylesheet">
    <link href="css/style1.css" rel="stylesheet">

    <style media="screen">
      table.dataTable {
        margin-top: 0px !important;
      }
    </style>
    <script type="text/javascript" src="./js_devel/datatables-1.10.16/jquery-2.2.4.min.js?22062018"></script>
    <script type="text/javascript" src="./js_devel/datatables-1.10.16/bootstrap-3.3.7/js/bootstrap.js?21062018"></script>
     <script type="text/javascript" src="./js_devel/datatables-1.10.16/datatables.min.js?21062018"></script>
     <script type="text/javascript" src="./js_devel/datatables-1.10.16/moment.min.js"></script>
		 <script src="datatable/crm_enquiry_analytics.js?<?php echo time(); ?>"></script>
  </head>
  <body>

    <?php
    include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_header.php');
    ?>
    <div id="ajax_loading"></div>
    <div class="modal fade bs-example-modal-lg" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
      <div class="modal-dialog modal-lg" role="document">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Enquiry Follow Up Details</h4>
          </div>

          <div class="modal-body">
            <div id="enquiry_details"></div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>

      </div>
    </div>
    <div class="main margin-top">
        <div class="main-inner">
          <div class="container">
            <div class="row">

                <div class="span6">

                <div class="widget widget-table action-table">
                  <div class="widget-header">
                    <h3>Enquiry Follow Analytics </h3>
                  </div>

                  <div style="height:91px !important" class="widget-header widget-toolbar">
                    <?php
                          if ($view_perms_list['status'] == SUCCESS) {
                              ?>
                      <form class="form-inline" method="post" id="file_search_form">
                        <select id="ddl_search_assigned_to" name="ddl_search_assigned_to" class="form-control input-sm" style="max-width: 150px;">
                          <option value="">- - Select User - -</option>
                          <?php
                          for($count = 0; $count < count($user_list_data); $count++)
                          {
                            ?>
                            <option value="<?php echo $user_list_data[$count]["user_id"]; ?>"
                              ><?php echo $user_list_data[$count]["user_name"]; ?></option>
                            <?php
                          }
                              ?>
                          </select>
                      <input type="date" id="start_date" name="dt_start_date" value="<?php echo date("Y-m-d"); ?>"  class="form-control input-sm"/>
                      <input type="date" id="end_date" name="dt_start_date" value="<?php echo date("Y-m-d"); ?>"  class="form-control input-sm"/>
                      <button type="button" onclick="tableDraw()" class="btn btn-primary">Submit</button>
                      </form>
                      <?php } ?>
                    </div>
                  </div>
                <div class="widget-content" style="margin-top:15px;">
                <table id="example" class="table table-striped table-bordered display nowrap">
                  <thead>
                    <tr>
                    <th colspan="2"></th>
                    <th colspan="2" class="center">06:00 AM-10:00 AM</th>
                    <th colspan="2" class="center">10:00 AM-11:00 AM</th>
                    <th colspan="2" class="center">11:00 AM-12:00 PM</th>
                    <th colspan="2" class="center">12:00 PM-01:00 PM</th>
                    <th colspan="2" class="center">01:00 PM-02:00 AM</th>
                    <th colspan="2" class="center">02:00 PM-03:00 PM</th>
                    <th colspan="2" class="center">03:00 PM-04:00 PM</th>
                    <th colspan="2" class="center">04:00 PM-05:00 PM</th>
                    <th colspan="2" class="center">05:00 PM-10:00 PM</th>
                    <th colspan="1"></th>
                    </tr>

                <tr>
                  <th>SL No</th>
                  <th>Name</th>
                  <th>Ans</th>
                  <th>NA</th>
                  <th>Ans</th>
                  <th>NA</th>
                  <th>Ans</th>
                  <th>NA</th>
                  <th>Ans</th>
                  <th>NA</th>
                  <th>Ans</th>
                  <th>NA</th>
                  <th>Ans</th>
                  <th>NA</th>
                  <th>Ans</th>
                  <th>NA</th>
                  <th>Ans</th>
                  <th>NA</th>
                  <th>Ans</th>
                  <th>NA</th>
                  <th>View</th>
                </tr>
                </thead>
                  </tbody>
                </table>
              </div>
            </div>
              <!-- /widget-content -->
            </div>
            <!-- /widget -->

            </div>
            <!-- /widget -->
          </div>
          <!-- /span6 -->
        </div>
        <!-- /row -->
      <!-- </div> -->
      <!-- /container -->
    <!-- </div> -->

</body>

  <div class="extra">

  	<div class="extra-inner">

  		<div class="container">

  			<div class="row">

                  </div> <!-- /row -->

  		</div> <!-- /container -->

  	</div> <!-- /extra-inner -->

  </div> <!-- /extra -->




  <div class="footer">

  	<div class="footer-inner">

  		<div class="container">

  			<div class="row">

      			<div class="span12">
      				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
      			</div> <!-- /span12 -->

      		</div> <!-- /row -->

  		</div> <!-- /container -->

  	</div> <!-- /footer-inner -->

  </div> <!-- /footer -->
</html>
