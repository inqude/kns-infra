<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */

// LAST UPDATED ON: 7th June 2015

// LAST UPDATED BY: Nitin Kashyap

/* FILE HEADER - END */



/* TBD - START */

// 1. Role dropdown should be from database

/* TBD - END */



/* DEFINES - START */
define('ADD_USER_FUNC_ID', '1');
/* DEFINES - END */



/* INCLUDES - START */

$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'tasks'.DIRECTORY_SEPARATOR.'general_task_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'da_user.php');

/* INCLUDES - END */

if ((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != "")) {

    // Session Data
    $user 		   = $_SESSION["loggedin_user"];
    $role 		   = $_SESSION["loggedin_role"];
    $loggedin_name = $_SESSION["loggedin_user_name"];

    // // Get permission settings for this user for this page

    $view_perms_list = i_get_user_perms($user, '', ADD_USER_FUNC_ID, '2', '1');

    $add_perms_list  = i_get_user_perms($user, '', ADD_USER_FUNC_ID, '1', '1');

    /* DATA INITIALIZATION - START */
    $alert = "";
    $alert_type = -1;
    /* DATA INITIALIZATION - END */

    // Capture the form data
    if (isset($_POST["create_persona_submit"])) {

        $persona_department_id   = $_POST["ddl_department_id"];
        $persona_user_id         = $_POST["sel_user"];
        $bill_date              = $_POST["bill_date"];
        $voice_bill              = $_POST["voice_bill"];
        $data_bill               = $_POST["data_bill"];
        $mobile_remarks          = $_POST["mobile_remarks"];

        if(($persona_department_id != "0") && ($persona_user_id != "0") && ($bill_date != "") &&( $voice_bill != "0") && ($data_bill != "0") && ($mobile_remarks != "")) {

          $isExists = db_mb_deduction_exists($persona_user_id, $bill_date);

          if($isExists["status"] == 'DB_RECORD_ALREADY_EXISTS') {

            ?>
            <script>
              alert('Bill already added');
            </script>
            <?php
          } else if($isExists["status"] == 'DB_NO_RECORD') {

              $response = db_add_mobile_deduction($persona_department_id, $persona_user_id, $bill_date, $voice_bill, $data_bill, $mobile_remarks);

                var_dump($response);
              if($response["status"] == 'SUCCESS') {
                $_POST = array();
                ?>
                <script>
                  alert("Mobile deduction added successfully ");
                </script>
                <?php
              }
          }
          var_dump($isExists);
        }
        else {
          $alert = "Please fill all the mandatory fields";
          $alert_type = 0;

        ?>
          <script>
          alert("Please fill all the mandatory fields");
            console.log('values ', <?= json_encode($_POST); ?>);
          </script>
          <?php
        }

    }



    // Get list of users

    // $user_list = i_get_user_list('', '', '', '');
    //
    //
    //
    // if ($user_list["status"] == SUCCESS) {
    //     $user_list_data = $user_list["data"];
    // } else {
    //     $alert = $alert."Alert: ".$user_list["data"];
    // }



    // Get list of Department*

    $department_list = i_get_department_list('', '');

    if ($department_list["status"] == SUCCESS) {
        $department_list_data = $department_list["data"];
    } else {
        $alert = $alert."Alert: ".$department_list["data"];
      }
    
    $designation_list = db_get_designation_list();
    if($designation_list['status'] == 'DB_RECORD_ALREADY_EXISTS') {
      $designation_list_data = $designation_list['data'];
    }

    // Get Location List

    // $stock_location_master_search_data = array('location_id'=>$location_id);
    //
    // $location_list =  i_get_stock_location_master_list($stock_location_master_search_data);
    //
    // if ($location_list['status'] == SUCCESS) {
    //     $location_list_data = $location_list['data'];
    // } else {
    //     $alert = $location_list["data"];
    //
    //     $alert_type = 0;
    // }
} else {
    header("location:login.php");
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">
    <title>Mobiel Bill</title>
    <!-- Bootstrap core CSS -->
    <link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <!-- <link href="../../assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet"> -->
    <!-- Custom styles -->
    <link href="assets/multistepform/css/style.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
  <?php

  include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');

  ?>
<!-- MultiStep Form -->
<div class="row">
    <div class="span8" style="margin-top:-50px; margin-bottom:50px">
        <form id="msform" method="post">
            <!-- progressbar -->


            <fieldset>
                <h2 class="fs-title">Amenities Expense Form</h2>
                <h3 class="fs-subtitle">Enter Mobile and SIM Bill expenditure details</h3>
                <div class="col-xs-6" style="text-align:left">
                  <div class="form-group">
                    <label style="font-weight:300">Department</label>
                    <select class="form-control" id="ddl_department_id" name="ddl_department_id">
                      <option value='0'>- - Select Department - -</option>
                      <?php
                        for ($count = 0; $count < count($department_list_data); $count++) {
                      ?>
                      <option value="<?php echo $department_list_data[$count]["general_task_department_id"]; ?>"><?php echo $department_list_data[$count]["general_task_department_name"]; ?></option>
                      <?php
                      }
                      ?>
                    </select>
                  </div>
                  <div class="form-group">
                    <label style="font-weight:300">Name</label>
                    <select class="form-control" id="sel_user" name="sel_user">
                    <option value='0'>- - Select User - -</option>
                    </select>
                  </div>

                <label style="font-weight:300">SIM Number</label>
                <input type="text" disabled id="sim_number" name="sim_number" placeholder="SIM Number"/>
                <label style="font-weight:300">Voice Limit</label>
                <input type="number" disabled name="voice_limit" id="voice_limit" placeholder="Voice Limit"/>
                <label style="font-weight:300">Data Limit</label>
                <input type="number" disabled name="data_limit" id="data_limit" placeholder="Data Limit"/>
                <label style="font-weight:300">Mobile</label>
                <input type="text" maxlength="10" id="company_phone" name="company_phone" placeholder="Mobile" disabled/>
                </div>
                <div class="col-xs-6" style="text-align:left">
                  <label style="font-weight:300">Bill Date</label>
                  <input type="date" id="bill_date" name="bill_date" placeholder="Billing Date" />
                  <label style="font-weight:300">Actual Voice Bill</label>
                  <input type="number" min="1" id="voice_bill" name="voice_bill" onchange="calculate_diff()" placeholder="Voice Bill" />
                  <label style="font-weight:300">Actual Data Bill</label>
                  <input type="number" min="1" id="data_bill" name="data_bill" onchange="calculate_diff()" placeholder="Data Bill"/>

                  <label style="font-weight:300">Deduction Voice Amount</label>
                  <input type="text" id="voice_deduction" name="voice_deduction" placeholder="Voice Bill" disabled/>
                  <label style="font-weight:300">Deduction Data Amount</label>
                  <input type="text" id="data_deduction" name="data_deduction" placeholder="Data Bill" disabled/>
                  <label style="font-weight:300">Remarks</label>
                  <input type="text" name="mobile_remarks" placeholder="Remarks"/>
                  <!-- <input type="text" name="ip_number" placeholder="Insurance Policy Number"/> -->
                  <!-- <input type="date" name="ip_expiry" placeholder="Insurance Policy Expiry date"/> -->
                  <!-- <input type="text" name="ip_limit" placeholder="Insurance Policy Amount"/> -->
                </div>
                <input type="button" name="previous" class="previous action-button-previous" value="Previous"/>
                <input type="submit" id="create_persona_submit" name="create_persona_submit" class="submit action-button" value="Submit"/>
            </fieldset>
        </form>
    </div>
</div>
<!-- /.MultiStep Form -->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js'></script>
<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/multistepform/js/msform.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<!-- <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script> -->

<script>
function calculate_diff() {
  console.log('am in diff amount');
  var voice_limit = document.getElementById('voice_limit').value ? document.getElementById('voice_limit').value : 0,
      data_limit = document.getElementById('data_limit').value ? document.getElementById('data_limit').value : 0,
      voice_bill = document.getElementById('voice_bill').value ? document.getElementById('voice_bill').value : 0,
      data_bill = document.getElementById('data_bill').value ? document.getElementById('data_bill').value : 0;

      if(voice_bill && data_bill) {
        document.getElementById('voice_deduction').value = parseInt(voice_bill) - parseInt(voice_limit);
        document.getElementById('data_deduction').value = parseInt(data_bill) - parseInt(data_limit);
      }

}
$(document).ready(function() {
  $("#ddl_department_id").change(function() {
		var department_id = $(this).val();
		console.log('department_id ', department_id);
		if(department_id == 0) {
			$("#sel_user").empty();
      $("#sel_user").append("  <option value='0'>- - Select User - -</option>");
			return false;
		}

		$.ajax({
			url: 'ajax/getUsers.php',
			data: {department_id: department_id},
			dataType: 'json',
			success: function(response) {
				console.log('hope i got all users ', response);

				$("#sel_user").empty();
        $("#sel_user").append("  <option value='0'>- - Select User - -</option>");
				for(var i=0; i< response.length; i++) {
					var id = response[i]['user_id'];
					var name = response[i]['user_name'];
          // var class = 'list-group-item';

					$("#sel_user").append("<option value='"+id+"'>"+name+"</option>");
					// $("#sel_user").append("<div id='"+id+"'>"+name+"</div>");
				}
			}
		})
	})

  $("#sel_user").change(function() {
    var user_id = $(this).val();
    console.log('sel_user ', user_id);
    if(user_id == 0) {
      $("#company_phone").val('');
      $("#data_limit").val('');
      $("#sim_number").val('');
      $("#voice_limit").val('');
      return false;
    }

    $.ajax({
      url: 'ajax/get_persona_basic_details.php',
      data: {user_id: user_id},
      dataType: 'json',
      success: function(response) {
        console.log('hope users basic', response);
        $("#company_phone").val('');
        $("#data_limit").val('');
        $("#sim_number").val('');
        $("#voice_limit").val('');
        if(response && response.length){
          var data = response[0];
          $("#company_phone").val(data['company_phone']);
          $("#data_limit").val(data['data_limit']);
          $("#sim_number").val(data['sim_number']);
          $("#voice_limit").val(data['voice_limit']);
        }
      }
    })
  })


})

</script>
</body>
</html>
