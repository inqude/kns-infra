<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: crm_approved_booking_list.php
CREATED ON	: 04-Nov-2015
CREATED BY	: Nitin Kashyap
PURPOSE     : Approved Booking List
*/
/* DEFINES - START */define('CRM_SALE_STATUS_REPORT_FUNC_ID','91');/* DEFINES - END */
/*
TBD: 
*/$_SESSION['module'] = 'CRM Projects';

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_post_sales_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_sales_process.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_projects'.DIRECTORY_SEPARATOR.'crm_project_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];		// Get permission settings for this user for this page		$view_perms_list   = i_get_user_perms($user,'',CRM_SALE_STATUS_REPORT_FUNC_ID,'2','1');
	// Query String Data
	// Nothing

	// Temp data
	$alert = "";
	
	if(isset($_POST["search_blocking_submit"]))
	{
		$project_id  = $_POST["ddl_project"];		
		if(($role == 1) || ($role == 5) || ($role == 10))
		{
			$added_by = $_POST["ddl_user"];		
		}
		else
		{
			$added_by = '';
		}
		$start_date = $_POST["dt_start_date"];
		$end_date   = $_POST["dt_end_date"];
		$site_no    = $_POST["stxt_site_no"];
	}
	else
	{
		$project_id = "-1";		
		$added_by   = "";
		$start_date = "";
		$end_date   = "";
		$site_no    = "";
	}

	$approved_booking_list = i_get_site_booking('',$project_id,'','','','1',$added_by,'','','',$start_date,$end_date,'','','','',$site_no,'','','','','',$user);
	if($approved_booking_list["status"] == SUCCESS)
	{
		$approved_booking_list_data = $approved_booking_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$approved_booking_list["data"];
	}
	
	$booking_list = i_get_site_booking('',$project_id,'','','','1',$added_by,'','','','','','','',$start_date,$end_date,$site_no,'','','','','',$user);
	if($booking_list["status"] == SUCCESS)
	{
		$booking_list_data = $booking_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$booking_list["data"];
	}
	
	// Get project list	$crm_user_project_mapping_search_data =  array("user_id"=>$user,"active"=>'1',"project_active"=>'1');	$project_list =  i_get_crm_user_project_mapping($crm_user_project_mapping_search_data);	if($project_list["status"] == SUCCESS)	{		$project_list_data = $project_list["data"];	}	else	{		$alert = $alert."Alert: ".$project_list["data"];		$alert_type = 0; // Failure	}
	
	// User List
	$user_list = i_get_user_list('','','','','1');
	if($user_list["status"] == SUCCESS)
	{
		$user_list_data = $user_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$user_list["data"];
		$alert_type = 0; // Failure
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Sale Status Report</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>
    

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Sold Site Report (Total Sites Sold = <span id="total_count_section">
			  <?php
			  if($booking_list["status"] == SUCCESS)
			  {
				$preload_count = count($booking_list_data);			
			  }
			  else
			  {
			    $preload_count = 0;
			  }
			  
			  echo $preload_count;
			  ?></span>)</h3>
            </div>
			<div class="widget-header" style="height:80px; padding-top:10px;">               
			  <form method="post" id="sale_Status_search" action="crm_sale_status_report.php">			  		  
			  <span style="padding-left:8px; padding-right:8px;">
			  <input type="date" name="dt_start_date" value="<?php echo $start_date; ?>" />
			  </span>
			  <span style="padding-left:8px; padding-right:8px;">
			  <input type="date" name="dt_end_date" value="<?php echo $end_date; ?>" />
			  </span>
			  <span style="padding-left:8px; padding-right:8px;">
			  <input type="text" name="stxt_site_no" value="<?php echo $site_no; ?>" placeholder="Enter site number" />
			  </span>
			  <span style="padding-left:8px; padding-right:8px;">
			  <select name="ddl_project">
			  <option value="">- - Select Project - -</option>
			  <?php
				for($count = 0; $count < count($project_list_data); $count++)
				{
					?>
					<option value="<?php echo $project_list_data[$count]["project_id"]; ?>" <?php 
					if($project_id == $project_list_data[$count]["project_id"])
					{
					?>					
					selected="selected"
					<?php
					}?>><?php echo $project_list_data[$count]["project_name"]; ?></option>								
					<?php					
				}
      		  ?>
			  </select>
			  </span>						
			  <?php if(($role == 1) || ($role == 5) || ($role == 10))
			  {
			  ?>
			  <span style="padding-left:8px; padding-right:8px;">
			  <select name="ddl_user">
			  <option value="">- - Select STM - -</option>
			  <?php
				for($count = 0; $count < count($user_list_data); $count++)
				{
					if(($user_list_data[$count]["user_role"] == "1") || ($user_list_data[$count]["user_role"] == "5") || ($user_list_data[$count]["user_role"] == "6") || ($user_list_data[$count]["user_role"] == "7") ||($user_list_data[$count]["user_role"] == "8"))
					{
					?>
					<option value="<?php echo $user_list_data[$count]["user_id"]; ?>" <?php 
					if($added_by == $user_list_data[$count]["user_id"])
					{
					?>												
					selected="selected"
					<?php
					}?>><?php echo $user_list_data[$count]["user_name"]; ?></option>								
					<?php
					}
				}
      		  ?>
			  </select>
			  </span>
			  <?php
			  }
			  ?>
			  <span style="padding-left:8px; padding-right:8px;">
			  <input type="submit" name="search_blocking_submit" />
			  </span>
			  </form>			  
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
			
              <table class="table table-bordered" style="table-layout: fixed;">
                <thead>
                  <tr>
					<th>SL No</th>
					<th>Enquiry No</th>
					<th>Project</th>
					<th>Site No.</th>
					<th>Release Status</th>					
					<th>Client Name</th>
					<th>Sales Consider Date</th>
					<th>Current Status</th>			
					<th>Status Date</th>
					<th>Lag</th>					
					<th>Booked By</th>	
					<th>Source</th>	
					<th>Amount</th>
					<th>Received Amount</th>
					<th>Pending Amount</th>
					<th>Others - Amount</th>
					<th>Others - Received Amount</th>
					<th>Others - Pending Amount</th>
					<th>Loan</th>					
				</tr>
				</thead>
				<tbody>							
				<?php						
				if($booking_list["status"] == SUCCESS)
				{										
					$sl_no = 0;
					$cancelled_count = 0;
					for($count = 0; $count < count($booking_list_data); $count++)
					{		
						// Get customer profile details, if entered
						$cust_details = i_get_cust_profile_list('',$booking_list_data[$count]["crm_booking_id"],'','','','','','','','');
						if($cust_details["status"] == SUCCESS)
						{
							if($cust_details["data"][0]["crm_customer_funding_type"] == '2')
							{
								$bank_loan = $cust_details["data"][0]["crm_bank_name"];
							}
							else if($cust_details["data"][0]["crm_customer_funding_type"] == '3')
							{
								$bank_loan = "Waiting for info";
							}
							else
							{
								$bank_loan = "Self Funding";
							}
						}
						else
						{
							$bank_loan = "";
						}
					
						// Current Status Data
						switch($booking_list_data[$count]["crm_site_status"])
						{
						case BOOKED_STATUS:
						if($booking_list_data[$count]["crm_booking_date"] != "0000-00-00")
						{
							$status_date = $booking_list_data[$count]["crm_booking_date"]; 
						}
						else
						{
							$status_date = $booking_list_data[$count]["crm_booking_approved_on"]; 
						}
						$current_status = "Booking";
						break;
						
						case AGREEMENT_STATUS:
						$agreement_list = i_get_agreement_list('',$booking_list_data[$count]["crm_booking_id"],'','','','','','','','','1');
						if($agreement_list["status"] == SUCCESS)
						{
							$status_date = $agreement_list["data"][0]["crm_agreement_date"]; 
						}
						else
						{
							if($booking_list_data[$count]["crm_booking_date"] != "0000-00-00")
							{
								$status_date = $booking_list_data[$count]["crm_booking_date"]; 
							}
							else
							{
								$status_date = $booking_list_data[$count]["crm_booking_approved_on"]; 
							}
						}
						$current_status = "Agreement";
						break;
						
						case REGISTERED_STATUS:
						$registration_list = i_get_registration_list('',$booking_list_data[$count]["crm_booking_id"],'','','','','','','','','');
						if($registration_list["status"] == SUCCESS)
						{
							$status_date = $registration_list["data"][0]["crm_registration_date"]; 
						}
						else
						{
							if($booking_list_data[$count]["crm_booking_date"] != "0000-00-00")
							{
								$status_date = $booking_list_data[$count]["crm_booking_date"]; 
							}
							else
							{
								$status_date = $booking_list_data[$count]["crm_booking_approved_on"]; 
							}
						}
						$current_status = "Registration";
						break;
						
						case KATHA_TRANSFER_STATUS:
						$kt_list = i_get_katha_transfer_list('',$booking_list_data[$count]["crm_booking_id"],'','','','','','','','','','');
						if($kt_list["status"] == SUCCESS)
						{
							$status_date = $kt_list["data"][0]["crm_katha_transfer_date"]; 
						}
						else
						{
							if($booking_list_data[$count]["crm_booking_date"] != "0000-00-00")
							{
								$status_date = $booking_list_data[$count]["crm_booking_date"]; 
							}
							else
							{
								$status_date = $booking_list_data[$count]["crm_booking_approved_on"]; 
							}
						}
						$current_status = "Katha Transfer";
						break;
						break;
						}
						
						// Payment Data
						$total_payment_done = 0;
						$payment_details = i_get_payment('',$booking_list_data[$count]["crm_booking_id"],'','','');
						if($payment_details["status"] == SUCCESS)
						{
							for($pay_count = 0; $pay_count < count($payment_details["data"]); $pay_count++)
							{
								$total_payment_done = $total_payment_done + $payment_details["data"][$pay_count]["crm_payment_amount"];
							}
						}
						else						
						{
							$total_payment_done = 0;
						}
						
						// Other Payment - TO be paid data
						$other_total_payment = 0;
						$other_payment_details = i_get_payment_terms($booking_list_data[$count]["crm_booking_id"]);
						if($other_payment_details["status"] == SUCCESS)
						{
							$other_total_payment = $other_payment_details["data"][0]["crm_pt_layout_maintenance_charges"] + $other_payment_details["data"][0]["crm_pt_water_charges"] + $other_payment_details["data"][0]["crm_pt_club_house_charges"] + $other_payment_details["data"][0]["crm_pt_documentation_charges"] + $other_payment_details["data"][0]["crm_pt_other_charges"];
						}
						else						
						{
							$other_total_payment = 0;
						}
						
						// Other Payment Data						
						$other_total_payment_done_data = i_get_other_payment_total($booking_list_data[$count]["crm_booking_id"],'');						
						if(($other_total_payment_done_data["data"] == NULL) || ($other_total_payment_done_data["data"] == "NULL"))
						{
							$other_total_payment_done = 0;
						}
						else
						{
							$other_total_payment_done = $other_total_payment_done_data["data"];
						}	
					
						if($booking_list_data[$count]["crm_booking_status"] == "1")
						{
							$sl_no++;
							?>
							<tr>
							<td style="word-wrap:break-word;"><?php echo $sl_no; ?></td>
							<td style="word-wrap:break-word;"><?php echo $booking_list_data[$count]["enquiry_number"]; ?></td>
							<td style="word-wrap:break-word;"><?php echo $booking_list_data[$count]["project_name"]; ?></td>
							<td style="word-wrap:break-word;"><?php echo $booking_list_data[$count]["crm_site_no"]; ?></td>
							<td style="word-wrap:break-word;"><?php echo $booking_list_data[$count]["release_status"]; ?></td>
							<td style="word-wrap:break-word;"><?php echo $booking_list_data[$count]["name"]; ?></td>
							<td style="word-wrap:break-word;"><?php if(($booking_list_data[$count]["crm_booking_date"] != "") && ($booking_list_data[$count]["crm_booking_date"] != "0000-00-00"))
							{
								echo date("d-M-Y",strtotime($booking_list_data[$count]["crm_booking_date"]));
							}
							else
							{
								echo "Booking Date not updated yet";
							}?></td>
							<td style="word-wrap:break-word;"><?php echo $current_status; ?></td>
							<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($status_date)); ?></td>
							<td style="word-wrap:break-word;"><?php $date_diff = get_date_diff($status_date,date("Y-m-d")); 
							echo $date_diff["data"];?></td>					
							<td style="word-wrap:break-word;"><?php echo $booking_list_data[$count]["user_name"]; ?></td>
							<td style="word-wrap:break-word;"><?php echo $booking_list_data[$count]["enquiry_source_master_name"]; ?></td>
							<td style="word-wrap:break-word;"><?php if($booking_list_data[$count]["crm_booking_consideration_area"] != "0")
							{
								$consideration_area = $booking_list_data[$count]["crm_booking_consideration_area"];
							}
							else
							{
								$consideration_area = $booking_list_data[$count]["crm_site_area"];
							}
							$total_payment_to_be_done = ($consideration_area * $booking_list_data[$count]["crm_booking_rate_per_sq_ft"]);
							echo $total_payment_to_be_done; ?></td>
							<td style="word-wrap:break-word;"><?php echo $total_payment_done; ?></td>					
							<td style="word-wrap:break-word;"><?php echo ($total_payment_to_be_done - $total_payment_done); ?></td>							
							<td style="word-wrap:break-word;"><?php echo $other_total_payment; ?></td>
							<td style="word-wrap:break-word;"><?php echo $other_total_payment_done; ?></td>					
							<td style="word-wrap:break-word;"><?php echo ($other_total_payment - $other_total_payment_done); ?></td>
							<td style="word-wrap:break-word;"><?php echo $bank_loan; ?></td>
							</tr>
							<?php
						}
						else
						{
							$cancelled_count++;
						}
					}
				}
				else
				{
				?>
				<td colspan="14">No site booked yet!</td>
				<?php
				}
				?>	
				<script>
				document.getElementById("total_count_section").innerHTML = <?php echo $sl_no; ?>;
				</script>
                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>/* Open the sidenav */function openNav() {    document.getElementById("mySidenav").style.width = "75%";}/* Close/hide the sidenav */function closeNav() {    document.getElementById("mySidenav").style.width = "0";}</script>

  </body>

</html>