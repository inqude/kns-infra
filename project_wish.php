
<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 08-Nov-2016
// LAST UPDATED BY: Lakshmi
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */

	if(isset($_GET['project_id']))
	{
		$project_id = $_GET['project_id'];
	}
	else
	{
		$project_id = "";
	}
	// Project data
	$project_management_master_search_data = array("active"=>'1',"project_id"=>$project_id, "user_id"=>$user);
	$project_management_master_list = i_get_project_management_master_list($project_management_master_search_data);
	if($project_management_master_list["status"] == SUCCESS)
	{
		$project_management_master_list_data = $project_management_master_list["data"];
		$project_name = $project_management_master_list_data[0]["project_master_name"];
	}
	else
	{
		$alert = $alert."Alert: ".$project_management_master_list["data"];
		$project_name = "";
	}

	if(isset($_POST["file_search_submit"]))
	{
		$project_id         = $_POST["hd_project_id"];
		$remarks		    = $_POST["remarks"];
		$wish_to_start_date = $_POST["start_date"];

		$plan_wish_iresults = i_add_project_plan_wish($project_id,$remarks,$wish_to_start_date,'Not Locked','','',$user);
		if($plan_wish_iresults["status"] == SUCCESS)
		{
			$plan_wish_iresults_data = $plan_wish_iresults["data"];
			$alert_type = 1;
			$alert = "Data Added Successfully";
			header("project_master_project_management_list.php");
		}
	}
}
else
{
	header("location:login.php");
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    <link href="js/handsontable-master/dist/handsontable.full.css" media="screen" rel="stylesheet">

    <link href="css/style.css" rel="stylesheet">



    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>
<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">

	<div class="main-inner">

	    <div class="container">

	      <div class="row">

	      	<div class="span12">

	      		<div class="widget ">

	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				 <h3>Project Task Planning &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Project :<?php echo $project_name ;?></h3>	  				</div> <!-- /widget-header -->

					<div class="widget-content">



						<div class="tabbable">
						<br>
							<div class="control-group">
								<div class="controls">
								<?php
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>

								<?php
								if($alert_type == 1) // Success
								{
								?>
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <?php echo $alert; ?>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								 <form method="post" id="file_search_form" action="project_wish.php">
								  <fieldset>


								  <input type="hidden" name="hd_project_id" id="hd_project_id" value=<?php echo $project_id ;?>>

								<div class="control-group">
									<label class="control-label" for="measurement">Date</label>
									<div class="controls">
								   <input type="date" name="start_date"  autocomplete="off" id="start_date" placeholder="Enter No Of days" />
								  </span>
								   </div> <!-- /controls -->
								 </div> <!-- /control-group -->

									<div class="control-group">
										<label class="control-label" for="remarks">Remarks</label>
										<div class="controls">
									   <input type="text" name="remarks"  autocomplete="off" id="remarks"/>
									 </div> <!-- /controls -->
										</div> <!-- /control-group -->
									<input type="submit" name="file_search_submit" />
									</fieldset>
								  </form>
								</div>

							</div>

					</div> <!-- /widget-content -->

					<div class="widget-content">
            </div>

				</div> <!-- /widget -->

		    </div> <!-- /span8 -->




	      </div> <!-- /row -->

	    </div> <!-- /container -->

	</div> <!-- /main-inner -->

</div> <!-- /main -->
</div>





<div class="footer">

	<div class="footer-inner">

		<div class="container">

			<div class="row">

    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->

    		</div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /footer-inner -->

</div> <!-- /footer -->

<script src="js/jquery-1.7.2.min.js"></script>
<script src="js/handsontable-master/dist/handsontable.full.js"></script>
<script src="js/handson.js"></script>

<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>

  </body>

</html>
